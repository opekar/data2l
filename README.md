data2l
======
helps to inspect, document, alter and generate binary formats of large sizes.

[We](http://eccam.com) used data2l for validating, inspecting and tweaking of car navigation databases. 

What can you do with that:

- describe a binary format (including simple documentation)
- inspect binary files according to your format description
- specify validators for your binary format
- serialize filled structures to file
- generate C++ based on the description 
- DOM and SAX like access to data


Supported primitive types:

- Basic
  - byte  (8bit)
  - word  (16bit)
  - dword (32bit)
  - simple array of any Basic
  - VarInteger - variable length integer
- Attribute - basic type but evaluated during parsing
- Struct 
- Array
- Alternative
- Void


History
-------

Was developed and used in 2006-2011, opensourced in 2013.
