REM This batch trys to build simple distributable package for our Data2l tool.
REM
REM Executables that have to be accessible through PATH:
REM python, cscript, xsltproc, hhc, svn.

REM Make changes to environment local
setlocal

REM
set INNOSETUPDIR="C:\Program Files\Inno Setup 5"

REM Set up correct PYTHONPATH
set PYTHONPATH=.\lib;.\src

python src/generators/GenerateSerialNumberRightNow.py %1

REM Create working directory for the base part of the package.
rmdir /S /Q dist
mkdir dist

REM Create working directory for the examples component.
rmdir /S /Q dist-examples
mkdir dist-examples

REM Package all the Python stuff.
python -OO setup_win.py py2exe --skip-archive
if not %ERRORLEVEL% == 0 exit /B
ren dist\main.exe data2l.exe

REM Prepare all necessary parts of Gtk that py2exe does not install.
xcopy /I /F /Y /E %GTK_BASEPATH%\etc dist\etc
if not %ERRORLEVEL% == 0 exit /B
mkdir dist\lib
mkdir dist\lib\gtk-2.0
mkdir dist\lib\gtk-2.0\2.10.0
xcopy /I /F /Y /E %GTK_BASEPATH%\lib\gtk-2.0\2.10.0 dist\lib\gtk-2.0\2.10.0
if not %ERRORLEVEL% == 0 exit /B
xcopy /I /F /Y /E %GTK_BASEPATH%\lib\pango dist\lib\pango
if not %ERRORLEVEL% == 0 exit /B
mkdir dist\share
xcopy /I /F /Y /E %GTK_BASEPATH%\share\themes dist\share\themes
if not %ERRORLEVEL% == 0 exit /B


REM Build help.chm.
pushd docs\help
call build-help.bat
popd
copy docs\help\data2l.chm dist\gtkGUI
if not %ERRORLEVEL% == 0 exit /B


REM Check out unversioned copy of examples.
pushd dist
svn export "svn://intranet/subversion/DataViz/trunk/examples"  examples
if not %ERRORLEVEL% == 0 exit /B
svn export "svn://intranet/subversion/DataViz/trunk/include"  include
if not %ERRORLEVEL% == 0 exit /B
popd


REM Clean up changes to environment.
endlocal
