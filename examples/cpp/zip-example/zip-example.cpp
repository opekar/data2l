#include "Zip.h"
#include "data2l.h"

using namespace data2l;
using namespace db_Zip;

int main(int argc, char** argv)
{
	if (argc<2)
	{
		printf("usage: zip-example.exe file.zip\n");
		printf("   will show filen ames inside a zip\n");
		return 1;
	}
try {
	ReaderT r1(argv[1]);

	std::auto_ptr<CZip> zip(new CZip(r1));
	zip->init();  //loads data to zip structures

	for(CFiles::iterator i=zip->Files->begin();i!=zip->Files->end();++i)
		printf("File in zip file: %s\n", i->File->FileName.c_str());

} catch (const StreamException& e)
{
	printf("%s",e.what());
}
	return 0;
}