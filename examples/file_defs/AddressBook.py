from file_defs.prelude import *

AddrRec = Struct('AddrRec',
    Basic('NameLen', Byte()),
    'Static size of for C++ read only generator',
    Basic('Name', String() ),
        Dependency(size = 'NameLen', customProperties = {
"static_size":"255",
}

),
    Basic('StreetLen', Byte()),
    'Static size of for C++ read only generator',
    Basic('Street', String() ),
        Dependency(size = 'StreetLen', customProperties = {
"static_size":"255",
}

),
)

AddressArray = Array('AddressArray',
    Basic('AddrRecOffs', DWord()),
        Dependency(offset = 'offsetof(AddressArray) + actindexof(AddressArray)*sizeof(AddrRecOffs)', onsave = """if (actindexof(AddressArray) != 0) then
  AddressArray[actindexof(AddressArray)-1].AddrRecOffs + 
	binsizeof(AddressArray[actindexof(AddressArray)-1].AddrRec)
else 0""", in_memory = True, 
),
    AddrRec,
        Dependency(offset = 'offsetof(AddressArray) +arraysizeof(AddressArray)*4 + AddrRecOffs', in_memory = True, 
),
)

Addressbook = Struct('Addressbook',
    ['Simple binary file showing addressing of variable sized record (AddrRec)'],
    'Number of adresses in the file.',
    Basic('NumAddresses', DWord()),
        Dependency(onsave = 'arraysizeof(AddressArray)', 
),
    AddressArray,
        Dependency(size = 'NumAddresses', 
),
)


definition = Definition(Addressbook, rootProps = None , rootAssertions = None , userEnumList = None )