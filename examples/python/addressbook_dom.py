import DOM
import Reader
import Writer
import Definition 


compiled_def = Definition.load_and_compile("../file_defs/AddressBook.py")

# Create empty DOM structure of the address book file format.
addressBook = DOM.DOMStruct( Reader.ReaderA("addressbook.dat"), compiled_def.dep_root , None)
addressBook.Init()

# dot notation reflects the tree structure of AddressBook Definition
print addressBook.AddressArray.AddrRec.Name.getValue()

addressBook.AddressArray.loadIndex(1) #DOM state is changed 
print addressBook.AddressArray.AddrRec.Name.getValue()
    