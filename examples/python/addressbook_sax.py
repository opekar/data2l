import DOM
import SAX
import Reader
import Writer
import Definition 


class SaxCallback(SAX.SAXHandler):
    def beginElement(self, dom_element):
        print "beginElement " + str(dom_element)

    def endElement(self, dom_element):
        print "endElement " + str(dom_element) +  " " + str(dom_element.getValue())


compiled_def = Definition.load_and_compile("../file_defs/AddressBook.py")
addressBook = DOM.DOMStruct( Reader.ReaderA("addressbook.dat"), compiled_def.dep_root , None)
addressBook.Init()
parser = SAX.SAXParser(addressBook, skip_simple = True)
parser.parse(SaxCallback())

    