#ifndef DATAVIZ_BASECLASS_H
#define DATAVIZ_BASECLASS_H

#include "types.h"
#include "reader.h"
#include <iosfwd>


namespace data2l
{

class Base
{
public:
    OffsetType offset;
    SizeType ownindex;
    Reader & reader;

    Base (Reader & r)
        : offset (0), ownindex (0), reader(r)
    { }

    virtual ~Base ()
    { }

    virtual Base * clone () = 0;
    virtual Base * get_parent () const = 0;
    virtual Base * print_state (std::ostream &) const = 0;
};

} // namespace db

#endif // DATAVIZ_BASECLASS_H
