#ifndef DATAVIZ_DBSTRING_H
#define DATAVIZ_DBSTRING_H

#include <cstring>
#include <algorithm>


namespace data2l
{


// Forward declare operators for use in String class template body.


//! Body of String class template, for use in generated C++ code as
// wrapper of string fields.
template <size_t Length, typename CharT>
class Block
{
public:
    typedef CharT char_type;

    enum {length = Length};

    Block ()
    {
        std::fill_n (buffer, Length, 0);
    }

    //! Proper copy constructor.
    Block (Block const & x)
    {
        std::copy (x.buffer, x.buffer + Length, buffer);
    }

    //! Proper assignment operator.
    Block &
    operator = (Block const & x)
    {
        if (this == &x)
            return *this;
        std::copy (x.buffer, x.buffer + Length, buffer);
    }


    Block (CharT const * buf, size_t len)
    {
        std::copy (buf, buf + len, std::min (Length, len));
    }

    size_t size () const
    {
        return Length;
    }

    operator CharT * ()
    {
        return buffer;
    }

    operator CharT const * () const
    {
        return buffer;
    }


protected:
    CharT buffer[Length];
};


#if  (defined(WIN32) && _MSC_VER >= 1300) || !defined(WIN32)
// Forward declare String class for use in operator declarations.
template <size_t Length>
class String;

// Forward declare operators for use in String class template body.
template <size_t LengthA, size_t LengthB>
bool
operator == (String<LengthA> const & a, String<LengthB> const & b);

template <size_t LengthA, size_t LengthB>
bool
operator != (String<LengthA> const & a, String<LengthB> const & b);

// Forward declare operators for interaction with char *.

template <size_t Length>
bool
operator == (String<Length> const & a, char const * s);


template <size_t Length>
bool
operator == (char const * s, String<Length> const & a);


template <size_t Length>
bool
operator != (String<Length> const & a, char const * s);


template <size_t Length>
bool
operator != (char const * s, String<Length> const & a);

#endif // good compiler




template <size_t Length>
class String : public Block<Length+1,Char>
{
public:
    String(): Block<Length+1,Char>() {}

    String (Char const * s, size_t n)
    {
        size_t const len = std::min (n, Length);
        std::copy (s, s + len, this->buffer);
        std::fill_n (this->buffer + len, Length - len, 0);
    }

    const Char* c_str() const
    {
        return this->buffer;
    }

#if (defined(WIN32) && _MSC_VER >= 1300) || !defined(WIN32)
	// Friend operators for interaction with another db::String<>.
    template <size_t LengthB>
    friend
    bool
    operator == (String<Length> const & a,
        String<LengthB> const & b);

    template <size_t LengthB>
    friend
    bool
    operator != (String<Length> const & a,
        String<LengthB> const & b);

    // Friend operators for interaction with char *.

    friend
    bool
    operator == <> (String<Length> const & a, char const * s);

    friend
    bool
    operator == <> (char const * s, String<Length> const & a);

    friend
    bool
    operator != <> (String<Length> const & a, char const * s);

    friend
    bool
    operator != <> (char const * s, String<Length> const & a);

#elif defined(WIN32) && _MSC_VER < 1300
//workarounds here for VC6 crap

    operator == (char const * s)
    {
        return strncmp (buffer, s, Length) == 0;
    }

    operator != (char const * s)
    {
        return strncmp (buffer, s, Length) != 0;
    }


#endif
};

#if (defined(WIN32) && _MSC_VER >= 1300) || !defined(WIN32)
// Implementation of declared operators.

template <size_t LengthA, size_t LengthB>
inline
bool
operator == (String<LengthA> const & a,
    String<LengthB> const & b)
{
    return std::strncmp (a.buffer, b.buffer, std::min(LengthA, LengthB)) == 0;
}


template <size_t LengthA, size_t LengthB>
inline
bool
operator != (String<LengthA> const & a,
    String<LengthB> const & b)
{
    return std::strncmp (a.buffer, b.buffer, std::min(LengthA, LengthB)) != 0;
}


////////////

template <size_t Length>
bool
operator == (String<Length> const & a, char const * s)
{
    return std::strncmp (a.buffer, s, Length) == 0;
}


template <size_t Length>
bool
operator == (char const * s, String<Length> const & a)
{
    return std::strncmp (a.buffer, s, Length) == 0;
}


template <size_t Length>
bool
operator != (String<Length> const & a, char const * s)
{
    return std::strncmp (a.buffer, s, Length) != 0;
}


template <size_t Length>
bool
operator != (char const * s, String<Length> const & a)
{
    return std::strncmp (a.buffer, s, Length) != 0;
}

#endif //MSC_VER >=1300


namespace
{


template <bool a_less_than_b, typename StringA, typename StringB>
struct strindex_helper;

template <typename StringA, typename StringB>
struct strindex_helper<false, StringA, StringB>
{
    static
    int
    strindex (StringA const & str, StringB const & substr)
    {
        char const * const str_end = str + StringA::length;
        char const * const substr_end = substr + StringB::length;
        char const * const it = std::search (str, str_end, substr, substr_end);
        if (it != str_end)
            return static_cast<int>(it - str);
        else
            return -1;
    }
};


template <typename StringA, typename StringB>
struct strindex_helper<true, StringA, StringB>
{
    static
    int
    strindex (StringA const & str, StringB const & substr)
    {
        return -1;
    }
};

} // namespace


template <size_t LengthA, size_t LengthB>
int
strindex (String<LengthA> const & str, String<LengthB> const & substr)
{
    return
        strindex_helper<(LengthA < LengthB), String<LengthA>, String<LengthB> >
        ::strindex (str, substr);
}


template <size_t LengthA, size_t LengthB>
int
strindex (String<LengthA> const & str, char const (& substr) [LengthB])
{
    char const * const str_end = str + LengthA;
    char const * const substr_end = substr + LengthB - 1;
    char const * const it = std::search (static_cast<char const *>(str),
        str_end, substr, substr_end);
    if (it != str_end)
        return static_cast<int>(it - str);
    else
        return -1;
}


} // namespace db



#endif // DATAVIZ_DBSRING_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
