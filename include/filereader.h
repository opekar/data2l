#ifndef DATAVIZ_FILEREADER_H
#define DATAVIZ_FILEREADER_H

#include <cstdio>
#include <cerrno>

#include "types.h"
#include "reader.h"


#ifndef FRCHECK
#  define FRCHECK(cond, msg, where)                                     \
    if (! (cond))                                                       \
        throw FILEReaderException ((msg), errno,                        \
            FILEReaderException::where, this);
#else
#  error FRCHECK already defined
#endif // FRCHECK


namespace data2l
{

class FILEReader;
class FILEReaderException
    : public StreamException
{
public:
    enum Where {
        errUnspec,
        errOpen,
        errSeek,
        errRead,
        errClose,
        errTell
    };

    int const error;
    int const where;
    FILEReader * const stream;

    explicit
    FILEReaderException (std::string const & err, int eno, Where w,
        FILEReader * s)
        : StreamException (err), error (eno), where (w), stream (s)
    { }
};


class FILEReader
    : public Reader
{
    FILE * file;
    OffsetType act_pos;
    OffsetType filesize;
    OffsetType total_read;

public:
    FILEReader(char const * filename)
    {
        file = fopen(filename, "rb");
        FRCHECK (file, "error opening file", errOpen);
        OffsetType ret = fseek(file, 0, SEEK_END);
        FRCHECK (ret == 0, "error seeking to file's end", errOpen);
        filesize = ret = ftell(file);
        FRCHECK (ret >= 0, "ftell()", errOpen);
        ret = fseek(file, 0, SEEK_SET);
        FRCHECK (ret == 0, "error seeking back to the begining of file",
            errOpen);
        total_read = 0;
	act_pos = 0;
    }

    virtual ~FILEReader()
    {
        int ret = fclose(file);
        FRCHECK (ret == 0, "error closing file", errClose)
    }

    virtual void seek(OffsetType offs)
    {
        OffsetType res = fseek(file, offs, SEEK_SET);
        FRCHECK (res == 0, "error seeking", errSeek);
        act_pos = ftell(file);
        FRCHECK (act_pos != -1, "ftell()", errSeek);
    }

    virtual OffsetType tell()
    {
        long ret = ftell(file);
        FRCHECK (ret != -1, "ftell()", errTell);
        return ret;
    }

    virtual void readBin(char * chunk, size_t num_bytes)
    {
        if (num_bytes == 0)
            return;
        total_read += num_bytes;
        FRCHECK(static_cast<OffsetType>(act_pos + num_bytes) <= filesize,
            "out of bounds", errUnspec);
        long before = act_pos;
        long ret = fread(chunk, num_bytes, 1, file);
        FRCHECK (ret != -1, "error reading file", errRead);
        FRCHECK (ret == 1, "failed to read 1 chunk", errRead);
        act_pos = act_pos + num_bytes;
    }
};


} // namespace data2l


#undef FRCHECK

#endif // DATAVIZ_FILEREADER_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
