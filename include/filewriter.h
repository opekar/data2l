#ifndef DATAVIZ_FILEWRITER_H
#define DATAVIZ_FILEWRITER_H

#include <cstdio>

#include "types.h"
#include "writer.h"


#ifndef FWCHECK
#  define FWCHECK(cond, msg, where)                                     \
    if (! (cond))                                                       \
        throw FILEWriterException ((msg), errno,                        \
            FILEWriterException::where, this);
#else
#  error FWCHECK already defined
#endif // FWCHECK


namespace data2l
{

class FILEWriter;
class FILEWriterException
    : public StreamException
{
public:
    enum Where {
        errUnspec,
        errOpen,
        errSeek,
        errWrite,
        errClose,
        errTell
    };

    int const error;
    int const where;
    FILEWriter * const stream;

    explicit
    FILEWriterException (std::string const & err, int eno, Where w,
        FILEWriter * s)
        : StreamException (err), error (eno), where (w), stream (s)
    { }
};


class FILEWriter
    : public Writer
{
    FILE * file;
    OffsetType act_pos;
    OffsetType filesize;
    OffsetType total_write;

public:
    FILEWriter(char const * filename,
        unsigned oflags = mode::WRONLY | mode::CREAT)
        : act_pos (0)
    {
        modes = oflags;
        char const * fopen_mode = 0;
        if (oflags & mode::TRUNC)
            fopen_mode = "wb+";
        else if (oflags & mode::APPEND)
            fopen_mode = "ab+";
        else
            fopen_mode = "rb+";

        file = fopen(filename, fopen_mode);
        FWCHECK (file, "failed to open file", errOpen);
        OffsetType ret = fseek(file, 0, SEEK_END);
        FWCHECK (ret == 0, "failed to seek to end of file", errOpen);
        filesize = ret = ftell(file);
        FWCHECK (ret != -1, "ftell()", errOpen);
        ret = fseek(file, 0, SEEK_SET);
        FWCHECK (ret == 0, "failed to seek to start", errOpen);
        total_write = 0;
    }

    virtual ~FILEWriter()
    {
        int ret = fclose(file);
        FWCHECK (ret == 0, "failed to close file", errClose);
    }

    virtual void seek(OffsetType offs)
    {
        OffsetType res = fseek(file, offs, SEEK_SET);
        FWCHECK (res == 0, "failed to seek", errSeek);
        act_pos = res = ftell(file);
        FWCHECK (res != -1, "ftell()", errSeek);
    }

    virtual OffsetType tell()
    {
        OffsetType ret = ftell(file);
        FWCHECK (ret != -1, "ftell()", errTell);
        FWCHECK (ret == act_pos, "ftell() != act_pos", errTell);
        return ret;
    }

    virtual void writeBin(char const * chunk, size_t num_bytes)
    {
        if (num_bytes == 0)
            return;

        if (this->check_file_boundaries)
            FWCHECK (static_cast<OffsetType>(act_pos + num_bytes) <= filesize,
                "out of bounds", errWrite);
        long ret = fwrite(chunk, num_bytes, 1, file);
        FWCHECK (ret != -1, "write failed", errWrite);
        FWCHECK (ret == 1, "ret != 1", errWrite);
        act_pos += num_bytes;
        total_write += num_bytes;
    }
};


} // namespace db


#undef FWCHECK

#endif // DATAVIZ_FILEWRITER_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
