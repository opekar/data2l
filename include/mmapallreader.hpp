#ifndef DATAVIZ_MMAPALLREADER_H
#define DATAVIZ_MMAPALLREADER_H

#include <cstdio>
#include <cerrno>
#include <cstring>
#include <cmath>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "types.h"
#include "reader.h"


#ifndef FRCHECK
#  define FRCHECK(cond, msg, where)                                     \
    if (! (cond))                                                       \
        throw MmapAllReaderException ((msg), errno,                     \
            MmapAllReaderException::where, this);
#else
#  error FRCHECK already defined
#endif // FRCHECK


#ifndef DATAVIZ_MMAPALLREADER_BUFFER_SIZE
#  define DATAVIZ_MMAPALLREADER_BUFFER_SIZE (8*1024)
#endif


namespace data2l
{

class MmapAllReader;
class MmapAllReaderException
    : public StreamException
{
public:
    enum Where {
        errUnspec,
        errOpen,
        errSeek,
        errRead,
        errClose,
        errTell
    };

    int const error;
    int const where;
    MmapAllReader * const stream;

    explicit
    MmapAllReaderException (std::string const & err, int eno, Where w,
        MmapAllReader * s)
        : StreamException (err), error (eno), where (w), stream (s)
    { }
};


namespace mmapall_reader
{

size_t const page_size = static_cast<size_t>(sysconf(_SC_PAGE_SIZE));

} // namespace mmapall_reader


class MmapAllReader
    : public Reader
{
    int handle;
    char * buffer;
    OffsetType buffer_len;

    OffsetType act_pos;
    OffsetType filesize;
    OffsetType total_read;

public:
    MmapAllReader(char const * filename)
        : handle (-1), buffer (0), buffer_len (0)
    {
        handle = open (filename, O_RDONLY);
        FRCHECK (handle != -1, "error opening file", errOpen);
        OffsetType ret = lseek(handle, 0, SEEK_END);
        FRCHECK (ret != -1, "error seeking to file's end", errOpen);
        filesize = ret;
        ret = lseek(handle, 0, SEEK_SET);
        FRCHECK (ret != -1, "error seeking back to the begining of file",
            errOpen);

        buffer_len = round_up_to_page (filesize);
            //+ mmapall_reader::page_size
            ;
        buffer = reinterpret_cast<char *>(
            mmap (0, buffer_len, PROT_READ, MAP_SHARED, handle, 0));
        FRCHECK (buffer != MAP_FAILED, "error mapping file", errOpen);

        total_read = 0;
	act_pos = 0;
    }

    virtual ~MmapAllReader()
    {
        int ret;
        ret = munmap (buffer, buffer_len);
        FRCHECK (ret != -1, "error in munmap", errClose);
        ret = close(handle);
        FRCHECK (ret != -1, "error closing file", errClose)
    }

    virtual void seek(OffsetType offs)
    {
        FRCHECK (offs < filesize, "offs >= filesize", errSeek);
        act_pos = offs;
    }

    virtual OffsetType tell()
    {
        return act_pos;
    }

    virtual void readBin(char * chunk, size_t num_bytes)
    {
        if (num_bytes == 0)
            return;

        FRCHECK (0 <= act_pos && act_pos < filesize, "bad actual position",
            errRead);
        FRCHECK (act_pos + static_cast<OffsetType>(num_bytes) <= filesize,
            "read size out of range", errRead);
        memcpy (chunk, buffer + act_pos, num_bytes);

        act_pos += num_bytes;
        total_read += num_bytes;
    }

private:
    static OffsetType
    round_down_to_page (OffsetType off)
    {
        return (off / mmapall_reader::page_size) * mmapall_reader::page_size;
    }

    static OffsetType
    round_up_to_page (OffsetType off)
    {
        return mmapall_reader::page_size
            * (off / mmapall_reader::page_size
                + (off % mmapall_reader::page_size != 0));
    }
};


} // namespace db


#undef FRCHECK

#endif // DATAVIZ_MMAPALLREADER_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
