#ifndef DATAVIZ_MMAPREADER_H
#define DATAVIZ_MMAPREADER_H

#include <cstdio>
#include <cerrno>
#include <cstring>
#include <cmath>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "types.h"
#include "reader.h"


#ifndef FRCHECK
#  define FRCHECK(cond, msg, where)                                     \
    if (! (cond))                                                       \
        throw MmapReaderException ((msg), errno,                        \
            MmapReaderException::where, this);
#else
#  error FRCHECK already defined
#endif // FRCHECK


#ifndef DATAVIZ_MMAPREADER_BUFFER_SIZE
#  define DATAVIZ_MMAPREADER_BUFFER_SIZE (8*1024)
#endif


namespace data2l
{

class MmapReader;
class MmapReaderException
    : public StreamException
{
public:
    enum Where {
        errUnspec,
        errOpen,
        errSeek,
        errRead,
        errClose,
        errTell
    };

    int const error;
    int const where;
    MmapReader * const stream;

    explicit
    MmapReaderException (std::string const & err, int eno, Where w,
        MmapReader * s)
        : StreamException (err), error (eno), where (w), stream (s)
    { }
};


namespace mmap_reader
{

size_t const page_size = static_cast<size_t>(sysconf(_SC_PAGE_SIZE));

} // namespace mmap_reader


class MmapReader
    : public Reader
{
    int handle;
    char * buffer;
    size_t buffer_len;
    OffsetType buffer_start;
    size_t pref_buffer_len;

    OffsetType act_pos;
    OffsetType filesize;
    OffsetType total_read;

public:
    MmapReader(char const * filename,
        size_t buflen = DATAVIZ_MMAPREADER_BUFFER_SIZE)
        : handle (-1), buffer (0), buffer_len (0), buffer_start (0),
          pref_buffer_len (buflen)
    {
        FRCHECK (pref_buffer_len > 0, "bad prefered buffer length", errUnspec);
        handle = open (filename, O_RDONLY);
        FRCHECK (handle != -1, "error opening file", errOpen);
        OffsetType ret = lseek(handle, 0, SEEK_END);
        FRCHECK (ret != -1, "error seeking to file's end", errOpen);
        filesize = ret;
        ret = lseek(handle, 0, SEEK_SET);
        FRCHECK (ret != -1, "error seeking back to the begining of file",
            errOpen);
        total_read = 0;
	act_pos = 0;
    }

    virtual ~MmapReader()
    {
        int ret;
        if (buffer)
        {
            ret = munmap (buffer, buffer_len);
            FRCHECK (ret != -1, "error in munmap", errClose);
        }
        ret = close(handle);
        FRCHECK (ret != -1, "error closing file", errClose)
    }

    virtual void seek(OffsetType offs)
    {
        FRCHECK (offs < filesize, "offs >= filesize", errSeek);
        act_pos = offs;
    }

    virtual OffsetType tell()
    {
        return act_pos;
    }

    virtual void readBin(char * chunk, size_t num_bytes)
    {
        if (num_bytes == 0)
            return;

        while (num_bytes)
        {
            if (! (buffer_start <= act_pos
                    && act_pos < (buffer_start
                        + static_cast<OffsetType>(buffer_len))))
            {
                if (buffer)
                {
                    int ret = munmap (buffer, buffer_len);
                    FRCHECK (ret != -1, "error in munmap", errRead);
                }
                buffer_start = round_down_to_page (act_pos)
                    //- mmap_reader::page_size
                    ;
                buffer_len = round_up_to_page (pref_buffer_len)
                    //+ mmap_reader::page_size
                    ;
                buffer = reinterpret_cast<char *>(
                    mmap (0, buffer_len, PROT_READ, MAP_SHARED, handle,
                        buffer_start));
                FRCHECK (buffer != MAP_FAILED, "error mapping file", errOpen);
            }

            size_t readable_size
                = std::min ((buffer_start + buffer_len) - act_pos,
                    num_bytes);
            memcpy (chunk, buffer + (act_pos - buffer_start), readable_size);
            act_pos += readable_size;
            num_bytes -= readable_size;
            total_read += readable_size;
            chunk += readable_size;
        }
    }

private:
    static OffsetType
    round_down_to_page (OffsetType off)
    {
        return (off / mmap_reader::page_size) * mmap_reader::page_size;
    }

    static OffsetType
    round_up_to_page (OffsetType off)
    {
        return mmap_reader::page_size
            * (off / mmap_reader::page_size
                + (off % mmap_reader::page_size != 0));
    }
};


} // namespace db


#undef FRCHECK

#endif // DATAVIZ_MMAPREADER_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
