#ifndef DATA2L_NULLWRITER_HPP
#define DATA2L_NULLWRITER_HPP

#include "types.h"
#include "writer.h"


namespace data2l
{

class NullWriter
    : public Writer
{
    OffsetType act_pos;

public:
    NullWriter (char const * filename,
        unsigned oflags = mode::WRONLY | mode::CREAT)
        : act_pos(0)
    { }

    virtual ~NullWriter()
    { }

    virtual void seek(OffsetType offs)
    {
        act_pos = offs;
    }

    virtual OffsetType tell()
    {
        return act_pos;
    }

    virtual void writeBin(char const * chunk, size_t num_bytes)
    {
        act_pos = act_pos + num_bytes;
    }
};

} // namespace db


#endif // DATA2L_NULLWRITER_HPP

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// tab-width: 4
// End:
