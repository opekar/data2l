#ifndef DATAVIZ_READERWIRTERT_H
#define DATAVIZ_READERWRITERT_H
//platform independent definition of readers and writers
#ifdef WIN32
#include "win32sysreader.h"
#include "win32syswriter.h"
typedef data2l::Win32SysReader ReaderT;
typedef data2l::Win32SysWriter WriterT;

// TODO: Implement and add logic for picking the right writer on 32
// bit Unix platform (fseeko() and friends). The following is right
// only for LP64 platforms, thanks to their long being 64 bits.
#else
#include "filereader.h"
#include "filewriter.h"
typedef data2l::FILEReader ReaderT;
typedef data2k::FILEWriter WriterT;
#endif

#endif //DATAVIZ_READER_H
