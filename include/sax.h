#ifndef DATAVIZ_SAX_H
#define DATAVIZ_SAX_H

namespace data2l
{
class Base;

class SAXHandler
{
public:
    enum {
        sax_nothing,
        sax_stop
    };

public:
    virtual int beginElement(int, data2l::Base *) = 0;
    virtual void endElement(int, data2l::Base *) = 0;
    virtual ~SAXHandler()
    { }
};

}

#endif // DATAVIZ_SAX_H
