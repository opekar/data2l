#ifndef DATAVIZ_TYPES_H
#define DATAVIZ_TYPES_H

#include <vector>
#include <stdexcept>
#include <string>


#ifdef WIN32
typedef unsigned int   DWord;
typedef unsigned short Word;
typedef unsigned char  Byte;
typedef char           Char;
typedef __int64        LongLong;
typedef float          Float;

typedef size_t         SizeType;
#if (_MSC_VER >= 1300)
typedef __int64        OffsetType;
#else
typedef unsigned long  OffsetType;
#endif

#elif __CYGWIN32__

typedef unsigned int   DWord;
typedef unsigned short Word;
typedef unsigned char  Byte;
typedef char           Char;
typedef long long      LongLong;
typedef float          Float;

typedef size_t         SizeType;
typedef long long      OffsetType

#elif __LP64__

typedef unsigned int   DWord;
typedef unsigned short Word;
typedef unsigned char  Byte;
typedef char           Char;
typedef long           LongLong;
typedef float          Float;

typedef size_t         SizeType;
typedef long           OffsetType;

#else
#error "Unsupported architecture or compiler."
#endif

#include "dbstring.h"

namespace
{
    template<bool val>
    class StaticAssert;

    template<>
    class StaticAssert<true>
    { };

    template<typename T, unsigned size>
    class StaticAssertSizeOf
        : public StaticAssert<sizeof(T) == size>
    { };

    StaticAssertSizeOf<DWord, 4> _check01;
    StaticAssertSizeOf<Word, 2> _check02;
    StaticAssertSizeOf<Byte, 1> _check03;
    StaticAssertSizeOf<Char, 1> _check04;
    StaticAssertSizeOf<Float, 4> _check05;
}

namespace data2l
{

class StreamException
    : public std::runtime_error
{
public:
    explicit
    StreamException (std::string const & reason)
        : std::runtime_error (reason)
    { }
};


namespace mode
{

const unsigned RDONLY = (1u << 0);
const unsigned WRONLY = (1u << 1);
const unsigned APPEND = (1u << 2);
const unsigned TRUNC = (1u << 3);
const unsigned EXCL = (1u << 4);
const unsigned CREAT = (1u << 5);
const unsigned RDWR = RDONLY | WRONLY;

} // namespace mode


class StreamBase
{
public:
    StreamBase ()
        : failure (false)
    { }

    virtual ~StreamBase ()
    { }

    bool
    good () const
    {
        return ! failure;
    }

    bool 
    failbit () const
    {
        return failure;
    }

protected:
    unsigned modes;
    bool failure;
};


} // namespace db

#endif // DATAVIZ_TYPES_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
