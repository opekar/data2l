#ifndef DATAVIZ_WIN32MMAPREADER_H
#define DATAVIZ_WIN32MMAPREADER_H

#define NOMINMAX

#include <cstdio>
#include <cerrno>
#include <cstring>
#include <algorithm>

#include <windows.h>

#include "types.h"
#include "reader.h"


#ifndef FRCHECK
#  define FRCHECK(cond, msg, where)                                     \
    if (! (cond))                                                       \
        throw Win32MmapReaderException ((msg), GetLastError(),          \
            Win32MmapReaderException::where, this);
#else
#  error FRCHECK already defined
#endif // FRCHECK


#ifndef DATAVIZ_MMAPREADER_BUFFER_SIZE
#  define DATAVIZ_MMAPREADER_BUFFER_SIZE (8*1024)
#endif


namespace data2l
{

class Win32MmapReader;
class Win32MmapReaderException
    : public StreamException
{
public:
    enum Where {
        errUnspec,
        errOpen,
        errSeek,
        errRead,
        errClose,
        errTell
    };

    DWORD const error;
    int const where;
    Win32MmapReader * const stream;

    explicit
    Win32MmapReaderException (std::string const & err, DWORD eno, Where w,
        Win32MmapReader * s)
        : StreamException (err), error (eno), where (w), stream (s)
    { }
};


namespace win32mmap_reader
{

size_t const page_size = 0x10000;

} // namespace win32mmap_reader


class Win32MmapReader
    : public Reader
{
    HANDLE file_handle, map_handle;
    char * buffer;
    size_t buffer_len;
    OffsetType buffer_start;
    size_t pref_buffer_len;

    OffsetType act_pos;
    OffsetType filesize;
    OffsetType total_read;

public:
    Win32MmapReader(char const * filename,
        size_t buflen = DATAVIZ_MMAPREADER_BUFFER_SIZE)
        : file_handle (INVALID_HANDLE_VALUE),
        map_handle (INVALID_HANDLE_VALUE), buffer (0), buffer_len (0),
        buffer_start (0), pref_buffer_len (buflen)
    {
        FRCHECK (pref_buffer_len > 0, "bad prefered buffer length", errUnspec);

        file_handle = CreateFileA (filename, GENERIC_READ, FILE_SHARE_READ,
            0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
        FRCHECK (file_handle != INVALID_HANDLE_VALUE, "error opening file",
            errOpen);

        map_handle = CreateFileMapping (file_handle, 0, PAGE_READONLY, 0, 0,
            0);
        FRCHECK (map_handle != INVALID_HANDLE_VALUE && map_handle,
            "error in CreateFuleMapping", errOpen);

        LARGE_INTEGER tmp, new_offset;
        tmp.QuadPart = 0;
        BOOL ret = SetFilePointerEx (file_handle, tmp, &new_offset, FILE_END);
        FRCHECK (ret, "error seeking to file's end", errOpen);
        filesize = new_offset.QuadPart;
        ret = SetFilePointerEx (file_handle, tmp, 0, FILE_BEGIN);
        FRCHECK (ret, "error seeking back to the begining of file", errOpen);

        total_read = 0;
	    act_pos = 0;
    }

    virtual ~Win32MmapReader()
    {
        BOOL ret;
        if (buffer)
        {
            ret = UnmapViewOfFile (buffer);
            FRCHECK (ret, "error in UnmapViewOfFile", errClose);
        }
        ret = CloseHandle (map_handle);
        FRCHECK (ret, "error closing mapping handle", errClose);
        ret = CloseHandle (file_handle);
    }

    virtual void seek(OffsetType offs)
    {
        FRCHECK (offs < filesize, "offs >= filesize", errSeek);
        act_pos = offs;
    }

    virtual OffsetType tell()
    {
        return act_pos;
    }

    virtual void readBin(char * chunk, size_t num_bytes)
    {
        if (num_bytes == 0)
            return;

        while (num_bytes)
        {
            if (! (buffer_start <= act_pos
                    && act_pos < (buffer_start
                        + static_cast<OffsetType>(buffer_len))))
            {
                if (buffer)
                {
                    BOOL ret = UnmapViewOfFile (buffer);
                    FRCHECK (ret, "error in munmap", errRead);
                    buffer = 0;
                }
                buffer_start = round_down_to_page (act_pos)
                    //- win32mmap_reader::page_size
                    ;
                buffer_len = static_cast<size_t>(
                    round_up_to_page (pref_buffer_len)
                    //+ win32mmap_reader::page_size
                    );
                LARGE_INTEGER file_offset;
                file_offset.QuadPart = buffer_start;
                buffer = reinterpret_cast<char *>(
                    MapViewOfFile (map_handle, FILE_MAP_READ,
                    file_offset.u.HighPart, file_offset.u.LowPart,
                    buffer_len));
                FRCHECK (buffer, "error mapping file", errRead);
            }

            size_t readable_size
                = static_cast<size_t>(
                    std::min ((buffer_start + buffer_len) - act_pos,
                        static_cast<OffsetType>(num_bytes)));
            memcpy (chunk, buffer + (act_pos - buffer_start), readable_size);
            act_pos += readable_size;
            num_bytes -= readable_size;
            total_read += readable_size;
            chunk += readable_size;
        }
    }

private:
    static OffsetType
    round_down_to_page (OffsetType off)
    {
        return (off / win32mmap_reader::page_size) * win32mmap_reader::page_size;
    }

    static OffsetType
    round_up_to_page (OffsetType off)
    {
        return win32mmap_reader::page_size
            * (off / win32mmap_reader::page_size
                + (off % win32mmap_reader::page_size != 0));
    }
};


} // namespace db


#undef FRCHECK

#endif // DATAVIZ_WIN32MMAPREADER_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
