#ifndef WIN32SYSREADER_H
#define WIN32SYSREADER_H

#include "types.h"
#include "reader.h"

#include <fcntl.h>
#include <share.h>
#include <io.h>


#ifndef W32RCHECK
#  define W32RCHECK(cond, msg, where)                                   \
    if (! (cond))                                                       \
        throw Win32SysReaderException ((msg), errno,                    \
            Win32SysReaderException::where, this);
#else
#  error W32RCHECK already defined
#endif // W32RCHECK


namespace data2l
{

class Win32SysReader;
class Win32SysReaderException
    : public StreamException
{
public:
    enum Where {
        errUnspec,
        errOpen,
        errSeek,
        errRead,
        errClose,
        errTell,
    };

    int const error;
    int const where;
    Win32SysReader * const stream;

    explicit
    Win32SysReaderException (std::string const & msg, int eno, Where w,
        Win32SysReader * s)
        : StreamException (msg), error (eno), where (w), stream (s)
    { }
};

//#define BUFFERED

class Win32SysReader
    : public Reader
{
    int file;
    OffsetType filesize;
    OffsetType act_pos;

#ifdef BUFFERED
    unsigned char buffer[4096];
    OffsetType bufferedFrom;
    long hit;
    long miss;
#endif

public:
    Win32SysReader (char const * filename)
        : act_pos(0)
    {
        file = _sopen(filename, _O_BINARY | _O_RANDOM | _O_RDONLY, _SH_DENYNO);
        W32RCHECK (file != -1, "failed to open file", errOpen);
        OffsetType ret;
        ret = _lseeki64(file, 0, SEEK_END);
        W32RCHECK (ret != -1, "failed to seek to files end", errSeek);
        filesize = _telli64(file);
        W32RCHECK (filesize != -1, "telli64()", errTell);
        ret = _lseeki64(file, 0, SEEK_SET);
        W32RCHECK(ret != -1, "failed to seek back to file start", errSeek);

#ifdef BUFFERED
        hit = miss =0;
        preRead(0);

#endif
    }

#ifdef BUFFERED
    void preRead(OffsetType offset)
    {
        if (filesize > offset)
        {
            bufferedFrom = offset;
            seek(bufferedFrom);
            SizeType bytes_to_read = filesize - bufferedFrom;
            if ( bytes_to_read > sizeof(buffer) )  bytes_to_read = sizeof(buffer);
            int bytes_read = _read(file, buffer, bytes_to_read);
            W32RCHECK (bytes_read != -1, "error reading file", errRead);
            W32RCHECK (static_cast<size_t>(bytes_read) == bytes_to_read,
            "failed to read requested amount of bytes", errRead);
        }
    }
#endif



    virtual ~Win32SysReader()
    {
        int ret = _close(file);
        W32RCHECK (ret != -1, "failed to close file", errClose);
    }

    virtual void seek(OffsetType offs)
    {
        OffsetType ret = _lseeki64(file, offs, SEEK_SET);
        W32RCHECK(ret != -1, "failed to seek", errSeek);
        act_pos = offs;
    }

    virtual OffsetType tell()
    {
        return act_pos;
    }

#ifdef BUFFERED
    virtual void readBin(char * chunk, SizeType num_bytes)
    {
        if (num_bytes == 0)
            return;
        W32RCHECK (act_pos + num_bytes <= filesize, "out of bounds", errUnspec);
        W32RCHECK (num_bytes <= sizeof(buffer), "number o bytes is bigger then the buffer", errUnspec);
        OffsetType before = act_pos;

        //do we have it buffered, if not read the chunk into the buffer
        if (!(act_pos>=bufferedFrom && act_pos+num_bytes<bufferedFrom + sizeof(buffer)))
        {
            miss++;
            preRead(act_pos);
        } else
            hit++;
        if ((miss + hit) % 2000000 ==0)
            printf("hit/mis ratio %d/%d, hits per miss %d \n", hit, miss,hit/miss);

        memcpy(chunk, buffer + (act_pos-bufferedFrom), num_bytes);


        act_pos = act_pos + num_bytes;
    }
#else
    virtual void readBin(char * chunk, SizeType num_bytes)
    {
        if (num_bytes == 0)
            return;
        OffsetType o = _telli64(file);
        W32RCHECK (o == act_pos, "_telli64()", errTell);
        W32RCHECK (act_pos + num_bytes <= filesize, "out of bounds", errUnspec);
        OffsetType before = act_pos;
        int bytes_read = _read(file, chunk, num_bytes);
        W32RCHECK (bytes_read != -1, "error reading file", errRead);
        W32RCHECK (static_cast<size_t>(bytes_read) == num_bytes,
            "failed to read requested amount of bytes", errRead);
        act_pos = act_pos + num_bytes;
    }
#endif
};

} // namespace db


#undef W32RCHECK

#endif // WIN32SYSREADER_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
