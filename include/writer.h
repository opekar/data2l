#ifndef DATAVIZ_WRITER_H
#define DATAVIZ_WRITER_H

#include <cstdio>
#include <cassert>

#include "types.h"


namespace data2l
{

class Writer
    : public virtual StreamBase
{
public:
    Writer ()
        : check_file_boundaries (true)
    { }

    virtual ~Writer ()
    { }

    virtual void seek(OffsetType offs) = 0;
    virtual OffsetType tell() = 0;
    virtual void writeBin(char const *chunk, SizeType num_bytes) = 0;

    virtual void writeChar(Char const * chunk, SizeType num)
    {
        writeBin(reinterpret_cast<char const *>(chunk), num);
    }

    virtual void writeByte(Byte const * chunk, SizeType num)
    {
        writeBin(reinterpret_cast<char const *>(chunk), num);
    }

    virtual void writeByte(Word const * chunk, SizeType num)
    {
        writeBin(reinterpret_cast<char const *>(chunk), num);
    }

    virtual void writeByte(DWord const* chunk, SizeType num)
    {
        writeBin(reinterpret_cast<char const *>(chunk), num);
    }

    virtual void writeByte(LongLong const * chunk, SizeType num)
    {
        writeBin(reinterpret_cast<char const *>(chunk), num);
    }

    virtual void writeByte(Float const* chunk, SizeType num)
    {
        writeBin(reinterpret_cast<char const *>(chunk), num);
    }

    virtual void writeWord(Word const* chunk, SizeType num)
    {
        writeBin(reinterpret_cast<char const *>(chunk), num*sizeof(Word));
    }

    virtual void writeDWord(DWord const * chunk, SizeType num)
    {
        writeBin(reinterpret_cast<char const *>(chunk), num*sizeof(DWord));
    }

    virtual void writeFloat(Float const * chunk, SizeType num)
    {
        writeBin(reinterpret_cast<char const *>(chunk), num*sizeof(Float));
    }


    bool check_file_boundaries : 1;
};


} // namespace db

#endif // DATAVIZ_WRITER_H

// Local Variables:
// mode: C++
// eval: (c-set-style "microsoft")
// End:
