from distutils.core import setup
from data2l_version import data2l_version

setup(
    name = 'Data2l',
    description = 'Data2l',
    author='Eccam, s.r.o. The Czech Republic',
    author_email='@eccam.com',
    url='http://www.eccam.com',
    packages=['src',
              'src/customReaders',
              'src/exprs',
              'src/file_defs',
              'src/generators',
              'gtkGUI',
              'src/gtkGUI/clipboard',
              'src/gtkGUI/docs',
              'src/unit_tests',
              'lib/pyggy',
              'lib/Cheetah',
              'lib/PyGTKCodeBuffer',
              'lib/yaml'],
    version = data2l_version,
    package_dir =  { 'src'    : 'src' ,
                     'gtkGUI' : 'src/gtkGUI' },
    package_data = { 'gtkGUI' : 
                        ['gui.glade', 'eccam.ico', 'about.png']},
    data_files=[
#                   ('gtkGUI', ['src/gtkGUI/gui.glade',
#                               'src/gtkGUI/eccam.ico',
#                               'src/gtkGUI/about.png',
#                               ]),
                   ('images', ['src/images/header-bg.png',
                               ]),
                   ('customGenerators', ['src/generators/GraphVizGen.py'
                                         ])
               ]
)
