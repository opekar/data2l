from distutils.core import setup
import py2exe
from data2l_version import data2l_version

setup(
    options={"py2exe":{'dll_excludes': [ "mswsock.dll", "powrprof.dll"], # fix of vista build http://stackoverflow.com/questions/1979486/py2exe-win32api-pyc-importerror-dll-load-failed
					    "optimize":2
                       }},
    windows = [{'script': 'src/main.py',
               'icon_resources': [(0x0001, 'src/gtkGUI/eccam.ico')]}],
    name = 'Data2l',
    description = 'Data2l',
    version = data2l_version,
    data_files=[
                   ('gtkGUI', ['src/gtkGUI/gui.glade',
                               'src/gtkGUI/eccam.ico',
                               'src/gtkGUI/about.png',
                               ]),
                   ('images', ['src/images/header-bg.png',
                               'src/images/splashscreen.png',
                               ]),
                   ('customGenerators', ['src/generators/GraphVizGen.py'
                                         ])
               ],
)

setup()
