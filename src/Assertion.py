import ValueExtractors as VE
from exprs.ExprVE import ExprVE
import types
from Utility import xassert


__all__ = """\
Assertion
"""


## Object wrapping a value getter that validates
class Assertion(object):
    def __init__(self, code, name = None):
        xassert(isinstance(name, str) or isinstance(name, types.NoneType))
        self.name = name
        if isinstance(code, str):
            code = ExprVE(code)
        else:
            xassert(isinstance(code, VE.IQueryDOMValue))
        self.code = code
    ## \TODO shell be renamed to getDescription
    def getName(self):
        return self.name

    def getCode(self):
        return self.code

    def eval(self, dom):
        ret = self.code.getDOMValue(dom)
        return ret
