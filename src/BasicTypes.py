import sys
from Utility import xassert, pure_virtual


__all__ = """\
BasicType
BasicNumType
BasicArrayType
StaticSizeArray
String
Char
Byte
Word
DWord
Float
VarInteger
""".split()


class BasicType(object):
    def load(self, reader, n):
        pure_virtual()

    def save(self, writer, value, n):
        pure_virtual()

    def getStaticSize(self):
        pure_virtual()


class BasicNumType(BasicType):
    def __init__(self, enum = None):
        self.setEnum(enum)
    
    def getEnum(self):
        return self.enum
    
    def setEnum(self,enum):
        self.enum = enum

class BasicArrayType(BasicType):
    def __init__(self, elem):
        self.basicElement = elem

    def get_basic_element(self):
        return self.basicElement

    def set_basic_element(self, elem):
        self.basicElement = elem


class StaticSizeArray(BasicArrayType):
    def __init__(self, basicElement):
        BasicArrayType.__init__(self, basicElement)        

    def __str__(self):
        return str(self.basicElement) + '[]'

    def load(self, reader, n):
        result = []
        for i in xrange(n):
            result.append(self.basicElement.load(reader, 1))
        return result


    def save(self, writer, value, n):
        xassert(n <= len(value))
        for i in xrange(n):
            self.basicElement.save(writer, value[i], 1)
        return

    def getStaticSize(self):
        return self.basicElement.getStaticSize()


class String(BasicArrayType):  
    def __init__(self):
        BasicArrayType.__init__(self, Char())        

    def __str__(self):
        return 'String'

    def load(self,reader,n):
        return self.basicElement.load(reader, n)


    def save(self, writer, value, n):
        xassert(n <= len(value))
        writer.writeString(value[:n])

    def getStaticSize(self):
        return self.basicElement.getStaticSize()


class Char(BasicType):
    def __str__(self):
        return "Char"

    def load(self, reader, n):
        return reader.readString(n)

    def save(self, writer, value, n):
        buf = buffer(value)
        writer.writeString(buf)

    def getStaticSize(self):
        return  1


class Byte(BasicNumType):
    def __str__(self):
        return "Byte"

    def load(self, reader, n):
        xassert(n == 1, 'Byte class can read only 1 byte')
        return reader.readByte()

    def save(self, writer, value, n):
        writer.writeByte(value)

    def getStaticSize(self):
        return  1


class Word(BasicNumType):
    def __str__(self):
        return "Word"

    def load(self, reader, n):
        xassert(n == 1, 'Word class can read only 1 word')
        return reader.readWord()

    def save(self, writer, value, n):
        writer.writeWord(value)

    def getStaticSize(self):
        return  2


class DWord(BasicNumType):
    def __str__(self):
        return "DWord"

    def load(self, reader, n):
        xassert(n == 1, 'DWord class can read only 1 dword')
        return reader.readDWord()

    def save(self, writer, value, n):
        writer.writeDWord(value)

    def getStaticSize(self):
        return  4


class Float(BasicNumType):
    def __str__(self):
        return "Float"

    def load(self, reader, n):
        xassert(n == 1, 'Float class can read only 1 float')
        return reader.readFloat()

    def save(self, writer, value, n):
        writer.writeFloat(value)

    def getStaticSize(self):
        return  4


## Variable width integer.
class VarInteger(BasicArrayType):
    def __init__(self):
        BasicArrayType.__init__(self, Byte())
          
    def __str__(self):
        return "VarInteger"

    def load(self, reader, n):
        binstr = reader.readString(n)
        value = 0
        multi = 1
        for i in xrange(n):
            value = value + ord(binstr[i]) * multi
            multi = multi * 256
        return value
    
    
    def save(self, writer, value, n):
        s = ""
        tmp_val = value
        while n > 0:
            b = tmp_val & 0xFF
            s += chr(b)
            tmp_val >>= 8
            n -= 1
        if sys.byteorder == 'big':
            s = s[::-1]
        writer.writeString(s)

    # this is called only by getElementSize & getByteSize
    # , basic element size of aray is 
    # for var integer byte
    # there for the 1 here    
    def getStaticSize(self):
        return 1
