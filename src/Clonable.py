## This module defines interface Clonable.
from Utility import pure_virtual


__all__ = """\
Clonable
CopyClonable
"""


## Base interface for clonable classes.
class Clonable(object):
    def clone(self):
        pure_virtual()


## Implementation of Clonable interface that copies using simple shallow copy.
class CopyClonable(Clonable):
    def clone(self):
        from copy import copy
        return copy(self)
