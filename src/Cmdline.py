import sys, traceback
import Definition
from generators.cppgen2 import generator as cppgen


def generate_cpp(opts):
    try:
        module = Definition.loadDefinitionFile("_command_line_",
                                               opts.definition_file)
    except Exception, e:
        info = None
        s = ""
        try:
            info = sys.exc_info()
            s = traceback.format_tb(info[2])
        finally:
            del info
        s = str("").join(s)
        s = ("Specified path is not a Python source file!\n"
             + str(e) + "\n\n" + s)
        print >> sys.stderr, s
        return

    definition = None
    root_dep = None
    if "definition" not in module.__dict__:
        print >> sys.stderr, "File loaded but 'definition' was not found."
        return
    else:
        definition = module.__dict__["definition"]
    try:
        compiled_def = definition.getCompiledDefinition()
        root_dep = compiled_def.dep_root
    except Exception, e:
        print >> sys.stderr, str(e)
        return

    settings = {
        "TargetDirectory": '.',
        "HeaderName": root_dep.getElement().getName() + ".h",
        "CPPName": root_dep.getElement().getName() + ".cpp",
        "CapabilityParse": "True",
        "CapabilitySave": "True",
        "CapabilityIteratableArrays": "True",
    }
    if opts.output_dir is not None:
        settings["TargetDirectory"] = opts.output_dir
    if opts.header_file is not None:
        settings["HeaderName"] = opts.header_file
    if opts.cpp_file is not None:
        settings["CPPName"] = opts.cpp_file

    cppgen.generate(root_dep, None, settings)

    return
