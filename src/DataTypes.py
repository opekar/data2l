from BasicTypes import *
from Dependency import Dependency
from exprs.ExprVE import ExprVE
from Assertion import Assertion
from Utility import pure_virtual, impossible, xassert, xassert_type, validate_ident, valid_identifier_regex

## List of exported names by "from *".
__all__ = """\
DataElement
SimpleElement
Void
Basic
Attrib
Composite
Struct
Array
Alternative
DeclarationError
DataDefinitionException
""".split()

class DataDefinitionException(Exception):
    def __init__(self, elem_name, msg):
        self.elem_name = elem_name
        self.msg = msg 
        
    def __str__(self):
        return "Element: " + self.elem_name + "\n" + self.msg

class DataElement(object):
    def __init__(self, name):        
        self.setName(name)
        self.doc = ""
        self.parents = {}

    def accept(self, _):
        pure_virtual()

    def getName(self):
        return self.name

    def setName(self, new_name):        
        if not validate_ident(new_name):
            raise DataDefinitionException(new_name,
                 "Incorrect name format,identifier has to match "
                 "regex %s" % valid_identifier_regex)
        self.name = new_name

    def getAggregates(self):
        return []

    def getDeps(self):
        return []

    def setDoc(self, doc):
        self.doc = doc

    def getDoc(self):
        return self.doc

    def setParent(self, parent):
        self.parents[parent] = 1

    def getParents(self):
        return self.parents.keys()
    

## Base class for all simple types. It is used as easy to check against
# tag in SAX.py.
class SimpleElement(DataElement):
    def __init__(self, name):
        DataElement.__init__(self, name)


class Void(SimpleElement):
    def __init__(self):
        SimpleElement.__init__(self, "void")

    def __str__(self):
        return "Void"

    def accept(self, visitor):
        visitor.VisitVoid(self)


## Wrapper for basic types.
class Basic(SimpleElement):
    def __init__(self, name, basic_type):
        SimpleElement.__init__(self, name)
        xassert(isinstance(basic_type, BasicType))
        self.basic_type = basic_type

    def __str__(self):
        return "Basic:"+str(self.basic_type)+"("+self.name+")"

    def type(self):
        return self.basic_type

    def set_type(self, t):
        self.basic_type = t

    def accept(self, visitor):
        visitor.VisitBasic(self)

    def getElementSize(self):
        return self.basic_type.getStaticSize()


## Wrapper for Basic class, so that attributes are easily distinguishable
# from regular members.
class Attrib(Basic):
    def __init__(self, name, basic_type):
        Basic.__init__(self, name, basic_type)

    def __str__(self):
        return "Attrib:"+str(self.basic_type)+"("+self.name+")"
        
    def getElementSize(self):
        return 0


## Base class with common functionality of Struct, Array and Alternative.
class Composite(DataElement):
    ## Parses list of arguments of fields declarations.
    def __init__(self, name, *fields):
        DataElement.__init__(self, name)
        self.fields = []
        self.deps = []

        # Catch Composite's comment.
        if (len(fields) > 0 and isinstance (fields[0], list)):
            if isinstance(fields[0][0], str):
                self.setDoc(fields[0][0])
                fields = fields[1:]
            else:
                raise DeclarationError("Wrong use of composite comment.")

        comment = ""
        member = None
        dependency = None
        assertions = []
        for f in fields:
            # String is a comment.
            if isinstance(f, str):
                # Is this comment terminating previous field declaration?
                if member is not None:
                    if dependency is None:
                        dependency = Dependency()
                    if len(assertions) > 0:
                        dependency.addAssertions(assertions)
                    self.addField(member, dependency)
                    # Comments inside ctor above fields are only
                    # for Basic types.
                    if isinstance(member, SimpleElement):
                        member.setDoc(comment)
                    else:
                        # Ignore the comment if it is not for Basic type.
                        pass
                    member = dependency = None
                    assertions = []
                    comment = f
                else:
                    if len(comment) > 0:
                        comment += "\n" + f
                    else:
                        comment = f
            # Is this field terminating previous field declaration?
            elif isinstance(f, DataElement):
                if member is not None:
                    if dependency is None:
                        dependency = Dependency()
                    if len(assertions) > 0:
                        dependency.addAssertions(assertions)
                    self.addField(member, dependency)
                    # Comments inside ctor above fields are only
                    # for Basic types.
                    if isinstance(member, SimpleElement):
                        member.setDoc(comment)
                    else:
                        # Ignore the comment if it is not for Basic type.
                        pass

                    member = f
                    comment = ""
                    dependency = None
                    assertions = []
                else:
                    member = f
            elif isinstance(f, Dependency):
                xassert(dependency is None)
                dependency = f
            elif isinstance(f, Assertion):
                assertions.append(f)
            else:
                raise DeclarationError(
                    "Unexpected element in composite declaration:\n"
                    + str(f))

        # Finish last field declaration.
        if member is not None:
            if dependency is None:
                dependency = Dependency()
            if len(assertions) > 0:
                dependency.addAssertions(assertions)
            self.addField(member, dependency)
            # Comments inside ctor above fields are only
            # for Basic types.
            if isinstance(member, SimpleElement):
                member.setDoc(comment)
            else:
                # Ignore the comment if it is not for Basic type.
                pass

        return

    def addField(self, field, dep = None):
        self.addFieldAt(len(self.fields), field, dep)
        return self

    def addFieldAt(self, at, field, dep):
        xassert_type(field, DataElement)
        xassert(at <= len(self.fields))
        xassert(dep is not None)

        self.fields.insert(at,field)
        field.setParent(self)
        
        self.deps.insert(at, dep)
        dep.setElement(field)
        return self

    def deleteAt(self, at):
        xassert(at < len(self.fields))
        self.fields.pop(at)
        self.deps.pop(at)

    def getAggregates(self):
        return self.fields

    def getDeps(self):
        return self.deps


class Struct(Composite):
    def __init__(self, name, *fields):
        Composite.__init__(self, name, *fields)

    def __str__(self):
        return "Struct("+self.name+")"

    def accept(self,visitor):
        visitor.VisitStruct(self)


class Array(Composite):
    def __init__(self, name, *fields):
        Composite.__init__(self, name, *fields)

    def __str__(self):
        return "Array("+self.name+")"

    def accept(self, visitor):
        visitor.VisitArray(self)

    ## \deprecated
    def setIndexStruct(self, pointee, in_mem = False):
        self.setArrayElement(pointee, in_mem)

    ## \deprecated
    def setArrayElement(self, pointee, in_mem = False):
        xassert(isinstance(pointee, DataElement))
        self.fields.append(pointee)
        dep = Dependency(in_memory = in_mem,
                         offset = ExprVE("offsetof(%s) + actindexof(%s) * sizeof(%s)" %
                                         (self.name, self.name, pointee.name)))

        self.deps.append(dep)
        dep.setElement(pointee)

    def addArrayField(self, pointee, dep = None):
        return self.addField(pointee, dep)



class Alternative(Composite):
    def __init__(self, name, *fields):
        Composite.__init__(self, name, *fields)

    def __str__(self):
        return "Alternative("+self.name+")"

    def accept(self, visitor):
        visitor.VisitAlternative(self)

    def addAlternative(self, alternative, dep = None):
        return self.addField(alternative, dep)


class DeclarationError(Exception):
    def __init__(self, what):
       super(Exception, self).__init__(what)
