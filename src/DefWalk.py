import DataTypes as DT
from Dependency import Dependency
from Definition import Definition
from Utility import xassert, xassert_type


__all__ = """\
IDefWalkEvents
DefinitionWalker
""".split()


class IDefWalkEvents(object):
    def dependencyBegin(self, dep):
        pass

    def dependencyEnd(self, dep):
        pass

    def elementBegin(self, elem):
        pass

    def elementEnd(self, elem):
        pass

## Walk trough given definition with starting point at entity.
# -# on events object, which has to be IDefWalkEvents, run callback functions
#  to self-check rightness of structure.
# -# checking run by traverse function
# -# example:
#    class StructCheck(DefWalk.IDefWalkEvents):
#        ...
#        def check():
#            DefinitionWalker(self, self.definition).traverse()
#        ...
#        ## callback function defined by IDefWalkEvent interface
#        def elementBegin():
#            # some checks here
#
class DefinitionWalker(object):
    def __init__(self, events, entity):
        xassert_type(events, IDefWalkEvents)
        xassert(entity is not None)
        xassert(isinstance(entity, DT.DataElement)
                or isinstance(entity, Dependency)
                or isinstance(entity, Definition))

        self.events = events
        self.entity = entity

    def traverse(self):
        dep = None
        if isinstance(self.entity, DT.DataElement):
            dep = Dependency()
            dep.setElement(self.entity)
        elif isinstance(self.entity, Dependency):
            dep = self.entity
        elif isinstance(self.entity, Definition):
            dep = self.entity.dep_root
        self.__traverse_dep(dep)
        return

    def __traverse_dep(self, dep):
        self.events.dependencyBegin(dep)
        elem = dep.getElement()
        self.__traverse_elem(elem)
        self.events.dependencyEnd(dep)

    def __traverse_elem(self, elem):
        self.events.elementBegin(elem)
        deps = elem.getDeps()
        for d in deps:
            self.__traverse_dep(d)
        self.events.elementEnd(elem)
