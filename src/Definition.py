from Dependency import Dependency, CompiledDependency
import imp

from language.Preprocessing import *
from language.Parser import *
from language.DependencyTree import *

from language.LanguageRW import *
from UserEnumsHolder import UserEnumsHolder

from datascript import DSParser, DSLexer
from pyggy import pyggy,proctree

__all__ = """\
loadDefinitionFile
load_and_compile
Definition
""".split()


_module_counter = 0


## Loads file structure definition from .py source file into a module.
# \return Tuple of (definition, module).
def loadDefinitionFile(filename, module_name = None):
    global _module_counter
    if module_name is None:
        _module_counter += 1
        module_name = "_data2l_module_" + str(_module_counter)
    module = imp.load_source(module_name, filename)
    if "definition" not in module.__dict__:
        raise Exception("missing \"definition\" in \"%s\"" % filename)
    return module.__dict__["definition"], module

def loadDefinitionD2lFile(filename):
    input_file = open(filename, 'r')
    source = input_file.read()
    input_file.close()
    
    rootDep, enumList = LanguageRW.readLanguage(source, 8)
    
    return Definition(rootDep.getElement(), rootDep.getProperties(), rootDep.assertions, enumList)

def loadDefinitionDataScriptFile(filename):
    lexer = pyggy.lexer.lexer(DSLexer.lexspec)
    gram = pyggy.srgram.SRGram(DSParser.gramspec)
    parser = pyggy.glr.GLR(gram)    
    
    lexer.setinput(filename)    
    parser.setlexer(lexer)
    
    tree = parser.parse()
    root, enumList = proctree(tree, DSParser)
    
    return Definition(root, None, None, enumList)

def load_and_compile(filename):
    definition, _ = loadDefinitionFile(filename)
    return definition.getCompiledDefinition()


class Definition(object):
    def __init__(self, rootElement, rootProps = None, rootAssertions = None,
                 userEnumList = None):

        self.element = rootElement
        self.dep_root = Dependency()
        self.dep_root.setElement(self.element)        
        
        if rootProps is not None:
            for key, value in rootProps.items():
                self.dep_root.addProperty(key, value)
                
        if rootAssertions is not None:
            self.dep_root.addAssertions(rootAssertions)            
                
        self.userDefinedEnums = UserEnumsHolder()
        if userEnumList is not None:
            for enum in userEnumList:
                self.userDefinedEnums.addEnum(enum)

    ##
    # \brief "Compiles" defintion.
    # -# Construct parent links for and duplicate dependencies which
    # are in different branches. Why to duplicate dependencies? ->
    # structures are positioned on different places. Getters which
    # reference some values in the data tree will be therefor
    # different, the path to refferenced value will be different ->
    # two or more instances of getter are needed -> duplicated
    # dependencies.
    # -# Compile all ExprVEs.
    def getCompiledDefinition(self):
        definition = Definition(self.element)
        definition.dep_root = CompiledDependency(self.dep_root)
        definition.dep_root.compileExprs()
        return definition
