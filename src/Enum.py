## @package Enum
#  Enum class for user defined enum definition.

from Utility import xassert, hexstr_to_int
import re

__all__ = """\
Enum
""".split()

## Class for user's enum definition.
#  -# Enum is defined as list of tupple with pairs ("enumerator", value)  
class Enum(object):
    def __init__(self, enum_name, enum_list):        
        xassert(type(enum_name) == type(str()))
        xassert(type(enum_list) == type(list()))
        # private variables, manipulation possible from UserEnumsHolder class
        self.enum_name = enum_name
        self.enum_list = enum_list

    def getEnumerator(self, value):        
        for enumerator, val in self.enum_list:
            hex_regex = "^0{0,1}x[0-9a-fA-F]+$"
            if re.match(hex_regex, str(val)) is not None:
                val = hexstr_to_int(val)
            if val == value:
                return enumerator 
        return "UNDEFINED"

    def getName(self):
        return self.enum_name
    
    def getList(self):
        return self.enum_list
