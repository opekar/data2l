import os
import mmap
import platform
from struct import unpack, Struct
from Utility import pure_virtual, impossible


__all__ = """\
Reader
ReaderA
MmapReader
SafeReaderException
SafeReader
""".split()


## Reader interface.
class Reader(object):
    def seek(self, to, whence = os.SEEK_SET):
        pure_virtual()

    def tell(self):
        pure_virtual()

    def close(self):
        pure_virtual()

    def readString(self, n):
        pure_virtual()

    def readByte(self):
        pure_virtual()

    def readWord(self):
        pure_virtual()

    def readDWord(self):
        pure_virtual()

    def readFloat(self):
        pure_virtual()


class NullReader(Reader):
    def __init__(self, *args):
        self.offset = 0

    def seek(self, to, whence = os.SEEK_SET):
        if whence == os.SEEK_SET:
            self.offset = to
        elif whence == os.SEEK_CUR:
            self.offset = self.offset + to
        elif whence == os.SEEK_END:
            self.offset = 0xFFFFFFFFFFFFFFFF - to
        else:
            impossible()

    def tell(self):
        return self.offset

    def close(self):
        pass

    def readString(self, n):
        return ""

    def readByte(self):
        return 0

    def readWord(self):
        return 0

    def readDWord(self):
        return 0

    def readFloat(self):
        return 0.0


## Basic file reader.
class ReaderA(Reader):
 
    def __init__(self, filename):
        self.filename = filename
        self.file = open(filename, 'rb')
        self.offset = 0
        self.byteUnpack = Struct('<B')
        self.wordUnpack = Struct('<H')
        self.dwordUnpack = Struct('<I')    
        self.floatUnpack = Struct('<f')    


    def seek(self, to, whence = os.SEEK_SET):
        self.file.seek(to, whence)
        self.offset = self.file.tell()

    def tell(self):
        return self.offset

    def close(self):
        self.file.close()
        self.file = None
        self.filename = None
        self.offset = None

    def readString(self, n):
        binstr = self.file.read(n)
        self.offset += n
        return binstr

    def readByte(self):
        binstr = self.file.read(1)
        self.offset += 1
        value = self.byteUnpack.unpack(binstr)[0]
        return value

    def readWord(self):
        binstr = self.file.read(2)
        self.offset += 2
        value = self.wordUnpack.unpack(binstr)[0]
        return value

    def readDWord(self):
        binstr = self.file.read(4)
        self.offset += 4
        value = self.dwordUnpack.unpack( binstr)[0]
        return value

    def readFloat(self):
        binstr = self.file.read(4)
        self.offset += 4
        value = self.floatUnpack.unpack( binstr)[0]
        return value


## Memory mapping using reader.
class MmapReader(Reader):
    def __init__(self, filename):
        self.filename = filename
        flags = os.O_RDONLY
        if platform.system() == 'Windows':
            flags |= os.O_BINARY
        self.file = os.open(filename, flags)
        stat_data = os.fstat(self.file)
        self.size = stat_data.st_size
        self.buffer = mmap.mmap(self.file, self.size, access = mmap.ACCESS_READ)
        self.offset = 0
        self.byteUnpack = Struct('<B')
        self.wordUnpack = Struct('<H')
        self.dwordUnpack = Struct('<I')    
        self.floatUnpack = Struct('<f')

    def seek(self, to, whence = os.SEEK_SET):
        if whence == os.SEEK_SET:
            self.offset = to
        elif whence == os.SEEK_CUR:
            self.offset += to
        elif whence == os.SEEK_END:
            self.offset = self.size - to
        else:
            impossible()

    def tell(self):
        return self.offset

    def close(self):
        self.buffer.close()
        self.buffer = None
        os.close(file)
        self.file = None
        self.filename = None
        self.size = None
        self.offset = None

    def readString(self, n):
        offset = self.offset
        self.offset += n
        return self.buffer[offset:offset+n]

    def readByte(self):
        value = self.byteUnpack.unpack(self.buffer[self.offset:self.offset+1])[0]
        self.offset += 1
        return value

    def readWord(self):
        value = self.wordUnpack.unpack(self.buffer[self.offset:self.offset+2])[0]
        self.offset += 2
        return value

    def readDWord(self):
        value = self.dwordUnpack.unpack(self.buffer[self.offset:self.offset+4])[0]
        self.offset += 4
        return value

    def readFloat(self):
        value = self.floatUnpack.unpack(self.buffer[self.offset:self.offset+4])[0]
        self.offset += 4
        return value


## Exception thrown on call to pure virtual function.
class SafeReaderException(Exception):
    def __init__(self, reason):
        Exception.__init__(self, "SafeReaderException : " + reason  )


## Wrapper around readers that checks for out of bounds read attempts.
class SafeReader(Reader):
    def __init__(self, reader, throw_exceptions = False):
        self.reader = reader
        self.reader.seek(0, os.SEEK_END) # from end of file
        self.file_size  = self.reader.tell()
        self.reader.seek(0) # back to start
        ## Flag that is true if the reader encountered error.
        self.error = False
        ## Flag indicating whether setError should throw an exception.
        self.throw_exceptions = throw_exceptions

    def setError(self, reason):
        self.error = True
        if self.throw_exceptions:
            raise SafeReaderException(reason)
        else:
            return

    def clearError(self):
        self.error = False

    def getErrorFlag(self):
        return self.error

    def readCheck(self, bytes_to_read):
        if self.reader.tell() + bytes_to_read > self.file_size:
            self.setError("Read error: trying to read 0x%x bytes from offset 0x%x while the file size is 0x%x" %
                          (bytes_to_read,self.reader.tell(),self.file_size))
            return False
        return True

    def seek(self, to, whence = os.SEEK_SET):
        if whence == os.SEEK_SET or whence == os.SEEK_END :
            if to>self.file_size or to<0:
                self.setError("Seek error: trying to seek to offset 0x%x while the file size is 0x%x" %
                                (to,self.file_size))
                return
        else:
            offs = self.reader.tell() + to
            if offs>self.file_size or offs<0:
                self.setError("Seek error: trying to seek to offset 0x%x while the file size is %x" %
                                (offs,self.file_size))
                return
        self.reader.seek(to, whence)

    def tell(self):
        return self.reader.tell()

    def close(self):
        self.reader.close()
        self.reader = None
        self.file_size = None
        self.error = None

    def readString(self, n):
        if not self.readCheck(n): return 'e'
        a = self.reader.readString(n)
        return a

    def readByte(self):
        if not self.readCheck(1): return 0
        return self.reader.readByte()

    def readWord(self):
        if not self.readCheck(2): return 0
        return self.reader.readWord()

    def readDWord(self):
        if not self.readCheck(4): return 0
        return self.reader.readDWord()

    def readFloat(self):
        if not self.readCheck(4): return 0
        return self.reader.readFloat()

