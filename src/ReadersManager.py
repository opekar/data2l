import Reader
import os


class IReaderCreator(object):
    
    def createReader(self, filename):
        pure_virtual()
    
    def getName(self):
        pure_virtual()
        
        
class ReadersManager(object):
    def __init__(self):
        self.creators = {}
        self.module_counter = 0
    
    
    def get(self, readerCreatorName):
        return self.creators[readerCreatorName]
    
    ##
    # todo unify with CustGeneratorManager
    def scan(self,path):
        try:
            files = os.listdir(path)
            for f in files:
                if f.find(".py") !=-1 and  f.find(".py") +3 == len(f):
                    self.loadGenerator(os.path.join(path, f))
        except Exception, e:
            return

    def loadGenerator(self, filename):
        import imp
        try:
            module = None
            module_name = "_dataviz_custom_reader_" + str(self.module_counter)
            self.module_counter += 1
            module = imp.load_source(module_name, filename)
        except Exception, e:
            print("Specified path is not a Python source file!\n"
                           + str(e))
            return

        if "readerCreator" not in module.__dict__:
            print("File loaded but `readerCreator' was not found.")
            return

        g=module.__dict__["readerCreator"]

        if not isinstance(g,IReaderCreator) :
            print("`generator' is not inherited from IReaderCreator interface!")
            return

        self.creators[g.getName()] = g
      