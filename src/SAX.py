## This is implementation of SAX parser interface on top of existing DOM
# interface.

import DOM
import DataTypes as DT
from Utility import xassert_type


__all__ = """\
SAX_RETURN
SAX_CONTINUE
SAX_NEXTINDEX
SAX_DONTDESCEND
SAXHandler
SAXParser
""".split()


SAX_RETURN = 1
SAX_CONTINUE = 2
SAX_NEXTINDEX = 3
SAX_DONTDESCEND = 4


## Base class for all clients of SAXParser
class SAXHandler(object):
    def __init__(self):
        pass

    ## This method is called before SAX parser starts diving deeper
    # along the DOM tree.
    def beginElement(self, dom_element):
        xassert_type(dom_element, DOM.DOMElement)

    ## This method is called on return from the depths of the DOM tree.
    def endElement(self, dom_element):
        xassert_type(dom_element, DOM.DOMElement)


## Implementation of SAX parser.
# All of the Visit* and _visit* methods should have parameter named "dom".
# The name is expected by Errors.format_sax_trace() function which uses it
# to implement usable stack traces of DOM objects.
class SAXParser(DOM.DOMVisitor):
    def __init__(self, top_dom, skip_simple = True):
        xassert_type(top_dom, DOM.DOMElement)

        ## Client SAXHandler.
        self.saxHandler = None
        ## DOMElement of starting point.
        self.top_dom = top_dom
        ## When true, parser does not call handler for simple type DOMs.
        self.skip_simple = skip_simple


    ## Starts parsing with given SAXHandler.
    def parse(self, saxHandler):
        xassert_type(saxHandler, SAXHandler)
        self.saxHandler = saxHandler
        self.ret_vals = (None, None)
        self.top_dom.visit(self)
        self.saxHandler = None

    def VisitDOMVoid(self, dom):
        pass

    def VisitDOMBasic(self, dom):
        if not self.skip_simple:
            self.saxHandler.beginElement(dom)
            self.saxHandler.endElement(dom)

    def VisitDOMStruct(self, dom):
        if self.saxHandler.beginElement(dom) not in [None, SAX_CONTINUE]:
            return
        for part in dom.getValue():
            part.visit(self)
        self.saxHandler.endElement(dom)


    def VisitDOMArray(self, dom):
        if self.saxHandler.beginElement(dom) not in [None, SAX_CONTINUE]:
            return
        
        aggs = dom.getDataType().getAggregates()
        if ((len(aggs) == 1 and self.skip_simple and isinstance(aggs[0], DT.SimpleElement))):
            return
            
        # Loop over all indexes of array.
        for i in xrange(dom.getSize()):
            dom.loadIndex(i)
            # array can have more substructures same way structure can.
            for part in dom.getValue():
                part.visit(self)
        self.saxHandler.endElement(dom)


    def VisitDOMAlternative(self, dom):
        if self.saxHandler.beginElement(dom) not in [None, SAX_CONTINUE]:
            return
        alt = dom.getValue()
        alt.visit(self)
        self.saxHandler.endElement(dom)

