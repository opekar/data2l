from Utility import pure_virtual, xassert


__all__ = """\
Traversable
Path
Walker
Root
Up
Down
""".split()


## This is interface for classes that can be traversed.
class Traversable(object):
    ## \returns  traversable parent
    def Up(self):
        pure_virtual()

    ## \returns traversable child
    def Down(self, index):
        pure_virtual()


class Path(object):
    pass


## This is interface for classes that implement ability to travel along some path.
class Walker(object):
    def step(self, traversable):
        pure_virtual()


class Root(Walker):
    def __str__(self):
        return "Root"

    def step(self, traversable):
        xassert(traversable is not None)
        act = traversable
        while act.Up() is not None:
            act = act.Up()
        return act


class Up(Walker):
    def __str__(self):
        return "Up"

    def step(self, traversable):
        return traversable.Up()


class Down(Walker):
    def __init__(self, index):
        self.index = index

    def __str__(self):
        return "Down("+str(self.index)+")"

    def step(self, traversable):
        xassert(isinstance(traversable, Traversable))
        return traversable.Down(self.index)
