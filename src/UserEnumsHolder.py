## @package UserEnumsHolder
#  Holder for user defined enums, used for managing them and assure consistency.

all = """\
    UserEnumsHolder
""".split()

## Storage for user defined Enum types for current definition. 
class UserEnumsHolder:
    def __init__(self):
        self.enums_list = []
    
    def addEnum(self, enum):
        if self.getEnum(enum.getName()) is not None:
            return False
        self.enums_list.append(enum)
        return True

    ## remove instance of enum
    def removeEnum(self, enum):
        if not enum in self.enums_list:
            return False
                
        self.enums_list.remove(enum)
        return True
        
    def renameEnum(self, enum, new_enum_name):                
        if self.getEnum(new_enum_name) is not None:
            return False
        old_enum_name = enum.getName()
        enum.enum_name = new_enum_name
        return True

    ## sets new enum definition to enum object
    def setListToEnum(self, enum, new_enum_list):        
        enum.enum_list = new_enum_list
        
    ## return instance of enum for given name
    def getEnum(self, enum_name):
        for enum in self.enums_list:
            if enum.getName() == enum_name:
                return enum
        # required enum doesn't exist
        return None
        
    ## return list of defined enum types
    def getEnumList(self):
        return self.enums_list
