import sys
import os
import traceback
import re


__all__ = """\
PureVirtual
Impossible
pure_virtual
impossible
xassert
xassert_type
xraise
unexpected
breakpoint
caller
print_trace
deep_recursion
hexstr_to_int
validate_ident
valid_identifier_regex
""".split()


## Exception thrown on call to pure virtual function.
class PureVirtual(Exception):
    def __init__(self):
        Exception.__init__(self, "Pure virtual function called.")


## Exception thrown on reaching code paths that should be impossible to reach.
class Impossible(Exception):
    def __init__(self):
        Exception.__init__(self, "Impossible happened.")


## This function should be used to mark member function pure virtual.
def pure_virtual():
    raise PureVirtual()


## This function marks a place that should never be reached.
def impossible():
    raise Impossible()


## Same as built in assert. Provides a convenient place where one can insert
# his breakpoint.
def xassert(cond, msg = ""):
    if not bool(cond):
        raise AssertionError(msg)
    else:
        return


## Convenience wrapper for xassert(isinstance(var, type)).
def xassert_type(var, t, msg = ""):
    if not isinstance(var, t) or var is None:
        raise AssertionError(msg)
    else:
        return


## Helper function so that we can put a breakpoint on exception.
def xraise(ex):
    raise ex


## Marks conditions that are unusual, like high offsets,
# but not necessarily wrong.
def unexpected(cond):
    if bool(cond):
        msg = None
        try:
            # Get two top stack frames.
            frames = traceback.extract_stack(limit = 2)
            xassert(len(frames) >= 2)
            # Skip the frame of this function and format the single entry.
            msg = traceback.format_list([frames[0]])[0]
        finally:
            del frames
        # Print a notice.
        print >> sys.stderr, "Unexpected at", msg
        return
    else:
        return


## Helper function for setting breakpoints with condition. Because PyDev
# cannot do that, yet.
def breakpoint(cond = True):
    if bool(cond):
        return
    else:
        return


def caller():
    # Get three top stack frames.
    result = None
    try:
        frames = traceback.extract_stack(limit = 3)
        xassert(len(frames) >= 3)
        result = frames[0]
    finally:
        del frames
    # [0] == the caller of our caller, [1] == function that invoked caller(),
    # [2] == this function
    return result


## Prints location of the call
def print_trace(msg = None, stream = None):
    try:
        frames = traceback.extract_stack(limit = 2)
        xassert(len(frames) >= 2)
        filename, line, func, _ = frames[0]
        filename = os.path.basename(filename)
        if msg is None:
            msg = ""
        else:
            msg = ": " + msg
        if stream is None:
            stream = sys.stdout
        print >> stream, filename + ":" + str(line)+ ":" + func + "()" + msg
    finally:
        del frames
    return


def deep_recursion(levels = 1000):
    stack = None
    try:
        stack = traceback.extract_stack(limit = levels)
        if len(stack) == levels:
            dummy = 0
    finally:
        del stack
    return


def hexstr_to_int(s):
    # in a case of 0x...
    if (len(s) > 2):
        if s[0] == '0' and s[1].upper() == 'X':
            s = s[2:]    
    
    acc = 0
    for ch in s.upper():
        if '0' <= ch and ch <= '9':
            acc = (acc << 4) + ord(ch) - ord('0')
        elif 'A' <= ch and ch <= 'F':
            acc = (acc << 4) + ord(ch) - ord('A') + 10
        else:
            acc = -1
    return acc

# used on diferent places (please grep) and so it's here as constant
valid_identifier_regex = '^[a-zA-Z_][a-zA-Z_0-9]*$'

def validate_ident(name):
    identRE = re.compile(valid_identifier_regex)
    if identRE.match(name) is None:
        return False
    return True
