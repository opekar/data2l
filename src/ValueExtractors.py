from Utility import pure_virtual, xassert, xassert_type


class IQueryDOMValue(object):
    def getDOMValue(self, object):
        pure_virtual()


class IQueryDOM(object):
    def getDOM(self, object):
        pure_virtual()


class ValueGetter(IQueryDOMValue):
    def walk(self, callback):
        pure_virtual()


class Const(ValueGetter):
    def __init__(self, const):
        self.const = const

    def __str__(self):
        return "Const(" + str(self.const) + ")"

    def getAbsoluteGetter(self, dom):
        return self

    def getDOMValue(self, _):
        return self.const

    def walk(self, callback):
        callback(self, [])


#TODO rename AccessArrayIndex
class ReadArrayIndex(ValueGetter, IQueryDOM):
    """ Gives a value which is present on specific index
    """
    def __init__(self, array_extr, index_extr, tail_extr):
        self.array_extr = array_extr
        self.index_extr = index_extr
        self.tail_extr = tail_extr
        self.dom_of_origin = None
        self.original_dom = None
        self.clone = None

    def getDOM(self, dom):
        if self.dom_of_origin is None:
            self.dom_of_origin = dom
        # Get target DOM
        target = self.array_extr.getDOM(dom)
        index = self.index_extr.getDOMValue(self.dom_of_origin)
        #todo temporary hack -returns something (array_el) which claims to be array
        # with a specific state
        val = target.getElementAt(index)

        ##if we have no tail return the actual value
        if self.tail_extr is None:
            return val

        if isinstance(self.tail_extr, ReadArrayIndex):
            # Pass down the context for index getters.
            self.tail_extr.set_origin(self.dom_of_origin)

        tail = self.tail_extr.getDOM(val)
        self.dom_of_origin = None
        return tail
    
    def getTargetType(self, dep):
        target = self.array_extr.getDOM(dep)
        if self.tail_extr is None:
            return target

        if isinstance(self.tail_extr, ReadArrayIndex):
            return self.tail_extr.getTargetType(target)

        tail = self.tail_extr.getDOM(target)
        return tail

    def getDOMValue(self, dom):
        target_dom = self.getDOM(dom)
        value = target_dom.getValue()
        return value

    def set_origin(self, dom):
        self.dom_of_origin = dom

    def set_array_extr(self, array_extr):
        self.array_extr = array_extr

    def walk(self, callback):
        callback(self, [self.array_extr, self.index_extr, self.tail_extr])


class OffsetOf(ValueGetter):
    def __init__(self, target_getter):
        self.target_getter = target_getter

    def getDOMValue(self, dom):
        target = self.target_getter.getDOM(dom)
        return target.akt_offs

    def walk(self, callback):
        callback(self, [self.target_getter])


## This class is value getter for size of array in \em elements.
class SizeOf(ValueGetter):
    def __init__(self, target_getter):
        self.target_getter = target_getter

    def getDOMValue(self, dom):
        target = self.target_getter.getDOM(dom)
        return target.getDependency().getSizeOf()

    def walk(self, callback):
        callback(self, [self.target_getter])


class BinSizeOf(ValueGetter):
    def __init__(self, target_getter):
        self.target_getter = target_getter
        
    def getDOMValue(self, dom):
        target = self.target_getter.getDOM(dom)
        return target.getByteSize()

    def walk(self, callback):
        callback(self, [self.target_getter])

class ArraySizeOf(ValueGetter):
    def __init__(self, target_getter):
        self.target_getter = target_getter

    def getDOMValue(self, dom):
        target = self.target_getter.getDOM(dom)
        return target.array_size

    def walk(self, callback):
        callback(self, [self.target_getter])


class ArrayActIndexOf(ValueGetter):
    def __init__(self, target_getter):
        self.target_getter = target_getter

    def getDOMValue(self, dom):
        target = self.target_getter.getDOM(dom)
        idx = target.getCurrentElement()
        xassert(idx >= 0)
        return idx

    def walk(self, callback):
        callback(self, [self.target_getter])


class OwnIndexOf(ValueGetter):
    def __init__(self, target_getter):
        self.target_getter = target_getter

    def getDOMValue(self, dom):
        target = self.target_getter.getDOM(dom)
        own_index = target.ownIndex()
        return own_index

    def walk(self, callback):
        callback(self, [self.target_getter])


class LambdaAdapt(ValueGetter):
    def __init__(self, getter_obj, lambda_func):
        self.lambda_func = lambda_func
        self.getter_obj = getter_obj

    def getAbsoluteGetter(self, dom):
        if isinstance(self.getter_obj, RelativeValue):
            getter_obj = self.getter_obj.getAbsoluteGetter(dom)
        else:
            getter_obj = self.getter_obj

        return LambdaAdapt(getter_obj,self.lambda_func)

    def getDOMValue(self, dom):
        return self.lambda_func(self.getter_obj.getDOMValue(dom))

    def walk(self, callback):
        callback(self, [self.getter_obj])


class UnaryOp(ValueGetter):
    def __init__(self, getter_obj, lambda_func, op_string):
        self.getter_obj = getter_obj
        self.lambda_func = lambda_func
        self.op_string = op_string

    def getDOMValue(self, dom):
        return self.lambda_func(self.getter_obj.getDOMValue(dom))

    def walk(self, callback):
        callback(self, [self.getter_obj])

    def getOperatorStr(self):
        return self.op_string


## Helper function for creating unary operator classes.
def _unop(name, op, real_op = None):
    from new import classobj
    if real_op is None:
        real_op = op
    func = eval("lambda x: %s x" % real_op)
    init = lambda it, g1: UnaryOp.__init__(it, g1, func, op)
    klass = classobj(name, (UnaryOp,), {'__init__': init})
    globals()[name] = klass

_unop("OpUnMinus", "-")
_unop("OpCompl",   "~")
_unop("OpNot",     "!", "not")

del _unop


## Base class for all binary operator getters declared through _binop().
class BinaryOp(ValueGetter):
    def __init__(self, getter_obj1, getter_obj2, lambda_func , op_string):
        self.getter_obj1 = getter_obj1
        self.getter_obj2 = getter_obj2
        self.lambda_func = lambda_func
        self.op_string = op_string

    def getDOMValue(self, dom):
        return self.lambda_func(self.getter_obj1.getDOMValue(dom),
                                self.getter_obj2.getDOMValue(dom))

    def walk(self, callback):
        callback(self, [self.getter_obj1, self.getter_obj2])

    def getOperatorStr(self):
        return self.op_string


## Helper function for creating binary operator classes.
def _binop(name, op, real_op = None):
    from new import classobj
    if real_op is None:
        real_op = op
    func = eval("lambda x, y: x %s y" % real_op)
    init = lambda it, g1, g2: BinaryOp.__init__(it, g1, g2, func, op)
    klass = classobj(name, (BinaryOp,), {'__init__': init})
    globals()[name] = klass

_binop("OpPlus",   "+")
_binop("OpMinus",  "-")
_binop("OpMult",   "*")
_binop("OpDiv",    "/")
_binop("OpAnd",    "&")
_binop("OpRShift", ">>")
_binop("OpLShift", "<<")

_binop("OpEq",   "==")
_binop("OpNe",   "!=")
_binop("OpLt",   "<")
_binop("OpLe",   "<=")
_binop("OpGt",   ">")
_binop("OpGe",   ">=")
_binop("OpLAnd", "&&", "and")
_binop("OpLOr",  "||", "or")

del _binop


class LambdaAdapt2(ValueGetter):
    def __init__(self, len_obj1, len_obj2, lambda_func):
        self.lambda_func = lambda_func
        self.len_obj1 = len_obj1
        self.len_obj2 = len_obj2

    def getAbsoluteGetter(self,dom):
        if isinstance(self.len_obj1, RelativeValue):
            len_obj1 = self.len_obj1.getAbsoluteGetter(dom)
        else:
            len_obj1 = self.len_obj1

        if isinstance(self.len_obj2, RelativeValue):
            len_obj2 = self.len_obj2.getAbsoluteGetter(dom)
        else:
            len_obj2 = self.len_obj2

        return LambdaAdapt2(len_obj1, len_obj2, self.lambda_func)

    def getDOMValue(self, dom):
        a = self.len_obj1.getDOMValue(dom)
        xassert(a is not None)
        b = self.len_obj2.getDOMValue(dom)
        xassert(b is not None)
        val = self.lambda_func(a, b)
        return val

    def walk(self, callback):
        callback(self, [self.len_obj1, self.len_obj2])


class IfThenElse(ValueGetter, IQueryDOM):
    def __init__(self, cond_getter, then_getter, else_getter):
        xassert_type(cond_getter, ValueGetter)
        xassert_type(then_getter, ValueGetter)
        xassert_type(else_getter, ValueGetter)
        self.cond_getter = cond_getter
        self.then_getter = then_getter
        self.else_getter = else_getter

    def getDOMValue(self, dom):
        cond_result = self.cond_getter.getDOMValue(dom)
        cond_result = bool(cond_result)
        value = None
        if cond_result:
            #print "getDOMValue: then("+str(self.cond_getter)+")"
            value = self.then_getter.getDOMValue(dom)
        else:
            #print "getDOMValue: else("+str(self.cond_getter)+")"
            value = self.else_getter.getDOMValue(dom)
        return value

    def getDOM(self, dom):
        cond_result = self.cond_getter.getDOMValue(dom)
        cond_result = bool(cond_result)
        value = None
        if cond_result:
            #print "getDOM: then"
            value = self.then_getter.getDOM(dom)
        else:
            #print "getDOM: else"
            value = self.else_getter.getDOM(dom)
        return value

    def walk(self, callback):
        callback(self, [self.cond_getter, self.then_getter, self.else_getter])


class StrIndex(ValueGetter):
    def __init__(self, s, subs):
        xassert_type(s, ValueGetter)
        xassert_type(subs, ValueGetter)
        self.string = s
        self.sub_string = subs

    def getDOMValue(self, dom):
        s = self.string.getDOMValue(dom)
        subs = self.sub_string.getDOMValue(dom)
        ret = s.find(subs)
        return ret

    def walk(self, callback):
        callback(self, [self.string, self.sub_string])
        
        
class StopParseException:
    pass

class StopParse(ValueGetter):
    def __init__(self):
        pass
    
    def getDOMValue(self, dom):
        raise StopParseException()
        return self

    def walk(self, callback):
        callback(self, [])

class AbsolutValue(ValueGetter):
    def __init__(self, object):
        self.object = object

    def getAbsoluteGetter(self, dom):
        return self

    def getDOMValue(self, dom):
        return self.object.value

    def walk(self, callback):
        callback(self, [])


class RelativeValue(ValueGetter, IQueryDOM):
    def __init__(self, path):
        self.path = path

    def __str__(self):
        return "RelativeValue("+str(self.path)+")"

    def getAbsoluteGetter(self, dom):
        act_dom = dom
        for i in self.path:
            act_dom = i.step(act_dom)
        return AbsolutValue(act_dom)

    def getDOMValue(self, start):
        dom = self.getDOM(start)
        xassert(dom is not None)
        return dom.getValue()

    def getDOM(self, start):
        act_dom = start
        for i, step in enumerate(self.path):
             act_dom = step.step(act_dom)
        return act_dom

    def walk(self, callback):
        callback(self, [])
