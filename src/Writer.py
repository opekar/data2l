import os
import codecs
from struct import pack
from Utility import pure_virtual


class Writer(object):
    def seek(self, to, whence = os.SEEK_SET):
        pure_virtual()

    def tell(self):
        pure_virtual()

    def close(self):
        pure_virtual()

    def writeString(self, s):
        pure_virtual()

    def writeByte(self, b):
        pure_virtual()

    def writeWord(self, w):
        pure_virtual()

    def writeDWord(self, dw):
        pure_virtual()

    def writeFloat(self, dw):
        pure_virtual()

    def writeBin(self, data, l):
        pure_virtual()


class WriterA(Writer):
    def __init__(self, filename):
        self.filename = filename
        self.file = open(filename, 'wb+')
        self.offset = 0

    def get_file(self):
        return self.file

    def tell(self):
        return self.file.tell()

    def seek(self, to, whence = os.SEEK_SET):
        self.file.seek(to, whence)
        self.offset = self.file.tell()

    def close(self):
        self.file.close()
        self.file = None
        self.filename = None
        self.offset = None

    ## The result of this calle depends on internal representation of the
    # given string. Users of this function should use encode() method or the
    # string object or codecs.encode() function to get whatever represenation
    # they want.
    def writeString(self, s):
        buf = buffer(s)
        self.file.write(buf)

    def writeByte(self, x):
        buf = pack("B", x)
        self.file.write(buf)

    def writeWord(self, x):
        buf = pack("H", x)
        self.file.write(buf)

    def writeDWord(self, x):
        buf = pack("I", x)
        self.file.write(buf)

    def writeFloat(self, x):
        buf = pack("f", x)
        self.file.write(buf)
