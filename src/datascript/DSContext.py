from BasicTypes import *
from DataTypes import *
from Dependency import Dependency
from Utility import xassert

#TODO: unify type_name to type conversion, redundancy is e.g in 
#      DependencyTree._makeSimpleDependency, look for all redundancies 
def getTypeFromName(typename):
    typename = typename.lower()
    if typename == "byte":
        return Byte()
    elif typename == "word":
        return Word()
    elif typename == "dword":
        return DWord()
    else:
        #TODO: create transformation for all DataTypes!
        return None

class DSContext(object):
    typeMap = {
       'uint8' : 'Byte', 
       'uint16' : 'Word',
       'uint32' : 'DWord',
    }
    
    def __init__(self, verbose = False):
        self.root = Struct('DSRoot')
        self.enums = [] 
        self.verbose = verbose
    
    def printInfo(self, info):
        if self.verbose:
            print info
    
    def getD2lType(self, type):
        return DSContext.typeMap.get(type, type)        
        
    ## Return root of translated definition.
    def getRoot(self):        
        return self.root
        
    ## Return enums defined in DataScript
    def getEnums(self):
        return self.enums
        
    ## Add top level element to root.
    def addToRoot(self, element):        
        self.root.addField(element, Dependency())        
    
    ## takes name of Struct and as second param list of tupples (type, name) which
    #  determines it's elements.
    def createStruct(self, ID, elements):                
        struct = Struct(ID)
        for el in elements:            
            struct.addField(el, Dependency())        
        self.printInfo(("Struct", ID, "created. Elements:", elements))
        return struct
        
    def createBasic(self, type, name):
        type = self.getD2lType(type)
        self.printInfo(("Basic", type, name, "created."))        
        return Basic(name, getTypeFromName(type))
        
    def createBasicAlternative(self, type, name):
        type = self.getD2lType(type)        
        xassert(0) # TODO: implement this
        