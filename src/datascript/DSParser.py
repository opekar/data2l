
# This file was generated automatically

# action 0 for: [(3) DataScript -> CompositeDefinition], [(4) DataScript -> PackageDefinition], [(7) PackageDefinition -> ERR], [(15) UnionElement -> TypeDefinition MINUS COLON Constraint SEMICOLON], [(16) TypeDefinition -> TYPESPEC IDENT], [(19) Constraint -> IDENT ConstraintOperator NUMBER], [(20) ConstraintOperator -> EQ], [(21) ConstraintOperator -> LT], [(22) ConstraintOperator -> LE], [(23) ConstraintOperator -> GT], [(24) ConstraintOperator -> GE], [(25) ConstraintOperator -> NE]
def action0(kids) :
	return kids

# action 1 for: [(0) Start.clos1 ->]
def action1(kids) :
	return []

# action 2 for: [(1) Start.clos1 -> Start.clos1 DataScript]
def action2(kids) :
	kids[0].append(kids[1])
	return kids[0]

# action 3 for: [(2) Start -> Start.clos1]
def action3(kids) :
	return context.getRoot(), context.getEnums() # return root for parsed definition
	

# action 4 for: [(5) CompositeDefinition -> Sequence]
def action4(kids) :
	context.addToRoot(kids[0])                            
	

# action 5 for: [(6) CompositeDefinition -> Union]
def action5(kids) :
	context.addToRoot(kids[0])
	

# action 6 for: [(8) Sequence.clos2 ->]
def action6(kids) :
	return []

# action 7 for: [(9) Sequence.clos2 -> Sequence.clos2 Element]
def action7(kids) :
	kids[0].append(kids[1])
	return kids[0]

# action 8 for: [(10) Sequence -> IDENT LBRACE Sequence.clos2 RBRACE]
def action8(kids) :
	return context.createStruct(kids[0], kids[2])			
	

# action 9 for: [(11) Union.clos3 ->]
def action9(kids) :
	return []

# action 10 for: [(12) Union.clos3 -> Union.clos3 UnionElement]
def action10(kids) :
	kids[0].append(kids[1])
	return kids[0]

# action 11 for: [(13) Union -> UNION IDENT LBRACE Union.clos3 RBRACE]
def action11(kids) :
	    	       #context.createUnion(kids[1], kids[3])
	return kids[0], kids[1], kids[3]
	

# action 12 for: [(14) Element -> TypeDefinition OptionalConstraint SEMICOLON]
def action12(kids) :
	if kids[1] is None:    	   	
	        return context.createBasic(kids[0][0], kids[0][1])
	else:
	        return context.createBasicAlternative(kids[0][0], kids[0][1])
	

# action 13 for: [(17) OptionalConstraint -> IF Constraint]
def action13(kids) :
	return kids[1]
	

# action 14 for: [(18) OptionalConstraint ->]
def action14(kids) :
	return None
	
goto = {(26, 24): 36, (1, 3): 6, (25, 'MINUS'): 29, (26, 23): 36, (13, 'TYPESPEC'): 16, (26, 'LE'): 33, (19, 15): 24, (1, 'IDENT'): 4, (1, 'ERR'): 3, (4, 'LBRACE'): 11, (21, 'IDENT'): 26, (12, 'LBRACE'): 14, (5, 'IDENT'): 12, (19, 16): 25, (18, 18): 22, (26, 20): 36, (14, 12): 19, (13, 'RBRACE'): 15, (13, 16): 18, (19, 'RBRACE'): 23, (1, 5): 7, (1, 'UNION'): 5, (26, 25): 36, (22, 'SEMICOLON'): 28, (1, 10): 9, (26, 'NE'): 35, (26, 'GE'): 31, (21, 19): 27, (14, 11): 19, (26, 21): 36, (0, 0): 1, (1, 13): 10, (29, 'COLON'): 37, (11, 9): 13, (26, 'EQ'): 30, (1, 4): 6, (36, 'NUMBER'): 38, (39, 'SEMICOLON'): 40, (18, 17): 22, (37, 19): 39, (0, 1): 1, (11, 8): 13, (16, 'IDENT'): 20, (26, 22): 36, (26, 'GT'): 32, (13, 14): 17, (19, 'TYPESPEC'): 16, (37, 'IDENT'): 26, (1, 7): 8, (18, 'IF'): 21, (1, 6): 7, (26, 'LT'): 34, (0, 2): 2}
action = {(40, 'TYPESPEC'): [('reduce', ('UnionElement', 5, 15))], (17, 'RBRACE'): [('reduce', ('Sequence.clos2', 2, 9))], (6, 'ERR'): [('reduce', ('Start.clos1', 2, 1))], (10, 'ERR'): [('reduce', ('CompositeDefinition', 1, 6))], (26, 'LE'): [('shift', 33)], (20, 'SEMICOLON'): [('reduce', ('TypeDefinition', 2, 16))], (0, 'UNION'): [('reduce', ('Start.clos1', 0, 0))], (28, 'TYPESPEC'): [('reduce', ('Element', 3, 14))], (12, 'LBRACE'): [('shift', 14)], (5, 'IDENT'): [('shift', 12)], (8, 'IDENT'): [('reduce', ('DataScript', 1, 4))], (1, '$EOF$'): [('reduce', ('Start', 1, 2))], (18, 'SEMICOLON'): [('reduce', ('OptionalConstraint', 0, 18))], (17, 'TYPESPEC'): [('reduce', ('Sequence.clos2', 2, 9))], (3, 'ERR'): [('reduce', ('PackageDefinition', 1, 7))], (15, 'IDENT'): [('reduce', ('Sequence', 4, 10))], (8, 'ERR'): [('reduce', ('DataScript', 1, 4))], (26, 'NE'): [('shift', 35)], (7, 'UNION'): [('reduce', ('DataScript', 1, 3))], (14, 'TYPESPEC'): [('reduce', ('Union.clos3', 0, 11))], (7, '$EOF$'): [('reduce', ('DataScript', 1, 3))], (0, '$EOF$'): [('reduce', ('Start.clos1', 0, 0))], (23, 'IDENT'): [('reduce', ('Union', 5, 13))], (6, '$EOF$'): [('reduce', ('Start.clos1', 2, 1))], (10, 'UNION'): [('reduce', ('CompositeDefinition', 1, 6))], (33, 'NUMBER'): [('reduce', ('ConstraintOperator', 1, 22))], (35, 'NUMBER'): [('reduce', ('ConstraintOperator', 1, 25))], (26, 'GT'): [('shift', 32)], (18, 'IF'): [('shift', 21)], (23, '$EOF$'): [('reduce', ('Union', 5, 13))], (9, '$EOF$'): [('reduce', ('CompositeDefinition', 1, 5))], (32, 'NUMBER'): [('reduce', ('ConstraintOperator', 1, 23))], (1, 'ERR'): [('shift', 3)], (30, 'NUMBER'): [('reduce', ('ConstraintOperator', 1, 20))], (3, '$EOF$'): [('reduce', ('PackageDefinition', 1, 7))], (13, 'RBRACE'): [('shift', 15)], (19, 'RBRACE'): [('shift', 23)], (1, 'UNION'): [('shift', 5)], (22, 'SEMICOLON'): [('shift', 28)], (23, 'UNION'): [('reduce', ('Union', 5, 13))], (26, 'EQ'): [('shift', 30)], (34, 'NUMBER'): [('reduce', ('ConstraintOperator', 1, 21))], (0, 'IDENT'): [('reduce', ('Start.clos1', 0, 0))], (11, 'TYPESPEC'): [('reduce', ('Sequence.clos2', 0, 8))], (10, 'IDENT'): [('reduce', ('CompositeDefinition', 1, 6))], (23, 'ERR'): [('reduce', ('Union', 5, 13))], (0, 'ERR'): [('reduce', ('Start.clos1', 0, 0))], (15, 'ERR'): [('reduce', ('Sequence', 4, 10))], (13, 'TYPESPEC'): [('shift', 16)], (20, 'IF'): [('reduce', ('TypeDefinition', 2, 16))], (9, 'IDENT'): [('reduce', ('CompositeDefinition', 1, 5))], (2, '$EOF$'): [('accept', None)], (8, 'UNION'): [('reduce', ('DataScript', 1, 4))], (26, 'LT'): [('shift', 34)], (25, 'MINUS'): [('shift', 29)], (38, 'SEMICOLON'): [('reduce', ('Constraint', 3, 19))], (29, 'COLON'): [('shift', 37)], (15, 'UNION'): [('reduce', ('Sequence', 4, 10))], (15, '$EOF$'): [('reduce', ('Sequence', 4, 10))], (28, 'RBRACE'): [('reduce', ('Element', 3, 14))], (24, 'TYPESPEC'): [('reduce', ('Union.clos3', 2, 12))], (3, 'IDENT'): [('reduce', ('PackageDefinition', 1, 7))], (14, 'RBRACE'): [('reduce', ('Union.clos3', 0, 11))], (11, 'RBRACE'): [('reduce', ('Sequence.clos2', 0, 8))], (21, 'IDENT'): [('shift', 26)], (10, '$EOF$'): [('reduce', ('CompositeDefinition', 1, 6))], (31, 'NUMBER'): [('reduce', ('ConstraintOperator', 1, 24))], (39, 'SEMICOLON'): [('shift', 40)], (20, 'MINUS'): [('reduce', ('TypeDefinition', 2, 16))], (19, 'TYPESPEC'): [('shift', 16)], (4, 'LBRACE'): [('shift', 11)], (26, 'GE'): [('shift', 31)], (6, 'UNION'): [('reduce', ('Start.clos1', 2, 1))], (7, 'ERR'): [('reduce', ('DataScript', 1, 3))], (3, 'UNION'): [('reduce', ('PackageDefinition', 1, 7))], (8, '$EOF$'): [('reduce', ('DataScript', 1, 4))], (37, 'IDENT'): [('shift', 26)], (7, 'IDENT'): [('reduce', ('DataScript', 1, 3))], (1, 'IDENT'): [('shift', 4)], (9, 'UNION'): [('reduce', ('CompositeDefinition', 1, 5))], (36, 'NUMBER'): [('shift', 38)], (9, 'ERR'): [('reduce', ('CompositeDefinition', 1, 5))], (16, 'IDENT'): [('shift', 20)], (24, 'RBRACE'): [('reduce', ('Union.clos3', 2, 12))], (27, 'SEMICOLON'): [('reduce', ('OptionalConstraint', 2, 17))], (6, 'IDENT'): [('reduce', ('Start.clos1', 2, 1))], (40, 'RBRACE'): [('reduce', ('UnionElement', 5, 15))]}
semactions = [action1, action2, action3, action0, action0, action4, action5, action0, action6, action7, action8, action9, action10, action11, action12, action0, action0, action13, action14, action0, action0, action0, action0, action0, action0, action0]
gramspec = (goto, action, semactions)

import DSContext
    
context = DSContext.DSContext(verbose = True)


