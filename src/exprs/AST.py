## Classes for abstract syntax tree for expressions.

from Utility import pure_virtual, xassert


__all__ = """\
IExprVisitor
Expr
ConstExpr
BinaryExpr
UnaryExpr
CallExpr
DOMExpr
ArrayAccess
IfThenElse
process_string
""".split()


## Interface of visitors of these expressions.
class IExprVisitor(object):
    def eval_const(self, expr):
        pure_virtual()

    def eval_binexp(self, expr):
        pure_virtual()

    def eval_unexp(self, expr):
        pure_virtual()

    def eval_callexpr(self, expr):
        pure_virtual()

    def eval_domexpr(self, expr):
        pure_virtual()

    def eval_array_access(self, expr):
        pure_virtual()

    def eval_if_then_else(self, expr):
        pure_virtual()


class Expr(object):
    def __init__(self):
        pass

    def eval(self, ctx):
        pure_virtual()


class ConstExpr(Expr):
    def __init__(self, val):
        self.__val = val

    def value(self):
        return self.__val

    def eval(self, ctx):
        return ctx.eval_const(self)


class BinaryExpr(Expr):
    def __init__(self, o, l, r):
        Expr.__init__(self)
        self.__op = o
        self.__left = l
        self.__right = r

    def op(self):
        return self.__op

    def left(self):
        return self.__left

    def right(self):
        return self.__right

    def eval(self, ctx):
        return ctx.eval_binexp(self)


class UnaryExpr(Expr):
    def __init__(self, op, param):
        Expr.__init__(self)
        self.__op = op
        self.__param = param

    def op(self):
        return self.__op

    def param(self):
        return self.__param

    def eval(self, ctx):
        return ctx.eval_unexp(self)


class CallExpr(Expr):
    def __init__(self, idf, ps):
        Expr.__init__(self)
        self.__ident = idf
        self.__params = ps

    def ident(self):
        return self.__ident

    def params(self):
        return self.__params

    def eval (self, ctx):
        return ctx.eval_callexpr(self)


class DOMExpr(Expr):
    def __init__(self, id_list):
        Expr.__init__(self)
        self.__id_list = id_list

    def ids(self):
        return self.__id_list

    def eval(self, ctx):
        return ctx.eval_domexpr(self)


class ArrayAccess(Expr):
    def __init__(self, left, index_expr, tail):
        self.__left = left
        self.__index_expr = index_expr
        self.__tail = tail

    def left(self, new = None):
        if new is not None:
            self.__left = new
        return self.__left

    def index_expr(self):
        return self.__index_expr

    def tail(self):
        return self.__tail

    def eval(self, ctx):
        return ctx.eval_array_access(self)


class IfThenElse(Expr):
    def __init__(self, cond, then_branch, else_branch):
        self.__cond = cond
        self.__then_branch = then_branch
        self.__else_branch = else_branch

    def cond(self):
        return self.__cond

    def then_branch(self):
        return self.__then_branch

    def else_branch(self):
        return self.__else_branch

    def eval(self, ctx):
        return ctx.eval_if_then_else(self)


## Helper function for parser that decodes escape sequences.
def process_string(instr):
    xassert(len(instr) >= 2)
    s = instr[1:-1]
    out = ""
    i = 0
    slen = len(s)
    while i < slen:
        if s[i] == "\\":
            xassert(i + 1 <= slen)
            if s[i+1] == "0":
                out += "\0"
                i += 2
            elif s[i+1] == "n":
                out += "\n"
                i += 2                 
            else:
                i += 1  #unknown escape code just make it \foo -> fooo
        else:
            out += s[i]
            i += 1
    return out
