## Implementation of evaluator/compiler of expressions into ValueGetters.

import ValueExtractors as VE
import Traversal as T
import AST
import Lexer
import Parser
import ExprVE
import DataTypes as DT
from pyggy import pyggy
from Utility import xassert, breakpoint


__all__ = """\
Context
context
""".split()


def _check_true(context, *params):
    return True


def _check_single_dom_param(context, expr, ident, *params):
    text = ""
    if isinstance(expr, ExprVE.ExprVE):
        text = expr.getText()
    if len(params) != 1:
        raise EBadArguments(ident, text,
                            "This function takes only 1 argument.")
    if not isinstance(params[0], VE.RelativeValue) and not isinstance(params[0], VE.ReadArrayIndex):
        raise EBadArguments(ident, text,
                            "The argument must give a DOM  object.")
    return True


def _check_single_array_param(context, expr, ident, *params):
    _check_single_dom_param(context, expr, ident, *params)
    text = "In " + str(context.start_dep.getElement()) + ","
    target_dep = params[0].getDOM(context.start_dep)
    target_elem = target_dep.getElement()
    if not isinstance(target_elem, DT.Array):
        raise EBadArguments(ident, text,
                            "Argument should be an array.")
    return True


def _check_single_dom_and_static_size_param(context, expr, ident, *params):
    _check_single_dom_param(context, expr, ident, *params)
    text = "In " + str(context.start_dep.getElement()) + ","

    if isinstance(params[0], VE.ReadArrayIndex):
        raise EBadArguments(ident, text,
                            "DOM object shouldn't have array index.")
    
    target_dep = params[0].getDOM(context.start_dep)
    
    #TODO we cannot do the check here, could be that some getters are not yet compiled 
    # so we cannot get this infor here
    # we need to do  this check only if whole the tree is compiled
    #if target_dep.getSizeOf() < 0:
    #    raise EBadArguments(ident, text,
    #                        "DOM object should have static size.")
    return True

class Context(AST.IExprVisitor):
    def __init__(self):
        self.start_dep = None
        self.parsed_expr_cache = {}

    def compile(self, exprve, start_dep):
        self.source_text = exprve.getText()
        ast = self.parse_expr(exprve.getText())
        self.start_dep = start_dep
        ve = ast.eval(self)
        self.source_text = None
        self.start_dep = None
        exprve.setCompiledGetter(ve)

    def parse_expr(self, text):
        self.source_text = text
        tree = None
        if text in self.parsed_expr_cache:
            tree = self.parsed_expr_cache[text]
        else:
            l = pyggy.lexer.lexer(Lexer.lexspec)
            l.setinputstr(text)
            gram = pyggy.srgram.SRGram(Parser.gramspec)
            p = pyggy.glr.GLR(gram)
            p.setlexer(l)
            try:
                tree = p.parse()
            except pyggy.ParseError, e:
                raise EParseError(e.str, e.tok, text)
            tree = pyggy.helpers.proctree(tree, Parser, 0)
            self.parsed_expr_cache[text] = tree
        self.source_text = None
        return tree

    def eval_const(self, const):
        return VE.Const(const.value())

    def eval_binexp(self, expr):
        left_val = expr.left().eval(self)
        right_val = expr.right().eval(self)
        op_sym = expr.op()
        op_constr = Context.binops[op_sym]
        result = op_constr(left_val, right_val)
        return result

    def eval_unexp(self, expr):
        param_val = expr.param().eval(self)
        op_sym = expr.op()
        op_constr = Context.unops[op_sym]
        result = op_constr(param_val)
        return result

    def eval_if_then_else(self, expr):
        cond = expr.cond().eval(self)
        then_branch = expr.then_branch().eval(self)
        else_branch = expr.else_branch().eval(self)
        result = VE.IfThenElse(cond, then_branch, else_branch)
        return result

    def eval_callexpr(self, expr):
        params_vals = []
        params = expr.params()
        for i in xrange(len(params)):
            result = params[i].eval(self)
            params_vals.append(result)
        ident = expr.ident()
        if ident not in Context.funcs:
            raise ENameNotFound(ident, self.start_dep.getFullPath())
        func, check = Context.funcs[ident]
        check(self, expr, ident, *params_vals)
        result = func(*params_vals)
        return result

    def eval_domexpr(self, expr):
        xassert(self.start_dep is not None)
        path = self.find_relative_path(self.start_dep, expr.ids())
        if path is None:
            raise ENameNotFound(str(".").join(expr.ids()),
                                self.start_dep.getFullPath())
        result = VE.RelativeValue(path)
        return result

    def eval_array_access(self, expr):
        names = []
        names.extend(expr.left().ids())
        index_expr = [expr.index_expr().eval(self)]
        index_pos = [len(names) - 1]
        # Gather full target name and points where there should be current
        # element change.
        if expr.tail() is not None:
            self._eval_array_access_2(expr.tail(), names, index_expr, index_pos)
        # Use gathered name to prepare path.
        path = self.find_relative_path(self.start_dep, names)
        # Cut the path in parts and use them to construct
        # VE.ReadArrayIndex chain.
                        
        if path is None:
            name = names[0]
            for i in range(len(names)-1) : name += "." + names[i+1]
            raise ENameNotFound(name, self.start_dep.getFullPath())
        xassert(isinstance(path, list))            
        part_end = len(path)
        prev_name_end = len(names)
        cai = None
        # Construct chain of VE.ReadArrayIndex in reverse.
        for i in reversed(xrange(len(index_pos))):
            part_len = prev_name_end - index_pos[i] - 1
            part_beg = part_end - part_len
            rv = VE.RelativeValue(path[part_beg:part_end])
            if cai is not None:
                cai.set_array_extr(rv)
                new_cai = VE.ReadArrayIndex(None, index_expr[i], cai)
            else:
                new_cai = VE.ReadArrayIndex(None, index_expr[i], rv)
            # Move leftwards.
            cai = new_cai
            part_end = part_beg
            prev_name_end = index_pos[i] + 1
        # Finish the left most piece.
        rv = VE.RelativeValue(path[0:part_end])
        cai.set_array_extr(rv)
        # Finally return the start of constructed chain.
        return cai

    def _eval_array_access_2(self, expr, names, index_exprs, index_pos):
        if isinstance(expr, AST.DOMExpr):
            breakpoint(names[-1] == "MUSize")
            names.extend(expr.ids())
        else:
            names.extend(expr.left().ids())
            index_exprs.append(expr.index_expr().eval(self))
            index_pos.append(len(names) - 1)
            self._eval_array_access_2(expr.tail(), names, index_exprs,
                                      index_pos)

    def _find_index_by_name(self, data, name):
        for i, agg in enumerate(data.getAggregates()):
            if agg.getName() == name:
                return i
        # No such name has been found.
        return None
    
    #TODO [VO] nebylo by tohle jednodussi a rychlejsi bez rekurze ?
    def _try_walk_down(self, curr_dep, to_name, index):
        xassert(index < len(to_name))

        next_index = self._find_index_by_name(curr_dep.getElement(),
                                              to_name[index])

        if next_index is not None:
            rest = []
            if index + 1 < len(to_name):
                rest = self._try_walk_down(curr_dep.Down(next_index),
                                           to_name, index + 1)
                if rest is None:
                    return None

            path = [next_index] + rest
            return path
        else:
            return None
        
    ## Finds relative path from start_dep to dependency  which is represented by to_name.
    # 
    # @param to_name is list of indentifiers "Struct1.Struct2.memeber" 
    #  is represented as ["Strict1", "Struct2", "member"]
    # @return list of Traversal.Up() and Traversal.Down(x) instances, which describe the way 
    #   from start_dep to dom addressed by to_name 
    def find_relative_path(self, start_dep, to_name):
        if start_dep is None:
            return None
        xassert(start_dep.element is not None)

        up_path = 0
        while start_dep is not None:
            maybe_path = None
            component = 0
            # For cases where we are at node A and want name A or A.B etc.
            # skip the first name component.
            first_step = None
            if start_dep.element.getName() == to_name[0]:
                if len(to_name) == 1:
                    # We are at A and asking for A.
                    return [T.Up()] * up_path
                else:
                    # We are at A and asking for A.B...
                    component = 1
                    
            first_step = self._find_index_by_name(start_dep.element,
                                                  to_name[component])
            if first_step is not None:
                next_dep = start_dep.Down(first_step)
                if len(to_name) > component + 1:
                    maybe_path = self._try_walk_down(next_dep, to_name,
                                                     component + 1)
                else:
                    maybe_path = []
                    
            if maybe_path is not None:
                path = ([T.Up()] * up_path + [T.Down(first_step)]
                        + [T.Down(i) for i in maybe_path])
                return path
            else:
                up_path += 1
                start_dep = start_dep.Up()

        return None


    ## Dictionary of available unary operators and their implementations.
    unops = {
        '-': VE.OpUnMinus,
        '~': VE.OpCompl,
        '!': VE.OpNot,
        }

    ## Dictionary of available binary operators and their implementations.
    binops = {
        '+': VE.OpPlus,
        '-': VE.OpMinus,
        '*': VE.OpMult,
        '/': VE.OpDiv,
        '&': VE.OpAnd,
        '>>': VE.OpRShift,
        '<<': VE.OpLShift,

        '==': VE.OpEq,
        '!=': VE.OpNe,
        '<':  VE.OpLt,
        '<=': VE.OpLe,
        '>':  VE.OpGt,
        '>=': VE.OpGe,
        '&&': VE.OpLAnd,
        '||': VE.OpLOr,
        }

    ## Dictionary of available functions and their implementations.
    # The elements of the value tuple are:
    # - Constructor of the getter;
    # - arguments checking function that should throw an exception
    #   on error.
    funcs = {
        'offsetof': (VE.OffsetOf, _check_single_dom_param),
        'sizeof': (VE.SizeOf, _check_single_dom_and_static_size_param),
        'ownindexof': (VE.OwnIndexOf, _check_single_dom_param),
        'arraysizeof': (VE.ArraySizeOf, _check_single_array_param),
        'actindexof': (VE.ArrayActIndexOf, _check_single_array_param),
        'strindex' : (VE.StrIndex, _check_true),
        'stopparse' : (VE.StopParse, _check_true),
        'binsizeof' : (VE.BinSizeOf, _check_single_dom_param)      
        }


## Base class for this module's exceptions.
class EvalException(Exception):
    def __init__(self):
        Exception.__init__(self)


## Exception that is thrown when DOM's name in ExprVE is not valid.
class ENameNotFound(EvalException):
    def __init__(self, name, path = None):
        Exception.__init__(self)
        self.name = name
        if path is None:
            path = ""
        self.path = path

    def __str__(self):
        return ("ENameNotFound: " + self.name + "\n"
                " in \"\"\"" + self.path + "\"\"\"")
    
	# path in the dom tree to the location of error getter    
    def getPath(self):
        return self.path
    
    def getName(self):
        return self.name


## This class is a rewrapped pyggy.ParseError.
class EParseError(EvalException):
    def __init__(self, string, token, text):
        self.string = string
        self.token = token
        self.text = text

    def __str__(self):
        return "EParseError: str: '%s', tok: '%s'" % (self.string, self.token)


## Ambiguous name encountered exception.
class EAmbiguousName(EvalException):
    def __init__(self, name, candidates):
        Exception.__init__(self)
        self.name = name
        self.candidates = candidates

    def __str__(self):
        return "EAmbiguousName: " + self.name


##
class EBadArguments(EvalException):
    def __init__(self, func, text, msg):
        self.func = func
        self.text = text
        self.msg = msg

    def __str__(self):
        return ("EBadArgumens: %s(),\n\t%s\n\t%s"
                % (self.func, self.text, self.msg))


context = Context()
