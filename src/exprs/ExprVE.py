import ValueExtractors as VE
import DOM
from Utility import xassert


__all__ = """\
ExprVE
""".split()


class ExprVE(VE.ValueGetter, VE.IQueryDOM):
    def __init__(self, text):
        xassert(text is not None)
        self.text = text
        self.compiled_getter = None

    def __str__(self):
        return "ExprVE('" + self.text + "')"

    def getText(self):
        return self.text

    def getCompiledGetter(self):
        return self.compiled_getter

    def setCompiledGetter(self, getter):
        xassert(isinstance(getter, VE.ValueGetter))
        self.compiled_getter = getter

    def getDOMValue(self, dom):
        xassert(self.compiled_getter is not None)
        result = self.compiled_getter.getDOMValue(dom)
        return result

    def getDOM(self, dom):
        xassert(self.compiled_getter is not None)
        xassert(isinstance(self.compiled_getter, VE.IQueryDOM))
        result = self.compiled_getter.getDOM(dom)
        xassert(isinstance(result, DOM.DOMElement))
        return result

    def walk(self, callback):
        sub_getters = []
        if self.compiled_getter is not None:
            sub_getters.append(self.compiled_getter)
        callback(self, sub_getters)
