import os
import generators.Generator as GEN


__all__ = """\
CustomGeneratorManager
""".split()


##
# Responsible for managing list of custom generators and loading them
class CustomGeneratorManager:
    def __init__(self):
        self.generators = []
        self.module_counter = 1
    ##
    # scans a defined path for .py files with generator instance
    def scan(self,path):
        self.generators = []
        try:
            files = os.listdir(path)
            for f in files:
                if f.find(".py") !=-1 and  f.find(".py") +3 == len(f):
                    self.loadGenerator(os.path.join(path, f))
        except Exception, e:
            return

    def loadGenerator(self, filename):
        import imp
        try:
            module = None
            module_name = "_dataviz_custom_generatror_" + str(self.module_counter)
            self.module_counter += 1
            module = imp.load_source(module_name, filename)
        except Exception, e:
            print("Specified path is not a Python source file!\n"
                           + str(e))
            return

        if "generator" not in module.__dict__:
            print("File loaded but `generator' was not found.")
            return

        g=module.__dict__["generator"]

        if not isinstance(g,GEN.Generator) :
            print("`generator' is not inherited from Generator interface!")
            return


        self.generators.append( module.__dict__["generator"] )

