from generators.DocGenLogic import DocGenUtils
import re
import os
import generators.Generator as GEN
import string
import DataTypes
import BasicTypes
        
class D2LGenerator(GEN.Generator):
    """ This class implements the generator of the file 
        structure in recently born programing language
        
    """

    def __init__(self):
        super(D2LGenerator, self).__init__()
    
    ##  \brief Will write generated and accompanied classes to
    #   a specified folder
    #   \param root_dep is dependecy representing  top pomst structure of the description
	#	\param enumsHolder is instance of UserEnumsHolder for current definition
    #   \param setting is a property dictionary where key is a name of property and value
    #   is the value  of the propperty
    def generate(self, root_dep, enumsHolder, settings):
        self.utils = DocGenUtils()
                
        tableOfDefined = []
        self.indentSize = 8
        self.commentsAllowed = settings["Comments"]
        self.root_dep = root_dep        
        
        # fill queue with all elements needs to be defined
        i = 0
        queue = [self.root_dep]
        while i < len(queue):
            dep = queue[i]
            for agg in dep.getAggregates():
                if agg.isComposite() and tableOfDefined.count(agg.getType()) == 0:
                    queue.append(agg)
                    tableOfDefined.append(agg.getType())
            i+=1
        
        code = ""
        
        if enumsHolder is not None:
            code += self._generateEnumsCode(enumsHolder.getEnumList())
        
        for dep in queue:            
            code += self._generateComposite(dep)
            code += "\n"
        
        if settings["OutputName"] == '':
            return code
        
        out = open(os.path.join(settings["OutputDirectory"], settings["OutputName"]), 'w')
        out.write(code)
        out.close()
    
    def _generateEnumsCode(self, enums):
        enumsCode = ""
        for enum in enums:
            enumsCode += "enum " + enum.getName() + ":\n"
            for enumerator, value in enum.getList():
                enumsCode += self._getIndentedCode(1, 0, enumerator + " = " + value)
            enumsCode += "\n"        
        return enumsCode
    
    def _generateComposite(self, dep):        
        compositeCode = ""
        if self.commentsAllowed:
             compositeCode += self._getCommentedCode(dep.getElement().getDoc())
             
        if dep == self.root_dep:
            compositeCode += "root "
        compositeCode += self.utils.getTypeName(dep.getType()).lower() + " " + dep.getName() + ":\n"
        
        indentLevel = 1
        for agg in dep.getAggregates():
            # composite's comment will be printed later
            if (self.commentsAllowed and not agg.isComposite()):
                compositeCode += self._getIndentedCode(indentLevel, 0, 
                    self._getCommentedCode(agg.getElement().getDoc()))
            
            # print composite's element definition
            aggCode = ""
            if agg.in_memory:
                aggCode = "inmemory "            
            aggCode += self.utils.getTypeName(agg.getType()).lower() + " " + agg.getName()
            # does it have any getter, properties or any validators or is it enumerated?
            if (len(self.utils.getGetters(agg))+len(agg.getProperties())+len(self.utils.getValidators(agg)) > 0
                or (isinstance(agg.getType(), DataTypes.Basic) and 
                    isinstance(agg.getType().type(), BasicTypes.BasicNumType) and
                    agg.getType().type().getEnum() is not None)):
                aggCode += ":"
            compositeCode += self._getIndentedCode(indentLevel, 0, aggCode)
            
            compositeCode += self._getIndentedCode(indentLevel+1, 0, self._generateEnumAssignmentCode(agg))
            compositeCode += self._getIndentedCode(indentLevel+1, 0, self._generateGettersCode(agg))
            compositeCode += self._getIndentedCode(indentLevel+1, 0, self._generateValidatorsCode(agg))
            compositeCode += self._getIndentedCode(indentLevel+1, 0, self._generatePropertiesCode(agg))            
        
        # special case when composite type is empty
        if len(dep.getAggregates()) == 0:
            compositeCode += " " * indentLevel * self.indentSize + "empty" + "\n"
            
        # special case when dependecy is root
        if (dep == self.root_dep):
            compositeCode += self._getIndentedCode(indentLevel, 0, self._generateGettersCode(dep))            
            compositeCode += self._getIndentedCode(indentLevel, 0, self._generateValidatorsCode(dep))            
            compositeCode += self._getIndentedCode(indentLevel, 0, self._generatePropertiesCode(dep))
                        
        return compositeCode
    
    def _generateGettersCode(self, dep):
        gettersCode = ""
        for getter in self.utils.getGetters(dep):
            tmpCode = getter[0] + " " + getter[1]
            gettersCode += self._getIndentedCode(0, len(getter[0]+" "), tmpCode)
        return gettersCode
    
    def _generateValidatorsCode(self, dep):
        validatorsCode = ""
        for validator in self.utils.getValidators(dep):                
            # first print validator's comment
            if len(validator[0]) > 0:
                validatorsCode += self._getCommentedCode(validator[0])                     
            tmpCode = "validator: " + validator[1]
            validatorsCode += self._getIndentedCode(0, len("validator: "), tmpCode)
        return validatorsCode
    
    def _generatePropertiesCode(self, dep):
        propertiesCode = ""
        for property in dep.getProperties().items():
            key = property[0]
            value = property[1]
            propertiesCode += "property " + key + ": " + value + "\n"
        return propertiesCode
    
        
    def _generateEnumAssignmentCode(self, dep):
        enumCode = ""
        if (isinstance(dep.getType(), DataTypes.Basic) and
            isinstance(dep.getType().type(), BasicTypes.BasicNumType) and
            dep.getType().type().getEnum() is not None):
            enumCode += "enum: " + dep.getType().type().getEnum().getName() + "\n"
        return enumCode    
        
    ## returns indented code, first line without offset, next lines with it
    #  result ends with new line character!
    def _getIndentedCode(self, indentLevel, nextLinesOffset, code):
        if (len(code.splitlines()) == 0):
            return ""
        indentedCode = ""
        indent = " " * self.indentSize * indentLevel
        offset = " " * nextLinesOffset
        indentedCode += indent + code.splitlines()[0] + "\n"
        for line in code.splitlines()[1:]:
            indentedCode += indent + offset + line + "\n"
        return indentedCode
    
    ## adds comment character ('#') and space before each line
    #  result ends with new line character!    
    def _getCommentedCode(self, code): 
        commentedCode = ""
        for line in code.splitlines():
            commentedCode += "#" + line + "\n"
        return commentedCode 
        
    ## \return  property dictionary where key is the name of the property and
    #             the value is a tuple of (default value, property type)
    #             property type can be "string", " checkbox" , "directory"
    def getDefaultSettings(self, root_dep):
        return [("OutputDirectory",("Output directory","." , "dir")),
                ("OutputName",
                    ("Output file name",root_dep.getType().getName()+".d2l","string")),                
                ("Comments",("Include comments",True,"bool")),
                ]

    ## string for menu
    def getMenuString(self):
        return "Language generator"

    ## Description string ... necessary??
    def getDescription(self, lang):
        return "Language generator"
    
    
generator = D2LGenerator()    
