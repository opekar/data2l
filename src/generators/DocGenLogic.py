from Dependency import Dependency
from DataTypes import *
from exprs.ExprVE import ExprVE
import datetime
import string
from GetterCodeSyntaxHighlighter import GetterCodeSyntaxHighlighter


__all__ = ["DocGenLogic","DocGenUtils"]

class DocGenLogic:
    """ Performs data persistent operations for html templates 
    """
   
    def __init__(self):
        self.tabuListClear()
        self.stack = 0
        self.utils = DocGenUtils()
        self.uid = 0
    
    def getContents(self, dep, numColumns ):
        """ Generates the table of contents.
        @return list of n members lists 
           It is nice to see the table of contents in multiple columns esp. for
           long tables but it is necessary to reformat the natural input in a way
           it respects usual alphabetical order.
           
        """
        elementNames = []
        queue = [dep]
        self.tabuListClear()
        while len(queue) > 0:
            dep = queue.pop()
            for agg in dep.getAggregates():
                if agg.isComposite() and (not self.isTabu(agg.getElement())):
                    elementNames.append(agg.getName())
                    self.tabuListAdd(agg.getElement())
                    queue.append(agg)
        self.tabuListClear()
        elementNames.sort()
        
        contentTable = []
        if len(elementNames) % numColumns == 0:             
            rows = len(elementNames) / numColumns
        else:
            rows = len(elementNames) / numColumns+1              
                
        for i in range(rows):
            tuplet = [elementNames[i]]
            contentTable.append(tuplet)
            
        for i in range(numColumns):
            if i > 0:
                for j in range(rows):
                    if i * rows + j < len(elementNames):
                        contentTable[j].append(elementNames[i * rows + j])
                    else:
                        contentTable[j].append("")
                
        return contentTable
        
    def tabuListClear(self):
        self.tabuList = []
        return
        
    def tabuListAdd(self, item):
        self.tabuList.append(item)
        return
        
    def isTabu(self, item):
        """ @return: true for those entities which were already processed by an algorithm.
            used for avoiding repeating of multiply referenced dependencies
        """
        return self.tabuList.count(item) > 0
    
    def initStack(self, dep):
        self.stack = [(dep,0)]
        
    def isStackEmpty(self):
        return len(self.stack) == 0
        
    def popStack(self):
        if self.isStackEmpty():
            return None
        else:
            top = self.stack.pop()
            
            for dep in reversed( top[0].getAggregates() ):
                if dep.isComposite():
                    self.stack.append((dep,top[1]+1))
            return top
        
    def getUID(self):
        self.uid += 1
        return "i"+str(self.uid) 
                
    def setNoComment(self, nc):
        self.noCommentVal = nc
        
    def noComment(self):
        return self.noCommentVal



class DocGenUtils:
    """ Serves as a code dislocator for html templates in HtmlTemplate.py.
      Whatever utility function the template needs is here. 
    """
    def __init__(self):
        self.highlighter = GetterCodeSyntaxHighlighter()

    def length(self, list):
        return len(list)

    def range(self, num):
        return range(num)
     
    def getTypeName(self, element):
        if isinstance(element, Struct):
            return "Struct"
        if isinstance(element, Array):
            return "Array"
        if isinstance(element, Alternative):
            return "Alternative"
        if isinstance(element, Attrib):
            return "Attribute "+str(element.basic_type)
        if isinstance(element, Void):
            return "void"
        if isinstance(element, Basic):
            return str(element.type())
        return "other"

    def getMinorComment(self, dep):
        element = dep.getElement()
        if isinstance(element, SimpleElement):
            return self.formatHtmlParagraphs(element.getDoc())
        else:
            return ""
        
    @staticmethod
    def hasOffset(dep):
        return hasattr(dep,"offset") and isinstance(dep.offset, ExprVE) and (len(dep.offset.getText()) > 0) 

    @staticmethod
    def hasSize(dep):
        return hasattr(dep,"size") and isinstance(dep.size, ExprVE) and (len(dep.size.getText()) > 0) 

    @staticmethod
    def hasSelector(dep):
        return hasattr(dep,"selector") and isinstance(dep.selector, ExprVE) and (len(dep.selector.getText()) > 0) 

    @staticmethod
    def hasInit(dep):
        return hasattr(dep,"init") and isinstance(dep.init, ExprVE) and (len(dep.init.getText()) > 0) 

    @staticmethod
    def hasOnSave(dep):
        return hasattr(dep,"onsave") and isinstance(dep.onsave, ExprVE) and (len(dep.onsave.getText()) > 0) 

    @staticmethod
    def hasGetter(dep):
        return self.hasOffset(dep) or self.hasSize(dep) or self.hasSelector(dep) or self.hasInit(dep)
    
    @staticmethod
    def isInMemory(dep):
        return dep.in_memory != False
    
    @staticmethod
    def getOffset(dep):
        return dep.offset.getText()
    
    @staticmethod
    def getGetters(dep, replaceEntities = True):
        list = []
        if DocGenUtils.hasOffset(dep):
            text = dep.offset.getText()
            if replaceEntities: text = DocGenUtils.replaceEnt(text)
            list.append(('offset:',text))
        if DocGenUtils.hasSize(dep):
            text = dep.size.getText()
            if replaceEntities: text = DocGenUtils.replaceEnt(text)
            list.append(("size:",text))
        if DocGenUtils.hasSelector(dep):
            text = dep.selector.getText()
            if replaceEntities: text = DocGenUtils.replaceEnt(text)
            list.append(("selector:",text))
        if DocGenUtils.hasInit(dep):            
            text = dep.init.getText()
            if replaceEntities: text = DocGenUtils.replaceEnt(text)
            list.append(("init:",text))
        if DocGenUtils.hasOnSave(dep):            
            text = dep.onsave.getText()
            if replaceEntities: text = DocGenUtils.replaceEnt(text)
            list.append(("onsave:",text))
        return list
    
    @staticmethod
    def getValidators(dep):
        list = []        
        for a in dep.assertions:
            comment = a.name.strip()
            list.append((comment, a.code.text))
        return list
    
    
    def getParents(self, dep):
        list = dep.getElement().getParents()
        return list
    
    def formatHtmlParagraphs(self, text):
        return text.replace("\n\n","</p><p>").replace("\n","<br/>")
    
    def today(self):
        now = datetime.datetime.now()
        return now.strftime("%d.%m.%Y at %H:%M:%S")
    
    @staticmethod    
    def replaceEnt(text):
        text.replace('&','&amp;')
        text.replace('<','&lt;')
        text.replace('>','&gt;')
        return text

    def hasComplexAggregate(self, dep):
        if not dep.isComposite():
            return False
        for agg in dep.getAggregates():
            if agg.isComposite():
                return True
        return False   
    
    def getToolTipText(self, element):
        text = element.getName()
        text += " : " + self.getTypeName(element)
        if len(element.getParents()) > 0:
            text += " contained in"
            for item in element.getParents():
                text += " " + item.getName()
        return text
             
    def highlightCode(self, code):
        return self.highlighter.highlight(code)        

    @staticmethod
    def indentCode(code, level, num, key):
        if len(code.splitlines()) == 0: return ""
        indent = " " * (num * (level-1) + 1 + len(key))
        res = ""
        for line in code.splitlines()[1:]:
            res += "\n" + indent + line
        return " " + code.splitlines()[0] + res
        
    @staticmethod
    def indentComment(dep, logic, level, num):
        comment = dep.getElement().getDoc()
        if (comment == "") or (logic != None and logic.noComment()): return ""
        indent = " " * level * num
        res = indent + "#" + comment.splitlines()[0] 
        for line in comment.splitlines()[1:]:
            res += "\n" + indent + "#" + line
        return res+"\n"
    
    
    def indentProperties(self, dep, level, num):
        prop = dep.getProperties()
        list = []
        indent = " " * level * num
        for p in prop.items():
            key = p[0]
            value = p[1]
            list.append( indent + "property " + key + ": " + value )
        return list
