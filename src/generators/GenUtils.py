
def listFromDepTree(dep):
    multi_elem = {}
    list_stats = []
    #zjisti ktere struktury jsou vicenasobne umistene
    __elem_stats(dep, multi_elem, list_stats)
    #set 'multiple' flag for each dep which element is refereced more than once
    for i in list_stats:
        i.multiple = multi_elem[i.element] > 1

    return list_stats

def __elem_stats(dep, multi_elem, list_stats):
    if multi_elem.has_key(dep.element):
        multi_elem[dep.element] += 1
    else:
        multi_elem[dep.element]  = 1

    dep.rep_count = multi_elem[dep.element]
    dep.multiple = False


    for agg in dep.getAggregates():
        __elem_stats(agg, multi_elem,list_stats)
        
    if dep.isComposite():
        list_stats.append(dep)