from Traversal import *
from BasicTypes import *
from DataTypes import *
from string import *
import ValueExtractors as VE
import exprs.ExprVE as ExprVE
from Utility import xassert


class GeneratorException(Exception):
    def __init__(self, info):
        Exception.__init__(self, info)

class VG_generator(Traversable):
    def Up(self):
        self.dep_trav = self.dep_trav.Up()
        if ((isinstance(self.dep.element, Basic) and self.step_count > 1)
            or not isinstance(self.dep.element, Basic)):
            if len(self.rvg_str) > 0:
                self.rvg_str +="->"
#            self.rvg_str +="parent"
            if not self.eliminate_one_parent:
                self.rvg_str +="parent"
            else:
                self.eliminate_one_parent = False


#            self.rvg_str +="parent"
#            self.point = True

    def Down(self,idx):
        self.dep_trav = self.dep_trav.Down(idx)
        if len(self.rvg_str) > 0:
            self.rvg_str +="->"
        self.rvg_str +="%s" % (self.dep_trav.element.name)
 #       self.point = True


    def codeFromRelativeVG(self, dep, vg):
        xassert(isinstance(vg,VE.RelativeValue))
        self.eliminate_one_parent=False
        if self.gen_validator:
            if len(vg.path)>0 and isinstance(vg.path[0], Up):
                self.rvg_str=""
                if not isinstance(dep.getType(), Basic):
                    self.eliminate_one_parent=True
            else:
                self.rvg_str=dep.getName()
        else:
            self.rvg_str="this"
        self.dep_trav = dep
        self.step_count =1
        for i in vg.path:
            i.step(self)
            self.step_count +=1


        return self.rvg_str

    def _walk_value_getters(self, getter, sub_getters):
        if  isinstance(getter, VE.BinaryOp):
            self.str += "("
            sub_getters[0].walk(self._walk_value_getters)
            self.str += " %s " % (getter.getOperatorStr())
            sub_getters[1].walk(self._walk_value_getters)
            self.str += ")"

        if  isinstance(getter, VE.UnaryOp):
            self.str += " %s " % (getter.getOperatorStr())
            sub_getters[0].walk(self._walk_value_getters)

        if  isinstance(getter, VE.RelativeValue):
                        self.str += self.codeFromRelativeVG(self.dep,getter)

        if  isinstance(getter, VE.Const):
            v = getter.getDOMValue(None)
            if isinstance(v,str):
                self.str += "\"%s\"" % (getter.getDOMValue(None))
            else:
                self.str += "0x%x" % (getter.getDOMValue(None))

        if  isinstance(getter, VE.SizeOf):
            size_of = getter.target_getter.getDOM(self.dep).getSizeOf()
            if size_of <= 0:
                raise GeneratorException((self.dep.getName(), self.vg.getText()))
            self.str += "%d /*sizeof(%s)*/"  % (size_of, self.dep.element.name)

        if  isinstance(getter, VE.OffsetOf):
            sub_getters[0].walk(self._walk_value_getters)
            self.str += "->offset"


        if  isinstance(getter,   VE.ArraySizeOf):
            sub_getters[0].walk(self._walk_value_getters)
            self.str += "->arraySize"

        if  isinstance(getter,   VE.ArrayActIndexOf):
            sub_getters[0].walk(self._walk_value_getters)
            self.str += "->actIndex"

        if  isinstance(getter, VE.OwnIndexOf):
            sub_getters[0].walk(self._walk_value_getters)
            self.str += "->ownindex"

        if  isinstance(getter, VE.IfThenElse):
            self.str += "/*if*/("
            sub_getters[0].walk(self._walk_value_getters)
            self.str += " ) ? \n/*then*/ "
            sub_getters[1].walk(self._walk_value_getters)
            self.str += ": /*else*/"
            sub_getters[2].walk(self._walk_value_getters)

        if  isinstance(getter, VE.StrIndex):
            self.str += "data2l::strindex("
            sub_getters[0].walk(self._walk_value_getters)
            self.str += ", "
            sub_getters[1].walk(self._walk_value_getters)
            self.str += ")"
            
        if  isinstance(getter, VE.StopParse):
            self.str += "/*stopparse()*/ -1"
            
        if  isinstance(getter, VE.BinSizeOf):
            if isinstance(getter.target_getter, VE.ReadArrayIndex):
                target_dep = getter.target_getter.getTargetType(self.dep)
            else:
                target_dep = getter.target_getter.getDOM(self.dep)
            if isinstance(target_dep.getType(), Basic):
                size_of = getter.target_getter.getDOM(self.dep).getSizeOf()
                if size_of <= 0:
                    raise GeneratorException((self.dep.getName(), self.vg.getText()))
                self.str += "%d /*sizeof(%s)*/"  % (size_of, getter.target_getter.getDOM(self.dep).getElement().name)
            else:
                sub_getters[0].walk(self._walk_value_getters)
                self.str += "->getBinarySize()"

        if  isinstance(getter, VE.ReadArrayIndex):
            getter.array_extr.walk(self._walk_value_getters)

            target_array = getter.array_extr.getDOM(self.dep)

            #BasicArray has no tail
            if isinstance(target_array.getElement(),Basic):
                self.str += "["
                getter.index_extr.walk(self._walk_value_getters)
                self.str += "]"
            #normal array extractors
            else:
                self.str += "->get%sAt(" % (getter.tail_extr.path[0].step(target_array).element.name)
                getter.index_extr.walk(self._walk_value_getters)
                self.str +=")"


                #pro array s basic typem rovnou generuj, pro ostatni vynech top strukturu
                first = len(getter.tail_extr.path) > 0
                tail = target_array
                for i in getter.tail_extr.path:
                    tail=i.step(tail)
                    if first:
                        first= False
                    else:
                        self.str += "->%s" % (tail.element.name)

            #self.str +=self.codeFromRelativeVG(target_array,getter.tail_extr)


        if  isinstance(getter, ExprVE.ExprVE):
            getter.compiled_getter.walk(self._walk_value_getters)


    def generate(self,dep,vg, validator = False):
        self.gen_validator = validator
        self.dep = dep
        self.str="";
        self.vg = vg
        vg.walk(self._walk_value_getters)
        return self.str
