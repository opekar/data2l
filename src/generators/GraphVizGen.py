import os
from DataTypes import *
import BasicTypes
import ValueExtractors as VE
import generators.Generator as GEN

class DefToDotFormat:
    """Generates input file for DOT (www.graphviz.org)"""
    def print2Buffer(self,string):
        s = self.record_stack.pop() + string
        self.record_stack.append(s)

    def gen(self,root_dep):
        self.record_list = []
        self.connections_list = []
        self.record_stack = []
        self.element_stack = []
        self.subgraphs = []
        self.deps = []
        self.processed= {}

        self.compositeElement(root_dep.element, root_dep, "Root Struct")



    def print_to_file(self,file):
        file.write("digraph G { ")
        file.write("\n")
        for i in self.record_list:
            file.write(i)
            file.write("\n")
        file.write("edge [arrowtail=odiamond]")
        file.write("\n")
        for i in self.connections_list:
            file.write(i)
            file.write("\n")
        for i in self.subgraphs:
            file.write(i)
            file.write("\n")
        file.write("edge [arrowtail=none, color = red]")
        file.write("\n")
        for i in self.deps:
            file.write(i)
            file.write("\n")

        file.write("}")

    #find all nested relative value getters
    def _walk_value_getters(self, getter, sub_getters):
        if isinstance(getter, VE.RelativeValue):
            self._relative_getters.append(getter)

        # @todo na tehle getterech se to seka protoze jsou tam ruzne zkracene cesty - vymyslet jak toto vyzualizovat
        if isinstance(getter,VE.ReadArrayIndex):
            return

        for i in sub_getters:
            i.walk(self._walk_value_getters)

    def addAllNestedGeeters(self, getter, parent_element, start_dep, label, color):
        self._relative_getters = []

        if getter is None:
            return

        getter.walk(self._walk_value_getters)
        for i in self._relative_getters:
            a = i.getDOM(start_dep)
            self.deps.append("%s:%s:w->%s:%s:w [label=%s, color=%s, fontcolor=%s];" % (parent_element.name,
                                                                                  start_dep.element.name,
                                                                                  a.Up().element.name,
                                                                                  a.element.name,
                                                                                  label,
                                                                                  color,
                                                                                  color) )

    def dependenciesVisualization(self,dep):
        for i in dep.getAggregates():
            self.addAllNestedGeeters(i.size, dep.element ,i, "size", "red")
            self.addAllNestedGeeters(i.offset , dep.element,  i, "offs", "blue")
            self.addAllNestedGeeters(i.selector ,dep.element,  i, "sel", "green")


    def compositeElement(self, element , dep, element_type):
        if len(self.record_stack) > 0:
            self.print2Buffer(" | <%s> %s : %s \\l  " % (dep.getName(), dep.getName(), element_type ))

        #do not process the structures twice or more just put a reference there
        if self.processed.has_key(element) == 1:
            self.connections_list.append ("%s->%s ;" % (dep.parent.getName(), dep.getName()) )
            return
        else:
            self.processed[element] = 1


        self.element_stack.append(element)
        self.record_stack.append("")

        self.print2Buffer("%s [shape = record , label =\"{  %s : %s  " % (element.name, element.name, element_type) )
        for d in dep.getAggregates():
            self._dep=d
            d.element.accept(self)

        self.print2Buffer(" }\"];")
        self.record_list.append( self.record_stack.pop() )
        self.element_stack.pop()

        if dep.parent is not None:
            self.connections_list.append ("%s->%s ;" % (dep.parent.getName(), dep.getName()) )

        self.dependenciesVisualization( dep )



    #vrati ten spravny element
    def VisitStruct(self, struct):
        self.compositeElement(struct, self._dep, "struct")

    def VisitBasic(self, basic):
        type_str = str(basic.basic_type)

#        if isinstance(basic.dep.size, ValueExtractors.Const): type_str += '[%i]' % parseable.size.getDOMValue(None)
#        else: type_str += '[variable size]'
        self.print2Buffer(" | <%s> %s : %s \\l  " % (basic.name, basic.name, type_str ))

    def VisitVoid(self, basic):
        self.print2Buffer(" | void \\l  ")

    def VisitArray(self, array):
        self.compositeElement(array, self._dep,"array")

    def VisitAlternative(self, alternative):
        self.compositeElement(alternative, self._dep, "alternative")



class GraphVizGen(GEN.Generator):
    """ Generates  code which which eventualy can be used by dot (@see http://www.graphviz.org/)
    """
    def __init__(self):
        super(GraphVizGen, self).__init__()
        
        
    def generate(self, root_dep, enumsHolder, settings):
        try:
            v = DefToDotFormat()
            v.gen(root_dep)
        
            output_filename = os.path.join(settings["OutputDirectory"], settings["OutputName"])+".gif"
            s_in, s_out = os.popen2("%s -Tgif -o %s" % (settings["Dot"], output_filename) ) 
            v.print_to_file(s_in)
            s_in.close()
        
            for i in s_out.readlines():
                print i
            s_out.close()
            
            if settings["ShowGraph"]:
                os.startfile(output_filename)
        except Exception, e:
            import gtk;
            msg = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR,
                            gtk.BUTTONS_OK, 
                            "Error occurred!\n\nMost probably you don't have dot.exe installed\n\n" +
                            "pls see www.graphviz.org.")
            msg.run()
            msg.hide()              


    def getDefaultSettings(self, root_dep):
        return [("OutputDirectory",("Output directory",".\\" , "dir")),
                ("OutputName",
                    ("Output file name",root_dep.getElement().getName(),"string")),
                ("Dot",("Dot executible","dot","file")),
                ("ShowGraph",("Show graph",True,"bool")),
                ]

    ## String for menu.
    def getMenuString(self):
        return "Graphviz structure visualization"        
    
    
generator = GraphVizGen()    