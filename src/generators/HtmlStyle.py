

htmlStyle = """
  body {
    font-family: Verdana, Tahoma, sans-serif;    
    margin: 30px; 
    margin-top: 50px;   
    margin-bottom: 50px; 
    font-size: ${normal}pt;  
  }
    
  h1 {
    text-align: center;
    font-size : ${h1}pt;
  }

  h2 {
    margin-top: 2.5ex;
    font-size : ${h2}pt;
  }

  h3 {
    font-size: ${h3}pt;
    margin-bottom: 1ex;
  }
  
  p {
    margin-top : 0.5ex;
    margin-bottom : 0px;
    margin-left : 1.5em; 
  }
  
  /* footer with the date and time of genration */
  p.date {
    margin-top : 30px;
    text-align: center;
    font-size: ${small}pt;
  }

  table {
    margin-top : 0px;
    margin-bottom : 0px;
    margin-left : 0.4em;
    width : 100%; 
  } 
  
  /* the type name in the heading */
  span.type {
    font-size : ${normal}pt;
    margin-left : 1em;
  }
  

  /* the plus sign in the tree */
  span.treeplus {
    cursor: pointer;
    font-size: ${small}pt;
    width: 1em;
  }

  /* the circle sign in the tree */
  span.treebull {
    margin-left : 0.15em;
    font-size: ${small}pt;
  }
  
  /* the keyword in the getter code text */
  span.keyword {
    font-weight: bold;
  }

  /* the number in the getter code text */
  span.numeric {
    color: blue;
  }

  /* the function in the getter code text */
  span.function {
    color : green;
    font-weight: bold;
  }
  
  td {
    vertical-align: top;
  }
  
  /* the cell with plus in the table */
  td.plus {
    font-weight: bold;
    width : 0.7em;
    vertical-align: middle;
  }

  /* the cell with name in the table */
  td.name {
    font-weight: bold;
    width : 15em;
  }

  /* the cell with getter name in the table */
  td.getterName {
    font-weight: bold;
    width : 5em;
    font-size: ${small}pt;
  }

  /* the cell with getter code in the table */
  td.getter {
    font-family : monospace;
    white-space : pre;
    font-size: ${small}pt;
  }
  
  /* the cell with type name in the table */
  td.type {
    width : 5em;
    
  }

  /* the cell with comment in the table */
  td.comment {
    font-style: italic;
    font-size: ${small}pt;
    width: auto;
  }

  a {
    color: navy;
    text-decoration: none;
  }
  
  /* the link in the table of contents */
  a.content {
    font-weight: bold;
    font-size: ${small}pt;
    margin-right : 2em;
  }
  
  a:visited {
    color: navy;
  }
  
  /* the link to the parent structure in heading */
  a.parent {
    margin-left : 0.5em;
    font-size: ${normal}pt;
  }
  
  li {
    list-style-type: none;
  }
"""