import os
import generators.Generator as GEN
from Cheetah.Template import Template
from gtkGUI.Config import config, save_dirname_into
from generators.templ.Common import common_funcs

template = Template(common_funcs + """\
#import DataTypes as DT
#implements respond
################
#if $gen_assignment
$elem.getName() = #slurp
#end if
${ctor}('$elem.getName()',
#if len($elem.getDoc()) > 0
    [$right_quotes($elem.getDoc())],
#end if
#for $d in $elem.getDeps()
 #if len($d.getElement().getDoc()) > 0 \
    and isinstance($d.getElement(), DT.SimpleElement)
    $right_quotes($d.getElement().getDoc()),
 #end if
    $genStructElem($d.getElement()),
 #if $d.size is not None or $d.offset is not None or $d.selector is not None or $d.onsave is not None\
    or $d.init is not None or $d.in_memory == True or len($d.getProperties()) >0
        $genDependency($d),
 #end if
 #for $a in $d.assertions
        $genAssertion($a),
 #end for
#end for
)

""")

# hack for root's custom properties
rootPropsTempl = Template(common_funcs + """\
#implements respond
rootProps = #slurp
#if len($props) >0:
    {
    #for $k,$v in $props.iteritems():
        "$k":"$v",
    #end for
    } #slurp
#else
None #slurp
#end if
""")

# hack for root's assertions (validators)
rootAssertionsTempl = Template(common_funcs + """\
#implements respond
rootAssertions = #slurp
#if len($assertions) > 0:
    [
    #for $a in $assertions
        $genAssertion($a),
    #end for
    ] #slurp
#else
None #slurp
#end if
""")

# user defined enums
userDefEnumsTempl = Template("""\
#implements respond
userEnumList = #slurp
#if len($enums) > 0:
[#slurp
#for $e in $enums
$e.getName, #slurp
#end for    
] #slurp
#else
None #slurp
#end if
""")

enumsDefinitionTempl = Template("""\
#implements respod
#for $e in $enums
$e.getName = Enum("$e.getName()",
                [
    #for $enumerator,$value in $e.getList()                
                    ("$enumerator", $value),
    #end for
                ])
                
#end for
""")

 ##  For output format generation (to save the structure to persisten file).
 # Its using pregenerated Cheetah template in templ/Common.tmpl probably for speed up.
 # 
 # how to compile
 # python.exe cheetah compile c:\Projekty\small_projects\DataViz\src\generators\templ\Common.tmpl
 #
 # this will write a Common.py to the same directory.
class NewFormDef(GEN.Generator):
    def __init__(self):
        super(NewFormDef, self).__init__()
        self.__output = None
        self.__visited = None
        self.__elem_dep = None
        self.__rec_limit = None
        self.__rec_limit_val = None
        self.__gen_assignment = True
        self.__gen_top_assignment = False
        self.__gen_imports = True

    def generate_str(self, root_dep, enums = None):
        #list_stats = S.gather_statistics(root_dep)
        self.__rec_limit = self.__rec_limit_val
        self.__output = ""
        self.__visited = set()
        elem = root_dep.getElement()
        elem.accept(self)
        self.__visited = None
        self.__elem_dep = None
        self.__rec_limit = None
        tmp, self.__output = self.__output, None
        header = enums_def = footer = ""
        if self.__gen_imports:
            header = "from file_defs.prelude import *\n\n"
        if enums is not None:
            enumsDefinitionTempl.enums =  enums
            enums_def = str(enumsDefinitionTempl)
        if self.__gen_top_assignment:
            footer = "\n_top_entity = %s\n" % root_dep.getElement().getName()

        tmp = header + enums_def  + tmp + footer
        return tmp

    def generate(self, root_dep, enumsHolder, settings):        
        enums = enumsHolder.getEnumList()
        output = self.generate_str(root_dep, enums)        

        target_dir = settings["TargetDirectory"]
        #save_dirname_into(config, 'custom_generators_output_path', target_dir)
        config.dump()

        #if the directory is not available create it
        if not os.access(target_dir, os.F_OK):
                os.makedirs(target_dir)

        def_file = open(os.path.join(target_dir, settings["DefinitionName"]), 'w')

		# generate root's properties and validators and user enums
        rootPropsTempl.props = root_dep.getProperties()
        rootAssertionsTempl.assertions = root_dep.assertions
        userDefEnumsTempl.enums = enums

        def_file.write(output)
        def_file.write("""
definition = Definition(%s, %s, %s, %s)""" % 
            (root_dep.getElement().getName(), str(rootPropsTempl),
             str(rootAssertionsTempl), str(userDefEnumsTempl)))
        def_file.close()

    def getDefaultSettings(self, root_dep):
        return [("TargetDirectory",
                    ("Target directory", config.get('custom_generators_output_path'), "dir")),
                ("DefinitionName",
                    ("Definition name",root_dep.getElement().getName() + ".py", "string"))]

    ## String for menu.
    def getMenuString(self):
        return "Generate new form"

    def _print_declaration(self, elem, ctor):
        if elem in self.__visited:
            return
        else:
            self.__visited.add(elem)

        dep = self.__elem_dep
        deps = elem.getDeps()
        aggs = elem.getAggregates()
        # We want to walk the tree in postorder, so that required structures
        # are defined before use.
        if (self.__rec_limit is not None and self.__rec_limit > 0
            or self.__rec_limit is None):
            if self.__rec_limit is not None:
                self.__rec_limit -= 1;
            for a, d in zip(aggs, deps):
                self.__elem_dep = d
                a.accept(self)
            if self.__rec_limit is not None:
                self.__rec_limit += 1;

        temp = template
        temp.elem = elem
        temp.dep = dep
        temp.ctor = ctor
        temp.gen_assignment = self.__gen_assignment;
        self.__output += str(temp)
        return

    def VisitStruct(self, elem):
        self._print_declaration(elem, "Struct")

    def VisitArray(self, elem):
        self._print_declaration(elem, "Array")

    def VisitAlternative(self, elem):
        self._print_declaration(elem, "Alternative")

    def VisitBasic(self, _):
        pass

    def VisitVoid(self, _):
        pass

    def set_recursive_limit(self, limit):
        self.__rec_limit_val = limit

    def set_gen_assignment(self, onoff):
        self.__gen_assignment = onoff

    def set_gen_top_assignment(self, onoff):
        self.__gen_top_assignment = onoff

    def set_gen_imports(self, onoff):
        self.__gen_imports = onoff


generator = NewFormDef()
