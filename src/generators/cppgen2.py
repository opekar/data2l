from Traversal import *
import os
import re
from generators.GetterGen import *
from Cheetah.Template import Template


__all__ = """\
common_funcs
CppGen2
generator
""".split()

def _escape_c_string(s):
    tmp, _ = re.subn(r'(["\\])', r'\\\1', s)
    tmp, _ = re.subn(r'\n', r'\n', tmp)
    tmp, _ = re.subn(r'\t', r'\t', tmp)
    return tmp

common_funcs ="""
#from DataTypes import *
#from BasicTypes import *
#from generators.Generator import GeneratorException
##################################
#def getBaseClassName(dep)
#if $dep.multiple
C$dep.getName()#slurp
#else
data2l::Base#slurp
#end if
#end def
##################################
#def getClassName($dep)
#if $dep.multiple
C${dep.getName()}_$dep.rep_count#slurp
#else
C$dep.getName()#slurp
#end if
#end def
#################################
#def constructor($s)
#if $s.parent is not None
#if $s.multiple
$getClassName($s)::$getClassName($s)($getClassName($s.parent) * p, Reader& r) : C$s.getName()(r),parent(p)
#else
$getClassName($s)::$getClassName($s)($getClassName($s.parent) * p, Reader& r) : Base(r),parent(p)
#end if
#else
$getClassName($s)::$getClassName($s)(Reader&r) : Base(r)
#end if
#end def
##################################
#def copy_constructor($s)
#if $s.parent is not None
#if $s.multiple
$getClassName($s)::$getClassName($s)($getClassName($s.parent)* p, const C$s.getName()& peer) : C$s.getName()(peer.reader)
#else
$getClassName($s)::$getClassName($s)($getClassName($s.parent)* p, const C$s.getName()& peer) : Base(peer.reader)
#end if
#else
$getClassName($s)::$getClassName($s)(const C$s.getName()& peer) : Base(peer.reader)
#end if
#end def
##################################
#def clone_method_imp($s)
$getClassName($s)* $getClassName($s)::clone()
{
#if $s.parent is not None
    return new $getClassName($s)(parent, *this);
#else
    return new $getClassName($s)(*this);
#end if    
}
#end def
##################################
#def get_parent_method_imp($s)
data2l::Base * $getClassName($s)::get_parent() const
{
#if $s.parent is not None
    return parent;
#else
    return 0;
#end if
}#slurp
#end def
##################################
#def print_state_method_imp($s)
data2l::Base * C$s.getName()::print_state(std::ostream & os) const
{
#if isinstance($s.element, Array)
    os << "$s.getName(): index = " << actIndex
       << ",  size = " << arraySize << "\\n";
#elif isinstance($s.element, Struct)
    os << "$s.getName(): offset = " << offset << "\\n";
#elif isinstance($s.element, Alternative)
    os << "$s.getName(): alt = " << validAlternative << "\\n";
#else
    os << "Unknown structure.\\n";
#end if
#if $s.parent is not None
    return get_parent();
#else
    return 0;
#end if
}#slurp
#end def
##################################
#def pick_var_int_type($dep)
 #set $var_size = $dep.getSizeOf()
 #if $var_size != -1
  #if $var_size == 1
Byte#slurp
  #elif $var_size == 2
Word#slurp
  #elif $var_size <= 4
DWord#slurp
  #else
LongLong#slurp
  #end if
 #else
LongLong#slurp
 #end if
#end def
##################################
#def readBasic($dep)
#if $dep.init is not None:
    $dep.getName() = $getterGen.generate($dep, $dep.init);
 #stop
#end if
#if $dep.offset is not None
    _offs = $getterGen.generate($dep,$dep.offset);
    reader.seek(_offs);
#end if
#if $dep.isArray()
#set $op = ""
#else
#set $op = "&"
#end if
#if $dep.size is not None
    _size = $getterGen.generate($dep,$dep.size);
#set var = "_size"
#elif isinstance($dep.element.basic_type, BasicArrayType)
##nothing needs to be done here
It should never come here - $dep.getName()
#else
#set var = "1";
#end if
#if isinstance($dep.element.basic_type, BasicArrayType)
    reader.read${dep.element.basic_type.basicElement}($op$dep.getName(), $var);
#if isinstance($dep.element.basic_type,String)
    $dep.getName()[$var]=0;
#end if
#else
 #if isinstance($dep.element.type(), VarInteger)
    assert (_size <= sizeof ($dep.getName()));
    $dep.getName() = 0;
    reader.readBin(reinterpret_cast<char *>($op$dep.getName()), _size);
 #else
    reader.read${dep.element.basic_type}($op$dep.getName(), $var);
 #end if
#end if
#end def
##################################
#def writeBasic($dep)
#if $dep.init is not None:
 #stop
#end if
#if $dep.isArray()
#set $op = ""
#else
#set $op = "&"
#end if
#if $dep.onsave is not None
    //OnSave
    $dep.getName() = $getterGen.generate($dep,$dep.onsave);
#end if
#if $dep.size is not None
    _size = $getterGen.generate($dep,dep.size);
#set var = "_size"    
#elif isinstance($dep.element.basic_type, BasicArrayType)
##nothing needs to be done here
// nothing, probably error in generator element $dep.getName()
#else
#set var = "1"
#end if
####
#if isinstance($dep.element.basic_type, BasicArrayType)
    writer.write${dep.element.basic_type.basicElement}($op$dep.getName(), _size);
#else
 #if isinstance($dep.element.type(), VarInteger)
    assert (_size <= sizeof ($dep.getName()));
    writer.writeBin(reinterpret_cast<char *>($op$dep.getName()), _size);
 #else
    writer.write${dep.element.basic_type}($op$dep.getName(), $var);
 #end if
#end if
#end def
##################################
#def  getArrayStaticSize($d)
#if  $d.getProperty("static_size") is not None:
    #return $d.getProperty("static_size")#slurp
#else
#try
    #return $d.size.getDOMValue($d)#slurp
#except
#set ret = "Element '"+ $d.getParent().getName() +"."+$d.getName() + "' has variable size.\\n\\n" 
#set ret = $ret + "Generator is not able to deduce the static size of C++ variable.\\n"
#set ret = $ret + "Please specify static size using 'static_size' custom property.\\n\\n"
#set ret = $ret + "Or use a different code generator which generates code with dynamic sized arrays.\\n"
    #raise GeneratorException($ret)
#end try    
#end if    
#end def
#########################
#def cpp_type($dep)
#if isinstance($dep.element, Basic)
 #if isinstance($dep.element.type(), String)
data2l::String<$getArrayStaticSize($dep)>#slurp
 #elif isinstance($dep.element.basic_type, BasicArrayType)
data2l::Block<$getArrayStaticSize($dep),$dep.element.basic_type.basicElement>#slurp
 #elif isinstance($dep.element.type(), VarInteger)
$pick_var_int_type($dep)#slurp
 #else
$dep.element.basic_type#slurp
 #end if
#elif not isinstance($dep.element, Void)
C${dep.getName()}*#slurp
#end if
#end def
#########################
#def any_in_memory($dep)
#for i in $dep.getAggregates()
#if i.in_memory: #return True
#end for
#return False
#end def
##########################
#def SizeOffsetLocalVariables($dep)
#if $len( $filter(lambda d: isinstance(d.element, Basic) and d.size is not None, $dep.getAggregates()) ) > 0
    SizeType _size;
#end if
#if $len( $filter(lambda d: isinstance(d.element, Basic) and d.offset is not None, $dep.getAggregates()) ) > 0
    OffsetType _offs;
#end if
#end def
##########################
#def ValidatorGen($dep)
#if $settings["CapabilityValidate"]
#for $val in $dep.assertions
        //$dep.getName() - associated validator
        if (!( $getterGen.generate($dep, $val.getCode(), True )) ) {
               #if $val.getName() is not None:
                std::cout << "Element: $dep.getName() - $val.getName()\\n";
               #else
                std::cout << "Element: $dep.getName() \\n";
               #end if
               #if isinstance($dep.getType(), Basic)
                    std::cout << "Value: " << $dep.getName() << "\\n";
               #end if
               data2l::Base * tmp = this;
               do {
                   tmp = tmp->print_state(std::cout);
               } while (tmp);
               std::cout << std::endl;
        }
#end for
#end if
#end def
"""


class h_gen:
    def generate(self,list_stats):
        base_generated = {}
        output = ""

        for s in list_stats:
            if s.isComposite():
                self.s = s
                #jestlize je nektera struktura pouzita dvakrat vygenerujeme pro ni bazovou tridu
                if s.multiple and not base_generated.has_key(s.element):
                    base_generated[s.element] = 1
                    output += self.genBody(True)

                output += self.genBody(False)
        return output

    def genBody(self, base_class_gen):
        t = Template(common_funcs +"""
#if $base_class_gen
#set $virtual = " = 0"
class C$s.getName()
    : public data2l::Base
#else
#set $virtual = ""
class $getClassName($s)
    : public $getBaseClassName($s)
#end if
{
public:
## MEMBERS AND TYPE SPECIFIC MEMBERS
#if $base_class_gen or not $s.multiple
###MEMBER DEFINITION
#for $agg in $filter( lambda a: not isinstance(a.element,Void),  $s.getAggregates() )
    $cpp_type($agg)  $agg.getName();
#end for
### type specific memebers
#if  isinstance($s.element, Array)
    SizeType arraySize;
    long actIndex;

#if $any_in_memory($s)
    //stl like interfaces for in_memory aggregates of $s.getName()
    struct iterator_value {
    #for $agg in $filter( lambda a: a.in_memory, $s.getAggregates() )
            $cpp_type($agg)  $agg.getName();
    #end for
    };
    std::vector<iterator_value> in_memory;
    
    //normal iterators which might be unsafe fo some operations
    typedef std::vector<iterator_value>::iterator iterator;
    iterator begin() {return in_memory.begin();}
    iterator end() {if (arraySize == in_memory.size()) return in_memory.end();
                    else return in_memory.begin()+arraySize;}
    
    //iterator which is changing state of the tree                
    typedef array_iterator<C$s.getName()>  ch_iterator;
    ch_iterator ch_begin() {return ch_iterator(*this,0);}
    ch_iterator ch_end()   {return ch_iterator(*this,arraySize);}
                                        
    size_t size() {return arraySize;}
    iterator_value* at(size_t i) { return &in_memory[i];}
    //till here
    //
    #for $agg in $s.aggregates
      #if $agg.in_memory
    virtual $cpp_type($agg)  get${agg.getName()}At(long idx) {

       return in_memory[idx].$agg.getName();
    }
      #end if
    #end for
#end if  ## any_in_memory

#elif isinstance($s.element, Alternative)
    class_types validAlternative;
#end if

#end if  ## if base_class_gen or not $s.multiple
###
#if not $base_class_gen
## constructor definition
 #if $s.parent is not None
    $getClassName($s.parent) *parent;

    $getClassName($s)($getClassName($s.parent) *p, data2l::Reader &r);
    $getClassName($s)($getClassName($s.parent) *p, const C$s.getName()& peer);
 #else
    $getClassName($s)(data2l::Reader &r); 
    $getClassName($s)(const C$s.getName()& peer);
 #end if
     virtual ~$getClassName($s)();
#else  ####if not $base_class_gen
    C$s.getName()(data2l::Reader &r) : data2l::Base(r) {}
#end if

    virtual int init()$virtual;
    virtual int parse(data2l::SAXHandler& sax)$virtual;
#if $base_class_gen or not $s.multiple
    virtual data2l::Base * print_state(std::ostream &) const;
#end if
    virtual data2l::Base * get_parent() const $virtual;
    virtual SizeType getBinarySize()$virtual;
    virtual void save(data2l::Writer &writer)$virtual;
#if $base_class_gen and $s.parent is not None  
    virtual C$s.getName() * clone(data2l::Base * parent)$virtual;
#end if     
#if not $base_class_gen
    $getClassName($s) * clone();
 #if $s.parent is not None    
    virtual C$s.getName() * clone(data2l::Base * parent)
    {
        $getClassName($s) *cl = clone();
        cl->parent = ($getClassName($s.parent) *)parent;
        return cl;
    }
#end if    

#end if
## TYPE SPECIFIC METHODS  ARRAY
#if  isinstance($s.element, Array)
    virtual void loadIndex(long idx)$virtual;
#end if
};
""")
        t.s= self.s
        t.escape_c_string = _escape_c_string
        t.base_class_gen = base_class_gen
        return str(t)


class cc_gen:
    def __init__(self, settings):
        self.base_class_gen = False
        self.s = None
        self.output = None
        self.settings = settings

    def generate(self,list_stats):
        self.output = ""
        base_generated = {}
        for s in list_stats:
            self.s = s
            if s.isComposite():
                if s.multiple and not base_generated.has_key(s.element):
                    base_generated[s.element] = 1
                    self.base_class_gen = True
                    s.element.accept(self)
                    self.base_class_gen = False
                s.element.accept(self)
        self.s = None
        tmp, self.output = self.output, None
        return tmp


    def VisitStruct(self, element):
        t = Template(common_funcs +"""

#if $base_class_gen or (not $base_class_gen and not $s.multiple)
$print_state_method_imp($s)
#end if

#if not $base_class_gen
$constructor($s)
{
#for $agg in $s.getAggregates()
    #if $agg.isComposite()
    $agg.getName() = new $getClassName(agg)(this, r);
    #end if
#end for
}

$copy_constructor($s)
{
#if $s.parent is not None
    parent = p;
#end if    
#for $agg in $s.getAggregates()
    #if $agg.isComposite()
    $agg.getName() = new $getClassName(agg)(this, *peer.$agg.getName());
    #else
    $agg.getName() = peer.$agg.getName();
    #end if
#end for
}

$getClassName($s)::~$getClassName($s)()
{
#for $agg in $s.getAggregates()
    #if $agg.isComposite()
    delete $agg.getName();
    #end if
#end for
}

$clone_method_imp($s)

$get_parent_method_imp($s)

int $getClassName($s)::init()
{
#def readStruct($func2call)
$SizeOffsetLocalVariables($s)

#if $s.offset is not None
    offset = $getterGen.generate($s,$s.offset);
    reader.seek(offset);
#else
    offset = reader.tell();
#end if

    int res = 0;
#for $agg in $s.getAggregates()
    #if isinstance($agg.element, Basic)
$readBasic($agg)#slurp
    #elif $agg.isComposite()
    res = ${agg.getName()}->${func2call};
     if (res == -1) return -1;
    #else
    // void
    #end if
$ValidatorGen($agg) #slurp
#end for
#end def
$readStruct("init()")
    return res;
}


int $getClassName($s)::parse(SAXHandler &sax)
{
    if (sax.beginElement(e_$s.getName(), this)) return 0;
    $readStruct("parse(sax)")
    sax.endElement(e_$s.getName(), this);
    return res;
}


void $getClassName($s)::save(Writer &writer)
{
#if $len( $filter(lambda d: isinstance(d.element, Basic) and d.size is not None, $s.getAggregates()) ) > 0
    SizeType _size;
#end if

#for $agg in $s.getAggregates()
    #if isinstance($agg.element, Basic)
$writeBasic($agg)#slurp
    #elif $agg.isComposite()
    ${agg.getName()}->save(writer);
    #end if
#end for
}


SizeType $getClassName($s)::getBinarySize()
{
    SizeType size;

    size = 0
#for $agg in $s.getAggregates()
    #if $agg.init is not None
        #continue
    #end if
    #if $agg.isComposite()
        + ${agg.getName()}->getBinarySize()
    #else
     #if isinstance($agg.element.basic_type, BasicArrayType) and $agg.size is not None
        // bin size of $agg.getName()
        + $getterGen.generate($agg, $agg.size)* sizeof($agg.element.basic_type.basicElement)
     #elif isinstance($agg.element.basic_type, BasicArrayType) and $agg.size is None
        // bin size of $agg.getName()
        I should never come here
     #elif isinstance($agg.element.basic_type, VarInteger)
        + $getterGen.generate($agg, $agg.size)
     #elif isinstance($agg.element, Basic)
        + sizeof($agg.element.basic_type)
     #end if
    #end if
#end for
    ;
    return size;
}
#end if
""")
        t.s = self.s
        t.newline ="\\n"
        t.settings = self.settings
        t.escape_c_string = _escape_c_string
        t.getterGen = VG_generator()
        t.base_class_gen = self.base_class_gen
        self.output += str(t)

    def VisitArray(self, element):
        t = Template(common_funcs +"""

#if $base_class_gen or (not $base_class_gen and not $s.multiple)
$print_state_method_imp($s)
#end if
#
#if not $base_class_gen
$constructor($s)
{
   actIndex = 0;
   arraySize = 0;
#for $agg in $s.getAggregates()
    #if $agg.isComposite()
     $agg.getName()= new ${getClassName($agg)}(this,r);
    #end if
#end for
}

//copy constructor
$copy_constructor($s)
{
   parent =p;
   actIndex = peer.actIndex;
   arraySize = peer.arraySize;
#if $any_in_memory($s)   
   iterator_value ins;
   for (SizeType i=0;i<arraySize;i++)
   {
#for $agg in $s.getAggregates()
  #if $agg.in_memory
    #if $agg.isComposite()
      ins.$agg.getName()= new ${getClassName($agg)}(this, *(peer.in_memory[i].$agg.getName()));
    #else
      ins.$agg.getName()= peer.in_memory[i].$agg.getName();
    #end if
  #end if
#end for
      in_memory.push_back(ins);
   }
#end if ## if any_in_memory

##copy the current state
#for $agg in $s.getAggregates()
  #if $agg.in_memory
    #if $agg.isComposite()
   if (arraySize!=0) 
     $agg.getName()= in_memory[actIndex].$agg.getName();
    #else
   if (arraySize!=0)    
     $agg.getName()= in_memory[actIndex].$agg.getName();
    #end if
  #else  ##not i memory
    #if $agg.isComposite()
   $agg.getName()= new ${getClassName($agg)}(this, *(peer.$agg.getName()));
    #else
   $agg.getName()= peer.$agg.getName();
    #end if       
  #end if
#end for
}

$getClassName($s)::~$getClassName($s)()
{
#if len($filter(lambda d: (d.in_memory and d.isComposite()), $s.getAggregates())) >0
  for (iterator i=in_memory.begin();i<in_memory.end();++i)
  {
#for $agg in $filter(lambda d: (d.in_memory and d.isComposite()), $s.getAggregates())
    delete i->$agg.getName();
#end for    
  }
#end if
#for $agg in $filter(lambda d: (d.isComposite() and not d.in_memory), $s.getAggregates())
    delete $agg.getName();
#end for    
}

$clone_method_imp($s)

$get_parent_method_imp($s)

int $getClassName($s)::init()
{
$SizeOffsetLocalVariables($s)

#def arrayInitBlock($func2call)
#if $s.offset is not None
    offset = $getterGen.generate($s,$s.offset);
    reader.seek(offset);
#else
    offset = reader.tell();
#end if

    arraySize = $getterGen.generate($s,$s.size);
#if  $any_in_memory($s)
    ## //allocate some memory if needed
    ## while( in_memory.size() < arraySize)
    ## {
    ##      iterator_value s;
    ##   #for mem_agg in $filter(lambda d: d.in_memory, $s.getAggregates())
    ##    #if mem_agg.isComposite()
    ##      s.$mem_agg.getName() = new $getClassName($mem_agg)(this,reader);
    ##   #end if
    ##   #end for
    ##      in_memory.push_back(s);
    ## }

#set previous = []
    size_t i;
#set firstCycle = 1
#for agg in $filter(lambda d: d.in_memory, $s.getAggregates())
      $previous.append($agg)
    for(i=0; i<arraySize; i++)
    {
  #if firstCycle == 1
        //allocate memory only if more is needed
        if(in_memory.size() <= i) {
            iterator_value s;
    #for mem_agg in $filter(lambda d: d.in_memory, $s.getAggregates())
      #if mem_agg.isComposite()
            s.$mem_agg.getName() = new $getClassName($mem_agg)(this,reader);
      #end if
    #end for
            in_memory.push_back(s);
        }
  #end if
  
        //initialize memory
      #for p  in $previous
        $p.getName() = in_memory[i].$p.getName();
      #end for
        actIndex = i;
      #if $agg.isComposite
        static_cast<$getClassName($agg)*>(${agg.getName()})->parent = this;
        $agg.getName()->ownindex = i;
        if($agg.getName()->${func2call}) {
            arraySize = i;
            break;
        }
      #else
        $readBasic($agg)
        ## Found bug in original code: in_memory[i].$mem_agg.getName() = $mem_agg.getName();
        in_memory[i].$agg.getName() = $agg.getName();
      #end if
$ValidatorGen($agg)
    }
    #set firstCycle = 0;
#end for
#end if ##$any_in_memory($s)
#end def
$arrayInitBlock("init()")
    loadIndex(0);
    return 0;
}


void $getClassName($s)::loadIndex(long idx)
{
    if (arraySize == 0)
        return;
$SizeOffsetLocalVariables($s)

    actIndex = idx;
#def arrayLoadIndexBlock($func2call)
#for $agg in $s.getAggregates()
#if $agg.in_memory
    $agg.getName() =  in_memory[actIndex].$agg.getName();
#else
#if $agg.isComposite
    $agg.getName()->ownindex = actIndex;
    ${agg.getName()}->${func2call};
#else
    $readBasic($agg)

#end if    ####if $agg.isComposite
$ValidatorGen($agg)
#end if   ###if $agg.in_memory
#end for
#end def
$arrayLoadIndexBlock("init()")
}


int $getClassName($s)::parse(SAXHandler &sax)
{
$SizeOffsetLocalVariables($s)

    if (sax.beginElement(e_$s.getName(), this)) return 0;

    //init part
$arrayInitBlock("parse(sax)")

    //walk though the array
    for(size_t j =0; j<arraySize; ++j)
    {
       actIndex =j;
$arrayLoadIndexBlock("parse(sax)")
    }
    sax.endElement(e_$s.getName(), this);
    return 0;
}


void $getClassName($s)::save(Writer &writer)
{
#if $len($filter(lambda d: (not d.in_memory), $s.getAggregates)) > 0
   //not possible to save because some aggregates are not marked as in_memory
   assert(0);
#else
#if $len($filter(lambda d: isinstance(d.element,Basic), $s.getAggregates)) > 0
  SizeType _size;
#end if
  size_t i;

#set previous = []
#for agg in  $s.getAggregates()
    $previous.append($agg)
  for(i=0; i<arraySize; i++)
  {
    actIndex = i;
    #for p  in $previous
    $p.getName() = in_memory[i].$p.getName();
    #end for
    #if $agg.isComposite
     actIndex = i;
     $agg.getName()->save(writer);
    #else
     $writeBasic($agg)
     #for p  in $previous
     #if $agg.onsave is not None
     in_memory[i].$p.getName() = $p.getName();
     #end if
     #end for
    #end if
   }
#end for
#end if
}


SizeType $getClassName($s)::getBinarySize()
{
    SizeType size= 0;

    for (size_t i=0;i<arraySize;i++)
    {
        loadIndex(i);
        size +=
#for $agg in $s.getAggregates()
    #if $agg.init is not None
        #continue
    #end if
    #if $agg.isComposite()
            ${agg.getName()}->getBinarySize() +
    #else
    #if $agg.size is not None
            $getterGen.generate($agg, $agg.size) * sizeof($agg.element.basic_type) + /*bin size of $agg.getName() */ +
    #else
            sizeof($agg.element.basic_type) + /*bin size of $agg.getName() */ +
    #end if
    #end if
#end for
    0;

    }
    return size;
}
#end if
""")
        t.s = self.s
        t.settings = self.settings
        t.escape_c_string = _escape_c_string
        t.newline ="\\n"
        t.getterGen = VG_generator()
        t.base_class_gen = self.base_class_gen
        self.output += str(t)

    def VisitAlternative(self, element):
        t = Template(common_funcs +"""
#if $base_class_gen or (not $base_class_gen and not $s.multiple)
$print_state_method_imp($s)


#end if
#
#if not $base_class_gen
$constructor($s)
{
#for $agg in $s.getAggregates()
    #if $agg.isComposite()
   $agg.getName() = new $getClassName($agg)(this,r) ;
    #end if
#end for
}

$copy_constructor($s)
{
    
#for $agg in $s.getAggregates()
    #if $agg.isComposite()
     $agg.getName() = new $getClassName(agg)(this, *(peer.$agg.getName())); 
    #elif not isinstance($agg.element, Void)
     $agg.getName() = peer.$agg.getName(); 
    #end if
#end for
     validAlternative = peer.validAlternative;
     parent = p;
}

$getClassName($s)::~$getClassName($s)()
{
#for $agg in $s.getAggregates()
    #if $agg.isComposite()
    delete $agg.getName();
    #end if
#end for
}

$clone_method_imp($s)

$get_parent_method_imp($s)

int $getClassName($s)::init()
{
#def alternativeInitBlock($func2call)
#if $len( $filter(lambda d: isinstance(d.element, Basic) and d.size is not None, $s.getAggregates()) ) > 0
    SizeType _size;
#end if
    int selector = $getterGen.generate($s, $s.selector);
    if (selector == -1) {
        //Call of stopparse() function in definition handling
        return -1;
    }
#if $s.offset is not None
    offset = $getterGen.generate($s, $s.offset);
    reader.seek(offset);
#else
    offset = reader.tell();
#end if

    switch (selector)
    {
#for $i,$agg in enumerate($s.getAggregates())
    case $i:
#if isinstance($agg.element,Void)
        validAlternative = e_void;
#elif isinstance($agg.element,Basic)
        $readBasic($agg)
        //does not represent the class, just the id of the selection
        validAlternative = (class_types)selector; 
#else
        validAlternative = e_$agg.getName();
        $agg.getName()->$func2call;
#end if
$ValidatorGen($agg)
        break;
#end for
    default:
        printf("problem in $getClassName($s)::$func2call");
    }
#end def
$alternativeInitBlock("init()")
    return 0;
}


int $getClassName($s)::parse(SAXHandler &sax)
{
    if (sax.beginElement(e_$s.getName(), this)) return 0;
    $alternativeInitBlock("parse(sax)")
    sax.endElement(e_$s.getName(), this);
    return 0;
}


void $getClassName($s)::save(Writer & writer)
{
#if $len( $filter(lambda d: isinstance(d.element, Basic) and d.size is not None, $s.getAggregates()) ) > 0
    SizeType _size;
#end if
    int selector = $getterGen.generate($s, $s.selector);

    switch (selector)
    {
#for $i,$agg in enumerate($s.getAggregates())
    case $i:
#if isinstance($agg.element,Void)
        validAlternative = e_void;
#elif isinstance($agg.element,Basic)
        $writeBasic($agg)
#else
        validAlternative = e_$agg.getName();
        $agg.getName()->save(writer);
#end if
        break;
#end for
    default:
        printf("problem in $getClassName($s)::save");
    }
}


SizeType $getClassName($s)::getBinarySize()
{
    SizeType size= 0;

    int selector = $getterGen.generate($s, $s.selector);

    switch (selector)
    {
#for $i,$agg in enumerate($s.getAggregates())
    case $i:
#if isinstance($agg.element,Void)
        size = 0;
#elif isinstance($agg.element,Basic)
     #if isinstance($agg.element.basic_type, BasicArrayType) and $agg.size is not None
        size = $getterGen.generate($agg, $agg.size)* sizeof($agg.element.basic_type.basicElement) /*bin size of $agg.getName() */;
     #elif isinstance($agg.element.basic_type, BasicArrayType) and $agg.size is None
        Should never come here /*bin size of $agg.getName() */;
     #elif isinstance($agg.element, Basic)
        size = sizeof($agg.element.basic_type);
     #end if
#else
        size = $agg.getName()->getBinarySize();
#end if
        break;
#end for
    default:
        printf("problem in $getClassName($s)::save");
        assert(0);
    }

    return size;
}
#end if
""")
        t.s = self.s
        t.settings = self.settings
        t.escape_c_string = _escape_c_string
        t.getterGen = VG_generator()
        t.base_class_gen = self.base_class_gen
        self.output += str(t)


import generators.Generator as GEN
from gtkGUI.Config import config, save_dirname_into
from generators.GenUtils import *



class CppGen2(GEN.Generator):
    def generate(self, root_dep, enumsHolder, settings):
        list_stats = listFromDepTree(root_dep)
        target_dir = settings["TargetDirectory"]
        save_dirname_into(config, 'cpp_gen_directory', target_dir)
        config.dump()

        #if the directory is not available create it
        if not os.access(target_dir, os.F_OK):
                os.makedirs(target_dir)

        h_file =  open(os.path.join(target_dir, settings["HeaderName"]), 'w')
        cc_file = open(os.path.join(target_dir, settings["CPPName"]), 'w')

        h_tmpl = Template(common_funcs+"""
// this file is generated by Data2l
// do not change
// @see http://www.eccam.com

#set $hash="#"
${hash}ifndef DATAVIZ_${root_dep.getName()}_H
${hash}define DATAVIZ_${root_dep.getName()}_H

${hash}include "types.h"
${hash}include "reader.h"
${hash}include "writer.h"
${hash}include "baseclass.h"
${hash}include "sax.h"
${hash}include <boost/iterator/iterator_facade.hpp>

namespace db_${root_dep.getName()}
{
/** iterator which changes state of the tree.
 * using this iterator things like getBinarySize() to objects whose size is defined by others work fine
 */
template <class ArrayClass>
class array_iterator
  : public boost::iterator_facade<
        array_iterator<ArrayClass>
        , typename ArrayClass::iterator_value
      , boost::random_access_traversal_tag
    >
{
 public:
    array_iterator(ArrayClass& ar)
      : m_currentIndex(0), m_array(ar) {}

    explicit array_iterator(ArrayClass& ar, int index)
      : m_currentIndex(index), m_array(ar) {}

 private:
    friend class boost::iterator_core_access;

    void increment() 
    {     ++m_currentIndex; }

    void decrement() 
    {     --m_currentIndex;     }

    void advance(int n)    
    {    m_currentIndex += n;    }

    bool equal(array_iterator<ArrayClass> const& other) const
    {  return m_currentIndex == other.m_currentIndex;   }

    typename ArrayClass::iterator_value& dereference() const 
    { 
        if (m_currentIndex != m_array.actIndex)
            m_array.loadIndex(m_currentIndex); // set the propper state of the three if needed
        return m_array.in_memory[m_currentIndex]; 
    }

    int m_currentIndex;
    ArrayClass&  m_array;
};

enum class_types {
      e_void,
#for $i in $classes
#if  $i.isComposite()
  #if  $i.multiple
     #if  $i.rep_count == 1
      e_$i.getName(),
     #end if
   #else
      e_$i.getName(),
   #end if
#end if
#end for
};

//Forward definitions
#set base_generated = {}
#for $i in $classes
#if  $i.isComposite
#if $i.multiple and not $base_generated.has_key($i.element):
#set $base_generated[$i.element] = 1
class C$i.getName();
#end if
class $getClassName($i);
#end if
#end for

$body.generate($classes)

} //namespace db_${root_dep.getName()}


${hash}endif // DATAVIZ_${root_dep.getName()}_H
""")
        h_tmpl.classes = list_stats
        h_tmpl.root_dep = list_stats[len(list_stats) -1]
        h_tmpl.settings = settings
        h_tmpl.body = h_gen()


        h_file.write(str(h_tmpl))
        h_file.close()

        cc_tmpl = Template("""\
// this file is generated by Data2l
// do not change
// @see http://www.eccam.com

#set $hash="#"
${hash}include <iostream>
${hash}include "$settings["HeaderName"]"

namespace db_${root_dep.getName()} {

using namespace data2l;

$body.generate($classes)

} // namespace db_${root_dep.getName()}

""")
        cc_tmpl.classes = list_stats
        cc_tmpl.settings = settings
        cc_tmpl.root_dep = list_stats[len(list_stats) -1]
        cc_tmpl.body = cc_gen(settings)
        cc_file.write(str(cc_tmpl))
        cc_file.close()

    def getMenuString(self):
        return "Generate better C++ code"


    def getDefaultSettings(self,root_dep):
        def_settings = [("TargetDirectory",
                            ("Target directory", config.get('cpp_gen_directory'), "dir")),
                        ("HeaderName",
                            (".h file name", root_dep.getName()+".h", "string" )),
                        ("CPPName", (".cpp file name", root_dep.getName() +".cpp", "string")),
                        ("CapabilityValidate",("Capability validate",True, "bool"))
                       ]
        return def_settings


generator = CppGen2()

