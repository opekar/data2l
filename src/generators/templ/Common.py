## package generators.templ.Common
#  Common functions for generation.

__all__ = """\
    common_funcs
""".split()

common_funcs = """\
#extends Cheetah.Template
#implements respond
#import DataTypes as DT
#import BasicTypes as BT
#import Enum
###############
##
#def right_quotes($s)
#if \"\\x0A\" in $s or \"\\x0D\" in $s or \"'\" in $s or '"' in $s or '\\\\' in $s
\"""$s.replace('\\\\', '\\\\\\\\').replace('\"', '\\\\\"')\"""#slurp
#else
'$s'#slurp
#end if
#end def
###############
##
#def genGetter($name, $getter)
#if $getter is not None
$name = $right_quotes($getter.getText()), #slurp
#end if
#end def
###############
##
#def genStructElem($elem)
#if isinstance($elem, DT.Basic)
 #if isinstance($elem, DT.Attrib)
Attrib#slurp
 #else
Basic#slurp
 #end if
 #if isinstance($elem.basic_type, BT.String)
('$elem.getName()', String() )#slurp
 #elif isinstance($elem.basic_type, BT.StaticSizeArray)
('$elem.getName()', StaticSizeArray(${elem.type().basicElement}()))#slurp
 #else
('$elem.getName()', ${elem.type()}(#slurp
    #if isinstance($elem.type(), BT.BasicNumType) and $elem.type().getEnum() is not None
$elem.type().getEnum().getName()))#slurp
    #else
))#slurp
    #end if
 #end if
#elif isinstance($elem, DT.Void)
Void()#slurp
#else
$elem.getName()#slurp
#end if
#end def
################
##
#def genDependency($dep)
Dependency(#slurp
$genGetter("size", $dep.size)#slurp
$genGetter("offset", $dep.offset)#slurp
$genGetter("selector", $dep.selector)#slurp
$genGetter("onsave", $dep.onsave)#slurp
$genGetter("init", $dep.init)#slurp
#if $dep.in_memory
in_memory = True, #slurp
#end if
#if len($dep.getProperties()) >0:
customProperties = {
#for $k,$v in $dep.getProperties().iteritems():
"$k":"$v",
#end for
}
#end if  ## custom properties

)#slurp
#end def
################
##
#def genAssertion($ass)
Assertion(#slurp
#if $ass.getName() is not None
name = '$ass.getName()', code = $right_quotes($ass.getCode().getText()))#slurp
#else
$right_quotes($ass.getCode().getText()))#slurp
#end if
#end def
"""