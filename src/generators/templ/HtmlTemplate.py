
# generates the table of contents
templateContents = """    
    <h2>Contents List:</h2>
      <table>
      #for $row in $logic.getContents($queue[0], numColumns=4)
          <tr>
          #for $item in $row
              <td style="width : 25%"><a href="#$item" class="content">$item</a></td>
         #end for
         </tr>
      #end for
      </table>
"""

# generates the contents tree
templateTree = """    
    <h2>Contents Tree:</h2>
      <ul>
          $logic.initStack($dep)
          #set $level = -1
          #while not $logic.isStackEmpty()
             #set $top = $logic.popStack()
              #for i in $utils.range($level-$top[1]+1)
                  </ul></li>
              #end for
              <li>
              #set $uid = $logic.getUID()
              #if $utils.hasComplexAggregate($top[0])               
                #set $uidplus = $logic.getUID()
                <span class="treeplus" id="$uidplus" onclick="showOrHide('$uid','$uidplus')" >+</span>
              #else
                <span class="treebull">&bull;</span>   
              #end if                 
              <a href="#$top[0].getName()" class="content">$top[0].getName()</a>
                <ul style="display: none" id="$uid">
              #set $level = $top[1]
          #end while
      
          #for i in $utils.range($level+1)
            </ul></li>
          #end for
      </ul>
"""

template = """<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="generator" content="DataViz - www.eccam.com" />
    <title>$queue[0].getName()</title>
    <script type="text/javascript">
      function showOrHide(id, idt){
        el = document.getElementById(id).style;
        el.display=(el.display == 'block') ? 'none' : 'block';
        tl = document.getElementById(idt);
        tl.innerHTML = (el.display == 'block') ? '&minus;' : '+';
      } 
    </script>
    <style type="text/css">
        $style
    </style>    
  </head>
  <body>

    <h1>$queue[0].getName()</h1>
    
    $contents
    
    #while $utils.length($queue) > 0

      #set $dep = $queue.pop(0)

      <h2 id="$dep.getName()">$dep.getName() <span class="type">$utils.getTypeName($dep.getType())</span>
      #if $utils.length($utils.getParents($dep)) > 0
        <span class="type">contained in</span>
      #end if
      #for $parent in $utils.getParents($dep)
        <a class="parent" href="#$parent.getName()" title="$utils.getToolTipText($parent)">$parent.getName()</a>
      #end for
      </h2>

      #if $utils.length($dep.getElement().getDoc()) > 0
        <h3></h3>
        <p>$utils.formatHtmlParagraphs($dep.getElement().getDoc())</p>
      #end if

      <h3>Members:</h3>
        #set $i=0;
        #for $agg in $dep.getAggregates()
        #set $i=$i+1
        <table>
        #if  i%2==1:
          <tr bgcolor="#eaeaea">
        #else
          <tr bgcolor="#ffffff">
        #end if  
              
            <td class="name"> 
            #if $agg.isComposite()
              <a href="#$agg.getName()"> $agg.getName()</a>
            #else
               $agg.getName()
            #end if   
            </td>
            <td class="type"> $utils.getTypeName($agg.getType()) </td>
            <td class="comment"> 
            #if len($utils.getMinorComment($agg)) > 0
            <p class = "minor">$utils.getMinorComment($agg) &nbsp;</p>
            #end if
            #for $getter in $utils.getGetters($agg)
                $getter[0] <p class = "minor">$utils.highlightCode($getter[1])</p>
            #end for
             
            </td>
          </tr>
          </table>

          #if ($agg.isComposite() and (not $logic.isTabu($agg.getName())))  
            $queue.append($agg)
            $logic.tabuListAdd($agg.getName())
          #end if

        #end for      
      

    #end while  

    <p class="date"> generated on $utils.today() </p>

  </body>
</html>"""

