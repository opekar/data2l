import gtk, gobject
from gtkGUI.Config import config


__all__ = """\
ErrorDialog
""".split()


class CheckStructErrorDialog(object):
    def __init__(self, properties, title):
        self.properties = properties
        if title is None:
            title = "Check structure dialog"
        self.title = title

        xml = gtk.glade.XML(config.get('gui_path'),'ListViewDialog')
        self.dialog = xml.get_widget('ListViewDialog')
        self.dialog.set_title(self.title)
        self.list = xml.get_widget('listView')
        self.list_store = gtk.ListStore(str, str)

        prop_cell = gtk.CellRendererText()
        prop_col = gtk.TreeViewColumn('Error', prop_cell)
        prop_col.add_attribute(prop_cell, 'text', 0)
        self.list.append_column(prop_col)

        value_cell = gtk.CellRendererText()
        value_cell.set_property('editable', False)
        value_col = gtk.TreeViewColumn('Info text', value_cell)
        value_col.add_attribute(value_cell, 'text', 1)
        self.list.append_column(value_col)
        self.list.append_column(gtk.TreeViewColumn(' '))

        for k,v in properties.iteritems():
            self.list_store.append([k, v[0]])
        self.list.set_model(self.list_store)

    def run(self):
        response = self.dialog.run()
        return response

    def hide(self):
        self.dialog.hide()