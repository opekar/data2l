import DataTypes
import BasicTypes
import Dependency
import copy
from Utility import pure_virtual, xassert_type, xassert
from exprs.ExprVE  import *


__all__ = """\
ICommand
IUndoableCommand
EditGetter
EditInMemory
EditAssertion
DeleteAssertion
InsertElement
RenameElement
ChangeElement
EditByAccessors
AssignEnum
RenameEnum
GUICommand
""".split()


## Command pattern interface.
class ICommand(object):
    def do(self):
        pure_virtual()


## Undoable command interface.
class IUndoableCommand(ICommand):
    def undo(self):
        pure_virtual()


## Simple command editing object attributes.
class EditGetter(IUndoableCommand):
    def __init__(self, dep, name, code):
        ## Dependency to edit.
        self._dep = dep
        ## Name of getter to edit.
        self._name = name
        ## New ValueGetter instance for the named field.
        self._code = code
        ## Old value, for undo.
        self._old_code = None

    def do(self):
        self._old_code = getattr(self._dep, self._name)
        setattr(self._dep, self._name, self._code)

    def undo(self):
        setattr(self._dep, self._name, self._old_code)

class EditInMemory(IUndoableCommand):
    def __init__(self, dep, value):
        ## Dependency to edit.
        self._dep = dep
        ## New ValueGetter instance for the named field.
        self._value = value
        ## Old value, for undo.
        self._old_value= None

    def do(self):
        self._old_value = self._dep.in_memory
        self._dep.in_memory = self._value

    def undo(self):
        self._dep.in_memory = self._old_value

class CustomPropertiesAdd(IUndoableCommand):
    def __init__(self, dep, key, value):
        self._dep = dep
        self._key= key
        self._value = value
    def do(self):
        self._dep.addProperty(self._key, self._value)
    def undo(self):
        self._dep.delProperty(self._key)

class CustomPropertiesDel(IUndoableCommand):
    def __init__(self, dep, key):
        self._dep = dep
        self._key= key
    def do(self):
        self._value = self._dep.getProperty(self._key)
        self._dep.delProperty(self._key)
    def undo(self):
        self._dep.addProperty(self._key, self._value)

class CustomPropertiesValueChange(IUndoableCommand):
    def __init__(self, dep, key, new_value):
        self._dep = dep
        self._key= key
        self._old_value = dep.getProperty(key)
        self._new_value =new_value
    def do(self):
        self._dep.updateValueProperty(self._key, self._new_value)        
    def undo(self):
        self._dep.updateValueProperty(self._key, self._old_value)

class CustomPropertiesKeyChange(IUndoableCommand):
    def __init__(self, dep, old_key, new_key):
        self._dep = dep
        self._old_key= old_key
        self._new_key= new_key
    def do(self):
        self._dep.updateKeyProperty(self._old_key, self._new_key)        
    def undo(self):
        self._dep.updateKeyProperty(self._new_key, self._old_key) 
        

class ChangeBasicType(IUndoableCommand):
    def __init__(self, _dep, _new_basic_type):
        self.element = _dep.getType()
        xassert(isinstance(self.element,DataTypes.Basic))
        self.dep = _dep
        self.old_size_getter = self.dep.size
        if isinstance(_new_basic_type, BasicTypes.BasicArrayType):
            if self.dep.size is not None:
                self.new_size_getter = self.dep.size
            else:
                self.new_size_getter = ExprVE("1")
        else:
            self.new_size_getter = None
        self.new_basic_type = _new_basic_type 
        self.old_basic_type = self.element.type()
        
    def do(self):
        self.dep.size = self.new_size_getter
        self.element.set_type(self.new_basic_type)
        
    def undo(self):
        self.dep.size = self.old_size_getter        
        self.element.set_type(self.old_basic_type)

## Command editing Assertion objects in dependency.
class EditAssertion(IUndoableCommand):
    def __init__(self, dep, index, assertion):
        ## Dependency to be edited.
        self._dep = dep
        ## Index of dependency in the list
        self._index = index
        ## New assertion.
        self._assertion = assertion
        ## Old assertion.
        self._old_assertion = None

    def do(self):
        if self._index < len(self._dep.assertions):
            self._old_assertion = self._dep.assertions[self._index]
            self._dep.assertions[self._index] = self._assertion
        else:
            self._old_assertion = None
            self._dep.assertions.append(self._assertion)

    def undo(self):
        if self._old_assertion is None:
            self._dep.assertions.pop()
        else:
            self._dep.assertions[self._index] = self._old_assertion


## Command editing Assertion objects in dependency.
class DeleteAssertion(IUndoableCommand):
    def __init__(self, dep, index):
        ## Dependency to be edited.
        self._dep = dep
        ## Index of dependency in the list
        self._index = index
        ## Old assertion.
        self._old_assertion = None

    def do(self):
        self._old_assertion = self._dep.assertions.pop(self._index)

    def undo(self):
        self._dep.assertions.insert(self._index, self._old_assertion)


## Command adding/inserting fields to structure/array/alternative.
class InsertElement(IUndoableCommand):
    def __init__(self, dep, index, elem, new_dep = None):
        xassert_type(dep, Dependency.Dependency)
        xassert_type(elem, DataTypes.DataElement)
        ## Dependency associated with Composite that is being edited.
        self.dep = dep
        self.new_dep = new_dep;
        ## Index at which the new element will be inserted.
        self.index = index
        ## The element that is being inserted.
        self.elem = elem

    def do(self):
        composite = self.dep.getElement()
        if self.new_dep is None:
            elems_dep = Dependency.Dependency()
        else:
            elems_dep  = self.new_dep
        
        composite.addFieldAt(self.index, self.elem, elems_dep)

    def undo(self):
        composite = self.dep.getElement()
        composite.deleteAt(self.index)


class RenameElement(IUndoableCommand):
    def __init__(self, elem, text):
        xassert_type(elem, DataTypes.DataElement)

        self.elem = elem
        self.text = text
        self.old_text = elem.getName()

    def do(self):
        self.elem.setName(self.text)

    def undo(self):
        self.elem.setName(self.old_text)


## Command to delete field from structure/array/alternative.
class DeleteElement(IUndoableCommand):
    def __init__(self, dep, index):
        ## Dependency being edited.
        self._dep = dep
        ## Index from which we are deleting.
        self._index = index
        ## Cache for the deleted element.
        self._elem = None

    def do(self):
        composite = self._dep.getElement()
        aggregates = composite.getAggregates()
        deps = composite.getDeps()
        xassert(self._index < len(aggregates))
        xassert(self._index < len(deps))
        self._elem = (aggregates[self._index], deps[self._index])
        composite.deleteAt(self._index)

    def undo(self):
        composite = self._dep.getElement()
        elem, dep = self._elem
        composite.addFieldAt(self._index, elem, dep)


## Command that changes/swaps existing element for another.
class ChangeElement(IUndoableCommand):
    def __init__(self, dep, index, new_elem, new_dep = None):
        ## Dependency being edited.
        self._dep = dep
        ## Index of element being changed.
        self._index = index
        ## New element that will be placed at index instead of existing one.
        self._new_elem = (new_elem, new_dep)
        ## Cache for the old element.
        self._old_elem = None

    def do(self):
        composite = self._dep.getElement()
        aggregates = composite.getAggregates()
        deps = composite.getDeps()
        self._old_elem = (aggregates[self._index], deps[self._index])
        composite.deleteAt(self._index)
        new_elem, new_dep = self._new_elem
        composite.addFieldAt(self._index, new_elem, new_dep)

    def undo(self):
        composite = self._dep.getElement()
        composite.deleteAt(self._index)
        old_elem, old_deps = self._old_elem
        composite.addFieldAt(self._index, old_elem, old_deps)


## Command that edits objects using accessors.
class EditByAccessors(IUndoableCommand):
    def __init__(self, setter, getter, new_val):
        ## Setter accessor, expects one parameter that is the new value.
        self._setter = setter
        ## Getter accessor, expects no parameters, returns value.
        self._getter = getter
        ## New value to be set using setter.
        self._new_val = new_val
        ## Cache for old value.
        self._old_val = None

    def do(self):
        self._old_val = self._getter()
        self._setter(self._new_val)

    def undo(self):
        self._setter(self._old_val)

## Command that assigns enum to BasicNumType
class AssignEnum(IUndoableCommand):
    def __init__(self, basic_type, enum):
        ## BasicNumType
        self._basic_type = basic_type        
        ## New enum assigned to basic type
        self._new_enum = enum
        ## Cache for previous enum assigned to bysic type
        self._old_enum = None
    
    def do(self):
        self._old_enum = self._basic_type.getEnum()        
        self._basic_type.setEnum(self._new_enum)
    
    def undo(self):
        self._basic_type.setEnum(self._old_enum)

## Command than renames enum in giver UserEnumsHolder.
class RenameEnum(IUndoableCommand):
    def __init__(self, enums_holder, old_name, new_name):        
        self._enums_holder = enums_holder
        self._enum = None        
        self._new_name = new_name
        self._old_name = old_name 
        
    def do(self):
        self._enum = self._enums_holder.getEnum(self._old_name)
        xassert(self._enum is not None)
        self._enums_holder.renameEnum(self._enum, self._new_name)
        
    def undo(self):
        self._enums_holder.renameEnum(self._enum, self._old_name)

class ChangeEnumDefinition(IUndoableCommand):
    def __init__(self, enums_holder, enum_name, new_enum_list):
        self._enums_holder = enums_holder
        self._enum = None
        self._enum_name = enum_name
        self._new_enum_list = new_enum_list
        self._old_enum_list = None
    
    def do(self):
        self._enum = self._enums_holder.getEnum(self._enum_name)
        xassert(self._enum is not None)
        self._old_enum_list = self._enum.getList()
        self._enums_holder.setListToEnum(self._enum, self._new_enum_list)
        
    def undo(self):
        self._enums_holder.setListToEnum(self._enum, self._old_enum_list)

class AddEnum(IUndoableCommand):
    def __init__(self, enums_holder, enum):
        self._enums_holder = enums_holder
        self._new_enum = enum
    
    def do(self):
        self._enums_holder.addEnum(self._new_enum)
        
    def undo(self):
        self._enums_holder.removeEnum(self._new_enum)
        
class RemoveEnum(IUndoableCommand):
    def __init__(self, enums_holder, enum_name):
        self._enums_holder = enums_holder
        self._enum_name = enum_name
        self._enum = None
        
    def do(self):
        self._enum = self._enums_holder.getEnum(self._enum_name)
        xassert (self._enum is not None)
        self._enums_holder.removeEnum(self._enum)
        
    def undo(self):
        self._enums_holder.addEnum(self._enum)

class GUICommand(IUndoableCommand):
    def __init__(self, do_refresh = None, undo_refresh = None):
        self.sub_commands = []
        self.do_refresh = do_refresh
        self.undo_refresh = undo_refresh

    def do(self):
        for command, do_refresh, _ in self.sub_commands:
            command.do()
            if do_refresh is not None:
                do_refresh()
        if self.do_refresh is not None:
            self.do_refresh()
        return

    def undo(self):
        for command, _, undo_refresh in reversed(self.sub_commands):
            command.undo()
            if undo_refresh is not None:
                undo_refresh()
        if self.undo_refresh is not None:
            self.undo_refresh()
        return

    def add_sub_command(self, command, do_refresh = None,
                        undo_refresh = None):
        xassert_type(command, IUndoableCommand,
            "only undoable commands can be included to the macro list")
        if do_refresh is not None and undo_refresh is None:
            undo_refresh = do_refresh
        self.sub_commands.append((command, do_refresh, undo_refresh))
        return

    def get_sub_commands(self):
        return self.sub_commands
