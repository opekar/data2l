import os
import re
import platform
import yaml
import copy
from Utility import xassert


__all__ = """\
find_config_dir
prepend_unique
convert_convertible
Config
config
""".split()


## Default configuration.
_defaults = {}
_defaults['version'] = [0, 10, 0]
_defaults['recent_defs'] = []
_defaults['recent_data'] = []
_defaults['max_recent_files'] = 10
_defaults['last_inspect_dialog_path'] = '.' + os.sep
_defaults['last_open_def_dialog_path'] = '.' + os.sep
_defaults['last_save_def_dialog_path'] = '.' + os.sep
_defaults['last_validate_dialog_path'] = '.' + os.sep
_defaults['gui_path'] = os.path.join('gtkGUI', 'gui.glade')
_defaults['dot_exe'] = 'dot'
_defaults['dot_dest_file'] = 'dot.gif'
_defaults['cpp_gen_directory'] = '.' + os.sep
_defaults['filter_expressions'] = []
_defaults['max_filter_expressions'] = 30
_defaults['stack_filters'] = True
_defaults['custom_generators_path'] = '.' + os.sep + 'customGenerators'
_defaults['custom_readers_path'] = '..' + os.sep + 'customReaders'
_defaults['custom_generators_output_path'] = '.' + os.sep
_defaults['help_file'] = ''
_defaults['font_zoom'] = 1.0
_defaults['html_view_on_start'] = True


## Returns path to user config file.
def find_config_dir():
    osname = platform.system()
    env = os.environ
    if osname == "Windows":
        if "HOME" in env and os.path.isdir(env["HOME"]):
            return env["HOME"]
        if "LOCALAPPDATA" in env and os.path.isdir(env["LOCALAPPDATA"]):
            return env["LOCALAPPDATA"]        
        if "APPDATA" in env["APPDATA"] and os.path.isdir(env["APPDATA"]):
            return env["APPDATA"]
        if ("USERPROFILE" in env["USERPROFILE"]
              and os.path.isdir(env["USERPROFILE"]+"\\Application Data")):
            return env["USERPROFILE"]+"\\Application Data"
        if ("USERPROFILE" in env["USERPROFILE"]
              and os.path.isdir(env["USERPROFILE"])):
            return env["USERPROFILE"]
        else:
            print "!! configuration will be writtern to %s" % (env["WINDIR"]) 
            return env["WINDIR"]
    else:
        return env["HOME"]


## Prepends item to the list or moves existing item at first place,
# while keeping the size of the list lower or equal to limit.
def prepend_unique(list, item, limit):
    if item in list:
        index = list.index(item)
        del list[index : index + 1]
    list.insert(0, item)
    list[:] = list[0:limit]
    return list


## Matches strings that are considered to have true value.
_true_matcher = re.compile("^y(?:es)?|t(?:rue)?$", re.I)
## Matches strings that are considered to have false value.
_false_matcher = re.compile("^|no?|f(?:alse)$", re.I)

## Configuration helper function. Tries to convert val to given type using
# some sensible conversions and returns the converted value. If the conversion
# fails it returns None.
def convert_convertible(tp, val):
    xassert (isinstance(tp, type))
    converted = None
    try:
        if tp == type(val):
            converted = val
        elif tp == bool:
            if type(val) == str:
                val = val.strip()
                if _true_matcher.match(val) is not None:
                    return True
                elif _false_matcher.match(val) is not None:
                    return False
                conv_val = convert_convertible(int, val)
                if conv_val is not None:
                    return bool(conv_val)
                conv_val = convert_convertible(float, val)
                if conv_val is not None:
                    return bool(conv_val)
                return None
        else:
            converted = tp(val)
    except ValueError, e:
        converted = None
    except TypeError, e:
        converted = None
    return converted


## Helper function to save directory part of path into some Config variable.
def save_dirname_into(conf, var, filename):
    file_dir = filename
    if os.path.isfile(filename):
        file_dir = os.path.dirname(filename)
    file_dir = os.path.abspath(file_dir) + os.sep
    conf.set(var, file_dir)


## Class for handling configuration file.
class Config (object):
    def __init__(self):
        self.filename = None
        self.changed = False
        self.data = {}


    def get(self, opt):
        xassert(isinstance(opt, str))
        xassert(len(opt) > 0)
        if opt not in self.data:
            # Try to read default value or rise KeyError.
            self.data[opt] = _defaults[opt]
        return self.data[opt]


    def set(self, opt, val):
        xassert(isinstance(opt, str))
        xassert(len(opt) > 0)
        #if _defaults[opt] is not None:
            # Make sure the new value has appropriate type.
            #do not check the type
            #xassert(isinstance(val, type(_defaults[opt])))
        self.data[opt] = val
        self.changed = True
        return self


    def keys (self):
        return set(self.data.keys() + _defaults.keys())


    def _pick_file(self, filename):
        if filename is None:
            if self.filename is None:
                config_dir = find_config_dir()
                self.filename = os.path.join(config_dir, ".dataviz")
        else:
            self.filename = filename


    ## Loads configuration from config file.
    def load(self, filename = None):
        self._pick_file(filename)
        # Load existing configuration file.
        if os.path.isfile(self.filename):
            fh = open(self.filename, "rb")
            self.data = yaml.load(fh)
            fh.close()
            self.changed = False
            return self
        # Or create default configuration.
        else:
            for opt, val in _defaults.iteritems():
                self.data[opt] = copy.deepcopy(val)
            self.changed = False
            return self


    ## Dumps configuration to a file.
    def dump(self, filename = None):
        try:
            self._pick_file(filename)
            fh = open(self.filename, "wb")
            yaml.dump(self.data, fh, default_flow_style = False)
            fh.close()
            return self
        except IOError, e:
            print("Critical Error when saving configuration file %s\n%s" % (filename,str(e)))


## Global DataViz tool configuration.
config = Config()
