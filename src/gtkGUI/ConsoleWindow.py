import gtk
import pygtk
from gtkGUI.Config import (config, prepend_unique, save_dirname_into)



##
# Class is responsible for  displaying console messages which
# normally appear in debug console in  a separate window
#
#
class ConsoleWindow:
    def __init__(self, main_window):   
        self.main_window = main_window 
        
        self.dict = {
                     'on_consoleWindow_delete_event' : self.on_destroy,
                    }
        
        self.xml = gtk.glade.XML(config.get('gui_path'),'consoleWindow')
        self.xml.signal_autoconnect(self.dict)
        self.window = self.xml.get_widget('consoleWindow')
        self.textView = self.xml.get_widget('consoleTextView')
        self.buffer = self.textView.get_buffer()
        self.menu_item = self.main_window.get_widget('console_window')
        
        import sys
        ## comment these assignments to get outputs back to debug console
        #sys.stdout = self
        #sys.stderr = self        
        
    
    def write(self, string):
        self.buffer.insert(self.buffer.get_end_iter(), string)
        pass
    
    def on_destroy(self, widget, event = None):
        self.menu_item.set_active(False)
        self.window.hide()        
        return True
        
    def on_console_windows_activate(self, widget):
        if widget.get_active():
            self.window.show()
        else:
            self.window.hide()
        
    
    