import BasicTypes as BT
import DataTypes as DT
import ValueExtractors as VE
from exprs.ExprVE import ExprVE
from gtkGUI import Command, StructTree
from gtkGUI.Utils import string_from_buffer, validate_ident_with_err_dialog
from Utility import print_trace, xassert, xassert_type, impossible
import gtk
from GetterLangSyntaxHighlighter import GetterLangSyntaxHighlighter

__all__ = """\
DependenciesView
""".split()


class CustDepPropertiesView(object):
    def __init__(self,main,xml, dependencyView):
      self.main = main
      self.dependencyView = dependencyView
      self.dep = None

      self.custPropView = xml.get_widget("customPropView")
      self.list_store = gtk.ListStore(str, str)
      self.custPropView.set_model(self.list_store)

      keyCell= gtk.CellRendererText()
      keyCell.set_property("editable", True)
      keyCell.connect("edited", self.key_cell_edited, (self.list_store,0) )
      self.key_column = gtk.TreeViewColumn("Key     ",keyCell, text = 0)
      self.custPropView.append_column(self.key_column)

      valueCell= gtk.CellRendererText()
      valueCell.set_property("editable", True)
      valueCell.connect("edited", self.value_cell_edited, (self.list_store,1))
      self.value_column = gtk.TreeViewColumn("Value",valueCell, text = 1)
      self.custPropView.append_column(self.value_column)
      self.custPropView.connect('button_press_event', self.show_context_menu)


    def show_context_menu(self, widget, event):
        if event.button == 3:
            xassert(event.window == widget.get_bin_window())
            menu = None
            p = widget.get_path_at_pos( int(event.x), int(event.y) )
            if p is not None:
                widget.set_cursor(p[0])
                menu = self.buildMenu( with_delete = True )
            else:
                menu = self.buildMenu( with_delete = False )
            menu.show_all()
            menu.popup(None, None, None, 0, event.time)


    def buildMenu(self, with_delete):
        menu = gtk.Menu()
        mi = gtk.ImageMenuItem(stock_id = "gtk-add")
        mi.child.set_label("Add _Custom Property")
        mi.connect("activate", self.addProperty)
        menu.append(mi)
        
        mi = gtk.ImageMenuItem(stock_id = "gtk-add"                              )
        mi.child.set_label("Add _Predefined Property")        
        mi.set_submenu(self.buildPredefinedPropertyMenu())
        menu.append(mi)

        if with_delete:
            mi = gtk.ImageMenuItem(stock_id = "gtk-delete")
            mi.child.set_label("_Delete Custom Property")
            mi.connect("activate", self.deleteProperty)
            menu.append(mi)

        return menu
    def buildPredefinedPropertyMenu(self):
        menu = gtk.Menu()
        mi = gtk.MenuItem('cpp-block')
        mi.connect("activate", self.addProperty, ("cpp_ro_block","load_function_name"))
        menu.append(mi)
        
        mi = gtk.MenuItem('static-size')
        mi.connect("activate", self.addProperty, ("static_size","255"))
        menu.append(mi)

        menu.append(mi)
        
        return menu


    def Refresh(self,dep):
        self.dep = dep
        self.list_store.clear()
        for k,v in self.dep.getProperties().iteritems():
            self.list_store.append((k, v))


    def key_cell_edited(self,cell, path, new_text, user_data = None):
      list_store, column = user_data
      if not validate_ident_with_err_dialog(new_text):
          return
      command = Command.CustomPropertiesKeyChange(self.dep, list_store[path][column], new_text)
      self.main.do_command(command, self.dependencyView.refresh)

    def value_cell_edited(self,cell, path, new_text, user_data = None):
      list_store, column = user_data
      key = list_store[path][0]
      command = Command.CustomPropertiesValueChange(self.dep, key, new_text)
      self.main.do_command(command, self.dependencyView.refresh)

    def addProperty(self, item, user_data = None):      
      if user_data is None:
          command = Command.CustomPropertiesAdd(self.dep, "new_prop", "new value")
      else:          
          command = Command.CustomPropertiesAdd(self.dep, user_data[0], user_data[1])
      self.main.do_command(command, self.dependencyView.refresh)

    def deleteProperty(self, item):
      path, _ = self.custPropView.get_cursor()
      key = self.dep.getProperties().keys()[path[0]]
      command = Command.CustomPropertiesDel(self.dep, key)
      self.main.do_command(command, self.dependencyView.refresh)





## Controler pro zalozku Dependencies v hlavnim okne.
class DependenciesView(object):
    def __init__(self, main, xml, definition):
        self.main = main
        self.dep = None
        self.pathField = xml.get_widget('PathField')

        self.size_frame = xml.get_widget('size_frame')
        self.offset_frame = xml.get_widget('offset_frame')
        self.selector_frame = xml.get_widget('selector_frame')
        self.init_frame = xml.get_widget('init_frame')
        self.properties_frame = xml.get_widget('properties_frame')
        self.onsave_frame = xml.get_widget('onsave_frame')

        self.offsetTextView = xml.get_widget('OffsetTextView')
        self.sizeTextView = xml.get_widget('SizeTextView')
        self.selectorTextView = xml.get_widget('SelectorTextView')
        self.initTextView = xml.get_widget('InitTextView')
        self.onsaveTextView = xml.get_widget('OnSaveTextView')
        
        #the syntax highlighting module
        syntaxHighLighter = GetterLangSyntaxHighlighter()
        syntaxHighLighter.replaceBuffer(self.offsetTextView)
        syntaxHighLighter.replaceBuffer(self.sizeTextView)
        syntaxHighLighter.replaceBuffer(self.selectorTextView)
        syntaxHighLighter.replaceBuffer(self.initTextView)
        syntaxHighLighter.replaceBuffer(self.onsaveTextView)
        
        self.descrTextView = xml.get_widget('DescriptionTextView')
        self.custPropView = CustDepPropertiesView(main,xml,self)
        fields = [(self.offsetTextView, "offset"),
                  (self.sizeTextView, "size"),
                  (self.selectorTextView, "selector"),
                  (self.initTextView, "init"),
                  (self.descrTextView, "doc"),
                  (self.onsaveTextView, "onsave")]
        for tv, field in fields:
            self.register_text_view_callbacks(tv, field)
        self.tv_buf_changed_handle = None
        self.main.main_menu.connect("deactivate", self.main_menu_deactivate)

        self.in_memoryCheckbox = xml.get_widget('in_memory_check')
        self.in_memoryCheckbox.connect("released" , self.in_memory_toggle)

        self.tree = xml.get_widget('structuretree')

        ## Dictionary with descriptions/documentation.
        self.definition = definition
        self.tree.connect_object("cursor-changed", self.tree_cursor_changed,
                                 self)

        self._tv_focused = False
        self._tv = None
        self._tv_buffer = None
        self._tv_text = None
        self._tv_old_text = None
        self._focused_info = None


    def register_text_view_callbacks(self, tv, field):
        tv.connect("focus-out-event", self.tv_focus_out, tv, field)
        tv.connect("focus-in-event", self.tv_focus_in, tv, field)

    def main_menu_deactivate(self, menu_shell):
        #print_trace()
        if self._tv is None:
            return

        text_view, field = self._tv
        self._focus_out_action(text_view, field)

    def tv_focus_in(self, widget, event, text_view, field):
        #print_trace()
        path, _ = self.tree.get_cursor()
        if path is None:
            return
        else:
            model = self.tree.get_model()
            iter = model.get_iter(path)
            xassert(iter is not None)
            if iter is not None:
                self.dep = model[iter][StructTree.NAME_COL]
                if not self._tv_focused:
                    self._focused_info = (model[iter][StructTree.NAME_COL], path[-1])

        self._tv = (text_view, field)
        buf = text_view.get_buffer()
        self._tv_old_text = string_from_buffer(buf)
        self.tv_buf_changed_handle \
            = buf.connect("changed", self.tv_buffer_changed)
        self._tv_focused = True
        self._tv_buffer = buf

    def tv_focus_out(self, widget, event, text_view, field):
        #print_trace()
        self._focus_out_action (text_view, field)
        return

    def _focus_out_action(self, text_view, field):
        #print_trace()
        buf = text_view.get_buffer()
        if self._focused_info is not None:
            dep, index = self._focused_info
            code_text, self._tv_text = self._tv_text, None
            if code_text is None or code_text == self._tv_old_text:
                if self.tv_buf_changed_handle is not None:
                    buf.disconnect(self.tv_buf_changed_handle)
                    self.tv_buf_changed_handle = None
                self._tv_focused = False
                self._focused_info = None
                self._tv_text = None
                self._tv_buffer = None
                self._tv = None
                return

            command = None
            if field == "doc":
                elem = dep.getElement()
                command = Command.EditByAccessors(elem.setDoc, elem.getDoc,
                                                  code_text)
            else:
                # when we remove getter, we have to set the getter to None
                # not to a empty string which can't be evaluated
                if len( code_text.strip() ) > 0:
                    code = ExprVE(code_text)
                else:
                    code = None
                command = Command.EditGetter(dep, field, code)
            self.main.do_command(command, self.refresh)

        if self.tv_buf_changed_handle is not None:
            buf.disconnect(self.tv_buf_changed_handle)
            self.tv_buf_changed_handle = None
        self._tv_focused = False
        self._focused_info = None
        self._tv_text = None

        # Refresh after potential cursor change.
        path, _ = self.tree.get_cursor()
        self.refresh_at(path)
        return

    def tv_buffer_changed(self, buf):
        #print_trace()
        self._tv_text = string_from_buffer(buf)

    def tree_cursor_changed(self, widget, userdata = None):
        #print_trace()
        if self._tv_focused and self.tv_buf_changed_handle is not None:
            self._tv_buffer.disconnect(self.tv_buf_changed_handle)
            self.tv_buf_changed_handle = None

        path, _ = self.tree.get_cursor()
        if path is None:
            return
        else:
            model = self.tree.get_model()
            iter = model.get_iter(path)
            xassert(iter is not None)
            self.refresh_at(path)
            if not self._tv_focused:
                self._focused_info = (model[iter][StructTree.NAME_COL], path[-1])
        return

    def in_memory_toggle(self,widget, userdata =None):
        if self.dep == None:
            return
        val = self.in_memoryCheckbox.get_active();
        print self.dep.getName(), "toogled " , val
        command = Command.EditInMemory(self.dep, val)
        self.main.do_command(command, self.refresh)

    ## \TODO is it used ????
    def refresh(self):
        path, _ = self.tree.get_cursor()
        if path is None:
            return
        model = self.tree.get_model()
        iter = model.get_iter(path)
        dep = model[iter][StructTree.NAME_COL]

        self.refresh_at(path)
        self.custPropView.Refresh(dep)

    def setDefinition(self, definition):
        self.definition = definition

    ## Called whenever user changes selection in TreeView
    def refresh_at(self, path):
        if path is None:
            return

        model = self.tree.get_model()
        iter = model.get_iter(path)

        dep = model[iter][StructTree.NAME_COL]
        self.dep = dep
        parent_iter = model.iter_parent(iter)
        parent_dep = None
        if parent_iter is not None:
            parent_dep = model[parent_iter][StructTree.NAME_COL]

        elem = dep.getElement()
        if isinstance(elem, DT.Basic):
            self.selector_frame.hide()
            self.onsave_frame.show()
            if isinstance(elem, DT.Attrib):
                self.offset_frame.hide()
                self.init_frame.show()
            else:
                self.offset_frame.show()
                self.init_frame.hide()
            basic = elem.type()
            if (isinstance(basic, BT.VarInteger)
                or isinstance(basic, BT.BasicArrayType)):
                self.size_frame.show()
                self.onsave_frame.hide()
            else:
                self.size_frame.hide()
        elif isinstance(elem, DT.Composite):
            self.onsave_frame.hide()
            self.init_frame.hide()
            if dep is not self.definition.dep_root:
                self.offset_frame.show()
            else:
                self.offset_frame.hide()
            if isinstance(elem, DT.Array):
                self.selector_frame.hide()
                self.size_frame.show()
            elif isinstance(elem, DT.Struct):
                self.selector_frame.hide()
                self.size_frame.hide()
            elif isinstance(elem, DT.Alternative):
                self.selector_frame.show()
                self.size_frame.hide()
            else:
                impossible()

        self.in_memoryCheckbox.set_sensitive(False)
        #self.in_memoryCheckbox.hide()
        if parent_dep is not None:
            if isinstance(parent_dep.getType(), DT.Array):
                self.in_memoryCheckbox.set_sensitive(True)
                #self.in_memoryCheckbox.set_active(dep.in_memory)                
                #self.in_memoryCheckbox.show()



        self.RefreshField(dep.offset, self.offsetTextView)
        self.RefreshField(dep.size, self.sizeTextView)
        self.RefreshField(dep.selector, self.selectorTextView)
        self.RefreshField(dep.init, self.initTextView)
        self.RefreshField(dep.onsave, self.onsaveTextView)
        self.in_memoryCheckbox.set_active(dep.in_memory)

        buf = self.descrTextView.get_buffer()
        doc = dep.getElement().getDoc()
        buf.set_text(doc)

        self.RefreshPathField(dep)
        self.custPropView.Refresh(dep)

    def RefreshField(self, getter, textfield):
        buffer = textfield.get_buffer()
        if getter is None:
            buffer.set_text("")
        elif (isinstance(getter, ExprVE)):
            buffer.set_text(getter.text)
        elif (isinstance(getter, VE.Const)):
            buffer.set_text("%d" % (getter.getDOMValue(None)))

    def RefreshPathField(self,dep):
        str = ""
        d = dep
        str = d.getName()
        # @TODO pridat cestu ke koreni pomoci tree view modelu

        self.pathField.set_text(str)
