## @package gtkGUI.DirectoryChooser 
#  Allow select path to directory from dialog.

import platform
import gtk
import os
import string

__all__ = """\
    DirectoryChooser
""".split()

## Dialog for selecting path to folder.
# Show the native windows dialog if possible, otherwise show gtk.FileChooserDialog
# @see DirectoryChooserExample
class DirectoryChooser(object):
    def __init__(self, parent, title = "Select directory", curr_folder=".\\"):
        self.parent_window = parent
        self.title = title
        self.curr_folder = os.path.abspath(curr_folder)
        self.filename = ""
        
        if platform.system() == "Windows":
            try:
                import win32gui
                self.iswin = True
                return
            except ImportError:
                pass
        
        self.iswin = False

    def get_filename(self):
        return self.filename

    ## gtk.FileChooserDialog wants absolute path rather then relative path
    def set_current_folder(self, folder):
        self.curr_folder = os.path.abspath(folder)
    
    def destroy(self):
        if self.iswin:
            return
        else:
            self.chooser.destroy()
    
    ## Because of GetOpenFileNameW which takes away 
    # the main messageloop processing -> supresses refresh of main window     
    def OnTimer(self, first, second):
        while gtk.events_pending():
             gtk.main_iteration() 
        
    def do_windows(self):
        import win32gui, win32con
        import timer
        
        ret = gtk.RESPONSE_OK
        
        window_handle = None
        if self.parent_window is not None:
            handle = self.parent_window.window.handle 
        current_directory = os.getcwd() # save because of win32gui.GetOpenFileNamedoes change it
        filename = "Folder selection"
        self.timerId = timer.set_timer (250, self.OnTimer)
        try:    
            fname,customfilter,flags = win32gui.GetOpenFileNameW(
                        hwndOwner = window_handle,
                        InitialDir=self.curr_folder,
                        Flags=win32con.OFN_PATHMUSTEXIST | win32con.OFN_HIDEREADONLY,
                        File=filename, DefExt='',
                        Title=self.title,
                        Filter="Folders only",
                        CustomFilter="",
                        FilterIndex=1
                        )
            self.filename = fname[:string.rfind(fname, "\\")]
            
            if self.filename[len(self.filename)-1] == ":":
                self.filename += "\\"
                
        except Exception, e:
            ret = gtk.RESPONSE_CANCEL
        
        timer.kill_timer(self.timerId)  
        os.chdir(current_directory)  
        return ret
    
    def do_gtk(self):
        self.chooser = gtk.FileChooserDialog("Select ...", self.parent_window, 
                                        gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                        (gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_OPEN,gtk.RESPONSE_OK))
        
        self.chooser.set_filename(self.curr_folder)
        
        ret = self.chooser.run();
        
        if ret == gtk.RESPONSE_OK:
            self.filename = self.chooser.get_filename()                
        
        return ret
    
    def run(self):
        if self.iswin:
            return self.do_windows()
        else:
            return self.do_gtk()
        
def DirectoryChooserExample():
    dirChooser = DirectoryChooser(None, "C:\\")
    if dirChooser.run() == gtk.RESPONSE_OK:
        print dirChooser.get_filename()
    else:
        print "Dialog was canceled"
 
if __name__ == "__main__":
    DirectoryChooserExample()
    import gobject
    gobject.idle_add(gtk.main_quit) 
    gtk.main()
    
    
        
                