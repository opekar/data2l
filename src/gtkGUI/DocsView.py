import types
import gtk
import pango
import DataTypes as DT
from gtk.gdk import keyval_from_name
from gtkGUI.Tooltip import Tooltip
from exprs.ExprVE import ExprVE
from Definition import Definition
from gtkGUI.docs.DocsGen import DocsGen
from Utility import xassert, impossible, xassert_type, print_trace
from generators.DocGen import HtmlDocGen
from gtkGUI.FileChooser import FileChooser

__all__ = """\
DocsView
""".split()


def _elem_type_name_elaborate(dep):
    element = dep.getElement()
    xassert(isinstance(element, DT.DataElement))
    if isinstance(element, DT.Void):
        return "Void"
    elif isinstance(element, DT.Attrib):
        init = ""
        if isinstance(dep.init, ExprVE):
            init = dep.init.getText()
        return str(element.basic_type) + " attribute is " + init
    elif isinstance(element, DT.Basic):
        return str(element.basic_type)
    elif isinstance(element, DT.Struct):
        return "Struct"
    elif isinstance(element, DT.Array):
        size = ""
        if isinstance(dep.size, ExprVE):
            size = dep.size.getText()
        tmp = "Array [" + size + "] of ("
        tmp += str(", ").join([agg.getName()
                               for agg in element.getAggregates()])
        tmp += ")"
        return tmp
    elif isinstance(element, DT.Alternative):
        return "Alternative"
    else:
        # If this has been reached then there is a case missing above.
        impossible()


def _elem_type_name(element):
    xassert(isinstance(element, DT.DataElement))
    if isinstance(element, DT.Void):
        return "Void"
    elif isinstance(element, DT.Basic):
        return str(element.basic_type)
    elif isinstance(element, DT.Struct):
        return "Struct"
    elif isinstance(element, DT.Array):
        return "Array"
    elif isinstance(element, DT.Alternative):
        return "Alternative"
    else:
        # If this has been reached then there is a case missing above.
        impossible()


tags_table = gtk.TextTagTable()


## Helper function defining/inserting tags in our tags_table.
def _tag(name, **kwargs):
    tag = gtk.TextTag(name)
    for prop, v in kwargs.iteritems():
        prop = prop.replace("_", "-")
        tag.set_property(prop, v)
    tags_table.add(tag)
    return


# Define some tags with styles.
_tag("comment", foreground = "gray50", style = pango.STYLE_ITALIC)
_tag("type", foreground = "blue")
_tag("decl-body")
_tag("decl-body-comment", foreground = "gray50", style = pango.STYLE_ITALIC)
_tag("header-name", foreground = "black", weight = pango.WEIGHT_BOLD)
_tag("link")
_tag("anchor")


del _tag


# Define some keyboard constans.
KP_BACKSPACE = keyval_from_name("BackSpace")
KP_ENTER = keyval_from_name("KP_Enter")
KP_RETURN = keyval_from_name("Return")


# Define some mouse cursor appearances.
HAND_CURSOR = gtk.gdk.Cursor(gtk.gdk.HAND2)
REGULAR_CURSOR = gtk.gdk.Cursor(gtk.gdk.XTERM)


## Maximal size of comment that will be shown inline.
MAX_COMMENT_LEN = 100


## Controller class for Documentation tab in the main window.
# Fills the Documentation tab utilising the \c DocsGen class.
# It marks \c Composite declarations with marks named as "anchor-<name>".
# Instances of given \c Composite are then tagged using anonymous tag with
# the data item "ref" set to the <name>. The "ref" field is then used to
# find the marked definition of given \c Composite.
class DocsView(DocsGen):
    def __init__(self, xml):
        DocsGen.__init__(self)
        self.xml = xml
        self.window = xml.get_widget('mainWindow')
        self.notebook = xml.get_widget('notebook')
        self.docs_view = xml.get_widget('documentation_view')
        self.docs_view.set_buffer(gtk.TextBuffer(tags_table))
        self.buffer = self.docs_view.get_buffer()
        self.docs_page_num = self.notebook.page_num(self.docs_view)
        self.hovering_over_link = False
        self.tooltip = Tooltip()
        self._visited = set()
        self._iter = None
        self._tags_stack = None
        self._marks = set()
        self._last_mark = "start"
        self._bottom_docs = None
        self._docs_num = 0

        self.docs_view.connect("motion-notify-event",
                               self.motion_notify_event)
        self.docs_view.connect("key-press-event", self.key_press_event)
        self.docs_view.connect("event-after", self.event_after)
        self.docs_view.connect("visibility-notify-event",
                               self.visibility_notify_event)

        self.back_button = xml.get_widget("doc_back_button")
        self.back_button.set_sensitive(False)
        self.back_button.connect("clicked", self.doc_navigation, "back")

        self.refresh_button = xml.get_widget("doc_refresh_button")
        self.refresh_button.connect("clicked", self.doc_navigation,
                                    "refresh")
        
        self.generateHtmlBtn = xml.get_widget("GenerateHtmlButton")
        self.refresh_button.connect("clicked", self.html_generation,
                                    "generate html output")

        self.generate_htmt_button = xml.get_widget("GenerateHtmlButton")
        self.generate_htmt_button.connect("clicked", self.generate_structure_html, "html")

        self.forward_button = xml.get_widget("doc_forward_button")
        self.forward_button.set_sensitive(False)
        self.forward_button.connect("clicked", self.doc_navigation,
                                    "forward")

        self.back_stack = []
        self.forward_stack = []


    def generate_structure_html(self, _, action):
        fileChooser = FileChooser(self.window, "Save File...",[("HTML files",["*.html"])],"", FileChooser.SAVE)
        ret = fileChooser.run()
        if ret == gtk.RESPONSE_OK:        
            if self.get_definition() is None:
                return
            else:
                docgen = HtmlDocGen() 
                docgen.generateToFile(self.get_definition(), fileChooser.get_filename())


    def doc_navigation(self, _, action):
        if action == "back":
            xassert(len(self.back_stack) > 0)
            mark_name = self.back_stack.pop()
            mark = self.buffer.get_mark(mark_name)
            while mark is None and len(self.back_stack) > 0:
                mark_name = self.back_stack.pop()
                mark = self.buffer.get_mark(mark_name)
            if mark is not None:
                self.scroll_to_mark(self.docs_view, mark)
                self.forward_stack.append(self._last_mark)
                self._last_mark = mark_name
                self.forward_button.set_sensitive(True)
            if len(self.back_stack) == 0:
                self.back_button.set_sensitive(False)

        elif action == "refresh":
            self.buffer.begin_user_action()
            self.refresh()
            self.buffer.end_user_action()

        elif action == "forward":
            xassert(len(self.forward_stack) > 0)
            mark_name = self.forward_stack.pop()
            mark = self.buffer.get_mark(mark_name)
            while mark is None and len(self.forward_stack) > 0:
                mark_name = self.forward_stack.pop()
                mark = self.buffer.get_mark(mark_name)
            if mark is not None:
                self.scroll_to_mark(self.docs_view, mark)
                self.back_stack.append(self._last_mark)
                self._last_mark = mark_name
                self.back_button.set_sensitive(True)
            if len(self.forward_stack) == 0:
                self.forward_button.set_sensitive(False)

        else:
            impossible()

        return

    def html_generation(self):
        
        return

    def motion_notify_event(self, text_view, event):
        #print_trace()
        x, y = text_view.window_to_buffer_coords(gtk.TEXT_WINDOW_WIDGET,
            int(event.x), int(event.y))
        self.set_cursor_if_appropriate(text_view, event, x, y)
        text_view.window.get_pointer()
        return

    def set_cursor_if_appropriate(self, text_view, event, x, y):
        hovering = False

        buffer = text_view.get_buffer()
        iter = text_view.get_iter_at_location(x, y)

        ref = None
        tags = iter.get_tags()
        for tag in tags:
            ref = tag.get_data("ref")
            if ref is not None:
                hovering = True
                break

        if hovering != self.hovering_over_link:
            self.hovering_over_link = hovering

        win = text_view.get_window(gtk.TEXT_WINDOW_TEXT)
        if self.hovering_over_link:
            win.set_cursor(HAND_CURSOR)
            start_mark = self.buffer.get_mark("anchor-" + ref)
            start_iter = self.buffer.get_iter_at_mark(start_mark)
            end_mark = self.buffer.get_mark("end-anchor-" + ref)
            end_iter = self.buffer.get_iter_at_mark(end_mark)
            text = self.buffer.get_text(start_iter, end_iter)
            self.tooltip.set_label(text)
            self.tooltip.on_motion(event)
        else:
            win.set_cursor(REGULAR_CURSOR)
            self.tooltip.on_leave(event)
        return

    def key_press_event(self, text_view, event):
        if (event.keyval == KP_RETURN
            or event.keyval == KP_ENTER):
            buffer = text_view.get_buffer()
            iter = buffer.get_iter_at_mark(buffer.get_insert())
            self.follow_if_link(text_view, iter)
        elif (event.keyval == KP_BACKSPACE):
            if len(self.back_stack) > 0:
                self.doc_navigation(None, "back")
        return

    def event_after(self, text_view, event):
        if event.type != gtk.gdk.BUTTON_RELEASE:
            return
        if event.button != 1:
            return
        buffer = text_view.get_buffer()

        ret = buffer.get_selection_bounds()
        if ret is not None and len(ret) == 2:
            start, end = ret
            if start.get_offset() != end.get_offset():
                return

        x, y = text_view.window_to_buffer_coords(gtk.TEXT_WINDOW_WIDGET,
            int(event.x), int(event.y))
        iter = text_view.get_iter_at_location(x, y)

        self.follow_if_link(text_view, iter)
        return

    def visibility_notify_event(self, text_view, event):
        wx, wy, mod = text_view.window.get_pointer()
        bx, by = text_view.window_to_buffer_coords(gtk.TEXT_WINDOW_WIDGET,
                                                   wx, wy)
        self.set_cursor_if_appropriate (text_view, event, bx, by)
        return

    def scroll_to_mark(self, text_view, anchor_mark):
        anchor_iter = self.buffer.get_iter_at_mark(anchor_mark)
        text_view.scroll_to_mark(anchor_mark, 0, True, yalign = 0.05)
        self.buffer.select_range(anchor_iter, anchor_iter)
        self.tooltip.hide()
        return

    def follow_if_link(self, text_view, iter):
        tags = iter.get_tags()
        # Search given place for tag with "ref" data item.
        for tag in tags:
            ref = tag.get_data("ref")
            if ref is not None:
                buffer = text_view.get_buffer()
                anchor_name = "anchor-" + ref
                anchor_mark = buffer.get_mark(anchor_name)
                self.scroll_to_mark(text_view, anchor_mark)

                self.back_stack.append(self._last_mark)
                self.back_button.set_sensitive(True)
                self._last_mark = anchor_name
                if (len(self.forward_stack) > 0
                    and anchor_name != self.forward_stack[-1]):
                    self.forward_stack = []
                    self.forward_button.set_sensitive(False)

                break
        return
    
    ## called from StructTree when something is selected
    def scrollOnTreeViewSelect(self,name):
        mark = self.docs_view.get_buffer().get_mark("anchor-"+name)
        if mark is not None:
            self.scroll_to_mark(self.docs_view , mark)

    def set_definition(self, definition):
        #print_trace()
        xassert(isinstance(definition, Definition)
                or isinstance(definition, types.NoneType))
        DocsGen.set_definition(self, definition)
        self._visited = set()

    def refresh(self):
        self._visited = set()
        self.generate()
        return

    def generate(self):
        #print_trace()
        if self.get_definition() is None:
            return
        # Delete all old marks.
        [self.buffer.delete_mark(mark) for mark in self._marks
         if not mark.get_deleted()]
        # Empty the buffer.
        self.buffer.delete(self.buffer.get_start_iter(),
                           self.buffer.get_end_iter())
        self._iter = self.buffer.get_iter_at_offset(0)
        self._tags_stack = []
        self._marks = set()
        self._docs_num = 0
        DocsGen.generate(self)
        self._iter = None
        xassert(len(self._tags_stack) == 0)
        self._tags_stack = None
        self.buffer.select_range(self.buffer.get_start_iter(),
                                 self.buffer.get_start_iter())
        self.buffer.create_mark("start", self.buffer.get_start_iter(), True)
        return

    def decl_composite_begin(self, dep):
        self._bottom_docs = {}

    def decl_composite_end(self, dep):
        if len(self._bottom_docs) > 0:
            self._text("\n")

        nums = self._bottom_docs.keys()
        nums.sort()
        for num in nums:
            doc = self._bottom_docs[num]
            self._text(" ad " + `num` + ")\n", "comment")
            lines = doc.split("\n")
            text = ""
            if len(lines) > 0:
                text += str("\n").join([" " + line for line in lines])
            self._anchor("comment-" + str(num))
            self._text(text, "comment")
            self._end_anchor("comment-" + str(num))
            self._text("\n")

        if len(self._bottom_docs) > 0:
            self._text("\n")

        self.separator_entities()
        self._bottom_docs = None

    def decl_composite_header(self, dep):
        composite = dep.getElement()
        name = composite.getName()
        comment = composite.getDoc()
        text = ""
        if comment not in [None, ""]:
            comment = comment.rstrip()
            lines = comment.split("\n")
            if len(lines) > 0:
                text += str("\n").join([" " + line
                                        for line in lines])
                text += "\n"

        self._anchor(name)
        self._text(text, "comment")
        self._text(name, "header-name")
        self.separator_name_type()
        text = _elem_type_name(composite)
        self._text(text, "type")

        return

    def decl_composite_body(self, dep):
        composite = dep.getElement()
        name = composite.getName()
        aggs = composite.getAggregates()
        deps = composite.getDeps()
        xassert(len(aggs) == len(deps))
        # Generate the body.
        self._start("decl-body")
        DocsGen.decl_composite_body(self, dep)
        self._end("decl-body")
        self._end_anchor(name)
        return

    def use_composite_name(self, dep):
        agg = dep.getElement()
        name = agg.getName()
        self._text("    ")
        self._link_start(name)
        self._text(name)
        self._link_end()

    def _print_aggregates_list(self, aggs):
        it = iter(aggs)
        try:
            def _print_aggregate(agg):
                is_composite = isinstance(agg, DT.Composite)
                agg_name = agg.getName()
                if is_composite:
                    self._link_start(agg_name)

                self._text(agg_name)

                if is_composite:
                    self._link_end()
                return

            agg = it.next()
            _print_aggregate(agg)

            while True:
                agg = it.next()
                self._text(", ")
                _print_aggregate(agg)

        except StopIteration:
            pass

    def use_composite_type(self, dep):
        self._start("type")
        element = dep.getElement()
        if isinstance(element, DT.Struct):
            self._text("Struct")
        elif isinstance(element, DT.Array):
            size = ""
            if isinstance(dep.size, ExprVE):
                size = dep.size.getText()
            self._text("Array [" + size + "] of (")
            aggs = element.getAggregates()
            self._print_aggregates_list(aggs)
            self._text(")")

        elif isinstance(element, DT.Alternative):
            self._text("Alternative of (")
            aggs = element.getAggregates()
            self._print_aggregates_list(aggs)
            self._text(")")

        else:
            # If this has been reached then there is a case missing above.
            impossible()
        self._end()
        return

    def use_simple_name(self, dep):
        agg = dep.getElement()
        self._text("    ")
        self._text(agg.getName())

    def use_simple_type(self, dep):
        self._text(_elem_type_name_elaborate(dep), "type")
        element = dep.getElement()
        doc = element.getDoc()
        if doc not in ["", None]:
            self._text("\t")
            if len(doc) <= MAX_COMMENT_LEN and "\n" not in doc:
                self._text("# " + doc, "decl-body-comment")
            else:
                self._docs_num += 1
                self._bottom_docs[self._docs_num] = doc
                self._link_start("comment-" + `self._docs_num`)
                self._text("# ad " + `self._docs_num` + ")",
                           "decl-body-comment")
                self._link_end()
        return

    def separator_name_type(self):
        self._text(": ")

    def separator_header_body(self):
        self._text("\n")

    def separator_entities(self):
        self._text("\n")

    def separator_body_entities(self):
        self._text("\n")

    ## Helper function for output into buffer.
    def _text(self, text, tag = None):
        if tag is not None:
            self.buffer.insert_with_tags_by_name(self._iter, text, tag)
        else:
            self.buffer.insert(self._iter, text)
        return

    def _start(self, tag):
        start_mark = self.buffer.create_mark(None, self._iter, True)
        self._tags_stack.append((tag, start_mark))
        return

    def _end(self, t = None):
        xassert(len(self._tags_stack) > 0)
        tag, start_mark = self._tags_stack.pop()
        if t is not None:
            xassert (t == tag)
        start_iter = self.buffer.get_iter_at_mark(start_mark)
        self.buffer.delete_mark(start_mark)
        if isinstance(tag, types.StringType):
            self.buffer.apply_tag_by_name(tag, start_iter, self._iter)
        else:
            self.buffer.apply_tag(tag, start_iter, self._iter)
        return

    def _link_start(self, ref):
        link_tag = self.buffer.create_tag(None)
        link_tag.set_data("ref", ref)
        self._start(link_tag)
        return

    def _link_end(self):
        self._end()

    def _anchor(self, ref):
        anchor_mark = self.buffer.create_mark("anchor-" + ref, self._iter,
                                              True)
        self._marks.add(anchor_mark)

    def _end_anchor(self, ref):
        anchor_mark = self.buffer.create_mark("end-anchor-" + ref, self._iter,
                                              True)
        self._marks.add(anchor_mark)
