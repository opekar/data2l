import types
import gtk
import pango
import DataTypes as DT
from gtk.gdk import keyval_from_name
from gtkGUI.Tooltip import Tooltip
from exprs.ExprVE import ExprVE
from Definition import Definition
from gtkGUI.docs.DocsGen import DocsGen
from Utility import xassert, impossible, xassert_type, print_trace
from generators.DocGen import HtmlDocGen
from gtkGUI.FileChooser import FileChooser
from pygtkie import IEHtmlView
from pygtkie import IEHtmlViewCallback

from gtkGUI.Config import (config, prepend_unique, save_dirname_into) #for font zoom
import copy



class DocsView(IEHtmlViewCallback):
    def __init__(self, xml , main):
        self.xml = xml
        self.window = xml.get_widget('mainWindow')
        self.notebook = xml.get_widget('notebook')
        self.docs_view = xml.get_widget('documentation_view')
        parent = self.docs_view.get_parent()
        parent.remove(self.docs_view)
        self.HtmlView = IEHtmlView()
        self.HtmlView.show()
        self.HtmlView.setHtmlViewCallback(self)
        parent.add_with_viewport(self.HtmlView)
        parent.show_all()
        self.docgen = HtmlDocGen() 
        self.fontZoom=config.get("font_zoom");
            
        self.docs_page_num = self.notebook.page_num(self.docs_view)
        
        self.refresh_button = xml.get_widget("doc_refresh_button")
        self.refresh_button.connect("clicked", self.refresh )
        
        self.generate_htmt_button = xml.get_widget("GenerateHtmlButton")
        self.generate_htmt_button.connect("clicked", self.generate_structure_html, "html")
        
        xml.get_widget("ZoomInButton").connect("clicked", self.ZoomIn)
        xml.get_widget("ZoomOutButton").connect("clicked", self.ZoomOut)

        self.back_button = xml.get_widget("doc_back_button")
        self.back_button.connect("clicked", self.go_back)
        
        self.forward_button = xml.get_widget("doc_forward_button")
        self.forward_button.connect("clicked", self.go_forward)
        
        ## IE hack - because embedded IE doesnt return back the focus, we have to put it back manualy 
        self.main = main
        self.main.structTree.tree.connect('button_press_event', self.set_focus_back)
        self.main.notebook.connect("switch-page", self.set_focus_back)
        self.disable_automatic_scroll = False
        self.hist_queue = []
        self.hist_current = -1;
        self.set_fwd_back_sensitivity()
        
    def set_focus_back(self, widget, event, c1=None, c2=None):
        self.main.window.window.focus()            

    def generate_structure_html(self, _, action):
        fileChooser = FileChooser(self.window, "Save File...",[("HTML files",["*.html"])],"", FileChooser.SAVE)
        ret = fileChooser.run()
        if ret == gtk.RESPONSE_OK:        
            if self.definition  is None:
                return
            else:
                docgen = HtmlDocGen() 
                docgen.generateToFile(self.definition , fileChooser.get_filename())


    def scrollOnTreeViewSelect(self, element_name, add_to_history=True ):
        if self.disable_automatic_scroll:
            return
        htmldoc = self.HtmlView.GetDocument()
        el = None
        if htmldoc is not None:
            el = self.HtmlView.GetDocument().getElementById(element_name)
        if el is not None:
            el.scrollViewTo(True)
            if add_to_history:
                self.OnBeforeNavigate("fake#"+element_name)


    
    def refresh(self, widget=None):
        docgen = HtmlDocGen() 
        self.HtmlView.SetDocument(docgen.generate(self.definition,self.fontZoom))
        self.hist_queue = []
        self.hist_current = -1
        self.set_fwd_back_sensitivity()        
    
    def set_definition(self, new_definition):
        self.definition = new_definition

    def go_forward(self, widget, event=None, c1=None, c2=None):
        if self.hist_current < len(self.hist_queue) -1:
            self.hist_current +=1;
            self.scrollOnTreeViewSelect(self.hist_queue[self.hist_current], False)
            
        self.set_fwd_back_sensitivity()

                           
    def go_back(self, widget, event=None, c1=None, c2=None):
        if self.hist_current > 0 :
            self.hist_current -=1;
            self.scrollOnTreeViewSelect(self.hist_queue[self.hist_current], False)
        
        self.set_fwd_back_sensitivity()
        
    def set_fwd_back_sensitivity(self):
        self.forward_button.set_sensitive( (self.hist_current < len(self.hist_queue) -1) )    
        self.back_button.set_sensitive( self.hist_current> 0 )           
      
    def ZoomIn(self, widget, c0 = None, c1=None, c2=None):
#        
# all these attempts failed, so I play arount with HTML style instead :-((((((((
#
        #from html_ie.ie6_gen import *
 #       import win32com.client 
 #       import pythoncom
        #res =self.HtmlView.browser2.ExecWB(constants.OLECMDID_ZOOM, constants.OLECMDEXECOPT_DONTPROMPTUSER , 3, None )
#        br= self.HtmlView.browser2
#        tr=1
#        res =br.ExecWB(win32com.client.constants.OLECMDID_ZOOM, 2, 4, pythoncom.Empty)
 #       self.HtmlView.browser2.Document.ExecCommand("FontSize", False, "2")
        #res =self.HtmlView.browser2.ExecWB(63, constants.OLECMDEXECOPT_DONTPROMPTUSER , 2 )
 #       print "res %d  %x" %( res, res)
#        filename ="a"
#        res =self.HtmlView.browser2.ExecWB(
#              constants.OLECMDID_SAVEAS,
#              constants.OLECMDEXECOPT_DONTPROMPTUSER,
#              filename,
#              None) 
        self.fontZoom = self.fontZoom + 0.1
        self.HtmlView.SetDocument(self.docgen.generate(self.definition,self.fontZoom))
        config.set("font_zoom",self.fontZoom);
        config.dump()
        
    def ZoomOut(self, widget, c0 = None, c1=None, c2=None):
        if self.fontZoom < 0.2: 
            return         
        self.fontZoom = self.fontZoom - 0.1
        self.HtmlView.SetDocument(self.docgen.generate(self.definition,self.fontZoom))
        config.set("font_zoom",self.fontZoom);
        config.dump()
        
        
    def OnContextMenu(self, source_html_element):
        """ return True or False -false if you dont want the 
        default menu to be processed
        @param source_element None or IEHtmlElement instace"""
        
        if source_html_element is None:
            return False
        if source_html_element.getName() != "A":
            return False
       
        dum1,dum2,element2search = source_html_element.getAttr("href").encode('utf8').partition("#") 
        print "href attr " , element2search
        self.instance_list = [] # list of tuple (path string, path list)
        self.GetInstancesPath( self.definition.dep_root , element2search, [0], 
                               self.definition.dep_root.getName())
        
        menu = gtk.Menu()        
        mi = gtk.MenuItem(label = "Synchronize TreeView")
        submenu = gtk.Menu()       
        for path_str, path in self.instance_list:
              if len(path_str)>80:
                  smi = gtk.MenuItem(path_str[:38]+"..."+path_str[-38:])
              else:
                  smi = gtk.MenuItem(path_str)
              smi.connect("activate", self.select_in_tree, path )
              submenu.append(smi)
              
        mi.set_submenu(submenu)
        menu.append(mi)              
        
        #@TODO add copy functionality
        #mi = gtk.MenuItem(label = "Copy (TODO)")
        #menu.append(mi)
        
        
        menu.show_all()
        menu.popup(None, None, None, 0, 0)
        return False
    
    def select_in_tree(self,widget,path):
        self.disable_automatic_scroll = True
        self.main.structTree.select_element(tuple(path))
        self.disable_automatic_scroll = False
    
    def  GetInstancesPath(self, dep, element_name, cur_path, cur_path_string):
        if len(cur_path)>1:
            path =cur_path_string + "-" + dep.getName() 
        else:
            path = cur_path_string
        if dep.getName() == element_name:
            self.instance_list.append((path,cur_path))
            return # no recursivelyaddses structures
        for i,agg in enumerate(dep.getAggregates()):
            new_path=copy.copy(cur_path)
            new_path.append(i)
            self.GetInstancesPath(agg, element_name, new_path , path)
            
    def  OnBeforeNavigate(self, dest):
        """ For navigation history"""
        dum1,dum2,elementName = dest.partition("#")
        if len(self.hist_queue)-1 != self.hist_current:
            del self.hist_queue[ self.hist_current : len(self.hist_queue)-1]
            
        self.hist_queue.append(elementName)
        
        self.hist_current = len(self.hist_queue)-1 
        
        self.forward_button.set_sensitive( (self.hist_current < len(self.hist_queue) -1) )    
        self.back_button.set_sensitive( self.hist_current> 0 )              
            
            
        
       