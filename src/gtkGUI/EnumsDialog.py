## @package gtkGUI.EnumsDialog
#  Allow edit user defined enums

import gtk
import gtkGUI
from Definition import *
import DataTypes
import BasicTypes
from gtkGUI import Command
import copy
from Enum import Enum
from gtkGUI.Utils import validate_ident_with_err_dialog, string_from_buffer
from language.Parser import Parser
from Utility import xassert
from gtk.gdk import keyval_from_name
import gobject

all = """\
    EditEnumsDialog
""".split()

# Define some keyboard constans.
KP_ESC = keyval_from_name("Escape")

## This class implements functionality of the EnumsDialog.
class EditEnumsDialog(object):
    def __init__(self, definition):
        self.title = "Edit Enums"
        ## Due to check on deleting enum
        self.definition = definition
        ## Remember original enums to pass it to commands
        self.original_enums = definition.userDefinedEnums
        ## Deep copy of enums to be used as internal model
        self.enums = copy.deepcopy(definition.userDefinedEnums)        
        ## To this group is appended sub_commands when adding, editing or deleting Enums
        self.command_group = Command.GUICommand()
        
        # create dialog's enum to orig_enum mapping to assure consistency        
        self.enums_map = {}
        for idx, enum in enumerate(self.enums.enums_list):
            self.enums_map[enum] = self.original_enums.enums_list[idx]
                
    ## This methods wraps the dialog's run() method.
    def run(self):
        signal_dict = {
                     'on_add_button_clicked' : self.on_add_button,
                     'on_delete_button_clicked' : self.on_delete_button,
                      }
        xml = gtk.glade.XML(gtkGUI.Config.config.get('gui_path'),
                            'EnumsDialog')
        xml.signal_autoconnect(signal_dict)
        self._dialog = xml.get_widget('EnumsDialog')
        self._text_widget = xml.get_widget('enum_textview')
        self._list_widget = xml.get_widget('enum_listview')

        self._dialog.set_title(self.title)        

        self.list_store = gtk.ListStore(object)        
        self._list_widget.set_model(self.list_store)
        
        enumCell= gtk.CellRendererText()
        enumCell.set_property("editable", True)
        enumCell.connect("edited", self.enum_cell_edited, self.list_store)
        
        self._list_widget.insert_column_with_data_func(
            0, 'Enums:', enumCell, self.enumRenderer)
        
        self._cursor_change_handle = \
            self._list_widget.connect("cursor-changed", self.on_list_cursor_changed)
                        
        self._text_widget.connect("focus-in-event", self.on_textview_focus_in)
        self._text_widget.connect("focus-out-event", self.on_textview_focus_out)
        self._text_widget.connect("key-press-event", self.tv_esc_handler)
        # cache of enum definition stored on focus in event (for comparing when focus out)
        self._enum_def_cache = ""
        self._tv_focused = False
        self._selected_path = None
        self._change_error = False
        
        self.text_buffer = gtk.TextBuffer()
        self._text_widget.set_buffer(self.text_buffer)
        
        self.setEnums(self.enums.getEnumList())
        
        ret = self._dialog.run()
        return ret
    
    ## Renderer for enum's column in _list_widget
    def enumRenderer(self, column, cell_renderer, model, iter, user_data = None):
        enum = model[iter][0]
        cell_renderer.set_property('text', enum.getName())
    
    ## Calls dialog's hide() method.
    def hide(self):
        self._dialog.hide()
    
    ## Add new Enum.
    def on_add_button(self, widget):
        new_enum = Enum("new_enum_1",[])                
        i = 2
        while not self.enums.addEnum(new_enum):
            new_enum.enum_name = "new_enum_" + str(i)
            i += 1
                    
        self.list_store.append((new_enum,))
        
        command = Command.AddEnum(self.original_enums, new_enum)
        self.command_group.add_sub_command(command)
        
        # select new enum in listView (== select last row) and start edit it
        model = self._list_widget.get_model()
        iter = model.get_iter_first()
        while model.iter_next(iter) is not None:
            iter = model.iter_next(iter)
        path = model.get_path(iter)                
        self._list_widget.set_cursor(path, self._list_widget.get_column(0),start_editing=True)
        
    ## Delete selected Enum.
    def on_delete_button(self, widget):
        model, iter = self._list_widget.get_selection().get_selected()
        enum = model[iter][0]                 
        
        enumerated_elements = []
        if self.enums_map.has_key(enum):
            # check if some elements don't have assigned selected enum
            # it's needed to have original enum to check with definition
            # where changes are effected as late as dialog is confirmed
            orig_enum = self.enums_map[enum]
            visited = {}            
            self._find_enumerated_elements(self.definition.dep_root, orig_enum,
                                          visited, enumerated_elements)
            if len(enumerated_elements) > 0:
                dialog = gtk.MessageDialog(
                    None, gtk.DIALOG_MODAL, gtk.MESSAGE_QUESTION, gtk.BUTTONS_YES_NO,
                    "Some elemets have assigned currently selected enum.\n"
                    "Are you sure you want to delete selected enum and uasssign "
                    "it from associated elements?")
                ret = dialog.run()
                dialog.hide()
                if ret == gtk.RESPONSE_NO:
                    return
                
        if self.enums.removeEnum(enum):
            # usassign from associated elements  
            for element in enumerated_elements:
                command = Command.AssignEnum(element, None)
                self.command_group.add_sub_command(command)
            # delete enum
            self.list_store.remove(iter)
            self._selected_path = None
            command = Command.RemoveEnum(self.original_enums, enum.getName())            
            self.command_group.add_sub_command(command)                        
            # select next enum or previous enum if current was last
            if self.list_store.iter_is_valid(iter):                
                path = model.get_path(iter)
                self._list_widget.set_cursor(path)
                self._selected_path = path
            else:     
                iter = model.get_iter_first()
                if iter is None:
                    self.text_buffer.set_text("")                    
                    return
                while model.iter_next(iter) is not None:
                    iter = model.iter_next(iter)
                path = model.get_path(iter)                
                self._list_widget.set_cursor(path)
                self._selected_path = path
        
    ## Set data to model and show it in dialog.
    def setEnums(self, enum_list):
        self.list_store.clear()
        for enum in enum_list:
            self.list_store.append((enum,))

    ## Refresh enum definition in textView
    def refreshEnumDefinition(self, enum):
        text = ""
        enum_list = enum.getList()
        for enumerator,value in enum_list:
            text += str(enumerator) + " = " + str(value) + "\n"
        self.text_buffer.set_text(text)

    ## Enums dialog works only with internal model and creates command group.
    #  Only when commands are done, changes is passed to definition
    def getCommand(self):
        return self.command_group
    
    ## Handler of editing name of enum 
    def enum_cell_edited(self, cell, path, new_text, user_data = None):                
        if not validate_ident_with_err_dialog(new_text):
            return
        model = self._list_widget.get_model()
        iter = model.get_iter(path)
        enum = model[iter][0]
        old_text = enum.getName()
        if old_text == new_text:
            return        
        if self.enums.renameEnum(enum, new_text):
            command = Command.RenameEnum(self.original_enums, old_text, new_text)
            self.command_group.add_sub_command(command)
    
    ## Handler of enum selection change, show current enum's definition in textView.
    def on_list_cursor_changed(self, widget, data = None):               
        self.on_textview_focus_out(widget)
        if self._change_error == True: # definition error occured on textview focus out
            self._list_widget.disconnect(self._cursor_change_handle)
            self._list_widget.set_cursor(self._selected_path)
            self._cursor_change_handle = \
                self._list_widget.connect("cursor-changed", self.on_list_cursor_changed)
            gobject.timeout_add(20, lambda: self._text_widget.grab_focus())
            return
        model, iter = self._list_widget.get_selection().get_selected()
        if iter is not None:
            self._selected_path = model.get_path(iter)
            enum = model[iter][0]            
            self.refreshEnumDefinition(enum)
        else:
            self.text_buffer.set_text("")
            self._selected_path = None

    def on_textview_focus_in(self, widget, data = None):
        if self._change_error == False:
            self._enum_def_cache = string_from_buffer(self.text_buffer)
        self._tv_focused = True
        self._change_error = False        

    def on_textview_focus_out(self, widget, data = None):
        if not self._tv_focused or self._selected_path is None:
            return False
        self._tv_focused = False
        new_enum_def = string_from_buffer(self.text_buffer)
        if new_enum_def == self._enum_def_cache:
            return False
        enum_list = Parser.parseEnumeratorsAssignments(new_enum_def)
        if enum_list is None:            
            self._change_error = True
            msg = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR,
                            gtk.BUTTONS_OK_CANCEL,
                "Enum definition is incorrect.\n"
                "Please write definition in correct form, e.g.:\n"
                "\tCOLOR_BLACK = 0\n"
                "\tCOLOR_RED = 0xf00\n"
                "or press CANCEL to discard changes.")                            
            ret = msg.run()
            msg.hide()
            if ret == gtk.RESPONSE_OK:
                self._text_widget.grab_focus() # incorrect definition                
            else:
                self.text_buffer.set_text(self._enum_def_cache) # discard changes
            return False
        
        self._enum_def_cache = new_enum_def        
        xassert(self._selected_path is not None)
        model = self._list_widget.get_model()
        iter = model.get_iter(self._selected_path)
        enum = model[iter][0]
        xassert(enum is not None)
        self.enums.setListToEnum(enum, enum_list)                
        
        command = Command.ChangeEnumDefinition(self.original_enums, enum.getName(), enum_list)
        self.command_group.add_sub_command(command)
        
        self.refreshEnumDefinition(enum)
        
    def tv_esc_handler(self, text_view, event):        
        if event.keyval == KP_ESC:
            # return to original definition
            self.text_buffer.set_text(self._enum_def_cache)
            return True
        return False
    
    # TODO:    Similar method as find_influenced_getters in StructTree
    #          Shouldn't it be unified? 
    def _find_enumerated_elements(self, dep, enum, visited, list):
        if visited.has_key(dep):
            return
        visited[dep] = 1
        
        if (isinstance(dep.getType(),DataTypes.Basic)
            and isinstance(dep.getType().type(),BasicTypes.BasicNumType)):            
            if enum == dep.getType().type().getEnum():            
                list.append(dep.getType().type())
        
        for agg in dep.getType().getDeps():
            self._find_enumerated_elements(agg, enum, visited, list)
