import DOM
import SAX
from gtkGUI import TextViewDialog


__all__ = """\
format_dom_trace
format_sax_trace
sax_trace_elements
handle_EParseError
""".split()


## This function produces text of stack trace of DOM objects for error dialog.
def format_dom_trace(trace):
    debug_infos = []
    for rec in trace:
        frame = rec[0]
        obj = frame.f_locals.get("self")
        if (obj is not None and isinstance(obj, DOM.DOMElement)):
            s = obj.debug_string()
            debug_infos.append(s)
        del frame
    del trace

    # Remove consecutive duplicate lines.
    lines = _list_nub(debug_infos)
    dialog_text = str("\n").join(lines)
    dialog_text += "\n"

    return dialog_text


## This function produces text of stack trace of DOM objects from stack during
# parsing using SAX.SAXParser.
def format_sax_trace(trace):
    debug_infos = []
    for rec in trace:
        frame = rec[0]
        inst = frame.f_locals.get("self")
        obj = frame.f_locals.get("dom")
        if (inst is not None and isinstance(inst, SAX.SAXParser)
            and obj is not None and isinstance(obj, DOM.DOMElement)):
            s = obj.debug_string()
            debug_infos.append(s)
        del frame
    del trace

    # Remove consecutive duplicate lines.
    lines = _list_nub(debug_infos)
    dialog_text = str("\n").join(lines)
    dialog_text += "\n"

    return dialog_text


## Returns list of DOM.DOMElements on the stack during SAX parsing.
def sax_trace_elements(trace):
    debug_infos = []
    for rec in trace:
        frame = rec[0]
        inst = frame.f_locals.get("self")
        obj = frame.f_locals.get("dom")
        if (inst is not None and isinstance(inst, SAX.SAXParser)
            and obj is not None and isinstance(obj, DOM.DOMElement)):
            debug_infos.append(obj)
        del frame
    del trace

    # Remove consecutive duplicate objects.
    _list_nub(debug_infos)
    return debug_infos


## Removes consecutive duplicates. The src list should not contain any Nones.
def _list_nub(src, cmp = None):
    if cmp is None:
        cmp = lambda a, b: a == b
    lines = []
    prev_line = None
    for l in src:
        if not cmp(l, prev_line):
            lines.append(l)
            prev_line = l
    return lines


def handle_EParseError(e):
    error_text = ""
    if e.token == "#ERR#":
        error_text = "Unrecognized token."
    elif e.token == "$EOF$":
        error_text = "Unexpected end of input."
    else:
        error_text = "Unexpected token."

    dialog = TextViewDialog.TextViewDialog(
        "Error", "Error parsing filter expression:",
        "%s\nString: '%s'\nToken: '%s'\nInput: '%s'\n"
        % (error_text, e.string, e.token, e.text))
    dialog.run()
    dialog.hide()
    return
