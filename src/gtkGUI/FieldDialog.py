import gtk
from gtkGUI.Config import config


__all__ = """\
FieldDialog
""".split()


'''
funkce tridy by mela byt

-vstup field edit, nebo nic
-vystup zeditovany field


'''
class FieldDialog(object):
    def __init__(self, glade_file, field = None):
        self.field = field
        self.dialog = gtk.glade.XML(config.get('gui_path'),
                                    'fieldEditDialog').get_widget('fieldEditDialog')

    def  run(self):
        self.dialog.run()
