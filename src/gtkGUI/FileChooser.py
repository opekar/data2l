import platform, os, sys, threading, thread
import gtk, gobject



__all__ = """\
WinFileFilter
FileChooser
""".split()


## Helper class for Win32 variant of the FileChooser.
class WinFileFilter(object):
    def __init__(self,name,patternarray):
        self.name = name + " (" + ", ".join(patternarray) + ")"
        self.pattern = ";".join(patternarray)

    def __repr__(self):
        return '%s\0%s' % (self.name, self.pattern)


## FileChooser class that wraps around gtk.FileChooser or Win32 standard dialog.
# Copied from: http://www.daa.com.au/pipermail/pygtk/2006-March/011924.html
#
# Example:
# \code
# f = FileChooser()
# f.add_filter("ASCEND files",["*.a4c","*.a4l"])
# print "SELECTED FILE",f.do()
# \endcode
class FileChooser(object):
    
    ## Dialog types
    OPEN = 0    ## for open dialog
    SAVE = 1    ## for save dialog
    
    def __init__(self, parent, title = "Open", filter = None , filename = "", dlgType = 0):
        self.ext = {}
        self.title = title
        self.filters = []
        self.filename = filename
        self.parent_window = parent
        self.curr_folder = None
        self.dlgType = dlgType
                 
        if platform.system()=="Windows":
            try:
                import win32gui
                self.iswin = True
                if filter is not None:
                    for f in filter:
                        self.add_filter( f[0], f[1] )
                return
            except ImportError:
                pass
            
        self.iswin = False
        
        if self.dlgType == FileChooser.OPEN:                
            self.chooser = gtk.FileChooserDialog(title, parent, 
                 action=gtk.FILE_CHOOSER_ACTION_OPEN, 
                 buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
        else:
            self.chooser = gtk.FileChooserDialog(title, parent, 
                 action=gtk.FILE_CHOOSER_ACTION_SAVE, 
                 buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_SAVE,gtk.RESPONSE_OK))
        if filter is not None:
            for f in filter:
                self.add_filter( f[0], f[1] )

    def add_filter(self, name, patternarray):
        if self.iswin:
            self.filters.append(WinFileFilter(name, patternarray))
        else:
            _f = gtk.FileFilter()
            _f.set_name(name)
            for _p in patternarray:
                _f.add_pattern(_p)
            self.chooser.add_filter(_f)

    def get_filename(self):
        return self.filename

    def set_current_folder(self, folder):
        if self.iswin:
            self.curr_folder = folder
        else:
            self.chooser.set_current_folder(folder)

    def set_local_only(self, val):
        if self.iswin:
            return
        else:
            self.chooser.set_local_only(val)

    def run(self):
        if self.iswin:
            return self.do_windows()
        else:
            return self.do_gtk()  

    def destroy(self):
        if self.iswin:
            return
        else:
            self.chooser.destroy()
  
            
    ## Because of GetOpenFileNameW which takes away 
    # the main messageloop processing -> supresses refresh of main window     
    def OnTimer(self, first, second):
        while gtk.events_pending():
             gtk.main_iteration()    

    def do_windows(self):
        import win32gui, win32con
        import timer
        
        _fa = []
        for _f in self.filters:
            _fa.append(repr(_f))
        filter='\0'.join(_fa)+'\0'
        customfilter='All files (*.*)\0*.*\0'
        ret = gtk.RESPONSE_OK

        initDir = os.getcwd()
        push_dir = os.getcwd()
        if self.curr_folder is not None:
           initDir = self.curr_folder   
                      
        self.timerId = timer.set_timer (250, self.OnTimer)
        try:
            if self.dlgType == FileChooser.OPEN:
                fname,customfilter,flags = win32gui.GetOpenFileNameW(
                    hwndOwner = self.parent_window.window.handle,
                    InitialDir=initDir,
                    Flags=win32con.OFN_EXPLORER | win32con.OFN_HIDEREADONLY,
                    File=self.filename, DefExt='py',
                    Title=self.title,
                    Filter=filter,
                    CustomFilter=customfilter,
                    FilterIndex=1
                    )
            else:
                fname,customfilter,flags = win32gui.GetSaveFileNameW(
                    hwndOwner = self.parent_window.window.handle,
                    InitialDir=initDir,
                    Flags=win32con.OFN_EXPLORER | win32con.OFN_HIDEREADONLY,
                    File=self.filename, DefExt='py',
                    Title=self.title,
                    Filter=filter,
                    CustomFilter=customfilter,
                    FilterIndex=1
                    )                
            self.filename = fname.encode('utf8')
            (no_null_str, dum1, dum2) = self.filename.partition('\0')
            self.filename = no_null_str
            os.chdir(push_dir)
        except Exception, e:
            ret = gtk.RESPONSE_CANCEL
        
        timer.kill_timer(self.timerId)
        return ret


    def do_gtk(self):
        self.add_filter("Other files",["*.*"])
        response = self.chooser.run()
        self.filename = self.chooser.get_filename()
        self.chooser.destroy()
        return response
