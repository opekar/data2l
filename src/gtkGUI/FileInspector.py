import gtk, gobject
import sys
import copy
import inspect
import traceback
import exprs
import DataTypes as DT
from gtkGUI import InspectBox, HexView, Errors, TextViewDialog, HighlightManager
from DOM import DOMStruct, DOMElement, DOMException
from gtkGUI.Config import config
from Reader import ReaderA, SafeReader, SafeReaderException, MmapReader
from DataTypes import Dependency
from Utility import xassert, hexstr_to_int
from InteractiveConsole import GTKInterpreterConsole


import platform
if platform.system()=="Windows":
    FONT = "Lucida Console 9"
else:
    FONT = "Luxi Mono 10"


__all__ = """\
FileInspector
""".split()

       

class FileInspector(object):
    def __init__(self):
        self.lastx = 0
        self.lasty = 50
        self.inspectBoxCreator = InspectBox.InspectBoxCreator(self)
        self.under_move = False
        self.moving_widget = None
        self.motion_handler_id = None
        xml_inspect = gtk.glade.XML(config.get('gui_path'), 'inspectMenu')
        self.inspectMenu = xml_inspect.get_widget('inspectMenu')
        self.glade = gtk.glade.XML(config.get('gui_path'), 'fileInspect')
        self.window = self.glade.get_widget('fileInspect')

        self.offset_entry_form = 'H'
        self.offset_entry = self.glade.get_widget('offset_entry')

        self.consoleWindow = self.glade.get_widget('scrolledwindow9')


        self.console = GTKInterpreterConsole() 
        self.consoleWindow.add_with_viewport(self.console)
        self.console.show()
         
        self.offset_entry.connect('activate', self.hex_entry_activate)
        self.offset_hex = self.glade.get_widget('offset_hex')
        self.offset_hex.connect('toggled', self.hex_toggle)

        children = self.inspectMenu.get_children()
   
        sync_submenu = children[2].get_submenu()
        sync_submenu.get_children()[0].connect_object('activate', self.sync_handler, self, 0)
        sync_submenu.get_children()[1].connect_object('activate', self.sync_handler, self, 1)
        sync_submenu.get_children()[2].connect_object('activate', self.sync_handler, self, 2)

        children[3].connect_object('activate', self.toggle_handler, self)

        self.dep = None
        self.dom = None
        self.readerCreator = None

    ## hex view
    def hex_toggle(self, button):
        if button.get_active():
            self.offset_entry_form = 'H'
        else:
            self.offset_entry_form = 'D'

    ## hex view
    def hex_entry_activate(self, entry):
        text = entry.get_text()
        xassert(self.offset_entry_form in set('HD'))
        if self.offset_entry_form == 'H':
            num = hexstr_to_int(text)
            if num == -1:
                entry.set_text("!hex: " + text)
            else:
                self.hexView.showOffset(num, 16)
        else:
            try:
                num = int(text)
                self.hexView.showOffset(num, 16)
            except ValueError:
                entry.set_text("!num: " + text)

    def toggle_handler(self, widget, user_param = None):
        self.currentInspectBox.toggleUntoggle()   

    def sync_handler(self, widget, user_param):
        dom = self.currentInspectBox.dom_element
        # give an array of tuples
        element_size = dom.getByteSize()
        if element_size < 0: element_size = 1
        
        # now prepare arrays for highlighting
        if (isinstance(dom.getDataType(), DT.Array) or 
           isinstance(dom.getDataType(), DT.Basic)):
            hex_arr = [(dom.akt_offs, dom.akt_offs + element_size)]
            inspect_arr = [(self.currentInspectBox, dom.akt_offs)]
            
        elif isinstance(dom.getDataType(), DT.Struct):
            hex_arr = []
            inspect_arr = [(self.currentInspectBox, self.currentInspectBox.dom_element.akt_offs)] 
            
            boxes = self.currentInspectBox.fields                
            boxes = filter((lambda x : isinstance(x.dom_element.getDataType(),DT.Basic)), boxes)
            
            for ib in boxes:
                dom = ib.dom_element
                
                if dom.akt_offs is None: continue # no atributes
                
                element_size = dom.getByteSize()
                if element_size < 0: element_size = 1
                
                hex_arr.append((dom.akt_offs, dom.akt_offs + element_size))
                inspect_arr.append((ib, dom.akt_offs))
            if len(boxes) == 0:
                hex_arr.append((dom.akt_offs, dom.akt_offs + 1))
        else:
            print "Highlight warning: Unexpected element"
            hex_arr = []
            inspect_arr = []
        
        self.hexHlManager.highlight(hex_arr, user_param)
        self.inspectHlManager.highlight(inspect_arr, user_param)

    def press_handler(self, widget, event, cb = None):
        #print "press_handler, button", event.button
        # Left button.
        if event.button == 1:
            widget = widget.getBox()

            while widget.get_parent() != self.fixed:
                widget = widget.get_parent()
            self.x = event.x_root
            self.y = event.y_root
            self.under_move = True
            self.moving_widget = widget
            self.motion_handler_id = self.fixed.connect('motion-notify-event',
                                                        self.motion_handler)
            return True
        # Right button.
        elif event.button == 3:
            self.currentInspectBox = cb
            l=self.inspectMenu.get_children()[0].get_child()
            l.set_text("%s element" % (self.currentInspectBox.dom_element.getDataType().getName()))
            if not self.currentInspectBox.untogglable():
                self.inspectMenu.get_children()[3].set_sensitive(False)
            else:
                self.inspectMenu.get_children()[3].set_sensitive(True)
            
            if (self.currentInspectBox.dom_element.akt_offs is None or
                isinstance(self.currentInspectBox.dom_element.getDataType(),
                           DT.Alternative) or
                isinstance(self.currentInspectBox.dom_element.getDataType(),
                           DT.Void)): 
                self.inspectMenu.get_children()[2].set_sensitive(False)
            else:
                self.inspectMenu.get_children()[2].set_sensitive(True)
            
            
            self.inspectMenu.popup(None, None, None, event.button, event.time)
            # Return True so that this event doesn't get propagated into
            # enclosing event box.
            return True
        else:
            return False

    def motion_handler(self, widget, event):
        if self.under_move:
            eb = self.moving_widget
            self.fixed.move(eb, eb.allocation.x + int(event.x_root - self.x),
                            eb.allocation.y + int(event.y_root - self.y))
            self.x, self.y = event.x_root, event.y_root

    def release_handler(self, widget, event, cb = None):
        if event.button == 1:
            self.under_move = False
            self.moving_widget = None
            self.fixed.disconnect(self.motion_handler_id)
            self.motion_handler_id = None
            self.x = None
            self.y = None
            return True


    def inspect_file(self, file_name, struct_definition, compiled , ReaderCreator = None):
        self.setTitle(file_name)
        self.readerCreator = ReaderCreator
        self.fixed = self.glade.get_widget('fixedPane')
        try:
            if (self.readerCreator is None):
                f = SafeReader(ReaderA(file_name), throw_exceptions = True)
            else:
                f = SafeReader(self.readerCreator.createReader(file_name), 
                                                   throw_exceptions = True)
            self.dom = DOMStruct(f, compiled.dep_root , None)
            self.dom.Init()
                           
            self.console.addLocal("root",self.dom)
            
            self.hexView = HexView.HexView(f, self.glade.get_widget('hexView'),
                                           self.glade.get_widget('hexViewScroll'))

            inspectBox = self.inspectBoxCreator.create(self.dom)
            inspectFrame = inspectBox.getBox()
            self.fixed.put(inspectFrame, 50, 50)
            inspectBox.expand(True)
            inspectFrame.show()
            
            def hexHighlightFunc(arr, color):
                self.hexView.setHighlight(arr, color),
                self.hexView.showSelections(arr, color),
                self.hexView.highlightSelections()
                
            def hexUnhighlightFunc(arr):
                self.hexView.unhighlightSelection(arr)
            
            def inspectHighlightFunc(arr, color):
                for elem, offs in arr:
                    elem.highlight(offs, color)
            
            def inspectUnhighlightFunc(arr):
                for ib, offs in arr:
                    ib.unhighlight(offs)
            
            self.hexHlManager = HighlightManager.HighlightManager(
                hexHighlightFunc, hexUnhighlightFunc, [0,1,2])
            
            self.inspectHlManager = HighlightManager.HighlightManager(
                inspectHighlightFunc, inspectUnhighlightFunc)
            
        except SafeReaderException, e:
            self.window.hide()
            trace = None
            dialog_text = ""
            try:
                trace = inspect.trace()
                dialog_text = Errors.format_dom_trace(trace)
            finally:
                del trace
            dialog = TextViewDialog.TextViewDialog(title = "Error",
                                                   label = "Error context:",
                                                   text = dialog_text)
            dialog.run()
            dialog.hide()
            return
        except DOMException, e:
            self.window.hide()
            info = None
            error_dialog = TextViewDialog.TextViewDialog("Error", "Error context:",
                "The error during the file inspection .\n "+ str(e))
            error_dialog.run()
            error_dialog.hide()
            return
        except Exception, e:
            self.window.hide()
            info = None
            s = ""
            try:
                info = sys.exc_info()
                s = traceback.format_tb(info[2])
            finally:
                del info
            s = str("").join(s)
            error_dialog = TextViewDialog.TextViewDialog("Error", "Traceback:",
                "Unhandled exception in file inspector.\n"
                + str(e) + "\n\n" + s)
            error_dialog.run()
            error_dialog.hide()
            return


    def setTitle(self, filename):
        if len(filename) >0:
            self.window.set_title("File Inspector - "+ filename )
        else:
            self.window.set_title("File Inspector" )

