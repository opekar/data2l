import gtk
import DOM
import ValueExtractors as VE
from gtkGUI import Config
from Utility import xassert


__all__ = """\
FilteredDOMArray
FilterDialog
""".split()


## This class mimicks DOMArray or DOMMemoryArray and filters available elements
# by some rule.
class FilteredDOMArray(object):
    def __init__(self, dom_array, filter):
        xassert(isinstance(dom_array, DOM.DOMArray))
        xassert(isinstance(filter, VE.IQueryDOMValue))

        ## Slave array.
        self.dom_array = dom_array
        self.akt_offs = dom_array.akt_offs
        ## Value getter that represents filter.
        self.filter = filter
        ## List of indexes passed through the filter.
        self.valid_indexes = None

        self._filter_indexes()
        

    def get_filter(self):
        return self.filter

    def set_filter(self, filter):
        xassert(isinstance(filter, VE.IQueryDOMValue))
        self.filter = filter

    def get_slave_array(self):
        return self.dom_array

    def set_slave_array(self, dom_array):
        xassert(isinstance(dom_array, DOM.DOMArray))
        self.dom_array = dom_array
        self._filter_indexes()

    ## Gathers indexes that were not filtered out.
    def _filter_indexes(self):
        self.valid_indexes = []
        for i in xrange(self.dom_array.getSize()):
            self.dom_array.loadIndex(i)
            ret = self.filter.getDOMValue(self.dom_array)
            if ret:
                self.valid_indexes.append(i)

    # DOMElement wrapped methods.

    def getDataType(self):
        return self.dom_array.getDataType()

    def getValue(self):
        return self.dom_array.getValue()

    # DOMArrayBase wrapped methods.

    def getElemType(self):
        return self.dom_array.getDependency().getAggregates()[0]

    def getSize(self):
        return len(self.valid_indexes)

    def getByteSize(self):
        return self.dom_array.getByteSize()

    def get(self, name):
        return self.dom_array.get(name)
    
    def parentDOM(self):
        return self.dom_array.parentDOM()

    def loadIndex(self, index):
        xassert(index < len(self.valid_indexes))
        slave_index = self.valid_indexes[index]
        self.dom_array.loadIndex(slave_index)
        return self


## This class implements functionality of filter expression dialog.
class FilterDialog(object):
    def __init__(self, prev_filters):
        ## List of strings that are the previously used filter expressions.
        self.prev_filters = prev_filters
        ## Our gtk.Dialog.
        self.dialog = None
        ## Our Glade XML object.
        self.xml = None
        ## Filter text entry.
        self.text_entry = None

    def run(self):
        self.xml = gtk.glade.XML(Config.config.get('gui_path'), 'filterDialog')
        self.dialog = self.xml.get_widget('filterDialog')
        self.text_entry = self.xml.get_widget('filter_text')

        list_store = gtk.ListStore(str)
        for row in self.prev_filters:
            list_store.append([row])
        combo = self.xml.get_widget('prev_filters')
        combo.set_model(list_store)
        cell = gtk.CellRendererText()
        combo.pack_start(cell, True)
        combo.add_attribute(cell, 'text',0)
        combo.connect('changed', self._changed_cb)

        ret = self.dialog.run()
        return ret

    def hide(self):
        self.dialog.hide()

    def get_text(self):
        filter_text = self.xml.get_widget('filter_text')
        buffer = filter_text.get_buffer()
        filter_expr = buffer.get_text(buffer.get_start_iter(), buffer.get_end_iter())
        return filter_expr

    def _changed_cb (self, combo):
        buffer = gtk.TextBuffer()
        active = combo.get_active()
        buffer.set_text(self.prev_filters[active])
        self.text_entry.set_buffer(buffer)
        return gtk.TRUE
