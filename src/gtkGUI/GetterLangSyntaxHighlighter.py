import gtk
from PyGTKCodeBuffer.gtkcodebuffer import CodeBuffer, Pattern, String, LanguageDefinition, SyntaxLoader
from exprs.Eval import Context
import pango

class GetterLangSyntaxHighlighter:

    """ Encapsulates the PyGTKCodeBuffer.gtkcodebuffer.CodeBuffer
        which replaces the standard gtk.TextBuffer. It supports the code highlighting of 
        Data2l getters code language. 
    """

    STYLES = {              
    'DEFAULT':      {'font': 'monospace'},
    'keyword':      {'weight': pango.WEIGHT_BOLD},
    'function':     {'weight': pango.WEIGHT_BOLD, 'foreground': '#00cc00'},
    'numeric':      {'foreground': '#0000FF'},
    'comment':      {'foreground': '#003366', 'style' : pango.STYLE_ITALIC},
    'string':      {'foreground': '#cc0000', 'style' : pango.STYLE_ITALIC}
    }

    def __init__(self):
        
        self.functions = ""
        for fN in Context.funcs.keys():
            if len(self.functions) > 0:
                self.functions+="|"
            self.functions+=fN                        

        # Syntax definition
        keyword  = Pattern(r'\b(if|then|else)\b', style='keyword')
        numericHex =  Pattern(r'\b0x[0-9a-fA-F]+\b', style = 'numeric')
        numericDec =  Pattern(r'\b[0-9]+\b', style = 'numeric')
        function =  Pattern(r'\b('+self.functions+r')\b', style = 'function')
        comment =  String(starts = r'/\*', ends = r'\*/', style = 'comment')
        string =  String(starts = r'"', ends = r'"', escape = r'\\', style = 'string')
 
        # Language definition
        self.lang = LanguageDefinition([keyword, numericHex, numericDec, function, string, comment])
        self.lang._styles.clear()
        self.lang._styles = GetterLangSyntaxHighlighter.STYLES



    def replaceBuffer(self, textView):
        buff = CodeBuffer(lang=self.lang)
        textView.set_buffer(buff)