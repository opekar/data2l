## @package gtkGUI.HexView
#  For view inspected file in hex editor, can highlight se1lected elements. 

import gtk
import array
import pango
import gobject
from Utility import impossible


__all__ = """\
HexView
""".split()


acceptableChars = frozenset('0123456789abcdefghijklmnopqrstuvwxyz'
                            +'ABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&\''
                            +'()*+,-./:;<=>?@[\]^_`{|}~')

## Hex editor of inspected file, can highlight selected elements.
#  @see HexViewExample
class HexView(object):
    def __init__(self, filereader, textView, scroll, 
                 colors = ["red", "green", "blue"]):
        textView.modify_font(pango.FontDescription("monospace, sans"))
        self.textView = textView
        self.textBuffer = self.textView.get_buffer()
        self.scroll = scroll
        self.scroll.connect_object("value-changed", self.scroll_event, None)
        self.textView.connect_object("size-allocate", self.textview_resize, None)

        self.offset = 0
        self.startOffset = 0
        self.endOffset = 0
        
        self.numOfLines = 10
        
        self.hlColors = colors
        self.colorsStack = [x for x in range(len(colors))]
        
        self.tag = []
         
        self.hlSelections = []
        
        for i in range(len(self.hlColors)):
            self.hlSelections.append([(0,0)])
            self.tag.append( self.textBuffer.create_tag())
            self.tag[i].set_property("foreground",self.hlColors[i])
        
        self.bytesPerLine = 16
        
        self.file = filereader
        self.file.seek(0, 2)  #seek to the end
        self.filesize = self.file.tell()
        numLines = self.filesize / self.bytesPerLine
        if self.filesize % self.bytesPerLine > 0:
            numLines += 1

        self.adj = gtk.Adjustment(value=0, lower=0, upper=numLines, step_incr=1, page_incr=10, page_size=10)
        self.scroll.set_adjustment(self.adj)        

        self.tag_start_line = None
        self.tag_end_line = None

        self.tag_start_text = None
        self.tag_end_text = None

        self.updateView()
        
    ## @var hlSelections 
    #  array of arrays of tuples where first is offset_start and second is offset_end
    #  size of hlSelection is equal to number of colors.

    def scroll_event(self, range, cb = None, cb2 = None):        
        self.offset = int(self.scroll.get_value()) * 16
        self.updateView()    
        self.highlightSelections()    
        
    def textview_resize(self, area, event):
        self.scroll.props.adjustment.page_size = self.numOfLines
        self.scroll.set_value(self.scroll.props.adjustment.value)
        self.updateView()
        self.highlightSelections()

    def updateData(self):
        # @todo zjistit velikost fontu a zpresnit vypocet
#        # mozna by to slo pres toto
#        context =  self.textView.get_pango_context()
#        font_desc = context.get_font_description()
#        font_size = font_desc.get_size() / pango.SCALE
        self.numOfLines = self.textView.allocation.height / 15
        startOffs = self.offset - (self.offset % self.bytesPerLine)
        self.endOffset = startOffs + self.bytesPerLine * self.numOfLines
        
    def updateView(self):
        self.updateData()
        startOffs = self.offset - (self.offset % self.bytesPerLine)
        self.file.seek(startOffs)
        bytes = array.array('B')
        bytes_to_read = 1024
        if startOffs + bytes_to_read > self.filesize:
            bytes_to_read = self.filesize - startOffs
        bytes.fromstring(self.file.readString(bytes_to_read))

        act = 0
        output = ""
        for i in xrange(self.numOfLines):
            line = '0x%08X - ' % (startOffs + act)
            chars = ''
            for j in xrange(self.bytesPerLine):
                if act >= len(bytes):
                    line +='   '
                    chars += ' '
                    continue
                c = '%c' % ( bytes[act] )
                line = line + '%02X ' % bytes[act]

                if c in acceptableChars:
                    chars += c
                else:
                    chars += '.'
                act += 1
                if (startOffs + act>self.filesize):
                    break;

            line += '   ' + chars + "\n"
            output +=line
            if (startOffs + act>self.filesize):
                break;

        
        self.textBuffer.set_text(output)
        

    def highlightLine(self, line_num, start, end, color):
        # print "highlightLine %d - start %d end %d, color %d" % \
        #    (line_num ,start, end, color)
        
        # Highlight the hex view area.        
        tag_start = self.textBuffer.get_iter_at_line_offset(line_num, start * 3 + 13)
        tag_end = self.textBuffer.get_iter_at_line_offset(line_num, end * 3 + 13)
        
        self.textBuffer.remove_all_tags(tag_start, tag_end)
        self.textBuffer.apply_tag(self.tag[color], tag_start, tag_end)
        
        # Highlight the text view area.
        tag_start = self.textBuffer.get_iter_at_line_offset(
            line_num,
            self.bytesPerLine * 3 + 13 + 3 + start)
        tag_end = self.textBuffer.get_iter_at_line_offset(
            line_num,
            self.bytesPerLine * 3 + 13 + 3 + end)
        
        self.textBuffer.remove_all_tags(tag_start, tag_end)
        self.textBuffer.apply_tag(self.tag[color], tag_start, tag_end)

    def highlight(self, offs_from, offs_to, color):        
        if(offs_from == offs_to):
            return
        if(offs_from > self.endOffset):
            return
        if(offs_to < self.offset):
            return

        if(offs_from < self.offset):
            offs_from = self.offset
        if(offs_to > self.endOffset):
            offs_to = self.endOffset      
            
        # algoritmus pocita prunik offset intervalu
        lineNum = (offs_from - self.offset) / self.bytesPerLine
        while(True):            
            lineStart = self.offset + lineNum * self.bytesPerLine
            lineEnd = lineStart + self.bytesPerLine
         
            interval = self.intervalIntersect(lineStart, lineEnd, offs_from, offs_to)
            if(interval == None):
                break

            self.highlightLine(lineNum, interval[0]-lineStart, interval[1]-lineStart, color)
            lineNum = lineNum + 1

    
    def intervalIntersect(self, int1B, int1E, int2B, int2E):
        intB = max(int1B, int2B)
        intE = min(int1E, int2E)
        
        if(intB < intE):
            return (intB, intE)
        else:
            return None    

    def showSelections(self, selections, color):        
        if selections == []: return
        
        offset = selections[0][0]
        if (offset > self.offset and # if element is currently in view 
            offset < self.offset + ((self.numOfLines)*self.bytesPerLine)):
            pass
        else: # otherwise set new offset
            self.offset = offset - (offset % self.bytesPerLine)
            scroll_val = int (self.offset / self.bytesPerLine)
            self.scroll.set_value(scroll_val) 
    
    ## @param selections
    # array of tuples, where first is offset_start and second is offset_end.           
    def setHighlight(self, selections, color):
        self.unhighlightSelection(selections)
        self.hlSelections[color] = selections
                
        self.updateView() # clear highlights out of use
        
        # Make new selections bold for short time
        self.tag[color].set_property("weight", pango.WEIGHT_BOLD)
        gobject.timeout_add(2500, self.tag[color].set_property,
                                  "weight", pango.WEIGHT_NORMAL)
        
    def highlightSelections(self):
        self.updateData()
        for color in range(len(self.hlSelections)):
            for selection in self.hlSelections[color]:                
                self.highlight(selection[0], 
                               selection[1], color)
                
    def unhighlightSelection(self, selections):
        for i, sel_for_color in enumerate(self.hlSelections):
            self.hlSelections[i] = [x for x in sel_for_color if x not in selections]

    def showOffset(self, offset, base):
        self.offset = offset
        self.updateView()
        

def HexViewExample():
    from Reader import ReaderA
    
    window = gtk.Window()
    window.set_title("HexView example")
    
    textView = gtk.TextView()
    scroll = gtk.VScrollbar()
    
    hbox = gtk.HBox()
    hbox.add(textView)
    hbox.add(scroll)
    window.add(hbox)
    window.set_size_request(-1,500)
    
    window.show_all()
    
    hexView = HexView(ReaderA("HexView.py"),
            textView, scroll)
    
    window.connect("destroy", gtk.main_quit)
    
    time = 2000
    time_step = 2500
    
    akt_offset = 5
    selections = [(akt_offset,akt_offset+2),(12,14)]
    color = 0
    gobject.timeout_add(time, hexView.setHighlight, selections, color)
    gobject.timeout_add(time, hexView.showSelections, [(akt_offset,)], color)
    gobject.timeout_add(time, hexView.highlightSelections)
    time += time_step
    
    akt_offset = 8
    selections = [(akt_offset,akt_offset+5),(15,17),(20,21)]
    color = 1
    gobject.timeout_add(time, hexView.setHighlight, selections, color)
    gobject.timeout_add(time, hexView.showSelections, [(akt_offset,)], color)
    gobject.timeout_add(time, hexView.highlightSelections)
    time += time_step
    
    akt_offset = 500
    selections = [(akt_offset,akt_offset+50)]
    color = 2
    gobject.timeout_add(time, hexView.setHighlight, selections, color)
    gobject.timeout_add(time, hexView.showSelections, [(akt_offset,)], color)
    gobject.timeout_add(time, hexView.highlightSelections)
    time += time_step + 1500 # the same color too fast consecutive
    
    akt_offset = 600
    selections = [(akt_offset,akt_offset+10)]
    color = 2
    gobject.timeout_add(time, hexView.setHighlight, selections, color)
    gobject.timeout_add(time, hexView.showSelections, [(akt_offset,)], color)
    gobject.timeout_add(time, hexView.highlightSelections)
    time += time_step
    
    akt_offset = 5
    selections = [(akt_offset,akt_offset+2),(12,14)]
    color = 0
    gobject.timeout_add(time, hexView.setHighlight, selections, color)
    gobject.timeout_add(time, hexView.showSelections, [(akt_offset,)], color)
    gobject.timeout_add(time, hexView.highlightSelections)
    time += time_step
    
    gtk.main()

if __name__ == "__main__":
    HexViewExample()
    