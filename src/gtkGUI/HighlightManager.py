## @package gtkGUI.DirectoryChooser 
# Managing highlighting in defined object. 

import gtk

__all__ = """\
    HighlightManager
""".split()

## Managing highlighting in defined object.
# For gtkGUI.HexView and GtkGUI.InspectBox, both problems are variant,
# so the forwarded function is necessary.
# HighlightManager is only memorizing array with data for highlighting
# and unhighlighting and its main function is to manage, when unhighlight
# forward function should be run
class HighlightManager:
    def __init__(self, highlightFunc, unhighlightFunc,
                 colors = ["red", "green", "blue"]):
        self.highlightFunc = highlightFunc
        self.unhighlightFunc = unhighlightFunc
        self.hlSelections = [[] for x in range(len(colors))]
        self.hlColors = colors
    
    def highlight(self, array, color):
        # delete selection from other colors
        for i, selection in enumerate(self.hlSelections):
            if i == color: continue # not for current color
            self.hlSelections[i] = [x for x in selection if x not in array]
    
        if len(self.hlSelections[color]) > 0:
            self.unhighlightFunc(self.hlSelections[color])
        
        self.highlightFunc(array, self.hlColors[color])
        self.hlSelections[color] = array # store array for current color
