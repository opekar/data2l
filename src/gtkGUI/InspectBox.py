import gtk
import inspect
import sys
import traceback
import DataTypes
import BasicTypes
import Reader
import DOM
import exprs.Eval as E
from gtkGUI import Config, Filter, Errors
from exprs.ExprVE import ExprVE
from gtkGUI.TextViewDialog import TextViewDialog
from Utility import unexpected, xassert


__all__ = """\
InspectBox
InspectBoxCreator
VoidBox
BasicInspectBox
ArrayWithSpinnerBox
StructInspectBox
AlternativeInspectBox
""".split()


class InspectBox(object):
    def __init__(self, dom_element, fileInspector):
        self.dom_element = dom_element
        self.fileInspector = fileInspector
        self.frame = None
        self.eventBox = None
        self.toggled = True
        self.toggled_frame = None
        self.toggled_eb = None
        self.colors = {}

    def __str__(self):
        return "InspectBox:"+str(self.dom_element)

    ## returns an not updated inspect box, if its not created it calls createBox to create it
    def getBox(self):
        if self.frame is None:
            if self.eventBox is None:
                self.eventBox = self.createEventBox()
            self.frame = self.createBox()
            self.eventBox.add(self.frame)
            self.frame.show()

        return self.eventBox

    def createEventBox(self):
        eventBox = gtk.EventBox()
        eventBox.connect_object('button_press_event',
                                     self.fileInspector.press_handler,
                                     self, self)
        eventBox.connect_object('button_release_event',
                                     self.fileInspector.release_handler,
                                     self, self)
        eventBox.set_above_child(False)
        return eventBox

    ## refresh the views with new values
    def updateData(self, dom_element):
        pass
    
    def untogglable(self):
        if isinstance(self.dom_element.getDataType(),DataTypes.Basic):
            return False
        if isinstance(self.dom_element.getDataType(),DataTypes.Void):
            return False

        elem = self.dom_element.parentDOM()
        # please do not toogle out aggregates of Alternative
        # no Idea how to keep it in sync from the user point of view
        while elem is not None:
            if isinstance(elem.getDataType(), DataTypes.Alternative):
                return False
            elem = elem.parentDOM()
        
        return True


    def toggleUntoggle(self):
        if self.toggled:
            self.toggled = False;
            untoggled_eb = self.createEventBox()
            untoggled_frame = gtk.Frame("%s untoggled" %(self.dom_element.getDataType().getName()))
            untoggled_eb.add(untoggled_frame)
            untoggled_frame.show()

            for child in self.frame.get_children():
                self.frame.remove(child)
                if not isinstance(child,gtk.Label):
                    untoggled_frame.add(child)
                child.show()
            self.frame.set_label("%s untoggled" %(self.dom_element.getDataType().getName()))
            self.toggled_frame = self.frame
            self.toggled_eb = self.eventBox
            self.frame =  untoggled_frame
            self.eventBox = untoggled_eb

            widget = self.toggled_eb;
            while widget.get_parent() != self.fileInspector.fixed:
                widget = widget.get_parent()

            x = self.fileInspector.fixed.child_get_property(widget,"x")
            y = self.fileInspector.fixed.child_get_property(widget,"y")

            self.fileInspector.fixed.put(untoggled_eb, x+200, y)
            untoggled_eb.show()


        else:
            for child in self.frame.get_children():
                self.frame.remove(child)
                if not isinstance(child,gtk.Label):
                    self.toggled_frame.add(child)
                child.show()

            self.toggled_frame.set_label("%s" %(self.dom_element.getDataType().getName()))
            self.fileInspector.fixed.remove(self.eventBox)
            self.eventBox =  self.toggled_eb
            self.frame = self.toggled_frame
            self.toggled_frame=None
            self.toggled_eb=None
            self.toggled = True;
            
    def highlight(self, offs, color):
        self.colors[offs] = color
                
        if isinstance(self.frame, gtk.Frame):
            label = self.frame.get_label_widget()
        elif isinstance(self.frame, gtk.Label):
            label = self.frame
        else:
            print "No label for highlightig!"
            return
            
        text = label.get_text()
        label.set_markup('<span foreground="%s">%s</span>' % (color, text))
        
        
    def unhighlight(self, offs):
        if self.colors.has_key(offs):
            del self.colors[offs]
        if offs != self.dom_element.akt_offs:
            return # only del key if no wanted element in current inspect box

        # else unhighlight current inspect box
        if isinstance(self.frame, gtk.Frame):
            label = self.frame.get_label_widget()
        elif isinstance(self.frame, gtk.Label):
            label = self.frame
        else:
            print "No label for unhighlightig!"
            return
        
        text = label.get_text()
        label.set_markup('<span foreground="black">%s</span>' % (text))
        
    def updateColor(self):
        if self.colors.has_key(self.dom_element.akt_offs):
            self.highlight(self.dom_element.akt_offs, self.colors[self.dom_element.akt_offs])
        elif isinstance(self, StructInspectBox):
            # necessary for Struct headline
            self.unhighlight(self.dom_element.akt_offs)

class InspectBoxCreator(object):
    def __init__(self, fileInspector):
        self.fileInspector = fileInspector

    def create(self, dom_struct):
        xassert(isinstance(dom_struct, DOM.DOMElement))
        self.dom_struct = dom_struct
        dom_struct.getDataType().accept(self)
        return self.new

    def VisitBasic(self, _):
        a = BasicInspecBox(self.dom_struct, self.fileInspector)
        self.new = a

    def VisitStruct(self, _):
        a = StructInspectBox(self.dom_struct, self.fileInspector)
        self.new = a

    def VisitArray(self, _):
        a = ArrayWithSpinnerBox(self.dom_struct, self.fileInspector)
        self.new = a

    def VisitAlternative(self, _):
        a = AlternativeInspectBox(self.dom_struct, self.fileInspector)
        self.new = a

    def VisitVoid(self, _):
        a = VoidBox(self.dom_struct, self.fileInspector)
        self.new = a


class VoidBox(InspectBox):
    def __init__(self, dom_element, fileInspector):
        InspectBox.__init__(self, dom_element, fileInspector)

    def createBox(self):
        frame = gtk.Label("void")
        return frame

    def updateData(self, dom_element):
        pass

    def toggleUntoggle(self):
        pass


class BasicInspecBox(InspectBox):
    def __init__(self, dom_element, fileInspector):
        InspectBox.__init__(self,dom_element, fileInspector)
        self.label = gtk.Label("N/A")

    def createBox(self):
        self.label.show()
        return self.label

    def updateData(self, dom_element):
        self.dom_element = dom_element
        value = self.dom_element.getValue()
        tmp = self.format_basic_type(value, self.dom_element.getDataType().basic_type)
        tmp = tmp.replace('\0', ' ').strip()
        for ch in tmp:
            unexpected(ch == '\0') 
        self.label.set_label(tmp)
        InspectBox.updateColor(self)

    def format_basic_type(self, value, basic_type):
        tmp = "N/A"
        if isinstance(basic_type, BasicTypes.BasicNumType):
            tmp = "%s : " % self.dom_element.getDataType().getName() 
            val = "N/A" 
            if isinstance(basic_type, BasicTypes.Byte):
                val = "%d (0x%02X)" % (value, value)
            elif isinstance(basic_type, BasicTypes.Word):
                val = "%d (0x%04X)" % (value, value)
            elif isinstance(basic_type, BasicTypes.DWord):
                val = "%d (0x%08X)" % (value, value)
            elif isinstance(basic_type, BasicTypes.Float):
                val = "%f (%s)" % (value, float.hex(value))
            # BasicNumType can be Enum type
            if basic_type.getEnum() is not None:
                enum = basic_type.getEnum()
                if enum is None:                    
                    tpm += val
                tmp += enum.getEnumerator(value) + " [" + val + "]"
            else:
                tmp += val
        else:
            try:
                val = str(value)
                utf8_val = val.encode('utf-8')
            except:
                utf8_val = "."*len(val)
                                
            tmp = "%s : %s" %(self.dom_element.getDataType().getName(), utf8_val)
        return tmp


    def toggleUntoggle(self):
        pass


class ArrayWithSpinnerBox(InspectBox):
    def __init__(self, dom_element, fileInspector):
        InspectBox.__init__(self, dom_element, fileInspector)
        self.array_size = None
        self.actIndex = 0

        self.adjustment = gtk.Adjustment(step_incr = 1, page_incr = 10)
        self.spinner = gtk.SpinButton(self.adjustment)
        self.sizeLabel = gtk.Label(" of X ")
        self.ins = InspectBoxCreator(self.fileInspector)
        self.original_array = None
        self.filter_exprve = None
        self.filter_button = gtk.Button(label = "A", use_underline = False)
        self.filter_button.connect("clicked", self.onFilter)

        self.aggregates = []
        for i in self.dom_element.value:
            self.aggregates.append(self.ins.create(i))

    def createArrowButton(self, arrow_type, shadow_type):
        button = gtk.Button()
        arrow = gtk.Arrow(arrow_type, shadow_type)
        button.add(arrow)
        button.show()
        arrow.show()
        return button

    def createBox(self):
        struct = self.dom_element.getDataType()
        frame = gtk.Frame(struct.name)

        h = gtk.HBox()

        button = self.createArrowButton(gtk.ARROW_LEFT, gtk.SHADOW_ETCHED_IN)
        button.connect_object("clicked", self.onLeft, self, self)
        h.add(button)

        self.spinner.set_snap_to_ticks(True)
        self.spinner.set_wrap(False)
        self.spinner.set_numeric(True)
        self.spinner.connect('value-changed', self.onSpinnerChange)
        parent = self.spinner.get_parent()
        if parent is not None:
            parent.remove(self.spinner)
        h.add(self.spinner)
        self.spinner.show()

        parent = self.sizeLabel.get_parent()
        if parent is not None:
            parent.remove(self.sizeLabel)
        h.add(self.sizeLabel)
        self.sizeLabel.show()

        self.filter_button.set_label("A")
        self.filter_button.show()
        h.add(self.filter_button)

        button = self.createArrowButton(gtk.ARROW_RIGHT, gtk.SHADOW_ETCHED_IN)
        button.connect_object("clicked", self.onRight, self, self)
        h.add(button)

        v = gtk.VBox()
        v.add(h)

        for i in self.aggregates:
            subFrame = i.getBox()
            v.add(subFrame)
            subFrame.show()

        frame.add(v)

        h.show()
        v.show()

        return frame

    def onLeft(self, widget, event, cb = None):
        if self.actIndex <= 0:
            return
        old_val = self.spinner.get_value_as_int()
        self.spinner.set_value(old_val - 1)

    def onRight(self, widget, event, cb = None):
        if self.actIndex >= self.array_size - 1:
            return
        old_val = self.spinner.get_value_as_int()
        self.spinner.set_value(old_val + 1)

    ## Callback for value-changed event on spinner box.
    # It is called both on spinner box change from GUI and from
    # set_value() method through onLeft() or onRight() handlers.
    # only place which initiates loading of data - thats why the try catch
    def onSpinnerChange(self, spinner):
        self.actIndex = spinner.get_value_as_int()
        try:
            self.aggregateUpdate()
        except Reader.SafeReaderException, e:
            trace = None
            dialog_text = ""
            try:
                trace = inspect.trace()
                dialog_text = Errors.format_dom_trace(trace)
            finally:
                del trace
            dialog = TextViewDialog(title = "Error",
                                    label = "Error context:",
                                    text = dialog_text)
            dialog.run()
            dialog.hide()
            return
        except Exception, e:
            info = None
            s = ""
            try:
                info = sys.exc_info()
                s = traceback.format_tb(info[2])
            finally:
                del info
            s = str("").join(s)
            error_dialog = TextViewDialog("Error", "Traceback:",
                "Unhandled exception in file inspector.\n"
                + str(e) + "\n\n" + s)
            error_dialog.run()
            error_dialog.hide()
            return

    def updateData(self, dom_element):
        if (self.original_array is not None
            # Do not try to filter filtered array again.
            and not isinstance(dom_element, Filter.FilteredDOMArray)):
            # Filter the array again only if our source DOM has been updated
            # and stack_filters setting is on.
            if Config.config.get('stack_filters'):
                self.original_array = dom_element
                self.dom_element = Filter.FilteredDOMArray(dom_element,
                                                           self.filter_exprve)
            # Otherwise just reset the filtering as if the F button was pressed.
            else:
                self.dom_element = dom_element
                self.original_array = None
                self.filter_exprve = None
                self.filter_button.set_label('A')
        else:
            self.dom_element = dom_element
        self.actIndex = 0
        self.array_size = self.dom_element.getSize()
        self.spinner.set_range(0, self.array_size - 1)
        self.aggregateUpdate()
        InspectBox.updateColor(self)

    def aggregateUpdate(self):
        self.sizeLabel.set_text(" of %d" % self.array_size)
        if self.array_size == 0:
            for i in self.aggregates:
                i.getBox().hide_all()
            return
        self.dom_element.loadIndex(self.actIndex)

        value = self.dom_element.getValue()
        for idx, i in enumerate(self.aggregates):
            xassert(idx < len(value))
            i.getBox().show_all()
            i.updateData(value[idx])


    def onFilter(self, widget):
        if self.original_array is None:
            filterDialog = Filter.FilterDialog(Config.config.get('filter_expressions'))

            ret = filterDialog.run()
            filter_expr = filterDialog.get_text()
            if ret != gtk.RESPONSE_OK or filter_expr == "":
                filterDialog.hide()
                return

            filterDialog.hide()
            self.filter_exprve = ExprVE(filter_expr)
            try:
                E.context.compile(self.filter_exprve, self.dom_element.getDependency())
            except E.ENameNotFound, e:
                dialog = TextViewDialog("Error", "Error in filter expression:",
                                        "Name not found:\n%s\n" % e.name)
                dialog.run()
                dialog.hide()
                return
            except E.EParseError, e:
                Errors.handle_EParseError(e)
                return

            self.original_array = self.dom_element
            filtered_array = Filter.FilteredDOMArray(self.dom_element, self.filter_exprve)
            self.updateData(filtered_array)
            self.filter_button.set_label('F')

            filter_expressions = Config.config.get("filter_expressions")
            Config.prepend_unique(filter_expressions, filter_expr,
                                  Config.config.get('max_filter_expressions'))
            Config.config.dump()
        else:
            original_array = self.original_array
            self.original_array = None
            self.filter_exprve = None
            self.updateData(original_array)
            self.filter_button.set_label('A')
        return



class StructInspectBox(InspectBox):
    def __init__(self, dom_element, fileInspector):
        InspectBox.__init__(self, dom_element, fileInspector)

        ins = InspectBoxCreator(self.fileInspector)
        self.fields =[]
        struct = self.dom_element.getDataType()
        for i,field in enumerate(struct.getAggregates()):
            self.fields.append(ins.create(self.dom_element.getValue()[i]))


    def updateData(self, dom_element):
        self.dom_element = dom_element

        struct = self.dom_element.getDataType()
        for i,field in enumerate(struct.getAggregates()):
            self.fields[i].updateData(self.dom_element.getValue()[i])
        InspectBox.updateColor(self)

    def createBox(self):
        struct = self.dom_element.getDataType()
        frame = gtk.Frame(struct.name)
        self.expander = gtk.Expander()
        self.expander.connect('activate', self.onUnfold)

        self.vbox = gtk.VBox()
        self.expander.add(self.vbox)
        frame.add(self.expander)

        struct = self.dom_element.getDataType()
        for i in xrange(len(struct.fields)):
            self.vbox.pack_start(self.fields[i].getBox())
            self.fields[i].getBox().show()

        self.vbox.show()
        self.expander.show()

        return frame

    def onUnfold(self, expander):
        if not expander.get_expanded():
            self.updateData(self.dom_element)

    def expand(self, expand):
        self.updateData(self.dom_element)
        self.expander.set_expanded(expand)
        self.expander.show();


class AlternativeInspectBox(InspectBox):
    def __init__(self, dom_element, fileInspector):
        InspectBox.__init__(self, dom_element, fileInspector)
        self.ins = InspectBoxCreator(self.fileInspector)
        self.aggregate = self.ins.create(self.dom_element.getValue())

    def updateData(self, dom_element):
        self.dom_element = dom_element
        self.getBox()
        # Try to short-cut the update.
        if self.aggregate.dom_element.getDataType() == self.dom_element.getValue().getDataType():
            self.aggregate.updateData(self.dom_element.getValue())
            box = self.aggregate.getBox()
            box.show()
            return

        old_alt = self.aggregate.getBox()
        expanded = False
        if isinstance(self.aggregate, StructInspectBox):
            expanded = self.aggregate.expander.get_expanded()
        self.aggregate = self.ins.create(self.dom_element.getValue())
        self.frame.remove(old_alt)
        new_alt = self.aggregate.getBox()
        self.frame.add(new_alt)
        if isinstance(self.aggregate, StructInspectBox):
            self.aggregate.expand(expanded)
        else:
            self.aggregate.updateData(self.dom_element.getValue())
        new_alt.show()
        InspectBox.updateColor(self)

    def createBox(self):
        element = self.dom_element.getDataType()
        frame = gtk.Frame(element.name)
        alt_box = self.aggregate.getBox()
        frame.add(alt_box)
        alt_box.show()
        return frame
