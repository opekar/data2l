#import profile

import pygtk
import gtk
import gobject
import sys
import os
import copy
import traceback
import imp
import gtkGUI
import webbrowser
from gtkGUI import ConsoleWindow, DepsView, StructTree, Validator
from gtkGUI import ThreadsManager, FileInspector, Errors, TextViewDialog
from gtkGUI import PropDialog, CheckStructErrorDialog, StructCheck, Command, clipboard
import gtkGUI.FileChooser
from pango import FontDescription
from language.Parser import *# ParseException
from language.DependencyTree import SemanticException
from language.LanguageRW import *
import pyggy

import Definition
import exprs.Eval as E
import file_defs
from generators.Generator import GeneratorException
import generators.CustGeneratorManager as CustGenManager
import generators.NewFormDef as NewFormDef
from gtkGUI.Config import (config, prepend_unique, save_dirname_into)
from Utility import xassert_type, xassert, print_trace
from ReadersManager import ReadersManager
import platform
from DataTypes import DataDefinitionException
from Dependency import DependencyDefinitionException


## Shows error message box with given text.
def show_error_box(text):
    msg = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR,
                            gtk.BUTTONS_CANCEL, text)
    msg.run()
    msg.hide()
    
def MessageBox(text, parent=None):
    msg = gtk.MessageDialog(parent, gtk.DIALOG_MODAL, gtk.MESSAGE_INFO,
                            gtk.BUTTONS_OK, text)
    msg.run()
    msg.hide()    

class Main(object):    
    def __init__(self):
        self.main_window = gtk.glade.XML(config.get('gui_path'),'mainWindow')
        self.console_window = ConsoleWindow.ConsoleWindow(self.main_window)
        
        self.dict = {'on_new': 
                        lambda w: self._applyChangesBeforeCall(self.newFile, w),
                     'on_open_file': 
                        lambda w: self._applyChangesBeforeCall(self.openFile, w),
                     'on_save_as_activate':self.saveFileAs,
                     'on_save_file':
                        lambda w: self._applyChangesBeforeCall(self.saveFile, w),
                     'on_quit': self.on_quit,
                     'on_edit_activate': self.on_edit_activate,
                     'on_edit_undo_activate': self.on_undo_activate,
                     'on_edit_redo_activate': self.on_redo_activate,
                     'on_edit_copy_activate': self.on_edit_copy_activate,
                     'on_edit_paste_activate': self.on_edit_paste_activate,
                     'on_start_edit_element': lambda w: self.structTree.editSelected(),
                     'on_start_edit_enums' : lambda w: self.structTree.edit_enums(),
                     'on_edit_preferences':self.editPreferences,
                     'menu_view_deps_toggled':
                        lambda w: self.on_cards_toggle(w, 0),
                     'menu_view_docs_toggled':
                         lambda w: self.on_cards_toggle(w, 2),
                     'menu_view_structure_toggled':
                         lambda w: self.on_cards_toggle(w, 3),
                     'menu_view_validators_toggled':
                         lambda w: self.on_cards_toggle(w, 1),
                     'on_console_window':
                         self.console_window.on_console_windows_activate,
                     'on_show_start_page_activate': lambda w: self.StartHtmlView.toggle(),
                     'on_inspect_file': 
                        lambda w: self._applyChangesBeforeCall(self.inspectFile,w),
                     'on_validate_file': self.validateFile,
                     'on_generate_cpp_new': 
                        lambda w: self._applyChangesBeforeCall(self.generateCppNew, w),
                     'on_generate_cpp_readonly': self.generateCppReadonly,
                     'on_rescan_custom_generators' : self.scanCustomGenerators,
                     'on_check_structure': 
                        lambda w: self._applyChangesBeforeCall(self.checkStructure,w),
                     'on_about_activate': self.About,
                     'on_help_content_activate': self.helpContent,
                     'on_mainWindow_destroy' : gtk.main_quit}
                     

        self.module_counter = 0;
        self.config = config.load()
        self._init_empty_module()
        self.definition_changed = 0;

        self.main_window.signal_autoconnect(self.dict)
        self.window = self.main_window.get_widget('mainWindow')
        self.window.connect('destroy', self.on_quit)
        self.window.connect('key-release-event', self.keyRelease)
        self.window.connect('map-event', self.right_after_start)

        
        self.main_menu = self.main_window.get_widget("mainMenu")

        self.right_after_start_called = False
        self.v_tree_and_props = self.main_window.get_widget('v_tree_and_props')
        self.notebook = self.main_window.get_widget('notebook')
        self.notebook.connect("switch-page", self.on_switch_page)
        self.help_examples = self.main_window.get_widget('help_examples')
        self.construct_examples_menu()
        self.recent_files = self.main_window.get_widget('recent_files')
        self.recent_data_files = self.main_window.get_widget('recent_data_files')
        self.custom_generators = self.main_window.get_widget('custom_generators')
        self.depsView = DepsView.DependenciesView(self, self.main_window,
                                                  self.struct_def)
        self.structTree = StructTree.StructTree(self, self.main_window,
                                                self.struct_def)
        if ( platform.system() != "Windows"):
            from gtkGUI import DocsView
            self.docsView = DocsView.DocsView(self.main_window)
        else:
            from gtkGUI import DocsViewHTML
            self.docsView = DocsViewHTML.DocsView(self.main_window, self)        
        self.validatorsView = Validator.Validator(self, self.main_window)        
        self.set_definition_for_views()
        
        self.custGenManager = CustGenManager.CustomGeneratorManager()
        self.scanCustomGenerators()
        
        self.customReaders = ReadersManager()
        self.customReadersMenu = self.main_window.get_widget('custom_readers')
        self.scanCustomReaders()

        self.undo_menu = self.main_window.get_widget('undo')
        self.redo_menu = self.main_window.get_widget('redo')
        self.resetUndoRedoStack()

        self.help_handle = None
        self.opts = None
        self.statusBar = self.main_window.get_widget('statusbar')                
        
        if (platform.system() == 'Windows'):
            from StartPageHtmlView_ie import StartPageHtmlView
        else:
            try:
                from StartPageHtmlView_webkit import StartPageHtmlView
            except:
                print "ERROR: The webkit module can not be loaded!"
        self.StartHtmlView = StartPageHtmlView(self.main_window, self, self.config)
        if self.config.get("html_view_on_start"):
            self.StartHtmlView.show()
        else:
            self.StartHtmlView.hide()

    ## Necessary for applying changes made in TextViews. In TextView, changes are
    #  applied (stored to model) only when focus-out-event is emitted. 
    #  But clicking on toolbar menu item doesn't emit this event. 
    def _applyChangesBeforeCall(self, functor, *args):
        # apply changes made in currently selected widget 
        focused_widget = self.window.get_focus()
        if focused_widget is not None:
            focused_widget.emit("focus-out-event",None)        
        # call original function
        functor(*args)

    def showStatusText(self, text = ""):
        self.statusBar.pop(0)
        self.statusBar.push(0,text)


    ## Called on 'map-event' event of the main window.
    # Right now its only purpose is to allow us to open definition file given
    # on command line.
    def right_after_start(self, widget, user_date = None):
        #print_trace()
        
        if not self.right_after_start_called and self.opts is not None:
            self.right_after_start_called = True
            if self.opts.definition_file is not None:
                ret = self.open_def_file(self.opts.definition_file)
                if not ret:
                    self.newFile(None, None,False)
                self.struct_def_filename = self.opts.definition_file
            else:
                self.newFile(None, None,False)
        return
    

    def set_definition_for_views(self):
        #print_trace()
        self.structTree.setDefinition(self.struct_def)
        self.docsView.set_definition(self.struct_def)
        self.validatorsView.set_definition(self.struct_def)

    def newFile(self, widget, filename = None, HideHtmlView=True):
        self.save_before_change()
        self._init_empty_module()
        self.set_definition_for_views()
        self.refresh_gui()
        self.resetUndoRedoStack()
        self.refreshTitle()
        if HideHtmlView:
            self.StartHtmlView.hide()

    def openFile(self, widget):
        self.save_before_change()
        filew = gtkGUI.FileChooser.FileChooser(self.window, "Open Structure Definition...", 
            [("Data2l structure definitions",["*.d2l"]), ("Python structure definitions",["*.py"]),
             ("DataScript structure definitions (experimental)",["*.ds"])])
        filew.set_current_folder( os.path.dirname(self.config.get("last_open_def_dialog_path")) )
        ret = filew.run()
        if ret == gtk.RESPONSE_OK:
            self.struct_def_filename = filew.get_filename()
            self.config.set("last_open_def_dialog_path", self.struct_def_filename)
            self.open_def_file(self.struct_def_filename)

    def open_def_file(self, filename):
        if not os.path.isfile(filename):
            show_error_box("Specified path is not a file.")
            return False
        E.context = E.Context()
        if filename.find(".py") == len(filename)-3:
            self.open_py_def_file(filename)
        elif filename.find(".d2l") == len(filename)-4:        
            self.open_d2l_def_file(filename)
        elif filename.find(".ds") == len(filename)-3:
            self.open_ds_def_file(filename)            
        else:
            show_error_box("Unknown data file.")
            return False
        self.struct_def_filename = filename
        self.set_definition_for_views()
        self.refresh_gui()
        # Update recently opened definitions list.
        recent_defs = self.config.get("recent_defs")
        prepend_unique(recent_defs, filename, self.config.get("max_recent_files"))
        self.config.dump()
        self.update_recent_files_menu()
        self.refreshTitle()
        self.StartHtmlView.hide()
        return True

    def open_py_def_file(self, filename):
        definition = None
        module = None
        try:
            definition, module = Definition.loadDefinitionFile(filename)
        except DataDefinitionException, e:
            dialog = TextViewDialog.TextViewDialog("Error", 
                "Element definition error:", str(e))
            dialog.run()
            dialog.hide()
            return False
        except DependencyDefinitionException, e:
            dialog = TextViewDialog.TextViewDialog("Error", 
                "Dependency definition error:", str(e))
            dialog.run()
            dialog.hide()
            return False
        except Exception, e:
            info = None
            s = ""
            try:
                info = sys.exc_info()
                s = traceback.format_tb(info[2])
            finally:
                del info
            s = str("").join(s)
            dialog = TextViewDialog.TextViewDialog("Error", "Traceback:",
                "Specified path is not a Python source file!\n"
                + str(e) + "\n\n" + s)
            dialog.run()
            dialog.hide()
            return False
        if "definition" not in module.__dict__:
            show_error_box("File loaded but 'definition' was not found.")
            return False
        self.current_definition_module = module
        self.struct_def = definition
        self.resetUndoRedoStack()
        return True
    
    def open_d2l_def_file(self, filename):
        definition = None
        module = None
        try:
            definition = Definition.loadDefinitionD2lFile(filename)
        except ParseException, e:
            show_error_box(str(e))
            return False   
        except SemanticException, e:
            show_error_box(str(e))
            return False   
        except Exception, e:
            show_error_box(str(e))
            return False
        
        self.current_definition_module = module
        self.struct_def = definition
        self.resetUndoRedoStack()
        return True    

    def open_ds_def_file(self, filename):
        definition = None
        module = None
        try:
            definition = Definition.loadDefinitionDataScriptFile(filename)        
        except pyggy.ParseError,e:
            dialog = TextViewDialog.TextViewDialog("Parse error", 
                "DataScript parsing error:", str(e))
            dialog.run()
            dialog.hide()
            return False
        except Exception, e:
            show_error_box(str(e))
            return False
        
        self.current_definition_module = module
        self.struct_def = definition
        self.resetUndoRedoStack()
        return True

    def resetUndoRedoStack(self):
        self.undo_menu.set_sensitive(False)
        self.undo_stack = []
        self.redo_menu.set_sensitive(False)
        self.redo_stack = []
        self.definition_changed = 0

    def update_recent_files_menu(self):
        submenu = gtk.Menu()
        recent_defs = self.config.get("recent_defs")
        for fname in recent_defs:
            item = gtk.MenuItem(fname, use_underline = False)
            item.connect("activate", lambda w,filename: self.open_def_file(filename), fname)
            item.show()
            submenu.append(item)
        submenu.show()
        self.recent_files.set_submenu(submenu)

    def saveFile(self, widget):
        self.saveFileAs(widget, self.struct_def_filename)
            

    def saveFileAs(self, widget, filename = None):
        if filename is None:
            filew = gtkGUI.FileChooser.FileChooser(self.window, "Safe File...", 
                [("Data2l structure definitions",["*.d2l"]),("Python structure definitions",["*.py"])]
                ,self.struct_def_filename, gtkGUI.FileChooser.FileChooser.SAVE)
            filew.set_current_folder( os.path.dirname(self.config.get("last_save_def_dialog_path")) )            
            ret = filew.run()
            if ret == gtk.RESPONSE_OK:
                filename = filew.get_filename()
                save_dirname_into(self.config, "last_save_def_dialog_path",
                                  filename)
                self.config.dump()
            filew.destroy()
            if ret != gtk.RESPONSE_OK:
                return

            if os.path.isfile(filename):
                msg = gtk.MessageDialog(None, gtk.DIALOG_MODAL,
                                        gtk.MESSAGE_QUESTION,
                                        gtk.BUTTONS_OK_CANCEL,
                                        "Overwrite selected file?")
                ret = msg.run()
                msg.hide()
                if ret != gtk.RESPONSE_OK:
                    return

        if filename[-3:] == '.py':
            path = os.path.dirname(filename)
            fname = os.path.basename(filename)
            saver = NewFormDef.NewFormDef()
            saver.generate(self.struct_def.dep_root, self.struct_def.userDefinedEnums,
                   {"TargetDirectory":path,
                    "DefinitionName" :fname})
        elif filename[-4:] == '.d2l':
            try:
                out = open(filename, 'w')
                code = LanguageRW.writeLanguage(self.struct_def.dep_root, 
                                                self.struct_def.userDefinedEnums,
                                                comments = True)
                out.write(code)
                out.close()
            except Exception, e:
                show_error_box(str(e))
            
            
        self.struct_def_filename = filename
        self.definition_changed = 0
        self.refreshTitle()
        self.docsView.refresh()

        # Update recently opened definitions list.
        recent_defs = self.config.get("recent_defs")
        prepend_unique(recent_defs, filename, self.config.get("max_recent_files"))
        self.config.dump()
        self.update_recent_files_menu()
        

    def inspectFile(self, widget, readerCreator = None):        
        success, compiled = self.compileWithErrorDialog(self.struct_def)        
        success &= self.checkStructureWithErrorDialog(compiled)
        if not success: 
            return
        
        filew = gtkGUI.FileChooser.FileChooser(self.window, "Inspect File...")
        filew.set_current_folder( os.path.dirname(self.config.get("last_inspect_dialog_path")) )
        ret = filew.run()
        filename = filew.get_filename()
        filew.destroy()
        if ret == gtk.RESPONSE_OK:
            save_dirname_into(self.config, "last_inspect_dialog_path",
                              filename)
            self.config.dump()
            self.inspect_data_file(filename,compiled,readerCreator)

    def inspect_data_file(self, filename, already_compiled, readerCreator):                
        if not os.path.isfile(filename):
            show_error_box("Specified path is not a file.")
            return            

        # Update recently inspected data files list.
        recent_data = self.config.get("recent_data")
        prepend_unique(recent_data, filename, self.config.get("max_recent_files"))
        self.config.dump()
        self.update_inspected_files_menu()
        if already_compiled is None:
            success, compiled = self.compileWithErrorDialog(self.struct_def)
            success &= self.checkStructureWithErrorDialog(compiled)
            if not success: 
                return
        else:
            compiled = already_compiled
        inspector = FileInspector.FileInspector()
        inspector.inspect_file(filename, self.struct_def, compiled, readerCreator)

    def validateFile(self, widget):
        success, compiled = self.compileWithErrorDialog(self.struct_def)        
        success &= self.checkStructureWithErrorDialog(compiled)
        if not success: 
            return
        
        filew = gtkGUI.FileChooser.FileChooser(self.window, "Validate File...") 
        filew.set_current_folder( os.path.dirname(self.config.get("last_validate_dialog_path")) )
        ret = filew.run()
        if ret == gtk.RESPONSE_OK:
            filename = filew.get_filename()
            filew.destroy()
            save_dirname_into(self.config, "last_validate_dialog_path",
                              filename)
            self.config.dump()
            thread = Validator.ValidatorTaskThread(filename, compiled)
            ThreadsManager.register_thread(thread)
            thread.start()
        else:
            filew.destroy()
        return


    ## Rescans custom generators and creates menu items.
    def scanCustomGenerators(self, widget=None, user_data = None):
        self.custGenManager.scan(self.config.get('custom_generators_path'))
        submenu = gtk.Menu()
        for g in self.custGenManager.generators:
            item = gtk.MenuItem(g.getMenuString(), use_underline = False)
            item.connect("activate", self.CustomGenerateCallback, g)
            item.show()
            submenu.append(item)
        submenu.show()
        self.custom_generators.set_submenu(submenu)
    
    def scanCustomReaders(self, widget = None, user_data = None):
        self.customReaders.scan(self.config.get('custom_readers_path'))
        submenu = gtk.Menu()
        for g in self.customReaders.creators.values():
            item = gtk.MenuItem(g.getName() + "...", use_underline = False)
            item.connect("activate", self.inspectFile, g)
            item.show()
            submenu.append(item)
        submenu.show()
        self.customReadersMenu.set_submenu(submenu)
        self.customReadersMenu.set_sensitive( len(self.customReaders.creators.values()) != 0 )
        

    def CustomGenerateCallback(self, menu_item, generator):
        self.generate(generator)

    def generateCppNew(self, widget, user_data = None):
        import generators.cppgen2
        generator = generators.cppgen2.CppGen2()
        self.generate(generator)

    def generateCppReadonly(self, widget, user_data = None):
        import generators.cppgen_readonly
        generator = generators.cppgen_readonly.CppGen_ReadOnly()
        self.generate(generator)

    def compileWithErrorDialog(self, definition):
        try:
            compiled = definition.getCompiledDefinition()
        except E.ENameNotFound, e:
            dialog = TextViewDialog.TextViewDialog("Error",
                "Error in filter expression:",
                ("Name not found:\n%s\nin:\n%s" % (e.name, e.getPath())))
            dialog.run()
            dialog.hide()
            return  (False,None)
        except E.EParseError, e:
            Errors.handle_EParseError(e)
            return  (False,None)
        except E.EvalException, e:
            dialog = TextViewDialog.TextViewDialog("Error",
                "Compilation error:", str(e))
            dialog.run()
            dialog.hide()
            return  (False,None)            
        return (True,compiled)   
        
    def generate(self, generator):
        success, compiled = self.compileWithErrorDialog(self.struct_def)        
        success &= self.checkStructureWithErrorDialog(compiled)
        if not success: 
            return

        gen_settings = generator.getDefaultSettings(compiled.dep_root)
        dialog = PropDialog.PropertiesDialog(gen_settings,
                                             "Generator options")
        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            dialog.dialog.set_title("Generating ...")
            try:
                # tranformate gen_setting from list to dictionary
                gen_settings_dict = {}
                for setting in gen_settings: gen_settings_dict[setting[0]]=setting[1][1]
                generator.generate(compiled.dep_root, self.struct_def.userDefinedEnums, gen_settings_dict)                
            except GeneratorException, ge:
                error_dialog = TextViewDialog.TextViewDialog("Generator Error", "",
                    str(ge))
                error_dialog.run()
                error_dialog.hide()
            except Exception, e:
                info = None
                s = ""
                try:
                    info = sys.exc_info()
                    s = traceback.format_tb(info[2])
                finally:
                    del info
                s = str("").join(s)
                error_dialog = TextViewDialog.TextViewDialog("Error", "Traceback:",
                    "Unhandled exception in generator.\n"
                    + str(e) + "\n\n" + s)
                error_dialog.run()
                error_dialog.hide()
        dialog.hide()

    def checkStructure(self, widget):        
        success, compiled = self.compileWithErrorDialog(self.struct_def)
        if not success: 
            return;        
        if self.checkStructureWithErrorDialog(compiled):            
            MessageBox("No errors or wanings found.", self.window)
        #else:
        #    structure check failed
        
    def checkStructureWithErrorDialog(self, compiled):        
        #print_trace()
        check = StructCheck.StructCheck()
        check.check(compiled)
        res = check.results()
        dialog_list = {}
        for i, (err_type, info) in enumerate(res):
            if err_type == StructCheck.SC_MISSING_OFFSET:
                place, array, next = info
                dialog_list[place.getName() + ", " + str(i)] = [
                    ("Missing offset: Childern of array `%s' are not defined with"
                     +" in_memory=True, the array is followed by data element `%s' "
                     +" that does not have defined offset.") %
                     (array.getName(), next.getName())]
            elif err_type == StructCheck.SC_MISSING_OFFSET_ALT:
                place, array, alt, alt_elem = info
                dialog_list[place.getName() + ", " + str(i)] = [
                    ("Missing offset: Childern of array `%s' are not defined with"
                     +" in_memory=True, the array is followed by alternative `%s'"
                     +" in Alternative `%s' and does not have defined offset.")
                    % (array.getName(), alt_elem.getName(), alt.getName())]
            elif err_type == StructCheck.SC_DUPLICATE_NAMES:
                place, name, instances = info
                dialog_list[place.getName() + ", " + str(i)] = [
                    ("Name '%s' is used multiple times for elements %s."
                     % (name, str(", ").join([str(i) for i in instances])))]
            elif err_type == StructCheck.SC_DUPLICATE_STRUCT_NAMES:
                name, instances = info
                dialog_list[name + ", " + str(i)] = [
                    ("Multiple composite entities with name '%s'." % (name))]
            elif err_type == StructCheck.SC_ATTRIB_MISSING_INIT:
                place, name = info
                dialog_list[name + ", " + str(i)] = [
                    ("Attribute '%s' in '%s' does not have init filled.")
                    % (name, place.getName())]
            elif err_type == StructCheck.SC_ALTERNATIVE_MISSING_SELECTOR:
                place, name = info
                dialog_list[name + ", " + str(i)] = [
                     ("Alternative '%s' in '%s' does not have selector filled.")
                     % (name, place.getName())]
            elif err_type == StructCheck.SC_MISSING_ARRAY_SIZE:
                place, name = info
                dialog_list[name + ", " + str(i)] = [
                     ("Array '%s' in '%s' has not defined size.")
                     % (name, place.getName())]
        if (len(dialog_list.keys())>0):
            dialog = CheckStructErrorDialog.CheckStructErrorDialog(dialog_list, "Semantic errors")
            dialog.run()
            dialog.hide()
            return False
        else:
            return True        

    def recentlyInspectedFileItem(self, menu_item, filename):
        self.inspect_data_file(filename, None, None)

    def update_inspected_files_menu(self):
        submenu = gtk.Menu()
        recent_data = self.config.get("recent_data")
        for fname in recent_data:
            item = gtk.MenuItem(fname, use_underline = False)
            item.connect("activate", self.recentlyInspectedFileItem, fname)
            item.show()
            submenu.append(item)
        submenu.show()
        self.recent_data_files.set_submenu(submenu)

    def refreshTitle(self):
        ast = ""
        if self.definition_changed != 0: ast =" *"
        if self.struct_def_filename is not None:
            self.window.set_title("Data2l - "+ self.struct_def_filename + ast )
        else:
            self.window.set_title("Data2l" + ast)

    def About(self, widget, user_data = None):
        xml = gtk.glade.XML(config.get('gui_path'),'AboutDialog')
        about = self.dialog = xml.get_widget('AboutDialog')
        #about.set_parent_window(self.window)
        aboutTextView = xml.get_widget('AboutTextView')
        aboutTextView.modify_font(FontDescription('Sans 9'))
        changelogTextView = xml.get_widget('textview2')
        changelogTextView.modify_font(FontDescription('Sans 9'))
        licenseTextView = xml.get_widget('textview3')
        licenseTextView.modify_font(FontDescription('Sans 9'))
        
        about.run()
        about.hide()
        
    def helpContent(self, widget):
        self.ShowHelp("html/intro.html#intro")        

    def ShowHelp(self, where):
        webbrowser.open("https://bitbucket.org/opekar/data2l/wiki/Help")

    def get_example_defs_list(self):
        example_defs = []
        for ex_dir  in ["examples", os.path.join("..","examples")]:
            examples_dir = os.path.join(os.path.dirname(sys.argv[0]),
                                        ex_dir, "file_defs")
            if os.access(examples_dir, os.R_OK):
                example_defs += [os.path.join(examples_dir,f) for f in os.listdir(examples_dir)]
                                
        return  [f for f in example_defs if ((f.lower()[-3:] == '.py') or (f.lower()[-4:] == '.d2l'))]

    def construct_examples_menu(self):
        submenu = gtk.Menu()
        
        for fname in self.get_example_defs_list():
            item = gtk.MenuItem(fname, use_underline = False)
            item.connect("activate", lambda w,filename: self.open_def_file(filename), fname)
            item.show()
            submenu.append(item)
        submenu.show()
        self.help_examples.set_submenu(submenu)
    

    def editPreferences (self, widget):
        editable_props = [('max_recent_files',("Maximum of recent files",0,'int')), 
                      ('cpp_gen_directory',("Directory for cpp generator",".\\","dir")),
                      ('stack_filters',("Stack filters",False,"bool")),
                      ('custom_generators_path',("Path to custom generators",".\\","dir")),
                      ('custom_readers_path',("Path to custom readers",".\\","dir")),                      
                      ('html_view_on_start',("View html page on start",False,"bool"))]
        
        keys = self.config.keys()
        
        for i,prop in enumerate(editable_props):
            if prop[0] not in keys:
                print "Warning: property", prop[0], "is not in config file"
            else:            
                v = self.config.get(prop[0])
                editable_props[i] = (prop[0], (prop[1][0], v, prop[1][2]))
        
        prop_dialog = PropDialog.PropertiesDialog(editable_props,"Preferences", self.window)
        ret = prop_dialog.run()
        prop_dialog.hide()

        if ret != gtk.RESPONSE_OK:
            return
        
        for prop in editable_props:
            if prop[0] not in keys:
                print "Warning: property", prop[0], "is not in config file"
            else:
                self.config.set(prop[0],prop[1][1])

        self.config.dump()
        self.config.load()

    def keyRelease(self, widget, event):
        if event.keyval == gtk.keysyms.F1:
            if self.help_handle:
                dummy = 0
            else:
                self.helpContent(widget)
        return

    def refresh_gui(self):
        self.docsView.refresh()
        self.validatorsView.refresh()
        self.depsView.refresh()
        self.structTree.refresh()
        # TODO: Dodelat/predelat refresh Dependencies view

    def do_command(self, command, do_refresh = None, undo_refresh = None):
        xassert_type(command, Command.IUndoableCommand)
        if do_refresh is not None and undo_refresh is None:
            undo_refresh = do_refresh
        if (not isinstance(command, Command.GUICommand) or
            do_refresh is not None or undo_refresh is not None):
            tmp = Command.GUICommand()
            tmp.add_sub_command(command, do_refresh, undo_refresh)
            command = tmp

        command.do()

        self.undo_stack.append(command)
        self.undo_menu.set_sensitive(True)
        self.redo_stack = []
        self.redo_menu.set_sensitive(False)
        self.definition_changed += 1;
        self.refreshTitle()

    def undo_command(self):
        xassert(len(self.undo_stack) > 0)
        command = self.undo_stack.pop()

        command.undo()

        self.redo_stack.append(command)
        self.redo_menu.set_sensitive(True)
        if len(self.undo_stack) == 0:
            self.undo_menu.set_sensitive(False)
        self.definition_changed -= 1;
        self.refreshTitle()

    def redo_command(self):
        xassert(len(self.redo_stack) > 0)
        command = self.redo_stack.pop()

        command.do()

        self.undo_stack.append(command)
        self.undo_menu.set_sensitive(True)
        if len(self.redo_stack) == 0:
            self.redo_menu.set_sensitive(False)

        self.definition_changed += 1;
        self.refreshTitle()

    def on_edit_activate(self, widget):
        pass
        edit_paste = self.main_window.get_widget('edit_paste')
        if clipboard.valid_clipboard_data():
            edit_paste.set_sensitive(True)
        else:
            edit_paste.set_sensitive(False)

    def on_undo_activate(self, widget):
        if len(self.undo_stack) > 0:
            self.undo_command()

    def on_redo_activate(self, widget):
        if len(self.redo_stack) > 0:
            self.redo_command()

    def on_edit_copy_activate(self, widget):
        focused_widget=self.window.get_focus()
        if focused_widget == self.structTree.tree:
            self.structTree.clipboard_copy(widget)
        else:
            focused_widget.emit("copy-clipboard")

    def on_edit_paste_activate(self, widget):
        focused_widget=self.window.get_focus()
        if focused_widget == self.structTree.tree:
            self.structTree.clipboard_paste(widget)
        else:
            focused_widget.emit("paste-clipboard")


    def on_cards_toggle(self, widget, index):
        #take care of recursion
        if widget.get_active() and self.notebook.get_current_page() != index:
            self.notebook.set_current_page(index)

    def on_switch_page(self, widget, event, index, user = None):
        #take care of recursion
        cards = self.main_window.get_widget('cards').get_group()
        if not cards[2-index].get_active():
            cards[2-index].set_active(True)

    def save_before_change(self):
        if self.definition_changed != 0:
            msg = gtk.MessageDialog(self.window, gtk.DIALOG_MODAL,
                                    gtk.MESSAGE_QUESTION,
                                    gtk.BUTTONS_YES_NO,
                                    "Definition has been changed. Save changes?")
            msg.set_transient_for(self.window)
            resp = msg.run()
            msg.hide()
            if resp == gtk.RESPONSE_YES:
                self.saveFile(None)

    def on_quit(self, widget):
        #print_trace()
        self.save_before_change()
        gtk.main_quit()

    def _get_new_module(self):
        module_name = "_data2l_structure_" + str(self.module_counter)
        self.module_counter += 1
        module = imp.new_module(module_name)
        return module

    def _init_empty_module(self):
        self.struct_def_filename = None
        self.struct_def = copy.deepcopy(file_defs.empty.definition)
        module = self._get_new_module()
        self.current_definition_module = module
        for k, v in file_defs.empty.__dict__.iteritems():
            module.__dict__[k] = v
        module.definition = copy.deepcopy(file_defs.empty.definition)
        return
        
def main(opts, args):
#    gobject.threads_init()
   
    main = Main()
    
    if len(args) > 0 and len(args[-1]) > 3:
        try:
            main.open_def_file(args[-1])
        except Exception, e:
            print(str(e))
    
    while gtk.events_pending():
        gtk.main_iteration()
    main.opts = opts
    main.update_recent_files_menu()
    main.update_inspected_files_menu()
    print "GTK+ version:", gtk.gtk_version
    print "PyGTK version:", gtk.pygtk_version
    gtk.main()
    ThreadsManager.cleanup()        
    
        
#        pass
        #@TODO  send exceptions and all possible information about the crash to Eccam via https
        # possibly some GTK dialog would be good
        # if an exception it trown again here, dont care, or print something to command line

#profile.run('main()', 'dataviz.prof')
#main()
