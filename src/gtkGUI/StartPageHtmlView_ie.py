import gtk
from pygtkie import IEHtmlView, IEHtmlViewCallback
from StartPageHtmlView_common import StartPageHtmlViewBase


class StartPageHtmlView(IEHtmlViewCallback,StartPageHtmlViewBase):
    def __init__(self, xml, main, config):
        StartPageHtmlViewBase.__init__(self, xml, main, config, IEHtmlView() )
        self.htmlView.setHtmlViewCallback(self)
        self.htmlView.SetDocument(self.GetStartPageContent())

            
    def OnContextMenu(self, source_element):
        return False            
        
    def  OnBeforeNavigate(self, dest):
        return self.OnBeforeNavigateBase(dest)
        