import webkit
from StartPageHtmlView_common import StartPageHtmlViewBase


class StartPageHtmlView(StartPageHtmlViewBase):
    def __init__(self, xml, main, config):
        super(StartPageHtmlView,self).__init__( xml, main, config, webkit.WebView() )
        self.htmlView.load_string(self.GetStartPageContent(), "text/html", "iso-8859-15", "")
        self.htmlView.connect("populate-popup", self.link_clicked)
        self.htmlView.connect("navigation-requested",self.navigation_requested)   
        
    def link_clicked(self, view, menu):
        pass  #dont know how to disable right now1
        
    def navigation_requested(self, view, frame, networkRequest):
        self.OnBeforeNavigateBase(networkRequest.get_uri())
        return 1 #do not follow any link
        
        

        