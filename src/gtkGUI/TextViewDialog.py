import gtk
import gtkGUI


__all__ = """\
TextViewDialog
""".split()


## This class implements functionality of the textViewDialog.
class TextViewDialog(object):
    def __init__(self, title = "Dialog", label = "Message:", text = ""):
        ## Dialog's title.
        self.title = title
        ## Dialog's label.
        self.label = label
        ## Text for the TextView.
        self.text = text
        ## Store for the gtk.Dialog widget.
        self._dialog = None

    ## This methods wraps the dialog's run() method.
    def run(self):
        xml = gtk.glade.XML(gtkGUI.Config.config.get('gui_path'),
                            'textViewDialog')
        self._dialog = xml.get_widget('textViewDialog')
        label_widget = xml.get_widget('label')
        text_widget = xml.get_widget('text_view')

        self._dialog.set_title(self.title)
        label_widget.set_text(self.label)
        buffer = gtk.TextBuffer()
        buffer.set_text(self.text)
        text_widget.set_buffer(buffer)

        ret = self._dialog.run()
        return ret

    ## Calls dialog's hide() method.
    def hide(self):
        self._dialog.hide()
