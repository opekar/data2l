import threading
import Queue
from Utility import xassert, xassert_type, pure_virtual


__all__ = """\
MSG_TERMINATE
IGUIThread
ThreadsManager
register_thread
cleanup
""".split()


## Message signaling the thread it should terminate since
# the whole application is going down.
MSG_TERMINATE = -1


## Common base for all our GUI worker threads.
class IGUIThread(threading.Thread):
    def __init__(self, name = None, queue_len = 0):
        threading.Thread.__init__(self, name = name)

        # Use deamon threads, so that we can exit the application even if
        # we cannot exit all threads.
        self.setDaemon(True)

        self._queue_to_worker = Queue.Queue(queue_len)
        self._threads_manager = None

    def thread_proc(self):
        pure_virtual()

    ## Overriden threading.Thread method.
    def run(self):
        self.thread_proc()
        xassert(self._threads_manager is not None)
        self._threads_manager.unregister_thread(self)
        self._threads_manager = None

    ## This method puts message into queue for worker thread.
    def inform_worker(self, msg_type, data = None):
        self._queue_to_worker.put((msg_type, data))


## Simple "manager" of threads. The main reason for its existence is
# to attempt to let threads exit after signaling them with MSG_TERMINATE
# instead of terminating them in the middle of their work.
class ThreadsManager(object):
    def __init__(self):
        super(ThreadsManager, self).__init__()

        self._known_threads = set()
        self._lock = threading.Lock()

    def register_thread(self, th):
        xassert_type(th, IGUIThread)
        try:
            self._lock.acquire()
            self._known_threads.add(th)
        finally:
            self._lock.release()
        th._threads_manager = self

    def unregister_thread(self, th):
        xassert_type(th, IGUIThread)
        try:
            self._lock.acquire()
            xassert(th in self._known_threads)
            self._known_threads.discard(th)
        finally:
            self._lock.release()

    def cleanup(self):
        signaled_threads = set()
        try:
            self._lock.acquire()
            for th in self._known_threads:
                if th.isAlive():
                    th.inform_worker(MSG_TERMINATE)
                    signaled_threads.add(th)
        finally:
            self._lock.release()

        for th in signaled_threads:
            th.join(10)


## Global threads manager. Use public interface bellow to access it.
_tm_instance = ThreadsManager()


def register_thread(th):
    _tm_instance.register_thread(th)


def cleanup():
    _tm_instance.cleanup()
