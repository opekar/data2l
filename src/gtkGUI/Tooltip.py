import gtk
import gobject
from Utility import print_trace, xassert


__all__ = """\
Tooltip
""".split()


class Tooltip(gtk.Window):
    def __init__(self, text = "", delay = 2000):
        gtk.Window.__init__(self, gtk.WINDOW_POPUP)
        self.set_name("gtk-tooltips")
        self.set_resizable(False)
        self.set_border_width(4)
        self.set_app_paintable(True)
        self.connect('expose-event', self.on_expose_event)

        self.__timer_id = None
        self.__text = text
        self.__delay = delay
        self.__label = gtk.Label(text)
        self.__label.set_line_wrap(True)
        self.__label.set_alignment(0.5, 0.5)
        self.__label.set_use_markup(True)
        self.__label.show()
        self.add(self.__label)

    def hide(self):
        self._remove_timer()
        gtk.Window.hide(self)
        return

    def set_label(self, text):
        self.__text = text
        self.__label.set_text(text)
        #self.__label.set_markup(text)
        return

    def on_timeout(self):
        self.__timer_id = None
        self.hide()
        return

    def on_motion(self, event):
        #print_trace()
        width, _ = self.size_request()
        # TODO: Find out why some events on XWindows do not have this attribute.
        if not hasattr(event, "x_root"):
            return
        self.move(int(event.x_root - width/2), int(event.y_root + 12))
        self.show()
        # Reset the timer on movement.
        self._remove_timer()
        self.__timer_id = gobject.timeout_add(self.__delay, self.on_timeout)
        return

    def on_leave(self, event):
        #print_trace()
        self.hide()
        return

    def on_expose_event(self, tooltip, event):
        #print_trace()
        width, height = self.size_request();
        self.style.paint_flat_box(self.window, gtk.STATE_NORMAL, gtk.SHADOW_OUT,
                                  None, self, 'tooltip', 0, 0, width, height)
        return

    def _remove_timer(self):
        if self.__timer_id is not None:
            gobject.source_remove(self.__timer_id)
        return
