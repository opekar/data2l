from Utility import print_trace, xassert, validate_ident, valid_identifier_regex
from gtkGUI import TextViewDialog

__all__ = """\
string_from_buffer
refresh_tree_row
validate_ident_with_err_dialog
""".split()

## Convenience function for getting string from gtk.TextBuffer.
def string_from_buffer(buf):
    return buf.get_text(buf.get_start_iter(), buf.get_end_iter())


## Refreshes row of a TreeView.
def refresh_tree_row(tree, path = None):
    if path is None:
        path, _ = tree.get_cursor()
    xassert(path is not None)
    model = tree.get_model()
    iter = model.get_iter(path)
    model.row_changed(path, iter)

def validate_ident_with_err_dialog(name):
    if not validate_ident(name):
        dialog = TextViewDialog.TextViewDialog("Error",
                "Identifier name:", 
                "Incorrect identifier name used.\n"
                "First character has to be letter or '_', next characters have to be letters, digits or '_'.\n" 
                "Identifier has to match regex " + valid_identifier_regex)
        dialog.run()
        dialog.hide()
        return False
    return True