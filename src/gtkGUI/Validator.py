import gobject
import gtk
import copy
import inspect
import traceback
import threading
import locale
import Definition
import Dependency
import Assertion as A
import Reader
import DOM
import SAX
import exprs
from gtkGUI import Command, Errors, Config
import gtkGUI.ThreadsManager as TM
from gtkGUI.Utils import string_from_buffer
from Utility import xassert, xassert_type, impossible, print_trace
from GetterLangSyntaxHighlighter import GetterLangSyntaxHighlighter

__all__ = """\
Validator
ValidatorTaskThread
""".split()


_ENTITY_COL = 0
_NAME_COL = 1


def _entity_sort_func(model, iter1, iter2, user_data = None):
    dep1 = model[iter1][_ENTITY_COL]
    dep2 = model[iter2][_ENTITY_COL]
    res = locale.strcoll(dep1.getName().upper(), dep2.getName().upper())
    return res


def _desc_sort_func(model, iter1, iter2, user_data = None):
    i1, _ = model[iter1][_NAME_COL]
    i2, _ = model[iter2][_NAME_COL]
    dep1 = model[iter1][_ENTITY_COL]
    dep2 = model[iter2][_ENTITY_COL]
    ass1 = dep1.assertions[i1]
    ass2 = dep2.assertions[i2]

    name1 = ass1.getName()
    if name1 is None:
        name1 = ""
    name2 = ass2.getName()
    if name2 is None:
        name2 = ""
    res = locale.strcoll(name1.upper(), name2.upper())
    return res


def _dep_data_func(column, cell_renderer, model, iter, user_data = None):
    name = model[iter][_ENTITY_COL].getName()
    cell_renderer.set_property('text', name)


def _assertion_data_func(column, cell_renderer, model, iter,
                         user_data = None):
    i, _ = model[iter][_NAME_COL]
    assertion = model[iter][_ENTITY_COL].assertions[i]
    name = assertion.getName()
    cell_renderer.set_property('text', name)


## \brief This is a controler class for whole Validators tab in main
# window.
#
# The sequence of user action of selecting validator in table by
# clicking on it, clicking into code view of the selected validator,
# editing its text and finally clicking onto another validator in the
# table produces sequence following sequence of events:
#
# \verbatim
# Validator.py:cursor_changed()
# Validator.py:focus_in()
# Validator.py:buffer_changed()
# [...]
# Validator.py:buffer_changed()
# Validator.py:cursor_changed()
# Validator.py:focus_out()
# \endverbatim
#
# In \c cursor_changed() handler, before the code view is focused, we
# gather information about the validator that is going to be edited,
# like its Dependency, index of Assertion in list and the Assertion
# object itself. Next, in \c focus_in(), we enable \c buffer_changed()
# handler and watch for changes of the code view, caching the new text
# on each change in \c _code_text variable. When user clicks onto
# another validator in the table then \c cursor_changed() is fired
# again, this time disabling the \c buffer_changed()
# handler. Subsequent \c focus_out() updates the edited validator code
# in currently loaded definition.
class Validator(object):
    def __init__(self, main, xml):
        self.main = main
        self.xml = xml

        ## gtk.TreeView of the validators table.
        self.validators_table = xml.get_widget("validators_table")
        self.validators_table.insert_column_with_data_func(_ENTITY_COL,
                                                           "Entity",
                                                           gtk.CellRendererText(),
                                                           _dep_data_func)
        column = self.validators_table.get_column(_ENTITY_COL)
        column.set_sort_column_id(_ENTITY_COL)
        column.set_resizable(True)
        renderer = gtk.CellRendererText()
        renderer.set_property("editable", True)
        renderer.connect("edited", self.cell_edited, _NAME_COL)

        self.validators_table.insert_column_with_data_func(_NAME_COL,
                                                           "Description",
                                                           renderer,
                                                           _assertion_data_func)
        column = self.validators_table.get_column(_NAME_COL)
        column.set_sort_column_id(_NAME_COL)
        column.set_resizable(True)

        self.validators_table.connect("cursor-changed", self.cursor_changed)
        self.validators_table.connect('button_press_event', self.showContextMenu)
        ## Underlying list of validators for tree view.
        self.list_store = gtk.ListStore(object, object)
        self.list_store.set_sort_func(_ENTITY_COL, _entity_sort_func)
        self.list_store.set_sort_func(_NAME_COL, _desc_sort_func)
        self.list_store.set_sort_column_id(_ENTITY_COL, gtk.SORT_ASCENDING)
        self.validators_table.set_model(self.list_store)        

        ## gtk.TextView of validator's code.
        self.validator_code = xml.get_widget("validator_code")
        
        ## syntax highlighter
        syntaxHighlighter = GetterLangSyntaxHighlighter()
        syntaxHighlighter.replaceBuffer(self.validator_code)
        
        self.validator_code.connect("focus-out-event", self.focus_out)
        self.validator_code.connect("focus-in-event", self.focus_in)
        ## Underlying buffer of code view.
        self.validator_code_buf = self.validator_code.get_buffer()
        ## Handler ID of registered \c buffer_changed() handler.
        self.validator_code_buf_changed_handle = None
        ## Currently loaded file definition.
        self.definition = None

        self.main.main_menu.connect("deactivate", self.main_menu_deactivate)

        self.validator_instances = xml.get_widget("validator_instances")
        self.instances_store = gtk.ListStore(str, object)
        self.validator_instances.set_model(self.instances_store)
        self.validator_instances.append_column(gtk.TreeViewColumn("Path   ",gtk.CellRendererText(), text = 0))
        self.validator_instances.connect('button_press_event', self.synchronize_treeview)

        ## Set of already visited nodes in definition during search
        ## for validators.
        self._visited = None
        ## True when code view is focused.
        self._code_focused = False
        ## Cache for code view text. Updated in \c buffer_changed().
        self._code_text = None
        ## Tuple \c (Dependency, index, Assertion) of currently edited
        ## validator.
        self._focused_info = None

    def cell_edited(self, cell, path, new_text, column):
        #print_trace()
        if column == _NAME_COL:
            dep = self.list_store[path][_ENTITY_COL]
            index, _ = self.list_store[path][_NAME_COL]
            assertion = self.list_store[path][_ENTITY_COL].assertions[index]
            if new_text == assertion.getName():
                return
            if new_text == "":
                new_text = None
            new_assertion = \
                A.Assertion(name = new_text,
                            code = exprs.ExprVE.ExprVE(assertion.getCode().getText()))
            command = Command.EditAssertion(dep, index, new_assertion)
            path, col = self.validators_table.get_cursor()
            self.main.do_command(command, self.refresh)
            self.validators_table.set_cursor(path, col)

    def buffer_changed(self, buf):
        #print_trace()
        self._code_text = string_from_buffer(buf)

    def main_menu_deactivate(self, menu_shell):
        #print_trace()
        self._focus_out_action()
        return

    def focus_in(self, widget, event, data = None):
        #print_trace()
        self.validator_code_buf_changed_handle \
            = self.validator_code_buf.connect("changed", self.buffer_changed)
        self._code_focused = True

    def focus_out(self, widget, event, data = None):
        #print_trace()
        self._focus_out_action()
        return

    def _focus_out_action(self):
        #print_trace()
        if self._focused_info is not None:
            dep, index, assertion = self._focused_info
            code_text, self._code_text = self._code_text, None
            if code_text is None or code_text == assertion.getCode().getText():
                if self.validator_code_buf_changed_handle is not None:
                    self.validator_code_buf.disconnect(
                        self.validator_code_buf_changed_handle)
                    self.validator_code_buf_changed_handle = None
                self._code_focused = False
                self._focused_info = None
                self._code_text = None
                return

            new_assertion = A.Assertion(name = assertion.getName(),
                                        code = exprs.ExprVE.ExprVE(code_text))
            command = Command.EditAssertion(dep, index, new_assertion)
            self.main.do_command(command, self.refresh)

        if self.validator_code_buf_changed_handle is not None:
            self.validator_code_buf.disconnect(
                self.validator_code_buf_changed_handle)
            self.validator_code_buf_changed_handle = None
        self._code_focused = False
        self._focused_info = None
        self._code_text = None

    def cursor_changed(self, widget, data = None):
        #print_trace()
        if (self._code_focused
            and self.validator_code_buf_changed_handle is not None):
            self.validator_code_buf.disconnect(
                self.validator_code_buf_changed_handle)
            self.validator_code_buf_changed_handle = None

        buf = self.validator_code_buf
        path, _ = self.validators_table.get_cursor()
        if path is None:
            buf.set_text("")
        else:
            model, iter = self.validators_table.get_selection().get_selected()
            if iter is not None:
                i, _ = model[iter][_NAME_COL];
                assertion = model[iter][_ENTITY_COL].assertions[i]
                if not self._code_focused:
                    self._focused_info = (model[iter][_ENTITY_COL], i,
                                          assertion)
                    self.instances_store.clear()
                    self._search_validator_instances(self.main.struct_def.dep_root, model[iter][_ENTITY_COL],  "", [0])
                xassert_type(assertion.getCode(), exprs.ExprVE.ExprVE)
                xassert(assertion.getCode().getText() is not None)
                buf.set_text(assertion.getCode().getText())
        return

    def set_definition(self, definition):
        xassert_type(definition, Definition.Definition)
        self.definition = definition

    def refresh(self):
        # Refresh validators table.
        self.validators_table.set_model(None)
        self.list_store.clear()
        self._visited = set()
        self._rec_refresh(self.definition.dep_root)
        self._visited = None
        self.validators_table.set_model(self.list_store)
        # Refresh selected validator's code view through simulated
        # cursor change.
        self.validators_table.emit("cursor-changed")

    ## recursive refresh
    # fills up the validator list store which then updates the
    # view itself
    ##
    def _rec_refresh(self, element_dep):
        xassert(element_dep is not None)
        element = element_dep.getElement()
        if element in self._visited:
            return
        else:
            self._visited.add(element)

        if len(element_dep.assertions) > 0:
            for i, a in enumerate(element_dep.assertions):
                iter = self.list_store.append()
                ## \TODO _NAME_COL sloupec by si nemel ukladat assertion objekt je to nebezpcne,
                #   hlavne kvuli undu, zatime jsou vsechny tyto reference popsany tak aby assertion
                #   objekt byl bran podle indexu, to je asi to jedine co by tam melo zbyt
                self.list_store.set(iter, _ENTITY_COL, element_dep,
                                    _NAME_COL, (i, a))

        # Recurse.
        deps = element.getDeps()
        for d in deps:
            self._rec_refresh(d)

        return

    def _search_validator_instances(self, element_dep, validator_dep, path_name, path):
        if len(path_name) > 0:
            path_name = path_name + "-"
        path_name = path_name + element_dep.getName()
        if element_dep == validator_dep:
            self.instances_store.append((path_name, tuple(path)))

        for i, d in enumerate( element_dep.getType().getDeps() ):
            p = copy.copy(path)
            p.append(i)
            self._search_validator_instances(d, validator_dep, path_name, p )

    def synchronize_treeview(self, widget, event):
        p = widget.get_path_at_pos( int(event.x), int(event.y) )
        if p is not None:
            widget.set_cursor(p[0])
            path_name, treeview_path = self.instances_store[p[0]]
           
            self.main.structTree.select_element(treeview_path)



    def build_validators_table_popup_menu(self, event):
        path, _ = self.validators_table.get_cursor()
        xassert(path is not None)
        iter = self.list_store.get_iter(path)
        xassert(iter is not None)

        dep = self.list_store[iter][_ENTITY_COL]
        xassert_type(dep, Dependency.Dependency)

        menu = gtk.Menu()

        mi = gtk.ImageMenuItem(stock_id = "gtk-delete")
        mi.connect("activate", self.delete_validator)
        menu.append(mi)

        menu.show_all()
        menu.popup(None, None, None, 0, event.time)

    def showContextMenu(self, widget, event):
        if event.button == 3:
            xassert(event.window == widget.get_bin_window())
            p = widget.get_path_at_pos(int(event.x), int(event.y))
            if p is not None:
                widget.set_cursor(p[0])
                self.build_validators_table_popup_menu(event)

    def add_validator(self, validator):
        # needed to find out validator row to start editing it        
        iter = self.list_store.get_iter_first()                
        while iter is not None:
            _,assertion = self.list_store[iter][_NAME_COL]
            if assertion == validator:
                # iter to currently added assertion found, so set editing
                path = self.list_store.get_path(iter)
                self.validators_table.set_cursor(
                    path, self.validators_table.get_column(_NAME_COL), start_editing=True)
                break
            iter = self.list_store.iter_next(iter)            

    def delete_validator(self, item):
        path, _ = self.validators_table.get_cursor()
        xassert(path is not None)
        iter = self.list_store.get_iter(path)
        xassert(iter is not None)
        dep = self.list_store[iter][_ENTITY_COL]
        index, _ = self.list_store[iter][_NAME_COL]
        command = Command.DeleteAssertion(dep, index)
        path, _ = self.validators_table.get_cursor()
        self.main.do_command(command, self.refresh)
        if path is not None:
            xassert(len(path) == 1)
            if path[0] == 0 and len(self.list_store) == 0:
                path = None
            elif path[0] == len(self.list_store):
                path = list(path)[0] - 1
            if path is not None:
                self.validators_table.set_cursor(path)
            else:
                self.instances_store.clear()
        return


_MSG_DONE = 0
_MSG_PROGRESS = 1
_MSG_IOERROR = 2
_MSG_DIAGNOSTICS = 3
_MSG_FINISHED = 4
_MSG_UNKNOWN = 5

_REQ_QUIT = 0
_REQ_PROGRESS = 1

## Update progress every 500 miliseconds.
_TIMER_PERIOD = 500
## Number of lines of progress label in validator dialog.
_DIALOG_LINES = 5


## Custom exception that is used only when we are cancelling
# the worker thread.
class TerminateThreadException(Exception):
    def __init__(self):
        Exception.__init__(self)


## This class implements validator functionality. The validation itself is
# done in a worker thread. The main thread communicates with the worker thread
# using explicit message queue, while the worker thread uses
# gobject.idle_add(self.thread_callback, data).
class ValidatorTaskThread(TM.IGUIThread, SAX.SAXHandler):
    def __init__(self, filename, compiled_definition):
        TM.IGUIThread.__init__(self, name = "ValidatorTask")
        SAX.SAXHandler.__init__(self)

        self.filename = filename
        self.compiled_def = compiled_definition

        self._check_period = 100
        self._check_counter = 0
        self._done = False

        # Dialog setup.
        xml = gtk.glade.XML(Config.config.get('gui_path'),'validatorDialog')
        self._dialog = xml.get_widget('validatorDialog')
        self._dialog.connect('delete-event', self.on_close)
        label_widget = xml.get_widget('label')
        self._text_view = xml.get_widget('text_view')
        self._text_view.set_editable(False)
        self._text_view.set_wrap_mode(gtk.WRAP_WORD)
        self._progress_label = xml.get_widget('progress_label')

        self._dialog.set_title("Validating file " + self.filename)
        label_widget.set_text("Progress:")
        self._buffer = gtk.TextBuffer()
        self._buffer.set_text("Starting validation...\n\n")
        self._text_view.set_buffer(self._buffer)

        self._ok_button = xml.get_widget('ok_button')
        self._ok_button.connect('clicked', self._on_ok_button)
        self._ok_button.set_sensitive(False)
        self._cancel_button = xml.get_widget('cancel_button')
        self._cancel_button.connect('clicked', self._on_cancel_button)

    def _on_ok_button(self, button):
        self.hide()

    def _on_cancel_button(self, button):
        self.inform_worker(_REQ_QUIT)

    def hide(self):
        self._dialog.hide()

    def start(self):
        # Start the worker thread first...
        threading.Thread.start(self)
        # ...and then prime progress request timer.
        gobject.timeout_add(_TIMER_PERIOD, self.timer_callback)

    ## Overriden ThreadsManager.IGUIThread method.
    def thread_proc(self):
        f = Reader.SafeReader(Reader.ReaderA(self.filename),
                              throw_exceptions = True)
        try:            
            dom = DOM.DOMStruct(f, self.compiled_def.dep_root, None)
            dom.Init()
            sax_parser = SAX.SAXParser(dom, skip_simple = False)
            sax_parser.parse(self)
        except Reader.SafeReaderException, e:
            trace = None
            dialog_text = ""
            try:
                trace = inspect.trace()
                dialog_text = Errors.format_sax_trace(trace)
            finally:
                del trace
            self.inform_main(_MSG_IOERROR, dialog_text)
        except DOM.DOMException, e:
            self.inform_main(_MSG_IOERROR, e.message)            
        except TerminateThreadException:
            # Inform the main thread that we have finished and that it can
            # close the dialog window.
            self.inform_main(_MSG_FINISHED)
            return
        except Exception, e:
            trace = None
            stack_trace = traceback.format_exc()
            self.inform_main(_MSG_UNKNOWN, (str(e), stack_trace))
            return
        # Inform the main thread that we have finished normaly.
        self.inform_main(_MSG_DONE)
        return

    ## Overriden SAX.SAXHandler method, the core of the validation.
    def endElement(self, dom_element):
        self._check_counter += 1
        # Check messages queue if it is the right time.
        if self._check_counter % self._check_period == 0:
            self._check_counter = 0
            while not self._queue_to_worker.empty():
                msg = self._queue_to_worker.get()
                msg_type, _ = msg
                #print "Worker got message:", str(msg)
                if msg_type == _REQ_PROGRESS:
                    # Report progress as path to the current DOM element.
                    path = []
                    current_dom = dom_element
                    while current_dom is not None:
                        path.append(current_dom)
                        current_dom = current_dom.parentDOM()
                    path.reverse()
                    progress_text = ""
                    lines = []
                    for i, o in enumerate(path):
                        if i != 0:
                            progress_text += "."
                        progress_text += o.debug_string()
                        if isinstance(o, DOM.DOMArray) and i != 0:
                            lines.append(progress_text)
                            progress_text = ""

                    if len(lines) > _DIALOG_LINES:
                        # Cut out middle lines except the last one if there
                        # are more than X lines.
                        lines = (lines[0 : _DIALOG_LINES - 1]
                                 + lines[_DIALOG_LINES - 1 : _DIALOG_LINES])
                        lines[_DIALOG_LINES - 1] = ".." + lines[_DIALOG_LINES - 1]
                    else:
                        # Make sure there are at least X lines of text,
                        # even if some of them are empty. Keep this number
                        # in sync with height of the label in Glade template.
                        for _ in xrange(_DIALOG_LINES - len(lines)):
                            lines.append("")

                    self.inform_main(_MSG_PROGRESS, str("\n").join(lines))
                elif msg_type == _REQ_QUIT or msg_type == TM.MSG_TERMINATE:
                    # Terminate worker thread by throwing exception which gets
                    # caught in run() method.
                    raise TerminateThreadException()
                else:
                    impossible()

        dep = dom_element.getDependency()
        for assertion in dep.assertions:
            ret = assertion.eval(dom_element)
            if not bool(ret):
                trace_text = ""
                try:
                    trace = inspect.stack()
                    trace_text = Errors.format_sax_trace(trace)
                finally:
                    del trace
                self.inform_main(_MSG_DIAGNOSTICS, trace_text)

    ## This method sends message to main thread using gobject.idle_add().
    def inform_main(self, msg_type, data = None):
        gobject.idle_add(self.thread_callback, (msg_type, data))

    ## This function is the receiver of messages from worker thread
    # in main thread.
    def thread_callback(self, msg):
        #print "Got message from worker:", str(msg)
        msg_type, data = msg
        buf = self._text_view.get_buffer()
        if msg_type == _MSG_DONE:
            # Print "Done validating." and some summary to the window.
            buf.insert(buf.get_end_iter(), "Done.")
            self._ok_button.set_sensitive(True)
            self._cancel_button.set_sensitive(False)
            self._done = True
        elif msg_type == _MSG_UNKNOWN:
            ex_str, trace_text = data
            buf.insert(buf.get_end_iter(),
                       "\n\nUnknown exception: " + ex_str + "\n"
                       "Stack trace:\n" + trace_text + "\n")
            self._ok_button.set_sensitive(True)
            self._cancel_button.set_sensitive(False)
            self._done = True
        elif msg_type == _MSG_PROGRESS:
            # Show progress somehow.
            self._progress_label.set_text(data)
            self._progress_label.set_justify(gtk.JUSTIFY_LEFT)
        elif msg_type == _MSG_IOERROR:
            buf.insert(buf.get_end_iter(), data)
        elif msg_type == _MSG_DIAGNOSTICS:
            buf.insert(buf.get_end_iter(), "Error:\n" + data)
        elif msg_type == _MSG_FINISHED:
            self._done = True
            self._dialog.hide()
        else:
            impossible()

    ## Timer callback. Its purpose is to request status report form the worker
    # thread.
    def timer_callback(self):
        if not self._done:
            self.inform_worker(_REQ_PROGRESS)
            return True
        else:
            return False

    def on_close(self, widget, event):
        self.inform_worker(_REQ_QUIT)
