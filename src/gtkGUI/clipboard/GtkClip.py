import gtk
from Utility import print_trace, impossible


__all__ = """\
put_into_clipboard
get_from_clipboard
valid_clipboard_data
""".split()


## Register our own gtk.gdk.Atom.
_content_atom = gtk.gdk.atom_intern("DT_TREE_MODEL_ROW")
#print "Registred gtk.gdk.Atom", _content_atom

# if text form shouldn't be allowed, commend last three lines
#  - if text form allowed, you can copy/paste structures between
#	 notepad and data2l treeview
_SOURCES = [("DT_TREE_MODEL_ROW", 0, 0),
            ("text/plain", 0, 1),
            ("UTF8_STRING", 0, 2),
            ("text/unicode", 0, 3)
            ]


# Find out whether we want CLIPBOARD or PRIMARY clipboard?
_clipboard = gtk.clipboard_get()


def _get_func(clipboard, selection, info, data):
    #print_trace()
    if info == 0:
        selection.set("DT_TREE_MODEL_ROW", 8, data)
    elif info == 1:
        selection.set("text/plain", 8, data)
    elif info == 2:
        selection.set("UTF8_STRING", 8, data.encode("utf8"))
    elif info == 3:
        selection.set("text/unicode", 16, data.encode("utf16"))
    else:
        impossible()
    return


def _clear_func(clipboard, data):
    #print_trace()
    return


## This function pastes data onto clipboard.
def put_into_clipboard(text):
    #print_trace()
    _clipboard.set_with_data(_SOURCES, _get_func, _clear_func, text)
    return


## This retrieves data pasted on clipboard.
def get_from_clipboard():
    #print_trace()
    selection = _clipboard.wait_for_contents("DT_TREE_MODEL_ROW")
    data = selection.data
    return data


## Are the data in clipboard interesting for us?
def valid_clipboard_data():
    #print_trace()
    if _clipboard.wait_is_target_available("DT_TREE_MODEL_ROW"):
        return True
    else:
        return False
