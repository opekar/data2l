__all__ = """\
put_into_clipboard
get_from_clipboard
valid_clipboard_data
""".split()

## This function pastes data onto clipboard.
def put_into_clipboard(_):
    pass


## This retrieves data pasted on clipboard.
def get_from_clipboard():
    return None
    
    
## Are the data in clipboard interesting for us?
def valid_clipboard_data():
    return False
