import pythoncom
import win32com
import win32con
import winerror
import win32clipboard
from win32com.server import util
from win32com.server import exception
import atexit
from gtkGUI.clipboard.Exception import ClipboardException
from Utility import print_trace, xassert


__all__ = """\
put_into_clipboard
get_from_clipboard
valid_clipboard_data
""".split()


IDataObject_Methods = """GetData GetDataHere QueryGetData
                         GetCanonicalFormatEtc SetData EnumFormatEtc
                         DAdvise DUnadvise EnumDAdvise""".split()


## Registered clipboard format.
_entity_clipboard_format = win32clipboard.RegisterClipboardFormat("DataViz_entity")
#print ("Registered 'DataViz_entity' clipboard format as %d (0x%04x)"
#       % (_entity_clipboard_format, _entity_clipboard_format))


## Our currently used IDataObject, if any.
_current_idataobject = None

_supported_clipboard_formats = (_entity_clipboard_format, win32con.CF_UNICODETEXT)

## Cleanup function for the clipboard.
def _at_exit_cleanup():
    #print_trace()
    if (_current_idataobject is not None
        and pythoncom.OleIsCurrentClipboard(_current_idataobject)):
        pythoncom.OleFlushClipboard()
    return


# Register application exit cleaup callback.
atexit.register(_at_exit_cleanup)


## Implementation of IDataObject interface used by OLE clipboard functions.
class ClipboardEntityData(object):
    _com_interfaces_ = [pythoncom.IID_IDataObject]

    _public_methods_ = IDataObject_Methods

    def __init__(self, text):
        print_trace()
        #global num_do_objects
        #num_do_objects += 1
        self.__text = unicode(text)
        self.__supported_fe = []
        for cf in _supported_clipboard_formats:
            fe = cf, None, pythoncom.DVASPECT_CONTENT, -1, pythoncom.TYMED_HGLOBAL
            self.__supported_fe.append(fe)

    def __del__(self):
        print_trace()
        #global num_do_objects
        #num_do_objects -= 1
        pass

    def _query_interface_(self, iid):
        print_trace()
        if iid == pythoncom.IID_IEnumFORMATETC:
            return util.NewEnum(self.__supported_fe, iid = iid)

    def GetData(self, fe):
        print_trace()
        ret_stg = None
        cf, target, aspect, index, tymed = fe
        if (aspect & pythoncom.DVASPECT_CONTENT
            and tymed==pythoncom.TYMED_HGLOBAL):
            if (cf == _entity_clipboard_format
                or cf == win32con.CF_UNICODETEXT):
                ret_stg = pythoncom.STGMEDIUM()
                ret_stg.set(pythoncom.TYMED_HGLOBAL, self.__text)

        if ret_stg is None:
            raise exception.COMException(hresult = winerror.E_NOTIMPL)
        return ret_stg

    def GetDataHere(self, fe):
        print_trace()
        raise exception.COMException(hresult = winerror.E_NOTIMPL)

    def QueryGetData(self, fe):
        print_trace()
        cf, target, aspect, index, tymed = fe
        if aspect & pythoncom.DVASPECT_CONTENT == 0:
            raise exception.COMException(hresult = winerror.DV_E_DVASPECT)
        if tymed != pythoncom.TYMED_HGLOBAL:
            raise exception.COMException(hresult = winerror.DV_E_TYMED)
        return None # should check better

    def GetCanonicalFormatEtc(self, fe):
        print_trace()
        raise exception.COMException(winerror.DATA_S_SAMEFORMATETC)
        # return fe

    def SetData(self, fe, medium):
        print_trace()
        raise exception.COMException(hresult = winerror.E_NOTIMPL)

    def EnumFormatEtc(self, direction):
        print_trace()
        if direction != pythoncom.DATADIR_GET:
            raise exception.COMException(hresult = winerror.E_NOTIMPL)
        return util.NewEnum(self.__supported_fe,
                            iid = pythoncom.IID_IEnumFORMATETC)

    def DAdvise(self, fe, flags, sink):
        print_trace()
        raise exception.COMException(hresult = winerror.E_NOTIMPL)

    def DUnadvise(self, connection):
        print_trace()
        raise exception.COMException(hresult = winerror.E_NOTIMPL)

    def EnumDAdvise(self):
        print_trace()
        raise exception.COMException(hresult = winerror.E_NOTIMPL)


## This function pastes data onto clipboard.
def put_into_clipboard(text):
    try:
        obj = ClipboardEntityData(text)
        wrapped_obj = util.wrap(obj, pythoncom.IID_IDataObject)
        pythoncom.OleSetClipboard(wrapped_obj)
        _current_idataobject = wrapped_obj
        return True
    except exception.COMException, e:
        raise ClipboardException(e)
    except pythoncom.com_error, e:
        raise ClipboardException(e)
    return


## This retrieves data pasted on clipboard.
def get_from_clipboard():
    medium = None
    try:
        obj = pythoncom.OleGetClipboard()        
        cf = _get_cf_from_clipboard(obj)        
        xassert(cf is not None) # before get_from_clipboard is ever called valid_clipboard_data
        fe = (cf, None, pythoncom.DVASPECT_CONTENT, -1, pythoncom.TYMED_HGLOBAL)
        obj.QueryGetData(fe)
        medium = obj.GetData(fe)
    except exception.COMException, e:
        if e.hresult in [winerror.DV_E_DVASPECT, winerror.DV_E_TYMED]:
            print "Clipboard's data don't have expected format."            
            return None
        else:
            raise ClipboardException(e)
    except pythoncom.com_error, e:
        raise ClipboardException(e)
    data = medium.data
    text = data.decode("UTF-16")
    # Strip trailing \00 that is probably being introduced by the PythonCOM
    # machinery.
    if ord(text[-1]) == 0:
        text = text[:-1]
    return text


## Are the data in clipboard interesting for us?
def valid_clipboard_data():
    try:        
        obj = pythoncom.OleGetClipboard()        
        cf = _get_cf_from_clipboard(obj)
        if cf is None:
            return False
        fe = (cf, None, pythoncom.DVASPECT_CONTENT, -1, pythoncom.TYMED_HGLOBAL)
        obj.QueryGetData(fe)
    except exception.COMException, e:
        return False
    except pythoncom.com_error, e:
        return False
    return True

def _get_cf_from_clipboard(obj):
    fe_supported = obj.EnumFormatEtc(pythoncom.DATADIR_GET)                
    cf = None
    for fe in fe_supported:
        cf, _, _, _, _ = fe
        if cf in _supported_clipboard_formats:
            break
    return cf
