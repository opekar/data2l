import platform
from Exception import ClipboardException


_osname = platform.system().lower()
if _osname == "windows":
    #print "Using win32 clipboard."
    from gtkGUI.clipboard.Win32 import *
elif _osname in ["linux", "freebsd"]:
    from gtkGUI.clipboard.GtkClip import *
else:
    from gtkGUI.clipboard.Null import *
del _osname


__all__ = """\
put_into_clipboard
get_from_clipboard
valid_clipboard_data
ClipboardException
""".split()
