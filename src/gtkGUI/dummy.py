# Artificially add some modules for py2exe to find them.
import gtk.gdk
import gtk.glade
import gtk.keysyms
import cairo
import cairo._cairo
import atk
import pango
import pangocairo
import platform

import Cheetah.DummyTransaction

  
if platform.system() == "Windows":
    import win32com.axcontrol
    import win32com.server.exception
    import win32com.server.util
    import win32com.client
    import pythoncom
    
