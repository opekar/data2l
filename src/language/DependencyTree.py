
from Dependency import *
from DataTypes import *
from BasicTypes import *
from LanguageUtils import *
from Assertion import Assertion
from Enum import Enum

## List of exported names by "from *".
__all__ = """\
SemanticException
DependencyTree
""".split()


class SemanticException(Exception):
    def __init__(self, line, value):
         self.value = value
         self.line = line
    def __str__(self):
         if self.line >= 0: 
             return "Semantic error on line "+str(self.line)+": "+str(self.value)
         else: 
             return "Semantic error: "+str(self.value)

# class DependencyTree builds the dependecy tree from the structure created by the Parser class
# The work is done in the constructor and the result is stored in rootDependency member.
class DependencyTree:
        
    # param comments, codes: the list of commentst and sorce codes made 
    #                        by Preprocessing class    
    def __init__(self, forest, comments, codes):
        
        # _needs: a dictionary { element_name : tupple( list, list, list ) }
        # _needs: dictionary of elements which must be created before the key element is.
        # It also holds the tree of its key element in second half of value tupple.
        # The tree is replaced by the element itself as soon as it is created, so finally
        # it is a dictionary { element_name : tupple( [], Element ) }
        self._needs = {}
        self._declared = {}
        self._declaredRoot = None
        
        
        self.comments = comments
        self.codes = codes
        self.rootDependency = None
        # User defined enums
        self.enums = []
        
        for tree in forest:
             self._addToDependencies(tree)
            
        for name in self._declared.keys():
            if name not in self._needs.keys():
                raise SemanticException(self._declared[name], "Declaration of \""+name+"\" not defined.")
            
        i = 0
        last = -1
        elements = self._needs.keys()
        elements.reverse()
        while len(elements) > 0:            
            if i >= len(elements): 
                i = 0
                if last == len(elements): break
                last = len(elements)

            
            # we have an element with all dependences resolved    
            if len(self._needs[elements[i]][0]) == 0:
                dependency = self._makeDependency(self._needs[elements[i]][1])
                
                # try to delete all declarations of currently defined element
                for e in elements:                    
                    # it's possible than in same level is element multiple times
                    numb = len(self._needs[e][0])
                    for idx in range(numb):
                        try:
                            self._needs[e][0].remove(elements[i])
                        except Exception:
                            pass
                
                del elements[i]   
            else:
                i+=1
                                
        
        if len(elements) > 0:
            raise SemanticException(self._needs[elements[i]][1][3], "Cyclic declaration of \""+self._needs[elements[i]][0][0]+"\" can not be resolved.")
        
        if self.rootDependency == None:
            raise SemanticException(-1, "No root element declared.")
    
    
    class _ElementHeader:
        
        def __init__(self, tree):

            i=0
            self.root = False
            if tree[i] == 'root': 
                i+=1 
                self.root = True

        
            self.inmemory = False
            if tree[i] == 'inmemory': 
                i+=1 
                self.inmemory = True
                
            self.attribute = False
            if tree[i] == 'attribute': 
                i+=1 
                self.attribute = True
            
            type = tree[i]
            self.simpleArray = False
            if len(type) > 2 and type[-2:] == '[]' and Consts.isType(type[:-2]):
                type = type[:-2]
                self.simpleArray = True
            
            # check if it's definition of enum
            if type == 'enum':
                self.type = type
                self.name = tree[i+1]
                self.code = tree[i+2]
                self.line = tree[i+4]
                return
            
            #if the tree is not an element but a getter or a property...    
            if not Consts.isType(type):
                self.type = None
                return
            
            self.type = type
            self.name = tree[i+1]
            self.comment = tree[i+2]
            self.line = tree[i+3]
            
            self.getters = []
            self.valids = []
            self.attrs = []
            self.props = []
            self.enum = None

            usedG = set()
            usedP = set()            
            for j in range(i+4,len(tree)):
                if tree[j][0] == 'g': 
                    if tree[j][1] == 'validator' :
                        self.valids.append(tree[j])
                    else:
                        if tree[j][1] in usedG:
                            raise SemanticException(tree[j][4], "Ambiguous "+tree[j][1]+" getter definition.")
                        else:
                            self.getters.append(tree[j])
                            usedG.add(tree[j][1])
                elif tree[j][0] == 'p':
                    if tree[j][1] in usedP:
                        raise SemanticException(tree[j][4], "Ambiguous definition of property \""+tree[j][1]+"\".")
                    else:
                        usedP.add(tree[j][1])
                        self.props.append(tree[j])
                elif tree[j][0] == 'e':
                    if self.enum is not None:
                        raise SemanticException(tree[j][4], "Ambiguous assignment of enum.")
                    else:                         
                        self.enum = tree[j][1]
                else:
                    self.attrs.append(tree[j])
            
            
    def _addToDependencies(self, tree, top = True):
                
        i = 0
        elementHeader = self._ElementHeader(tree)
        deps = []
        defs = []
                        
        # if tree is type and not e.g. getter
        if elementHeader.type != None:
        
            if elementHeader.root and not elementHeader.type == "struct":
                raise SemanticException(elementHeader.line,"Root element has to be struct type.")
            
            if elementHeader.type == "enum":                
                # first check if enum with required name isn't yet defined
                if self._findEnumByName(elementHeader.name) is not None:
                    raise SemanticException(elementHeader.line, 
                        "Enum redefinition! Enum " + elementHeader.name + " is currently defined.")
                self.enums.append(Enum(elementHeader.name, self.codes[elementHeader.code]))
                return
            
            elif not Consts.isComposite(elementHeader.type):
                return []
            
            else:
                
                if top and not elementHeader.root and len(elementHeader.getters):
                    raise SemanticException(elementHeader.getters[0][4],"Top level non root element can not have getters.")
            
                definition = False
                for child in elementHeader.attrs:
                    if child[0] == 'empty':    # special keyword for defining empty composite
                        tree.remove(child)
                        definition = True
                        continue                        
                    dep = self._addToDependencies(child, False)
                    if dep != None:                            
                        deps.extend(dep)
                        if elementHeader.name in self._needs: 
                            raise SemanticException(elementHeader.line,"Element "+elementHeader.name+" redefinition.")
                        definition = True
                        
                if definition: 
                    self._needs[elementHeader.name] = [deps, tree, []]
                else:
                    self._declared[elementHeader.name] = elementHeader.line
                    if elementHeader.root:
                        self._declaredRoot = elementHeader
                return [elementHeader.name]
                
        else:         
            return None
            

    def _addGetsAndProps(self, dependency, header):

        for getter in header.getters:
            dependency.setGetter(getter[1], self.codes[getter[2]])
            
        for prop in header.props:
            dependency.addProperty(prop[1], self.codes[prop[2]])
            
        for val in header.valids:
            if val[3] > 0:
                comment = self.comments[val[3]]
            else:
                comment = ''
            a = Assertion(self.codes[val[2]], comment)
            dependency.assertions.append(a)
            
        dependency.in_memory = header.inmemory
                    
            
    def _makeSimpleDependency(self, tree):
        
        header = self._ElementHeader(tree)
                
        if header.type == 'void':
            element = Void()
        elif header.type == 'char':
            element = Basic(header.name, Char())
        elif header.type == 'byte':
            element = Basic(header.name, Byte())
        elif header.type == 'word':
            element = Basic(header.name, Word())
        elif header.type == 'dword':
            element = Basic(header.name, DWord())
        elif header.type == 'float':
            element = Basic(header.name, Float())
        elif header.type == 'varinteger':
            element = Basic(header.name, VarInteger())
        elif header.type == 'string':
            element = Basic(header.name, String())

        if header.attribute:
            element = Attrib(header.name, element.type())
            
        if header.simpleArray:
            element = Basic(header.name, StaticSizeArray(element.type()))

                    
        if header.comment > 0:
            element.setDoc(self.comments[header.comment])
            
        if header.enum is not None:
            if not isinstance(element.type(), BasicNumType):
                raise SemanticException(header.line, "Only numeric types can have assigned an enum!")            
            enum = self._findEnumByName(self.codes[header.enum])
            if enum is None:
                raise SemanticException(header.line, "Nonexistent enum assigned to element!")
            element.type().setEnum(enum)

        dependency = Dependency()
        dependency.setElement(element)
            
        self._addGetsAndProps(dependency, header)
        
        return element, dependency
                    
        
            
    def _makeDependency(self, tree):
        
        header = self._ElementHeader(tree)
        
        if header.type == 'struct':
            element = Struct(header.name)
        elif header.type == 'array':
            element = Array(header.name)
        elif header.type == 'alternative':
            element = Alternative(header.name)

        if header.comment > 0:
            element.setDoc(self.comments[header.comment])
            
        for a in header.attrs:
            h = self._ElementHeader(a)
            if Consts.isComposite(h.type):
                e = self._needs[h.name][1]                
                d = Dependency()
                d.setElement(e)                
                if len(self._needs[h.name][2]) > 0:
                    h.valids += self._needs[h.name][2]
                self._addGetsAndProps(d, h)
                
            else:
                e, d = self._makeSimpleDependency(a)
            element.addField(e, d)
                    
        self._needs[header.name][1] = element
        
        dr = self._declaredRoot
        if header.root or (dr != None and dr.name == header.name and dr.type == header.type):
            if self.rootDependency != None:
                raise SemanticException(header.line, "Mutiple root declarations. One and only one expected.")    
            self.rootDependency = Dependency()
            self.rootDependency.setElement(element)
            self._addGetsAndProps(self.rootDependency, header)
            
            
        self._needs[header.name][2] = header.valids
        
    def _findEnumByName(self, enumName):
        for enum in self.enums:
            if enum.getName() == enumName:
                return enum
        return None
