from Preprocessing import *
from Parser import *
from DependencyTree import *
from generators.D2LGenerator import D2LGenerator
from generators.DocGenLogic import *
from DataTypes import *
from exprs.ExprVE import ExprVE
from Dependency import Dependency

__all__ = ["LanguageRW"]


class LanguageRW():

    # param source: the definition source code
    # return: the tupple  (root dependency of the definition tree, user defined enums)
    # throws: ParseException, SemanticException
    @staticmethod
    def readLanguage(source, tabSpaces = 3):
    
        pp = Preprocessing(tabSpaces)    
        pp.transform(source)

        parser = Parser(tabSpaces)
        forest = parser.parse(pp.output, pp.comments, pp.codes)
 
        tree = DependencyTree(forest, pp.comments, pp.codes)
 
        return tree.rootDependency, tree.enums
 
 
    @staticmethod
    def writeLanguage(dep, enumsHolder = None, comments = True):
        generator = D2LGenerator()
        settings = { "OutputName" : '',  "Comments" : comments }
        code = generator.generate(dep, enumsHolder, settings)
        return code


    @staticmethod
    def makeSimpleText(dep, tabSpaces = 3):
        text = ''
        elem = dep.getElement()
        logic = DocGenLogic()
        if len(elem.doc.strip()) > 0:
            text = DocGenUtils.indentComment(dep, None , 0, tabSpaces)
        if dep.in_memory: text+='inmemory '
        if isinstance(elem, Attrib):
            text+="attribute "+str(elem.basic_type).lower()+' '+elem.getName()
        elif isinstance(elem, Void):
            text+='void'
        else:
            text+=str(elem.type()).lower()+' '+elem.getName()
        
        if len(DocGenUtils.getGetters(dep)) + len(dep.getProperties()) > 0:
            text+=":"
            
            for g in DocGenUtils.getGetters(dep):
                text+='\n   '+g[0]+DocGenUtils.indentCode( g[1], 1, tabSpaces, "   "+g[0])

            for p in dep.getProperties().keys():
                text+='\n   property '+p+':'+DocGenUtils.indentCode( dep.getProperties()[p], 1, tabSpaces, 'property '+p+':')

        return text    
        
    
    @staticmethod
    def parseSimpleText(source):
        
        pp = Preprocessing(3)
        pp.transform(source)
        
        parser = Parser(3)
        forest = parser.simpleParse(pp.output, pp.comments, pp.codes)
        
        tree = DependencyTree(forest, pp.comments, pp.codes)
        
        return tree.rootDependency.getAggregates()[0]
    