
__all__ = ["Consts","Lines"]


class Consts(object):
    
    specials = ['empty']

    def isSpecial(cls, typeName):        
        return Consts.specials.count(typeName) > 0
    isSpecial = classmethod(isSpecial)

    types = ['void','char','string','byte','word','dword','float','varinteger', 'struct','array','alternative']
    
    def isType(cls ,typeName):
        return  Consts.types.count(typeName) > 0
    isType = classmethod(isType)
    
    
    composites = ['struct','array','alternative']

    def isComposite(cls, typeName):
        return Consts.composites.count(typeName) > 0
    isComposite = classmethod(isComposite)
    
    
    getters = ['size','init','selector','onsave','offset','validator']
    
    def isGetter(cls, getterName):
        return Consts.getters.count(getterName) > 0
    isGetter = classmethod(isGetter)
        
    
class Lines:
    
    def __init__(self, source):
        self._lines = source.splitlines()
        self._lineIndex = 0
        
    def hasLine(self):
        return self._lineIndex < len(self._lines)
    
    def getLine(self):
        self._lineIndex += 1
        return self._lines[self._lineIndex-1]        
    
    def getCurrentLineNumber(self):
        return self._lineIndex
    
    def backLine(self):
        self._lineIndex -= 1
    

