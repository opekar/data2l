from DataTypes import *
from exceptions import *
from LanguageUtils import *
from Utility import validate_ident, valid_identifier_regex
import re

__all__ = ["Parser", "ParseException"]

class ParseException(Exception):
    def __init__(self, line, value):
         self.value = value
         self.line = line
    def __str__(self):
         return "Parse error on line "+str(self.line)+": "+str(self.value)


class Parser:
    
    def __init__(self, tabSpaces):
        self._tabSpaces = tabSpaces
        
    # main method of the class
    # return the forest (ie the set of trees) corresponding to the input structure code
    def parse(self, source, comments, codes):
        
        self._codes = codes
        self._comments = comments        

        self._indentStack = []
        self._lastIndent = 0
        self._output = []
        self._comment = 0
        self._indentExpected = (False, 0)
        self._lineNumber = 0
        self._lines = Lines(source)
        
        ## Parse line by line.
        while self._lines.hasLine():
                line = self._lines.getLine()
                self._parseLine(line)
                
        if self._indentExpected[0]:
            raise ParseException(self._indentExpected[1],"Indent expected but end of file found.")
            
        return self._output
        
    ## This method allows to parse subtrees of imaginary root element. 
    #  The forest is then a set of attributes of single root element.
    def simpleParse(self, source, comments, codes):
        
        self._codes = codes
        self._comments = comments
        
        self._indentStack = [(['root','struct','imaginaryRoot',0,-1], -1)]
        self._lastIndent = -1
        self._output = [self._indentStack[0][0]]
        self._comment = 0
        self._indentExpected = (True, 0)
        self._lineNumber = 0
        self._lines = Lines(source)
        
        ## Parse line by line
        while self._lines.hasLine():
                line = self._lines.getLine()
                self._parseLine(line)
            
        return self._output
        
        
                
    def _parseLine(self, line):    
        self._lineNumber += 1
        indent, text = self._cutIndent(line)
        if text == '': return
        
        ## Returning from recursion
        while (len(self._indentStack) > 0) and self._indentStack[-1][1] >= len(indent):
            self._indentStack.pop()
            if self._indentExpected[0]: 
                raise ParseException(self._lineNumber, "Indent expected but not found. Stray \":\"?")
                        
        ## Parent je tuple ([list kde jsou getry properties, deti, komentar pro dany prvek]], delka indentace)
        if len(self._indentStack) > 0:
            parent = self._indentStack[-1]
            if not self._indentExpected[0] and self._lastIndent < len(indent):
                raise ParseException(self._lineNumber, "Indent not expected but found. Forgotten \":\"?")
        else:
            parent = None
        
        text = text.strip()
        if text[-1] == ':':
            text = text[:-1]
            self._indentExpected = (True, self._lineNumber)
        else:
            self._indentExpected = (False, self._lineNumber)
            
        words = text.split()

        
        # comment line
        comment = self._parseComment(words[0])
        if (comment > 0):
            if self._comment != 0: raise ParseException(self._lineNumber, "Inconsistency in comment block.")
            self._comment = comment
            self._lineNumber += len(self._comments[comment].splitlines()) - 1
            self._lastIndent = len(indent)
            return
        
        # getter line
        getter = self._parseGetter(words)
        if getter != None:
            if parent == None: raise ParseException(self._lineNumber, "Irrelevant getter specification. Missing parent element.")
            getter.append(self._comment)
            getter.append(self._lineNumber)
            self._comment = 0
            parent[0].append(getter)
            self._lineNumber += len(self._codes[getter[2]].splitlines()) - 1
            self._lastIndent = len(indent)
            return
            
        # property line
        property = self._parseProperty(words)
        if property != None:
            if parent == None: raise ParseException(self._lineNumber, "Irrelevant property specification. Missing parent element.")
            property.append(self._comment)
            property.append(self._lineNumber)
            self._comment = 0
            parent[0].append(property)
            self._lineNumber += len(self._codes[property[2]].splitlines()) - 1
            self._lastIndent = len(indent)
            return
                
        # enum assignment line
        enum = self._parseEnumAssignment(words)
        if enum is not None:
            if parent is None: raise ParseException(self._lineNumber, "Irrelevant enum specification. Missing parent element.")
            enum.append(self._comment)
            enum.append(self._lineNumber)
            self.comment = 0
            parent[0].append(enum)
            self._lineNumber += len(self._codes[enum[1]].splitlines()) - 1
            self._lastIndent = len(indent)
            return

        # enum definition
        enum = self._parseEnumDefinition(words)
        if enum is not None:
            if parent is not None: raise ParseException(self._lineNumber, "Irrelevant \"enum\" specification for nested element.")
            enum.append(self._comment)
            enum.append(self._lineNumber)
            self._comment = 0
            self._lineNumber += len(self._codes[enum[2]])
            self._lastIndent            
            self._output.append(enum)
            return

        # element line
        element = []
        if words[0] == 'root':
            element.append(words.pop(0))
            if parent != None: raise ParseException(self._lineNumber, "Irrelevant \"root\" specification for nested element.")
        
        if words[0] == 'inmemory':
            element.append(words.pop(0))
            if parent == None: raise ParseException(self._lineNumber, "Irrelevant \"inmemory\" specification for non nested element.")
        
        if words[0] == 'attribute':
            element.append(words.pop(0))
            if parent == None: raise ParseException(self._lineNumber, "Irrelevant \"attribute\" specification for non nested element.")

        if self._verifyType(words[0]):
            element.append(words.pop(0))
            if len(words) == 0:
                if element[-1] == 'void' or element[-1] == 'empty':
                    words.append('')
                else: 
                    raise ParseException(self._lineNumber, "Missing element name.") 
            if self._verifyType(words[0]) and len(words) > 1:
                raise ParseException(self._lineNumber, "Multiple type definition.") 
            element.append(words.pop(0))
            element.append(self._comment)
            element.append(self._lineNumber)
            self._comment = 0
            if len(words) > 0:
                raise ParseException(self._lineNumber, "Unidentified text at the end of the line.")                 
        else:
            raise ParseException(self._lineNumber, "Unknown type name.") 
        
        if self._indentExpected:
            self._indentStack.append((element ,len(indent)))
        
        if parent == None:
            self._output.append(element)
        else:
            parent[0].append(element)            

        self._lastIndent = len(indent)

        
    def _parseComment(self, word):
        try:
            if (len(word) > 8) and (word[:8] == "comment_"):
                return int(word[8:])
        except TypeError:
            return -1


    def _parseGetter(self, words):
        if words[0][-1] == ":": 
            words[0] = words[0][:-1]
            try:
                index = Consts.getters.index(words[0])
                if (len(words[1]) > 5) and (words[1][:5] == "code_"):
                    return ['g', Consts.getters[index], int(words[1][5:])]
            except Exception:
                return None
        else: return None
            
            
    def _parseProperty(self, words):
        if len(words) < 3: return None
        
        if words[0] != "property": return None
        
        if words[1][-1] != ':':
            raise ParseException("Invalid property format.")
        
        try:
            if (len(words[2]) > 5) and (words[2][:5] == "code_"):
                return ['p', words[1][:-1], int(words[2][5:])]
        except TypeError:
                raise ParseException(self._lineNumber, "Internal: Invalid property format.")
        
    def _parseEnumAssignment(self, words):
        if len(words) != 2: return None
        if words[0] != "enum": return None
        try:
            if words[1][:5] == "code_":            
                return ['e', int(words[1][5:])]
        except TypeError:
            raise ParseError(self._lineNumber, "Internal: Invalid enum assignment")
        return None
        
    ## Parsing definition of user defined enum.
    def _parseEnumDefinition(self, words):
        if len(words) < 3: return None
        
        if words[0] != "enum": return None
        
        if words[1][-1] != ':': 
            raise ParseException("Invalid enum format.")
        
        try:
            if (len(words[2]) > 5) and (words[2][:5] == "code_"):
                code_idx = int(words[2][5:])
                enum_list = Parser.parseEnumeratorsAssignments(self._codes[code_idx])
                if enum_list is None:
                    raise ParseException(self._lineNumber, "Internal: Invalid enumerators definition.")
                self._codes[code_idx] = enum_list                
                return ['enum', words[1][:-1], code_idx]
        except TypeError:
            raise ParseException(self._lineNumber, "Internal: Invalid enum format.")        
    
    ## Parse lines with assignments of enumerators.
    #  e.g.: COLOR_BLUE = 1
    #        COLOR_GREEN = 0x2
    #  returns: list of tupples -> [(enumerator, value), ...]
    @staticmethod
    def parseEnumeratorsAssignments(text):
        # regex which matches: ENUMERATOR_NAME = VALUE
        # where VALUE can be either decimal or hexadecimal numb
        # (?: means only non grouping parenthesis due to allow empty line
        line_regex = "^(?:([a-zA-Z_][a-zA-Z_0-9]*)\s*=\s*([0-9]*|0{0,1}x[0-9a-fA-F]+)){0,1}\s*$"
        lines = text.splitlines()
        enum = []
        for line in lines:
            m = re.match(line_regex, line)
            if m is None:
                return None
            enumerator = m.group(1)
            value = m.group(2)
            if enumerator is not None and value is not None:
                enum.append((enumerator, value))
        return enum
        
    def _verifyType(self, word):    
        if Consts.isSpecial(word):
            return True
               
        if Consts.isType(word):
            return True
        
        # for a case of simple array
        if (len(word) > 2) and (word[-2:] == "[]") and (Consts.isType(word[:-2])) and (not Consts.isComposite(word[:-2])):
             return True
        return False
        
        
    def _cutIndent(self, line):
        i = 0
        ind = ''
        while i < len(line):
            if line[i] == ' ': ind += ' '
            elif line[i] == '\t' : ind += ' ' * self._tabSpaces
            else: break
            i+=1
        
        return ind, line[i:]
