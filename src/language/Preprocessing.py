import re
from Parser import ParseException
from LanguageUtils import *

class Preprocessing:
    
    def __init__(self, tabSpaces):
        self.tabSpaces = tabSpaces
        self.getterRe = re.compile("^(?:size|init|selector|onsave|offset|validator|enum|(enum [^:]+)|(property [^:]+))[ \t]*:.*$")
        
    def cutIndent(self, line):
        i = 0
        ind = ''
        while i < len(line):
            if line[i] == ' ': ind += ' '
            elif line[i] == '\t' : ind += ' ' * self.tabSpaces
            else: break
            i+=1
        
        return ind, line[i:]
        
    def isGetter(self, text):
        return self.getterRe.match(text)
    
    def formatCode(self, code):
        
        if len(code) == 0: return ""
                
        minI = 1e5
        for line in code[1:]:
            i,t = self.cutIndent(line)
            if len(t) > 0:
                minI = min(minI, len(i))

        out = code[0].strip()
        for line in code[1:]:
            i,t = self.cutIndent(line)
            out += '\n' + ' ' * (len(i)-minI) + t
        return out
        
        
    def transform(self, source):
          
        self.lines = Lines(source)                
        commentID = 0
        codeID = 0
        self.comments = {}
        self.codes = {}
        self.output = ""

        while self.lines.hasLine():
         
            line = self.lines.getLine()
            indent, text = self.cutIndent(line)
            
            if text == '': 
                self.output += "\n"
                continue

            #contra hack
            if len(text) >= 8 and text[:8] == 'comment_':
                raise ParseException(self.lineIndex,"Syntax error: unknown statement \""+text+"\".")

            #comment
            if text[0] == '#':
                commentID += 1
                comment = text[1:]                
                
                while self.lines.hasLine():
                    l = self.lines.getLine()
                    i, t = self.cutIndent(l)
                    if (len(i) != len(indent)) and (len(t) > 0):
                        raise ParseException(self.lineIndex,"Bad indentation of commentary block.")
                    if (len(t) == 0) or (t[0] != '#'): 
                        self.output += indent+"comment_"+str(commentID)+'\n'        
                        self.lines.backLine()
                        break
                    comment += '\n' + t[1:]
                    
                self.comments[commentID] = comment    
                

            #code
            elif self.isGetter(text):
                codeID += 1                                     
                code = []
                pos = text.find(':')
                code.append(text[pos+1:])
                
                while self.lines.hasLine():
                    l = self.lines.getLine()
                    i, t = self.cutIndent(l)
                    if (len(i) <= len(indent)) and len(t) > 0: 
                        self.lines.backLine()
                        break
                    code.append((len(i)-len(indent))*' '+t)                
                
                self.output += indent+text[:pos].strip()+": code_"+str(codeID)+'\n'        
                
                while (len(code) > 0) and (len(code[-1].strip()) == 0):
                    code = code[:-1]
                    self.output +='\n'
                
                self.codes[codeID] = self.formatCode(code)
                     
            else: self.output += line + "\n"

                        
