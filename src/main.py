import gtkGUI
import optparse
import Cmdline


option_parser = optparse.OptionParser()
OPT = option_parser.add_option

OPT("-d", "--definition", dest = "definition_file",
    help = "Source of data structure definition.")
OPT("-c", "--compile", dest = "compile", action = "store_true",
    default = False,
    help = "Compile given definition into C++ source.")
OPT("-o", "--dir", dest = "output_dir",
    help = "Resluts output directory.")
OPT("--header-file", dest = "header_file",
    help = "Name of resulting C++ header.")
OPT("--cpp-file", dest = "cpp_file",
    help = "Name of resulting C++ source.")


def main():
    (options, args) = option_parser.parse_args()

    if options.compile and options.definition_file:
        Cmdline.generate_cpp(options)
    else:
        gtkGUI.Main.main(options, args)


if __name__ == "__main__":
    main()
