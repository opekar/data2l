## @package CheckStructureTests
# Set of definition checking tests.
# Some kind of 'semantic' errors should not  be found before structure check is called.
# Created on 9.12.2009
# @author mkriz

__all__ = """\
CheckDefinitionTests
""".split()

# TODO: why no StructCheck error occurred????!!!!!

import unittest
from language.LanguageRW import *
from Definition import Definition
from gtkGUI import StructCheck

class CheckStructureTests(unittest.TestCase):
    def testCheckMissingStaticSizeArraySize(self):
        testDefinition = """
        #not defined word[] B size
        root struct A:
          word[] B
        """

        res = self._parseD2lCompileAndCheckStructure(testDefinition)        
        # now error should be found
        self.assertTrue(len(res) == 1)
        err_type, _ = res[0]
        self.assertTrue(err_type == StructCheck.SC_MISSING_ARRAY_SIZE)
    
    def testCheckArrayLateDefinedWithoutSize(self):
        testDefinition = """
        #only one element in array
        root struct A:
          array B
          
        array B:
          word x
        """        
        
        res = self._parseD2lCompileAndCheckStructure(testDefinition)        
        # now error should be found
        self.assertTrue(len(res) == 1)
        err_type, _ = res[0]
        self.assertTrue(err_type == StructCheck.SC_MISSING_ARRAY_SIZE)
        
    def testCheckMissingAlternativeSelector(self):
        testDefinition = """        
        root struct A:
            alternative B
            char C

        alternative B:
            empty 
        """
        
        res = self._parseD2lCompileAndCheckStructure(testDefinition)
        # now error should be found
        self.assertTrue(len(res) == 1)
        err_type, _ = res[0]
        self.assertTrue(err_type == StructCheck.SC_ALTERNATIVE_MISSING_SELECTOR)
        
    def _parseD2lCompileAndCheckStructure(self, testDefinition):
        # d2l parser should do not check what higher control level should check, so no error is occurred
        dep, _ = LanguageRW.readLanguage(testDefinition)        
        # neither compilation should found error
        definition = Definition(dep.getElement())
        compiled = definition.getCompiledDefinition()

        # now error should be found
        check = StructCheck.StructCheck()
        check.check(compiled)
        return check.results()

def getTestSuite():
    testSuite = unittest.TestLoader().loadTestsFromTestCase(CheckStructureTests)
    # this is way to load only specific tests
    #testSuite = unittest.TestSuite()
    #testSuite.addTest(D2LParserTests('testCheckArrayLateDefinedWithoutSize'))     
    return testSuite

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(getTestSuite())
