## @package CompilationTests
# Set of compilations tests.
# Created on 3.12.2009
# @author mkriz

__all__ = """\
CompilationSimpleTests
""".split()
     
import unittest
import language
from language.LanguageRW import *
from exprs.Eval import ENameNotFound, EParseError, EBadArguments
import ValueExtractors
from file_defs.prelude import *
        
## Compilation tests on simple definitions.        
class CompilationSimpleTests(unittest.TestCase):
    
    ## Compilation of simple right definition.
    def testSimpleDefinitionCompilation(self):
        Header = Struct('Header',
                        ['File header'],
                        'File format version',
                        Basic('Version', Word()),
                        Dependency(offset = '1'))
        
        compiled_def = Definition(Header).getCompiledDefinition()
        
        self.assertEqual(compiled_def.element,Header,
                         "Incorrect root element equals in compiled definition!")
        self.assertEqual(compiled_def.dep_root.element, compiled_def.element, 
                         "Not root elements equals in dependency and definition!")
        self.assertEqual(str(compiled_def.dep_root.aggregates[0].offset.compiled_getter),
                         str(ValueExtractors.Const(1)), 
                         "Compiled offset does not match the definition")      
          
    ## Compilation of definition with undefined function in offset.
    def testUndefinedFunctionInOffset(self):
        Header = Struct('Header',
                        ['File header'],
                        'File format version',
                        Basic('Version', Word()),
                        Dependency(offset = 'foo()'))
        
        self.assertRaises(ENameNotFound,Definition(Header).getCompiledDefinition)
    
    ## Compilation of definition with some mish-mash in offset.
    def testParseErrorInOffset(self):
        Header = Struct('Header',
                        ['File header'],
                        'File format version',
                        Basic('Version', Word()),
                        Dependency(offset = '2 2'))
        
        self.assertRaises(EParseError,Definition(Header).getCompiledDefinition)
 
    ## Compilation of definition with some bad argument in offsetof() function.
    def testBadArgumentInOffsetOfFunc(self):
        Header = Struct('Header',
                        ['File header'],
                        'File format version',
                        Basic('Version', Word()),
                        Dependency(offset = 'offsetof(0)'))
        
        self.assertRaises(EBadArguments,Definition(Header).getCompiledDefinition)
 
    ## Compilation of definition with some undefined function inside other defined function(s).
    def testUndefinedFunctionNestedInOtherFunc(self):
        Header = Struct('Header',
                        ['File header'],
                        'File format version',
                        Basic('Version', Word()),
                        Dependency(offset = 'arraysizeof(actindexof(foo()))'))
         
        self.assertRaises(ENameNotFound,Definition(Header).getCompiledDefinition)

    ## check if parser do not check what higher control level should check
    def testArraySizeIsLetter(self):
        # this is no any parsing or semantic error, this should be found as error in compilation level
        testDefinition = """
        #array size equals letter "m"
        root struct A:
          word[] B:
            size: m
        """
        dep, _ = LanguageRW.readLanguage(testDefinition)         
        self.assertEqual(dep.element.deps[0].size.text, "m")            
        
        definition = Definition(dep.getElement())
        self.assertRaises(ENameNotFound, definition.getCompiledDefinition)        
                
 
def getTestSuite():
	# this includes all methods which name starts with 'test'
    testSuite = unittest.TestLoader().loadTestsFromTestCase(CompilationSimpleTests)
    # this is way to load only specific tests
    #testSuite = unittest.TestSuite()
    #testSuite.addTest(D2LParserTests('testArraySizeIsLetter'))
    return testSuite

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(getTestSuite())
