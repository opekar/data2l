## @package D2LParserTests
# Set of d2l language parsing tests.
# Created on 10.9.2009
# @author mkriz, ppascenko

__all__ = """\
D2LParserTests
""".split()

import unittest
from language.LanguageRW import *
from language.Parser import *
from language.DependencyTree import *
import DataTypes as DT
import BasicTypes as BT

class D2LParserTests(unittest.TestCase):

    def testParsingComplexStructure(self):
        testDefinition = """
        # comment 1.1
        # comment 1.2
        root struct A:   
           # comment 2.1
           # comment 2.2   
           array B:     
              #comment 3.1
              inmemory byte C            
              #comment 4.1
              #comment 4.2      
              size: 25      
           struct D:   
              attribute word E:
                  init: 16      
              # comment of simple char array F
              char[] F:
                 size:                   
                  11
              dword G:
                 # validator comment
                 validator: G > E + sizeof(F)         
           alternative H:   
              # void comment
              void            
              array I:      
                 inmemory dword J:
                    validator: J > 0            
              selector: if A.K > 7 then 
                           0 
                       else /* comment in code */
                           1                         
           byte K
        """
        dep, _ =LanguageRW.readLanguage(testDefinition) 
        # assert that root element is Struct(A)
        self.assertTrue(isinstance(dep.element, DT.Struct), "Type of root element")        
        self.assertEqual(dep.element.getName(),"A")
        # assert some inner element        
        elem_J = dep.element.fields[2].fields[1].fields[0]
        self.assertTrue(isinstance(elem_J,DT.Basic))
        self.assertTrue(isinstance(elem_J.type(), BT.DWord))
        self.assertEqual(elem_J.getName(),"J");
        
    def testParsingNestedStructures(self):
        testDefinition = """
        # root comment
        root struct A:
         #comment B
         struct B:
          #comment C
          struct C:
           #comment D
           struct D:
            char[] Text:   
             size: 2
           #comment E  
           struct E:
            #comment F
            struct F:
             #comment G
             struct G:
              #comment H
              byte H
        """
        dep, _ =LanguageRW.readLanguage(testDefinition)
        # assert that root element is Struct(A)
        self.assertTrue(isinstance(dep.element, DT.Struct), "Type of root element")
        self.assertEqual(dep.element.getName(),"A")        
        # assert most nested element
        elem_H_leaf = dep.element.fields[0].fields[0].fields[1].fields[0].fields[0].fields[0]
        self.assertTrue(isinstance(elem_H_leaf ,DT.Basic))
        self.assertTrue(isinstance(elem_H_leaf.type(),BT.Byte))
        self.assertEqual(elem_H_leaf.getName(),"H")

    def testParsingRootLateDefinedStructre(self):
        testDefinition = """
        #comment
        struct A:
          byte B  
        root struct A
        """
        dep, _ =LanguageRW.readLanguage(testDefinition)
        # assert that root element is Struct(A)
        self.assertTrue(isinstance(dep.element, DT.Struct), "Type of root element")
        self.assertEqual(dep.element.getName(),"A")
        # assert nested element
        elem_B = dep.element.fields[0]   
        self.assertTrue(isinstance(elem_B ,DT.Basic))
        self.assertTrue(isinstance(elem_B.type(),BT.Byte))
        self.assertEqual(elem_B.getName(),"B")

    def testParsingPropertyForAttributeDefined(self):
        testDefinition = """
        #property defined for attribute
        root struct A:
          attribute byte B:
            property height: 188cm
        """        
        dep, _ =LanguageRW.readLanguage(testDefinition)
        self.assertEqual(dep.element.getName(), "A")
            
    def testParsingEmptyStructDefinition(self):
        testDefinition = """
        struct B:
            empty
            
        #struct B is empty                         
        root struct A:
            struct B                      
        """        
        dep, _ =LanguageRW.readLanguage(testDefinition)
        self.assertEqual(dep.element.getName(), "A")        
        # check that struct B is empty
        elem_B = dep.element.fields[0]
        self.assertTrue(isinstance(elem_B, DT.Struct))
        self.assertEqual(elem_B.getName(),"B")
        self.assertTrue(len(elem_B.fields) == 0)
        
    def testParsingStructDefinitionWithVoidAndEmptyElement(self):
        testDefinition = """
        # empty should be removed automatically
        struct B:        
            void
            empty
            
        #struct B is empty                         
        root struct A:
            struct B                      
        """        
        dep, _ =LanguageRW.readLanguage(testDefinition)
        self.assertEqual(dep.element.getName(), "A")        
        # check that struct B has only void element
        elem_B = dep.element.fields[0]
        self.assertTrue(isinstance(elem_B, DT.Struct))
        self.assertEqual(elem_B.getName(),"B")
        self.assertTrue(len(elem_B.fields) == 1)
        self.assertTrue(isinstance(elem_B.fields[0], DT.Void));
            
    def testParsingPropertiesAndValidatorsInRoot(self):
        testDefinition = """        
        #root with properties
        root struct A:
            byte B
            property height: 183cm
        """
        dep, _ = LanguageRW.readLanguage(testDefinition)
        self.assertEqual(dep.element.getName(), "A")
        self.assertTrue(isinstance(dep.element, DT.Struct))
        self.assertEqual(dep.customProperties['height'], "183cm")        
     
    def testParsingEnumDefinition(self):
        testDefinition = """        
        enum EColors:
            COLOR_BLUE = 0x1
            COLOR_RED = 2
                
        enum EStyles:
            STYLE_NORMAL = 0xff
            STYLE_ITALIC = 1
                
        root struct A:
            byte B:
                enum: EColors
        """
        dep, enumList = LanguageRW.readLanguage(testDefinition)
        self.assertEqual(dep.element.getName(), "A")
        enum = dep.element.fields[0].type().getEnum()
        self.assertEqual(len(enumList), 2)
        self.assertEqual(enum, enumList[0])
        # assure that order of enumerators is preserved
        self.assertEqual(enum.getList()[1], ("COLOR_RED", "2"))        
        
    def testParseErrorBadEnumeratorAssignment(self):
        testDefinition = """        
        enum EColors:
            COLOR_BLUE = 0x1
            COLOR_RED = 2a
                
        root struct A:
            byte B:
                enum: EColors
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)
        
    def testSematicErrorUndefinedEnumIsAssigned(self):
        testDefinition = """
        enum EColors:
            COLOR_BLUE = 0x1
            COLOR_RED = 2
                
        root struct A:
            byte B:
                enum: EStyles
        """
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)
        
    def testSemanticErrorEnumRedefinition(self):
        testDefinition = """
        enum EColors:
            COLOR_BLUE = 0x1
            COLOR_RED = 2
                
        root struct A:
            byte B:
                enum: EStyles
        """        
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)
    
    ## This case should be parsed correctly, because this definition
    #  could be created in GUI 
    def testParsingMultipleStructsWithSameName(self):
        testDefinition = """
        root struct A:
            char aaa
            char aaa
            struct nameB            
            struct C            
            struct nameB
        
        struct nameB:
            char X
        
        struct C:
            empty
        """
        dep, _ =LanguageRW.readLanguage(testDefinition)
        self.assertEqual(dep.element.getName(), "A")
            
    def testSemanticErrorMultipleStructsWithSameNameAndMoreoverCyclicReference(self):
        testDefinition = """
        root struct A:
            char aaa
            char aaa
            struct nameB            
            struct A
            struct nameB
        
        struct nameB:
            char X
        """
        # semantic exception because of cyclic reference of structA should be raised
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)        
            
    def testParseErrorBadKeyword(self):
        testDefinition = """
        #bad keyword sruct
        root sruct A:
          byte B        
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)

    def testParseErrorUnattachedComment(self):
        testDefinition = """        
        #unattached comment
                
        #comment A
        root struct A:
          byte B   
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)

    def testParseErrorUndefinedStruct(self):
        testDefinition = """
        #empty struct
        root struct A:
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)

    def testParseErrorIndentMissing(self):
        testDefinition = """        
        #missing indent
        root struct A:
        byte B        
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)
    
    def testParseErrorUndefinedGetterForByte(self):
        testDefinition = """                
        root struct A:
          byte B:        
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)    

    def testParseErrorWrongStructNameDefinition(self):
        testDefinition = """
        # error in struct name
        root struct A B:
          byte B        
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)

    def testParseErrorWrongStructDefinition(self):                
        testDefinition = """        
        #'B' after struct definition
        root struct A: B
          byte B        
        """ 
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)
    
    def testParseErrorWrongStructElemIndent(self):                
        testDefinition = """        
        #wrong indent of byte C
        root struct A:
          byte B
           byte C        
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)
    
    def testParseErrorWrongSizeElemDefinition(self):                
        testDefinition = """
        #wrong size definition, missing ':'
        root struct A:
          byte B:
            size 17 
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)
    
    def testParseErrorWrongIfElseIndent(self):                
        testDefinition = """        
        #no indent in else
        root struct A:
          byte B:
            size: 
             if a>b then 4 
            else 5        
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)
    
    def testParseErrorInmemoryNotUnderArray(self):                
        testDefinition = """        
        #inmemory for root element
        root inmemory struct A        
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)
    
    def testParseErrorRootInsideOtherElement(self):                
        testDefinition = """
        #wrong root definition
        struct A:
          root struct B:
            byte C    
        """
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)        

    def testParseErrorWrongInitDefinition(self):
        testDefinition = """
        #missing ':' after init
        root struct A:
          byte B:
            init 3            
        """        
        self.assertRaises(ParseException, LanguageRW.readLanguage, testDefinition)
        
    def testSemanticErrorRootMissing(self):
        testDefinition = """
        #missing root
        byte A
        """        
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)
                          
    def testSemanticErrorRootElemMustBeStruct(self):
        testDefinition = """
        #only root element
        root byte A        
        """        
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)                          

    def testSemanticErrorMultipleRoots(self):
        testDefinition = """
        #multiple roots
        root struct A:
           byte B
        
        root struct C:
           byte B                
        """        
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)                          

    def testSemanticErrorMultipleRootsOneLateDefined(self):
        testDefinition = """
        #multiple roots
        root struct A:
           byte B
        
        struct C:
           byte B
        
        root struct C
        """        
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)                          

    def testSemanticErrorStructNotDefined(self):
        testDefinition = """
        #struct not defined
        root struct A:
           struct C
           byte B
        """        
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)                          
        
    def testSemanticErrorStructRedefinition(self):
        testDefinition = """
        #redefinition of struct A
        root struct A:
           byte B
        
        struct C:
          struct A:
           byte B     
        """        
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)                          

    def testSemanticErrorCyclicReference(self):
        testDefinition = """
        #cyclic reference
        root struct A:
          struct B
          
        struct B:
          struct A
          byte B
        """        
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)

    def testSemanticErrorMultipleInit(self):
        testDefinition = """
        #multiple initializations
        root struct A:
          byte B:
            init: 3
            init: 6
        """
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)

    def testSemanticErrorMultiplePropertyDefinition(self):
        testDefinition = """
        #multiple property definition
        root struct A:
          byte B:
            property height: 186cm
            property height: 188cm
        """        
        self.assertRaises(SemanticException, LanguageRW.readLanguage, testDefinition)
                          
def getTestSuite():
	# this includes all methods which name starts with 'test'
    testSuite = unittest.TestLoader().loadTestsFromTestCase(D2LParserTests)
    # this is way to load only specific tests
    #testSuite = unittest.TestSuite()
    #testSuite.addTest(D2LParserTests('testParsingEnumDefinition'))    
    return testSuite

if __name__ == '__main__':
    unittest.TextTestRunner(verbosity=2).run(getTestSuite())
