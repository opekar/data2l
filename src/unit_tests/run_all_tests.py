## @package run_all_tests
#  Stand-alone package which run all test appended to allSuits list. 
#  Created on 3.12.2009
#  @author: mkriz

import unittest

## Import test modules. 
import CompilationTests
import D2LParserTests
import CheckStructureTests

# Select test suits form modules
allSuits = []
allSuits.append(unittest.TestLoader().loadTestsFromModule(CompilationTests))
allSuits.append(unittest.TestLoader().loadTestsFromModule(D2LParserTests))
allSuits.append(unittest.TestLoader().loadTestsFromModule(CheckStructureTests))

# Join the test suits to one TestSuit and run it in TestRunner
allTests = unittest.TestSuite(allSuits)    
unittest.TextTestRunner(verbosity=2).run(allTests)
